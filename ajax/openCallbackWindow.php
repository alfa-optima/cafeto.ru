<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = Array();
    parse_str($_POST["form_data"], $arFields);

    $html = '<p class="modal__title default-modal__title">Оставьте данные для звонка</p>';

    $html .= '<div class="modal__grid">';

        $html .= '<form class="form modal-form callback___form">';

            $html .= '<div class="form__row icon-person">';
                $html .= '<input type="text" name="name" class="form__input" placeholder="Имя">';
            $html .= '</div>';
            $html .= '<div class="form__row icon-tel">';
                $html .= '<input type="tel" name="phone" class="form__input" placeholder="Телефон">';
            $html .= '</div>';
            $html .= '<div class="form__row">';
                $html .= '<textarea name="message" class="form__input form__textarea" placeholder="Комментарий"></textarea>';
            $html .= '</div>';

            $html .= '<div class="form__row">';
                $html .= '<label class="sort__value form___label" style="white-space: unset;">';
                    $html .= '<input type="checkbox" value="Y" name="agree" class="form___checkbox" checked>';
                    $html .= '<p class="sort__value-link link sort__value-link_s">Я принимаю <a href="/oferta/" target="_blank">Условия использования</a></p>';
                $html .= '</label>';
            $html .= '</div>';


            $html .= '<div class="g-recaptcha" data-sitekey="'.\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'GOOGLE_RECAPTCHA2_KEY').'"></div>';

            $html .= '<p class="error___p"></p>';

            $html .= '<button type="button" class="btn form__btn callback___button to___process">Отправить</button>';

            $html .= '</form>';
    $html .= '</div>';

    $html .= '<script src="https://www.google.com/recaptcha/api.js"></script>';

	// Ответ
	echo json_encode(Array("status" => "ok", 'html' => $html));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
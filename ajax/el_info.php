<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $item_id = strip_tags($_POST['item_id']);

    $el = tools\el::info($item_id);

	// Ответ
	echo json_encode(Array("status" => "ok", 'el' => $el));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$results = array();

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = strip_tags($_GET['term']);

    $ds_pek = new project\ds_pek();
    $locations = $ds_pek->getLocations();

    $all_locs = [];
    foreach ( $locations as $parent => $ar ){
        foreach ( $ar as $id => $np ){
            $all_locs[$id]['ID'] = $id;
            $all_locs[$id]['NAME'] = $np;
            $all_locs[$id]['PARENT'] = $parent;
        }
    }

    foreach ( $all_locs as $id => $np ){
        if( tools\funcs::string_begins_with(strtolower(trim($term)), strtolower(trim($np['NAME']))) ){
            $label = $np['NAME'];
            if( $np['NAME'] != $np['PARENT'] ){
                $label .= ' ('.$np['PARENT'].')';
            }
            $results[$id] = array(
                'label' => $label,
                "short_name" => $np['NAME'],
                "value" => $id
            );
        }
    }

    echo json_encode($results);

}

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('catalog');

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $item_id = strip_tags($_POST['item_id']);
    $sku_id = strip_tags($_POST['sku_id']);

    $aProduct = false;
    $products = \CCatalogProduct::GetList([], [ "ID" => $item_id ]);
    if ( $arProduct = $products->GetNext() ){
        $aProduct = $arProduct;
    }
    
    ob_start();
        $GLOBALS['catalog_item_modal']['ID'] = $item_id;
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", "catalog_item_modal",
            Array(
                "sku_id" => $sku_id,
                'aProduct' => $aProduct,
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => 'NAME',
                "ELEMENT_SORT_ORDER" => 'ASC',
                "ELEMENT_SORT_FIELD2" => 'NAME',
                "ELEMENT_SORT_ORDER2" => 'ASC',
                "FILTER_NAME" => 'catalog_item_modal',
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => 1,
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array(),
                "OFFERS_LIMIT" => 0,
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => 'A',
                "CACHE_TIME" => '36000',
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PROPERTY_CODE" => array( 'SORT_PRICE', 'METOD', 'REGION', 'MORE_PHOTOS' ),
                "PRICE_CODE" => array(
                    project\catalog::PRICE_CODE
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y",
                "OFFERS_FIELD_CODE" => ['PROPERTY_WEIGHT', 'PROPERTY_MORE_PHOTOS'],
            )
        );
    	$html = ob_get_contents();
    ob_end_clean();



	// Ответ
	echo json_encode(Array("status" => "ok", 'html' => $html));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
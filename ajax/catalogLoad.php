<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    global $APPLICATION;

    if( strlen($_POST['sort_code']) > 0 ){
        $sort_field = explode('|', $_POST['sort_code'])[0];
        $sort_order = explode('|', $_POST['sort_code'])[1];
    } else {
        ob_start();
            $sort = $APPLICATION->IncludeComponent( "aoptima:catalog_sort_block", "" );
            $sort_field = $sort['SORT_FIELD'];
            $sort_order = $sort['SORT_ORDER'];
        ob_end_clean();
    }


    $section = tools\section::info($_POST['section_id']);
    $smart_filter_path = '';
    $url = strip_tags(trim($_POST['url']));
    if( substr_count( $url, '/filter/' ) ){
        if(preg_match("/\/filter\/(.+)\/apply\//", $url,$matches)){
            $smart_filter_path = rawurldecode( $matches[1] );
        }
    }

    if( strlen($smart_filter_path) > 0 ){
        // Пытаемся найти SEO-страницу
        $seo_page = project\SeopageTable::getByFilterPath($section, $smart_filter_path);
    }

    ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter", "catalog_filter",
            array(
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                "SECTION_ID" => $section['ID'],
                "FILTER_NAME" => project\catalog::FILTER_NAME.'_load',
                "PRICE_CODE" => [ project\catalog::PRICE_CODE ],
                "CACHE_TYPE" => 'A',
                "CACHE_TIME" => 3600,
                "CACHE_GROUPS" => 'Y',
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => 'VERTICAL',
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => 'N',
                "TEMPLATE_THEME" => "",
                'CONVERT_CURRENCY' => 'N',
                'CURRENCY_ID' => '',
                "SEF_MODE" => 'Y',
                "SEF_RULE" => $section['SECTION_PAGE_URL'].'filter/#SMART_FILTER_PATH#/apply/',
                "SMART_FILTER_PATH" => $smart_filter_path,
                "PAGER_PARAMS_NAME" => '',
                "INSTANT_RELOAD" => '',
            ),
            $component, array('HIDE_ICONS' => 'Y')
        );
    ob_end_clean();


    if( is_array($_POST['stop_ids']) ){
        $GLOBALS[project\catalog::FILTER_NAME.'_load']['!ID'] = $_POST['stop_ids'];
    }

    $GLOBALS[project\catalog::FILTER_NAME.'_load']['SECTION_ID'] = strip_tags($_POST['section_id']);

    ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", "catalog_goods",
            Array(
                "IS_AJAX" => "Y",
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => $sort_field,
                "ELEMENT_SORT_ORDER" => $sort_order,
                "ELEMENT_SORT_FIELD2" => 'NAME',
                "ELEMENT_SORT_ORDER2" => $sort['SORT_ORDER'],
                "FILTER_NAME" => project\catalog::FILTER_NAME.'_load',
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => project\catalog::MAX_CNT + 1,
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array(),
                "OFFERS_LIMIT" => 0,
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => 'A',
                "CACHE_TIME" => '36000',
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PROPERTY_CODE" => array(),
                "PRICE_CODE" => array(
                    project\catalog::PRICE_CODE
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        );
        $html = ob_get_contents();
    ob_end_clean();


	// Ответ
    $arResult = ["status" => "ok", 'html' => $html];
    if( intval($seo_page['ID']) > 0 ){
        $arResult['seo_page'] = $seo_page;
    }
    $arResult['section'] = $section;
	echo json_encode($arResult);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
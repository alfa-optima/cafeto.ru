<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    $USER->IsAuthorized()
){

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:personal_history", "",
            ['IS_AJAX' => 'Y']
        );
    	$html = ob_get_contents();
    ob_end_clean();

	// Ответ
	echo json_encode(Array("status" => "ok", "html" => $html));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
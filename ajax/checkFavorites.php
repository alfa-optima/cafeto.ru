<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $cur_favs_str = strip_tags($_POST['cur_favs']);
    $item_id = strip_tags($_POST['item_id']);
    $item_section_id = tools\el::sections($item_id)[0]['ID'];

    $cur_favs = [];   $new_favs = [];

    if( strlen($cur_favs_str) > 0 ){
        $cur_favs = explode('|', $cur_favs_str);
    }

    if( count($cur_favs) > 0 ){
        foreach ( $cur_favs as $key => $tov_id ){
            $el = tools\el::info($tov_id);
            if( intval($el['ID']) > 0 ){
                $new_favs[] = $el['ID'];
            }
        }
    }

    if( !in_array($item_id, $new_favs) ){
        if( count($new_favs) >= project\favorites::MAX_CNT ){
            // Ответ
            echo json_encode(Array(
                "status" => "error",
                "text" => "Максимальное количество товаров<br>в&nbsp;избранном - ".project\favorites::MAX_CNT.'шт.'
            ));
            return;
        }
        $new_favs[] = $item_id;
        $type = 'plus';
    } else {
        foreach ( $new_favs as $key => $id ){
            if( $id == $item_id ){    unset($new_favs[$key]);    }
        }
        $type = 'minus';
    }

	// Ответ
	echo json_encode(Array(
	    "status" => "ok",
        "new_favs" => $new_favs,
        "type" => $type,
    ));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
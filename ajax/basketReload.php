<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\CModule::IncludeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $_POST['action'] == 'save_coupon' ){
        $coupon = strip_tags(trim($_POST['coupon']));
        $check = project\coupon::check($coupon);
        if( $check ){
            project\coupon::clear();
            project\coupon::set($coupon);
        } else {
            project\coupon::clear();
        }
    }

    if(
        $_POST['action'] == 'unset_item'
        &&
        intval($_POST['item_id']) > 0
    ){
        $res = \CSaleBasket::Delete($_POST['item_id']);
    }

    if(
        $_POST['action'] == 'recalc_qt'
        &&
        intval($_POST['item_id']) > 0
        &&
        intval($_POST['quantity']) > 0
    ){
        $fields = array( "QUANTITY" => intval($_POST['quantity']) );
        \CSaleBasket::Update(intval($_POST['item_id']), $fields);
    }

    $basketInfo = project\user_basket::info();

    ob_start();
        $APPLICATION->IncludeComponent("aoptima:basket_small", "header", [
            'basketInfo' => $basketInfo
        ]);
        $top_basket_html = ob_get_contents();
    ob_end_clean();

    ob_start();
        $APPLICATION->IncludeComponent("aoptima:basket", "", [
            'basketInfo' => $basketInfo,
            'IS_AJAX' => 'Y'
        ]);
        $basket_html = ob_get_contents();
    ob_end_clean();

    $arResult = Array(
        "status" => "ok",
        "top_basket_html" => $top_basket_html,
        "basket_html" => $basket_html,
    );

	// Ответ
	echo json_encode($arResult);
	return;
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
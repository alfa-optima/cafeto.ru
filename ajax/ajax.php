<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,    
Bitrix\Sale\Basket,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('sale');

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;
\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){


    // Подписка на товар
    if ( $_POST['action'] == "subscribe_product_quantity" ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if( strlen($arFields['email']) > 0 && intval($arFields['id']) > 0 ){

            $email = strip_tags(trim(strtolower($arFields['email'])));

            // Инфо о товаре
            $el = tools\el::info($arFields['id']);
            if( intval($el['ID']) > 0 ){

                // Проверим есть ли записи
                $product_subscribe = new project\product_subscribe();
                $notes = $product_subscribe->getList( $el['ID'], $email );

                // Если подписка уже есть
                if( count($notes) > 0 ){

                    // Ответ
                    echo json_encode(["status" => "error", "text" => "Подписка на данный товар<br>уже оформлена ранее!"]);
                    return;

                // Если подписки ещё нет
                } else {

                    $products = \CCatalogProduct::GetList([], [ "ID" => $el['ID'] ]);
                    if ( $arProduct = $products->GetNext() ){
                        if( $arProduct['QUANTITY'] > 0 ){

                            ob_start();
                                $APPLICATION->IncludeComponent(
                                    "aoptima:mailProductSubscribe", "default",
                                    [ 'email' => $email, 'product' => $el ]
                                );
                                $mail_html = ob_get_contents();
                            ob_end_clean();

                            $mail_title = 'Поступление товара (подписка на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';

                            tools\feedback::sendMainEvent( $mail_title, $mail_html, $email );

                            // Ответ
                            echo json_encode(["status" => "ok"]);
                            return;


                        } else {

                            // Создание записи
                            $product_subscribe = new project\product_subscribe();
                            $res = $product_subscribe->add([
                                'UF_EMAIL' => $email,
                                'UF_PRODUCT_ID' => $el['ID'],
                            ]);

                            if( $res ){

                                // Ответ
                                echo json_encode(["status" => "ok"]);
                                return;

                            } else {

                                // Ответ
                                echo json_encode(["status" => "error", "text" => "Ошибка подписки"]);
                                return;
                            }

                        }
                    }
                }

            } else {

                // Ответ
                echo json_encode(["status" => "error", "text" => "Товар отсутствует в каталоге"]); return;
            }

        } else {

            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка данных"]); return;
        }

    }




    // Прочесть статью позже
    if ( $_POST['action'] == "send_article" ){
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if( intval($arFields['id']) > 0 && strlen($arFields['email']) > 0 ){
            $id = strip_tags($arFields['id']);
            $article = tools\el::info($id);
            $email = strip_tags($arFields['email']);
            $mail_title = 'Ссылка на статью "'.html_entity_decode($article['NAME']).'"';
            $mail_html = '<p>Вы отпраили себе ссылку на статью, которую захотели прочитать позже</p>';
            $mail_html .= '<p>Ссылка на статью: <a href="'.$article['DETAIL_PAGE_URL'].'">'.html_entity_decode($article['NAME']).'</a></p>';
            // отправка инфы на почту
            tools\feedback::sendMainEvent ( $mail_title, $mail_html, $email );

            // Ответ
            echo json_encode(["status" => "ok"]); return;

        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка данных"]); return;
        }
    }




    // Новый отзыв
    if ( $_POST['action'] == "new_review" ){
        global $USER;
        if( $USER->IsAuthorized() ){
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);

            $product_id = strip_tags($arFields['product_id']);
            $review_text = strip_tags($arFields['review_text']);
            $vote = strip_tags($arFields['vote']);

            if(
                intval($product_id) > 0
                &&
                strlen($review_text) > 0
                &&
                intval($vote) > 0 && intval($vote) <= 5
            ){

                $product_review = new project\product_review();
                $res = $product_review->add([
                    'UF_PRODUCT' => $product_id,
                    'UF_USER' => $user_id,
                    'UF_REVIEW_TEXT' => $review_text,
                    'UF_VOTE' => $vote,
                    'UF_TIMESTAMP' => time(),
                    'UF_ACTIVE' => $USER->IsAdmin()?1:0
                ]);

                if( $res ){

                    $product_vote = new project\product_vote();
                    // Удаляем предыдущие оценки пользователя по данному товару
                    $votes = $product_vote->getList( $product_id, $USER->GetID() );
                    foreach ( $votes as $voteItem ){
                        $product_vote->delete($voteItem['ID']);
                    }
                    // Новый голос за товар
                    $product_vote->add( $product_id, $USER->GetID(), $vote );

                    // Ответ
                    echo json_encode(["status" => "ok"]); return;

                } else {
                    // Ответ
                    echo json_encode(["status" => "error", "text" => "Ошибка создания отзыва"]); return;
                }
            } else {
                // Ответ
                echo json_encode(["status" => "error", "text" => "Ошибка данных"]); return;
            }
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Для написания отзыва необходимо авторизоваться"]); return;
        }
    }





	// Авторизация
	if ( $_POST['action'] == "auth" ){
        tools\user::auth();
	}


	// Регистрация
	if ($_POST['action'] == "register"){
        global $USER;
        if( !$USER->IsAuthorized() ){
            // данные в массив
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);
            if ( tools\user::getByLogin( $arFields['EMAIL'] ) ){
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на сайте" ) );
                return;
            }
            if ( tools\user::getByEmail( $arFields['EMAIL'] ) ){
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на сайте" ) );
                return;
            }
            // Регистрируем пользователя
            $regCode = md5(time().randString(12, array("abcdefghijklmnopqrstuvwxyz")));
            $user = new \CUser;
            $arUserFields = Array(
                "ACTIVE" => "Y",
                "NAME" => strip_tags(trim($arFields["NAME"])),
                "LAST_NAME" => strip_tags(trim($arFields["LAST_NAME"])),
                "SECOND_NAME" => strip_tags(trim($arFields["SECOND_NAME"])),
                "LOGIN" => strip_tags(trim($arFields["EMAIL"])),
                "EMAIL" => strip_tags(trim($arFields["EMAIL"])),
                "PASSWORD" => strip_tags($arFields["PASSWORD"]),
                "CONFIRM_PASSWORD" => strip_tags($arFields["PASSWORD"]),
                "GROUP_ID" => Array(6),
                "UF_REG_CODE" => $regCode
            );
            $userID = $user->Add($arUserFields);
            if ($userID){
                tools\user::sendRegInfo($userID, false, $regCode);

                $amo_id = project\amo_crm::addContact(
                    strip_tags(trim($arFields["NAME"])),
                    strip_tags(trim($arFields["LAST_NAME"])),
                    strip_tags(trim($arFields["SECOND_NAME"])),
                    strip_tags(trim($arFields["EMAIL"]))
                );

                if( intval($amo_id) > 0 ){
                    $user->Update($userID, ['UF_AMO_ID' => $amo_id]);
                }

                // Ответ
                echo json_encode( array("status" => 'ok') ); return;
            } else {
                // Ответ
                echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) ); return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Вы уже авторизованы" ) ); return;
        }
	}


    // Редактирование личных данных
    if ($_POST['action'] == "personal_data_save"){
        global $USER;
        if( $USER->IsAuthorized() ){
            // данные в массив
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);

            if( strlen($arFields['EMAIL']) > 0 ){
                if ( tools\user::getByLogin( $arFields['EMAIL'], $USER->GetID(), true ) ){
                    // Ответ
                    echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на сайте другим пользователем" ) );
                    return;
                }
                if ( tools\user::getByEmail( $arFields['EMAIL'], $USER->GetID(), true ) ){
                    // Ответ
                    echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на сайте другим пользователем" ) );
                    return;
                }
            }

            $user = new \CUser;
            $arUserFields = Array(
                "NAME" => strip_tags(trim($arFields["NAME"])),
                "LAST_NAME" => strip_tags(trim($arFields["LAST_NAME"])),
                "SECOND_NAME" => strip_tags(trim($arFields["SECOND_NAME"])),
                "EMAIL" => strip_tags(trim($arFields["EMAIL"])),
                "PERSONAL_PHONE" => strip_tags(trim($arFields["PERSONAL_PHONE"]))
            );
            if( strlen($arFields['EMAIL']) > 0 ){
                $arUserFields["LOGIN"] = strip_tags(trim(strlen($arFields["LOGIN"]) > 0?$arFields["LOGIN"]:$arFields["EMAIL"]));
            }
            $res = $user->Update($USER->GetID(), $arUserFields);
            if ( $res ){
                // Ответ
                echo json_encode( array("status" => 'ok') ); return;
            } else {
                // Ответ
                echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) ); return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка авторизации" ) ); return;
        }
    }


    // Редактирование пароля
    if ($_POST['action'] == "personal_password_save"){
        global $USER;
        if( $USER->IsAuthorized() ){
            // данные в массив
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);
            $user = new \CUser;
            $arUserFields = Array(
                "PASSWORD" => strip_tags(trim($arFields["PASSWORD"])),
                "CONFIRM_PASSWORD" => strip_tags(trim($arFields["CONFIRM_PASSWORD"]))
            );
            $res = $user->Update($USER->GetID(), $arUserFields);
            if ( $res ){
                // Ответ
                echo json_encode( array("status" => 'ok') ); return;
            } else {
                // Ответ
                echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) ); return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка авторизации" ) ); return;
        }
    }



    // Формирование контрольной строки для восстановления пароля
    if ($_POST['action'] == "password_recovery"){
        // данные в массив
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);
        $user_id = false;
        // Если какие-то данные определены
        if ( strlen($arFields['email']) > 0 ){
            // проверяем EMAIL на наличие у других пользователей
            $filter = Array( "=EMAIL" => $arFields["email"] );
            $users = \CUser::GetList(($by="id"), ($order="desc"), $filter); // выбираем пользователей
            if ($user = $users->GetNext()){
                $user_id = $user['ID'];
                $kod = md5(time().rand(1, 10000));
                // Заносим код в профиль пользователя
                $obUser = new CUser;
                $res = $obUser->Update($user['ID'], array( 'UF_RECOVERY_CODE' => $kod ));
                if ($res){

                    $title = 'Ссылка для восстановления пароля (сайт "'.$_SERVER['SERVER_NAME'].'")';
                    $html = '<p>Ссылка для восстановления пароля (сайта "'.$_SERVER['HTTP_HOST'].'"):</p>';
                    $html .= '<p><a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/password_recovery/?code='.$kod.'">'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/password_recovery/?code='.$kod.'</a></p>';

                    // Отправка на почту
                    tools\feedback::sendMainEvent (
                        $title,
                        $html,
                        $arFields["email"]
                    );

                    // Ответ
                    echo json_encode( Array( "status" => "ok" ) );  return;
                }
            }
            if ( !$user_id ){
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => "Такой Email на сайте не зарегистрирован" ) );
                return;
            }
        }
    }




    // Восстановление пароля (сохранение)
    if ($action == "password_recovery_save"){
        // данные в массив
        $arFields = Array();
        parse_str(tools\funcs::param_post("form_data"), $arFields);
        if( strlen($arFields['code']) > 0 ){
            $user_id = false;
            // Ищем пользователя с таким кодом
            $filter = Array( "=UF_RECOVERY_CODE" => strip_tags($arFields['code']) );
            $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter, array('FIELDS' => array('ID'), 'SELECT' => array('UF_*')));
            if ( $arUser = $rsUsers->GetNext() ){
                $user_id = $arUser['ID'];
                // Заносим в профиль пользователя
                $fields['UF_RECOVERY_CODE'] = '';
                $fields['PASSWORD'] = strip_tags($arFields['PASSWORD']);
                $fields['CONFIRM_PASSWORD'] = strip_tags($arFields['CONFIRM_PASSWORD']);
                $user = new \CUser;
                $res = $user->Update($user_id, $fields);
                if ($res){
                    $USER->Logout();
                    // Ответ
                    echo json_encode( Array("status" => "ok") );
                } else {
                    // Ответ
                    echo json_encode(array("status" => 'error', "text" => $user->LAST_ERROR));
                }
            } else {
                // Ответ
                echo json_encode(Array("status" => "error", "text" => "Ссылка ошибочная либо устарела"));
                return;
            }
        } else {
            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Ошибочная ссылка"));
            return;
        }
    }




	// Изменение пароля
	if ($_POST['action'] == "personal_password"){
		// данные в массив
		$arFields = Array();
		parse_str(tools\funcs::param_post("form_data"), $arFields);
		// Ищем пользователя с таким кодом
		$filter = Array( "=UF_RECOVERY_CODE" => $arFields['code'] );
		$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter, 
			array(
				'FIELDS' => array( 'ID' ),
				'SELECT' => array('UF_*')
			)
		);
		while ($arUser = $rsUsers->GetNext()){
			$user_id = $arUser['ID'];
		}
		// Если какие-то данные определены
		if ( intval($user_id) > 0 ){
			// Заносим в профиль пользователя
			$fields['UF_RECOVERY_CODE'] = '';
			$fields['PASSWORD'] = $arFields['PASSWORD'];
			$fields['CONFIRM_PASSWORD'] = $arFields['CONFIRM_PASSWORD'];
			$user = new CUser;
			$res = $user->Update($user_id, $fields);
			if ($res){
				$_SESSION['password_ok'] = 1;
				$USER->Logout();
				// Ответ
				echo json_encode( Array( "status" => "ok", 'html' => $html ) );	
			} else {
				// Ответ
				echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) );
			}
		} else {
			// Ответ
			echo json_encode( array("status" => 'error', "text" => 'Ошибочный код') );
		}
	}





	// Добавляем товар в корзину
	if ($_POST['action'] == "add_to_basket"){
		// Инфо о товаре
		$tov_id = strip_tags($_POST['item_id']);
        $el = tools\el::info( $tov_id );
        $iblock = tools\iblock::getByID( $el['IBLOCK_ID'] );
		$quantity = $_POST['quantity']?strip_tags($_POST['quantity']):1;
		$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		if ($item = $basket->getExistsItem('catalog', $tov_id)){
			$item->setField('QUANTITY', $item->getQuantity() + $quantity);
		} else {
			$item = $basket->createItem('catalog', $tov_id);
			$item->setFields(array(
				'QUANTITY' => $quantity,
				'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_XML_ID' => $el['XML_ID'],
                'CATALOG_XML_ID' => $iblock['XML_ID'],
				'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
			));
		}
		$basket->save();
		ob_start();
			$APPLICATION->IncludeComponent("aoptima:basket_small", "header");
            $html = ob_get_contents();
		ob_end_clean();
		$arResult = Array(
			"status" => "ok",
			"html" =>  $html
		);
		// Ответ
		echo json_encode($arResult);	
	}







	// *** ФОРМИРОВАНИЕ ЗАКАЗА***
	if ($_POST['action'] == "order"){

        \CModule::IncludeModule("catalog");
        \CModule::IncludeModule("sale");
        \CModule::IncludeModule("iblock");

        $rounding = project\catalog::PRICE_ROUND;

        // данные в массив
        $arFields = Array();   $arFields2    = Array();
        parse_str($_POST["form_data"], $arFields);

        foreach ( $arFields as $key => $value ){
            if( is_string($value) ){    $arFields[$key] = strip_tags($value);    }
        }

        // Пользователь по умолчанию
        $user_id = project\order::SERVICE_USER_ID;

        // Пользователь авторизован
        if( $USER->IsAuthorized() ){

            // берём ID пользователя
            $user_id = $USER->GetID();

        // Пользователь НЕ авторизован
        } else {

            // проверяем EMAIL
            $checkUser = tools\user::getByLogin($arFields["email"]);
            if( !$checkUser ){
                $checkUser = tools\user::getByEmail($arFields["email"]);
            }
            // Если найден пользователь с таким Email
            if( intval($checkUser['ID']) > 0 ){
                if( strlen( $arFields["password"] ) > 0 ){
                    $result = $USER->Login($checkUser['LOGIN'], $arFields["password"], "Y");
                    if (!is_array($result)){
                        $user_id = $USER->GetID();
                        $USER->Authorize($user_id);
                    } else {
                        echo json_encode( Array( "status" => "wrong_password" ) );  return;
                    }
                } else {
                    // Если Email уже есть
                    echo json_encode( Array( "status" => "need_auth" ) );  return;
                }

            // Пользователь не найден
            } else {

                // Регистрация пользователя

                $password = randString(10, array( "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789" ));

                //$regCode = md5(time().randString(12, array("abcdefghijklmnopqrstuvwxyz")));
                $arUserFields = Array(
                    "LOGIN" => strip_tags($arFields["email"]),
                    "EMAIL" => strip_tags($arFields["email"]),
                    "PASSWORD" => $password,
                    "CONFIRM_PASSWORD" => $password,
                    "GROUP_ID" => Array(6),
                    "UF_REG_CONFIRMED" => 1,
                   // "UF_REG_CODE" => $regCode,
                );
                if( strlen($arFields["name"]) > 0 ){
                    $arUserFields['NAME'] = strip_tags($arFields["name"]);
                }
                if( strlen($arFields["last_name"]) > 0 ){
                    $arUserFields['LAST_NAME'] = strip_tags($arFields["last_name"]);
                }
                if( strlen($arFields["second_name"]) > 0 ){
                    $arUserFields['SECOND_NAME'] = strip_tags($arFields["second_name"]);
                }
                if( strlen($arFields["phone"]) > 0 ){
                    $arUserFields['PERSONAL_PHONE'] = strip_tags($arFields["phone"]);
                }

                // Регистрация пользователя
                $user = new \CUser;
                $user_id = $user->Add($arUserFields);
                if( intval($user_id) > 0 ){

                    $USER->Authorize($user_id);

                    // Отправка рег. информации
                    tools\user::sendRegInfo( $user_id, $password /*, $regCode*/ );

                    $amo_id = project\amo_crm::addContact(
                        strip_tags(trim($arFields["name"])),
                        strip_tags(trim($arFields["last_name"])),
                        strip_tags(trim($arFields["second_name"])),
                        strip_tags(trim($arFields["email"])),
                        strip_tags(trim($arFields["phone"]))
                    );

                    if( intval($amo_id) > 0 ){
                        $user->Update($user_id, ['UF_AMO_ID' => $amo_id]);
                    }

                }
            }
        }

        $rounding = project\catalog::PRICE_ROUND;

        // Проверим достаточность остатков
        $quantityProducts = [];
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
        foreach ( $basket as $key => $basketItem ){
            $el = tools\el::info($basketItem->getField('PRODUCT_ID'));
            if(
                intval($el['ID']) > 0
                &&
                // Если это товар каталога
                $el['IBLOCK_ID'] == project\catalog::IBLOCK_ID
            ){
                // Получим категорию 1 уровня для товара
                $sectID = \AOptima\ToolsCafeto\el::sections( $el['ID'] )[0]['ID'];
                $chain = \AOptima\ToolsCafeto\section::chain( $sectID );
                $sect1Level = $chain[0];
                $sect1LevelCode = $sect1Level['CODE'];
                // Если категория входит в перечень отслеживаемых по остаткам на складе
                if( in_array( $sect1LevelCode, project\catalog::$availableCategories ) ){
                    // Получим инфу по CCatalogProduct
                    $products = \CCatalogProduct::GetList([], [ "ID" => $el['ID'] ]);
                    if ( $arProduct = $products->GetNext() ){
                        if( $arProduct['QUANTITY'] > 0 ){
                            if( $basketItem->getField('QUANTITY') > $arProduct['QUANTITY'] ){
                                $quantityProducts['toUpdate'][] = [
                                    'old_quantity' => round($basketItem->getField('QUANTITY'), 0),
                                    'new_quantity' => round($arProduct['QUANTITY'], 0),
                                    'el' => $el,
                                ];
                                $basketItem->setField('QUANTITY', $arProduct['QUANTITY']);
                                $basket->save();
                            }
                        } else {
                            $quantityProducts['toDelete'][] = [
                                'old_quantity' => round($basketItem->getField('QUANTITY'), 0),
                                'new_quantity' => 0,
                                'el' => $el,
                            ];
                            $basketItem->delete();
                            $basket->save();
                        }
                    }
                }
            }
        }
        if( count($quantityProducts) > 0 ){
            $quantity_error = '';
            if ( isset($quantityProducts['toUpdate']) ){
                $quantity_error .= '<b>Снижено количество по следующим товарам (превышает остаток на складе):</b><br>';
                foreach ($quantityProducts['toUpdate'] as $item){
                    $quantity_error .= '-&nbsp;'.$item['el']['NAME'].'&nbsp;-&nbsp;с '.$item['old_quantity'].'&nbsp;до&nbsp;'.$item['new_quantity'].'&nbsp;шт.<br>';
                }
            }
            if ( isset($quantityProducts['toDelete']) ){
                $quantity_error .= '<b>Следующие товары отсутствуют на складе. Они автоматически <u>удалены</u> из корзины:</b><br>';
                foreach ($quantityProducts['toDelete'] as $item){
                    $quantity_error .= '- '.$item['el']['NAME'].'<br>';
                }
            }
            echo json_encode([
                "status" => "quantity_error",
                "text" => $quantity_error
            ]);
            return;
        }

        // Инфо по корзине
        $basketInfo = project\user_basket::info();
        $basket_cnt = $basketInfo['basket_cnt'];
        $arBasket = $basketInfo['arBasket'];
        $basket_disc_sum = $basketInfo['basket_disc_sum'];
        $pay_sum = $basket_disc_sum + $_SESSION['DELIVERY_PRICE'];

        // ID варианта оплаты
        $ps_id = strip_tags($arFields['ps_id']);
        $ps = project\ps::getByID($ps_id);

        // ID варианта доставки
        $ds_id = strip_tags($arFields['ds_id']);
        $ds = project\ds::getByID($ds_id);

        $class_name = $arFields['ds_class'][$ds_id];
        if( isset($class_name) && class_exists($class_name) ){
            $dsObj = new $class_name();
            if(
                $dsObj->hasPVZ()
                &&
                !isset($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id])
                ||
                (
                    isset($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id])
                    &&
                    strlen($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id]) == 0
                )
            ){
                echo json_encode([
                    "status" => "error",
                    "text" => "Выберите, пожалуйста, пункт выдачи заказа!"
                ]);
                return;
            }
        }

        // Сохранение адреса (для последующего автозаполнения)
        project\order::saveDeliveryInfo( $user_id, $ds, $arFields );

        // СОЗДАЁМ ЗАКАЗ
        $order = Sale\Order::create('s2', $user_id);

        // Передаём корзину в заказ
        $basket = Basket::create('s1');
        foreach( $arBasket as $basketItem ){
            $item = $basket->createItem('catalog', $basketItem->getField('PRODUCT_ID'));
            $el = tools\el::info($basketItem->getField('PRODUCT_ID'));
            $obIblocks = \CIBlock::GetByID($el['IBLOCK_ID']);
            if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }
            $item->setFields(array(
                'QUANTITY' => $basketItem->getField('QUANTITY'),
                'CURRENCY' => 'RUB',
                'LID' => 's2',
                'PRODUCT_XML_ID' => $el['XML_ID'],
                'CATALOG_XML_ID' => $iblock_xml_id,
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
        }
        $order->setBasket($basket);

        // Тип плательщика
        $order->setPersonTypeId(1);

        // Оплата
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
        $payment->setField('SUM', $pay_sum); // Сумма к оплате
        $payment->setField('CURRENCY', 'RUB');

        // Доставка
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
        if ( !is_null($_SESSION['DELIVERY_PRICE']) ){
            $shipment->setField('CUSTOM_PRICE_DELIVERY', 'Y');
            $shipment->setField('BASE_PRICE_DELIVERY', $_SESSION['DELIVERY_PRICE']);
            $shipment->setField('CURRENCY', 'RUB');
        }
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        foreach ($basket as $basketItem){
            $item = $shipmentItemCollection->createItem($basketItem);
            $item->setQuantity($basketItem->getQuantity());
        }

        // Заполняем свойства заказа
        $pc = $order->getPropertyCollection();
        $p = $pc->getItemByOrderPropertyId(1);
        $p->setValue(strip_tags($arFields["name"]));
        $p = $pc->getItemByOrderPropertyId(14);
        $p->setValue(strip_tags($arFields["last_name"]));
        if( $arFields["second_name"] ){
            $p = $pc->getItemByOrderPropertyId(15);
            $p->setValue(strip_tags($arFields["second_name"]));
        }
        $p = $pc->getItemByOrderPropertyId(2);
        $p->setValue(strip_tags($arFields["phone"]));
        $p = $pc->getItemByOrderPropertyId(3);
        $p->setValue(strip_tags($arFields["email"]));
        $p = $pc->getItemByOrderPropertyId(4);
        $p->setValue(strip_tags($arFields["comment"]));

        if( $arFields['np'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(5);
            $p->setValue(strip_tags($arFields['np'][$ds_id]));
        }
        if( $arFields['np_id'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(9);
            $p->setValue(strip_tags($arFields['np_id'][$ds_id]));
        }
        $address = [];
        if( $arFields['zip_code'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(11);
            $p->setValue(strip_tags($arFields['zip_code'][$ds_id]));
            $address[] = strip_tags($arFields['zip_code'][$ds_id]);
        }
        if( $arFields['street'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(6);
            $p->setValue(strip_tags($arFields['street'][$ds_id]));
            $address[] = strip_tags($arFields['street'][$ds_id]);
        }
        if( $arFields['house'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(7);
            $p->setValue(strip_tags($arFields['house'][$ds_id]));
            $address[] = 'д.'.strip_tags($arFields['house'][$ds_id]);
        }
        if( $arFields['kvartira'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(8);
            $p->setValue(strip_tags($arFields['kvartira'][$ds_id]));
            $address[] = 'кв.'.strip_tags($arFields['kvartira'][$ds_id]);
        }
        if( $arFields['ds_comment'][$ds_id] ){
            $p = $pc->getItemByOrderPropertyId(16);
            $p->setValue(strip_tags($arFields['ds_comment'][$ds_id]));
        }
        $address = implode(', ', $address);
        if( isset($class_name) && class_exists($class_name) ){
            if( isset($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id]) ){
                $p = $pc->getItemByOrderPropertyId(10);
                $p->setValue(strip_tags($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id]));
                $address = strip_tags($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id]);
            }
            if( isset($arFields[$class_name::$PVZ_POINT_ID_FIELD_NAME][$ds_id]) ){
                $p = $pc->getItemByOrderPropertyId(12);
                $p->setValue(strip_tags($arFields[$class_name::$PVZ_POINT_ID_FIELD_NAME][$ds_id]));
            }
        }
        $callback = $arFields['callback']=='Да'?'Да':'Нет';
        $p = $pc->getItemByOrderPropertyId(13);
        $p->setValue($callback);

        // Сохраняем новый заказ
        $order->doFinalAction(true);
        $result = $order->save();

        // Определяем ID нового заказа
        $order_id = $order->GetId();

        if (intval($order_id) > 0){

            if( !$USER->IsAuthorized() ){    $USER->Authorize($user_id);   }

            // Очищаем корзину
            \CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

            // Уменьшаем остатки
            foreach ($arBasket as $basketItem){
                // Если это товар (не ТП)
                if( $basketItem->el['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){
                    // Получим категорию 1 уровня для товара
                    $sectID = \AOptima\ToolsCafeto\el::sections( $basketItem->el['ID'] )[0]['ID'];
                    $chain = \AOptima\ToolsCafeto\section::chain( $sectID );
                    $sect1Level = $chain[0];
                    $sect1LevelCode = $sect1Level['CODE'];
                    // Если категория товара входит в перечень отслеживаемых по остаткам на складе
                    if( in_array( $sect1LevelCode, project\catalog::$availableCategories ) ){
                        // Получим текущий остаток
                        $products = \CCatalogProduct::GetList([], [ "ID" => $basketItem->el['ID'] ]);
                        $arProduct = $products->GetNext();
                        $curQuantity = $arProduct['QUANTITY'];
                        // Получим новый остаток
                        $newQuantity = $curQuantity - $basketItem->getField('QUANTITY');
                        if( $newQuantity < 0 ){  $newQuantity = 0;  }
                        // Сохраним изменения
                        $dbProduct = new \CCatalogProduct;
                        $dbProduct->Update($basketItem->el['ID'], ['QUANTITY' => $newQuantity]);
                    }
                }
            }

            $mail_html = '<p>Новый заказ №'.$order_id.' на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').'</p>';

            $mail_html .= '<h2>Контактные данные:</h2>';
            $mail_html .= '<p>Имя: '.strip_tags($arFields["name"]).';</p>';
            $mail_html .= '<p>Фамилия: '.strip_tags($arFields["last_name"]).';</p>';
            if( $arFields["second_name"] ){
                $mail_html .= '<p>Отчество: '.strip_tags($arFields["second_name"]).';</p>';
            }
            $mail_html .= '<p>Телефон: '.strip_tags($arFields["phone"]).';</p>';
            $mail_html .= '<p>Email: '.strip_tags($arFields["email"]).';</p>';

            $mail_html .= '<h2>Информация о заказе</h2>';
            $mail_html .= '<p>Нужен звонок оператора: '.$callback.';</p>';
            if( strlen($arFields["comment"]) > 0 ){
                $mail_html .= '<p>Комментарий: '.strip_tags($arFields["comment"]).';</p>';
            }
            $mail_html .= '<p>Вариант доставки: '.$ds["NAME"].';</p>';
            $mail_html .= '<p>Вариант оплаты: '.$ps["NAME"].';</p>';

            if( $arFields['np'][$ds_id] ){
                $mail_html .= '<p>Населённый пункт: '.strip_tags($arFields['np'][$ds_id]).';</p>';
            }
            if( isset($class_name) && class_exists($class_name) ){
                if( isset($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id]) ){
                    $mail_html .= '<p>Точка самовывоза: '.strip_tags($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME][$ds_id]).';</p>';
                }
            }
            if( $arFields['zip_code'][$ds_id] ){
                $mail_html .= '<p>Почтовый индекс: '.strip_tags($arFields['zip_code'][$ds_id]).';</p>';
            }
            if( $arFields['street'][$ds_id] ){
                $mail_html .= '<p>Улица: '.strip_tags($arFields['street'][$ds_id]).';</p>';
            }
            if( $arFields['house'][$ds_id] ){
                $mail_html .= '<p>Номер дома / корпус: '.strip_tags($arFields['house'][$ds_id]).';</p>';
            }
            if( $arFields['kvartira'][$ds_id] ){
                $mail_html .= '<p>Квартира: '.strip_tags($arFields['kvartira'][$ds_id]).';</p>';
            }
            if( $arFields['ds_comment'][$ds_id] ){
                $mail_html .= '<p>Комментарий для курьера: '.strip_tags($arFields['ds_comment'][$ds_id]).';</p>';
            }

            // Формируем таблицу для письма
            $mail_html .= '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
            $mail_html .= '<tr>
                <td><b>Товар</b></td>
                <td><b>Цена за ед., руб.</b></td>
                <td><b>Количество</b></td>
                <td><b>Сумма, руб.</b></td>
            </tr>';

            $amo_prim = [];
            foreach ($arBasket as $basketItem){
                $product = $basketItem->product;
                $el = $basketItem->el;
                $price = $basketItem->price;
                $discPrice = $basketItem->discPrice;
                $quantity = round($basketItem->getField('QUANTITY'), 0);
                $pName = $product['NAME'];
                if( $product['ID'] != $el['ID'] ){
                    $pName .= ' ('.$el['PROPERTY_WEIGHT_VALUE'].' гр.)';
                }
                $amo_prim[] = $pName.' x '.$quantity;
                $mail_html .= '<tr>
                    <td>'.$pName.'</td>
                    <td>'.number_format($discPrice, $rounding, ",", " ").'</td>
                    <td>'.round($quantity, 0).'</td>
                    <td>'.number_format($discPrice * $quantity, $rounding, ",", " ").'</td>
                </tr>';
            }
            $mail_html .= '</table>';

            $disc_sum = number_format($basket_disc_sum, $rounding, ",", " ");
            $delivery_price = number_format($_SESSION['DELIVERY_PRICE'], $rounding, ",", " ");
            $pay_sum = number_format($pay_sum, $rounding, ",", " ");

            $mail_html .= '<p>Стоимость: '.$disc_sum.' руб.</p>';
            $mail_html .= '<p>Стоимость доставки: '.$delivery_price.' руб.</p>';
            $mail_html .= '<p>Итого к оплате: '.$pay_sum.' руб.</p>';

            //////////////////////
            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:mailOrder", "default", array('ORDER_ID' => $order_id)
                );
                $mail_html = ob_get_contents();
            ob_end_clean();

            $mail_title = 'Новый заказ №'.$order_id.' (сайт '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';

            // Письмо админу
            $email_to = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'ORDER_EMAIL');
            if( !$email_to ){
                $email_to = \Bitrix\Main\Config\Option::get('main', 'email_from');
            }
            tools\feedback::sendMainEvent( $mail_title, $mail_html, $email_to );
            
            // Письмо покупателю
            tools\feedback::sendMainEvent( $mail_title, $mail_html, strip_tags($arFields['email']) );
            // СМС покупателю
            $sms_message = strip_tags(trim($arFields["name"])).', спасибо за покупку! Ваш заказ "№'.$order_id.'". Доставка "'.$ds['NAME'].'" '.$address.'. Наш номер:  8 800 600-71-41';
            project\smsc::send(strip_tags($arFields['phone']), $sms_message);

            $comment = strip_tags($arFields["comment"]);

            $amo_contact_id = null;
            $arUser = tools\user::info( $user_id );
            if( intval($arUser['UF_AMO_ID']) > 0 ){
                // Проверка наличия такого контакта в AmoCRM
                $amoContact = project\amo_crm::getContactByID( $arUser['UF_AMO_ID'] );
                if( intval($amoContact['id']) > 0 ){
                    $amo_contact_id = $amoContact['id'];
                }
            }
            // Если такого контакта нет
            if( !( intval($amo_contact_id) > 0 ) ){
                // Создаём новый
                $amo_contact_id = project\amo_crm::addContact(
                    strip_tags(trim($arFields["name"])),
                    strip_tags(trim($arFields["last_name"])),
                    strip_tags(trim($arFields["second_name"])),
                    strip_tags(trim($arFields["email"])),
                    strip_tags(trim($arFields["phone"]))
                );
                if( intval($amo_contact_id) > 0 ){
                    // Сохраняем в профиле пользователя
                    $user = new \CUser;
                    $user->Update($user_id, ['UF_AMO_ID' => $amo_contact_id]);
                }
            }

            $sdelka_id = project\amo_crm::addOrder(
                $order_id,
                $pay_sum,
                $amo_contact_id,
                $_SESSION['DELIVERY_PRICE'],
                $ds['NAME'],
                $comment,
                strip_tags($arFields["phone"]),
                $basket_disc_sum,
                $address
            );
            if( intval($sdelka_id) > 0 ){
                $amo_prim = implode(', ', $amo_prim);
                $amo_prim = ($callback=='Да'?'Нужен звонок. ':'Выслано смс-оповещение. ').$amo_prim;
                $prim_id = project\amo_crm::sendPrim($sdelka_id, $amo_prim);
                $task_id = project\amo_crm::addTask($sdelka_id, 'Подтвердить заказ у клиента');
            }

            // Сохранение адреса (для последующего автозаполнения)
            project\order::saveDeliveryInfo( $user_id, $ds, $arFields );

            // Ответ
            echo json_encode([
                "status" => "ok",
                "order_id" => $order_id,
                "amo_contact_id" => $amo_contact_id,
                "sdelka_id" => $sdelka_id,
                "prim_id" => $prim_id,
                "task_id" => $task_id,
            ]);
            return;

        } else {

            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка оформления заказа" ) ); return;
        }

	}




    // *** ПОВТОР ЗАКАЗА***
    if ($_POST['action'] == "order_repeat"){

        \CModule::IncludeModule("catalog");  \CModule::IncludeModule("sale");
        \CModule::IncludeModule("iblock");

        // Проверка авторизации
        if( !$USER->IsAuthorized() ){
            echo json_encode( [ "status" => "error", "text" => "Ошибка авторизации" ] ); return;
        }
        $order_id = strip_tags($_POST['order_id']);
        if( intval($order_id) > 0 ){} else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка данных" ) ); return;
        }

        // Проверяем есть ли у пользователя такой заказ
        $filter_array = array(
            'USER_ID' => $USER->GetID(),
            'ID' => $order_id
        );
        $orders = \Bitrix\Sale\OrderTable::getList(array(
            'filter' => $filter_array, 'select' => array('USER_ID', 'ID'),
            'order' => array('ID' => 'DESC'), 'limit' => 1
        ));
        if ($arOrder = $orders->fetch()){

            $obOrder = Sale\Order::load($arOrder['ID']);
            $user_id = $obOrder->getUserId();

            $orderPropValues = project\order::getOrderPropValues($obOrder);
            $arFields['name'] = project\order::getPropValue($orderPropValues, 'NAME');
            $arFields['last_name'] = project\order::getPropValue($orderPropValues, 'LAST_NAME');
            $arFields['second_name'] = project\order::getPropValue($orderPropValues, 'SECOND_NAME');
            $arFields['phone'] = project\order::getPropValue($orderPropValues, 'PHONE');
            $arFields['email'] = project\order::getPropValue($orderPropValues, 'EMAIL');
            $arFields['comment'] = project\order::getPropValue($orderPropValues, 'COMMENT');
            $arFields['np'] = project\order::getPropValue($orderPropValues, 'NP');
            $arFields['np_id'] = project\order::getPropValue($orderPropValues, 'NP_ID');
            $arFields['zip_code'] = project\order::getPropValue($orderPropValues, 'ZIP_CODE');
            $arFields['street'] = project\order::getPropValue($orderPropValues, 'STREET');
            $arFields['house'] = project\order::getPropValue($orderPropValues, 'HOUSE');
            $arFields['kvartira'] = project\order::getPropValue($orderPropValues, 'KVARTIRA');
            $arFields['ds_comment'] = project\order::getPropValue($orderPropValues, 'DS_COMMENT');
            $arFields['pvz_address'] = project\order::getPropValue($orderPropValues, 'PVZ_POINT');
            $arFields['pvz_point_id'] = project\order::getPropValue($orderPropValues, 'PVZ_POINT_ID');

            $rounding = project\catalog::PRICE_ROUND;

            // Инфо по корзине
            $orderBasket = $obOrder->getBasket();
            $basket_cnt = 0;   $arBasket = [];
            foreach ( $orderBasket as $key => $basketItem ){
                $basket_cnt += $basketItem->getQuantity();
                $el = tools\el::info( $basketItem->getProductID() );
                $basketItem->el = $el;
                $result = \CCatalogSku::GetProductInfo( $el['ID'] );
                $isSKU = $result['ID'];
                $basketItem->isSKU = $isSKU;
                if ( $isSKU ){
                    $basketItem->product = tools\el::info($isSKU);
                } else {
                    $basketItem->product = $el;
                }
                $basketItem->price = $basketItem->getPrice();
                $basketItem->priceFormat = number_format($basketItem->price, $rounding, ",", " ");
                $basketItem->discPrice = $basketItem->getPrice();
                $basketItem->discPriceFormat = number_format($basketItem->discPrice, $rounding, ",", " ");
                $basketItem->sum = $basketItem->price * $basketItem->getField('QUANTITY');
                $basketItem->sumFormat = number_format($basketItem->sum, $rounding, ",", " ");
                $basketItem->discSum = $basketItem->discPrice * $basketItem->getField('QUANTITY');
                $basketItem->discSumFormat = number_format($basketItem->discSum, $rounding, ",", " ");
                $arBasket[] = $basketItem;
            }

            $basket_disc_sum = $orderBasket->getPrice();

            $shipmentCollection = $obOrder->getShipmentCollection();
            $delivery_price = $shipmentCollection[0]->getPrice();
            $ds_id = $shipmentCollection[0]->getDeliveryId();
            $ds = project\ds::getByID($ds_id);

            $class_name = '\AOptima\ProjectCafeto\ds_'.$ds['XML_ID'];

            $paymentCollection = $obOrder->getPaymentCollection();
            $ps_id = $paymentCollection[0]->getPaymentSystemId();
            $ps = project\ps::getByID($ps_id);

            $pay_sum = $basket_disc_sum + $delivery_price;

            // СОЗДАЁМ ЗАКАЗ
            $order = Sale\Order::create('s2', $user_id);

            // Очищаем текущую корзину
            \CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

            // Передаём корзину в заказ
            $basket = Basket::create('s1');
            foreach( $orderBasket as $basketItem ){
                $item = $basket->createItem('catalog', $basketItem->getProductID());
                $el = tools\el::info($basketItem->getProductID());
                $obIblocks = \CIBlock::GetByID($el['IBLOCK_ID']);
                if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }
                $item->setFields(array(
                    'QUANTITY' => $basketItem->getField('QUANTITY'),
                    'CURRENCY' => 'RUB',
                    'LID' => 's2',
                    'PRODUCT_XML_ID' => $el['XML_ID'],
                    'CATALOG_XML_ID' => $iblock_xml_id,
                    'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                ));
            }
            $order->setBasket($basket);

            // Тип плательщика
            $order->setPersonTypeId(1);

            // Оплата
            $paymentCollection = $order->getPaymentCollection();
            $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
            $payment->setField('SUM', $pay_sum); // Сумма к оплате
            $payment->setField('CURRENCY', 'RUB');

            // Доставка
            $shipmentCollection = $order->getShipmentCollection();
            $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
            if ( !is_null($delivery_price) ){
                $shipment->setField('CUSTOM_PRICE_DELIVERY', 'Y');
                $shipment->setField('BASE_PRICE_DELIVERY', $delivery_price);
                $shipment->setField('CURRENCY', 'RUB');
            }
            $shipmentItemCollection = $shipment->getShipmentItemCollection();
            foreach ($basket as $basketItem){
                $item = $shipmentItemCollection->createItem($basketItem);
                $item->setQuantity($basketItem->getQuantity());
            }

            // Заполняем свойства заказа
            $pc = $order->getPropertyCollection();
            $p = $pc->getItemByOrderPropertyId(1);
            $p->setValue(strip_tags($arFields["name"]));
            $p = $pc->getItemByOrderPropertyId(14);
            $p->setValue(strip_tags($arFields["last_name"]));
            if( $arFields["secon_name"] ){
                $p = $pc->getItemByOrderPropertyId(15);
                $p->setValue(strip_tags($arFields["secon_name"]));
            }
            $p = $pc->getItemByOrderPropertyId(2);   $p->setValue(strip_tags($arFields["phone"]));
            $p = $pc->getItemByOrderPropertyId(3);   $p->setValue(strip_tags($arFields["email"]));
            $p = $pc->getItemByOrderPropertyId(4);   $p->setValue(strip_tags($arFields["comment"]));

            if( $arFields['np'] ){
                $p = $pc->getItemByOrderPropertyId(5);
                $p->setValue(strip_tags($arFields['np']));
            }
            if( $arFields['np_id'] ){
                $p = $pc->getItemByOrderPropertyId(9);
                $p->setValue(strip_tags($arFields['np_id']));
            }
            $address = [];
            if( $arFields['zip_code'] ){
                $p = $pc->getItemByOrderPropertyId(11);
                $p->setValue(strip_tags($arFields['zip_code']));
                $address[] = strip_tags($arFields['zip_code']);
            }
            if( $arFields['street'] ){
                $p = $pc->getItemByOrderPropertyId(6);
                $p->setValue(strip_tags($arFields['street']));
                $address[] = strip_tags($arFields['street']);
            }
            if( $arFields['house'] ){
                $p = $pc->getItemByOrderPropertyId(7);
                $p->setValue(strip_tags($arFields['house']));
                $address[] = strip_tags($arFields['house']);
            }
            if( $arFields['kvartira'] ){
                $p = $pc->getItemByOrderPropertyId(8);
                $p->setValue(strip_tags($arFields['kvartira']));
                $address[] = strip_tags($arFields['kvartira']);
            }
            if( $arFields['ds_comment'] ){
                $p = $pc->getItemByOrderPropertyId(16);
                $p->setValue(strip_tags($arFields['ds_comment']));
            }
            $address = implode(', ', $address);
            if( $arFields['pvz_address'] ){
                $p = $pc->getItemByOrderPropertyId(10);
                $p->setValue(strip_tags($arFields['pvz_address']));
                $address = strip_tags($arFields['pvz_address']);
            }
            if( $arFields['pvz_point_id'] ){
                $p = $pc->getItemByOrderPropertyId(12);
                $p->setValue(strip_tags($arFields['pvz_point_id'][$ds_id]));
            }

            // Сохраняем новый заказ
            $order->doFinalAction(true);
            $result = $order->save();

            // Определяем ID нового заказа
            $order_id = $order->GetId();

            if (intval($order_id) > 0){

                // Очищаем корзину
                //\CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

                $mail_html = '<p>Новый заказ №'.$order_id.' на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').'</p>';

                $mail_html .= '<h2>Контактные данные:</h2>';
                $mail_html .= '<p>Имя: '.strip_tags($arFields["name"]).';</p>';
                $mail_html .= '<p>Телефон: '.strip_tags($arFields["phone"]).';</p>';
                $mail_html .= '<p>Email: '.strip_tags($arFields["email"]).';</p>';

                $mail_html .= '<h2>Информация о заказе</h2>';
                if( strlen($arFields["comment"]) > 0 ){
                    $mail_html .= '<p>Комментарий: '.strip_tags($arFields["comment"]).';</p>';
                }
                $mail_html .= '<p>Вариант доставки: '.$ds["NAME"].';</p>';
                $mail_html .= '<p>Вариант оплаты: '.$ps["NAME"].';</p>';

                if( $arFields['np'] ){
                    $mail_html .= '<p>Населённый пункт: '.strip_tags($arFields['np']).';</p>';
                }
                if( isset($class_name) && class_exists($class_name) ){
                    if( isset($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME]) ){
                        $mail_html .= '<p>Точка самовывоза (PickPoint): '.strip_tags($arFields[$class_name::$PVZ_ADDRESS_FIELD_NAME]).';</p>';
                    }
                }
                if( $arFields['zip_code'] ){
                    $mail_html .= '<p>Почтовый индекс: '.strip_tags($arFields['zip_code']).';</p>';
                }
                if( $arFields['street'][$ds_id] ){
                    $mail_html .= '<p>Улица: '.strip_tags($arFields['street'][$ds_id]).';</p>';
                }
                if( $arFields['house'] ){
                    $mail_html .= '<p>Номер дома / корпус: '.strip_tags($arFields['house']).';</p>';
                }
                if( $arFields['kvartira'] ){
                    $mail_html .= '<p>Квартира: '.strip_tags($arFields['kvartira']).';</p>';
                }
                if( $arFields['ds_comment'] ){
                    $mail_html .= '<p>Комментарий для курьера: '.strip_tags($arFields['ds_comment']).';</p>';
                }

                // Формируем таблицу для письма
                $mail_html .= '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
                $mail_html .= '<tr>
                    <td><b>Товар</b></td>
                    <td><b>Цена за ед., руб.</b></td>
                    <td><b>Количество</b></td>
                    <td><b>Сумма, руб.</b></td>
                </tr>';

                foreach ($arBasket as $basketItem){
                    $product = $basketItem->product;
                    $el = $basketItem->el;
                    $price = $basketItem->price;
                    $discPrice = $basketItem->discPrice;
                    $quantity = $basketItem->getField('QUANTITY');
                    $pName = $product['NAME'];
                    if( $product['ID'] != $el['ID'] ){
                        $pName .= ' ('.$el['PROPERTY_WEIGHT_VALUE'].' гр.)';
                    }
                    $mail_html .= '<tr>
                        <td>'.$pName.'</td>
                        <td>'.number_format($discPrice, $rounding, ",", " ").'</td>
                        <td>'.round($quantity, 0).'</td>
                        <td>'.number_format($discPrice * $quantity, $rounding, ",", " ").'</td>
                    </tr>';
                }
                $mail_html .= '</table>';

                $disc_sum = number_format($basket_disc_sum, $rounding, ",", " ");
                $delivery_price_format = number_format($delivery_price, $rounding, ",", " ");
                $pay_sum = number_format($pay_sum, $rounding, ",", " ");

                $mail_html .= '<p>Стоимость: '.$disc_sum.' руб.</p>';
                $mail_html .= '<p>Стоимость доставки: '.$delivery_price_format.' руб.</p>';
                $mail_html .= '<p>Итого к оплате: '.$pay_sum.' руб.</p>';

                $mail_title = 'Новый заказ №'.$order_id.' (сайт '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';

                // Письмо админу
                $email_to = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'ORDER_EMAIL');
                if( !$email_to ){
                    $email_to = \Bitrix\Main\Config\Option::get('main', 'email_from');
                }
                tools\feedback::sendMainEvent( $mail_title, $mail_html, $email_to );

                // Письмо покупателю
                tools\feedback::sendMainEvent( $mail_title, $mail_html, strip_tags($arFields['email']) );

                $sdelka_id = project\amo_crm::addOrder(
                    $order_id,
                    $pay_sum,
                    tools\user::info($user_id)['UF_AMO_ID'],
                    $delivery_price,
                    $ds['NAME'],
                    $arFields['comment'],
                    strip_tags($arFields["phone"]),
                    $basket_disc_sum,
                    $address
                );
                if( intval($sdelka_id) > 0 ){
                    $amo_prim = implode(', ', $amo_prim);
                    $amo_prim = 'Нужен звонок. '.$amo_prim;
                    project\amo_crm::sendPrim($sdelka_id, $amo_prim);
                    project\amo_crm::addTask($sdelka_id, 'Подтвердить заказ у клиента');
                }

                // Ответ
                echo json_encode( Array(
                    "status" => "ok",
                    "order_id" => $order_id
                ) );

            } else {
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => "Ошибка оформления заказа" ) ); return;
            }

        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка доступа к заказу" ) ); return;
        }

    }






	// Подписка на рассылку
	if ($_POST['action'] == "subscribe"){
	    $arFields = Array();
	    parse_str($_POST["form_data"], $arFields);
	    $email = strip_tags($arFields['email']);
	    if( strlen($email) > 0 ){
	        $subscribe = new project\subscribe();
	        $res = $subscribe->save( $email, [1] );
            // Ответ
            echo json_encode( $res );
	    } else {
            // Ответ
            echo json_encode( [ "status" => "error", 'text' => 'Ошибка данных' ] );
	    }
	}


    // Заявка на обратный звонок
    if ($_POST['action'] == "callback"){

        require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if (strlen($arFields['g-recaptcha-response']) > 0){

            $client = new Client(array('timeout' => 10));

            try {

                $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', array('form_params' => array('secret' => \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'GOOGLE_RECAPTCHA2_SECRET_KEY'), 'response' => strip_tags($arFields['g-recaptcha-response']), 'remoteip' => $_SERVER['REMOTE_ADDR'])));

                if ($response->getStatusCode() == 200){

                    $body = $response->getBody();
                    $json = $body->getContents();
                    $res = tools\funcs::json_to_array($json);

                    if ($res['success'] == true){

                        /////////////////////////////////
                        $iblock_code = 'callback_cafeto';
                        $iblock_name = 'Заявки на обратный звонок';
                        ////////////////////////////////
                        $settings = array(
                            'name' => 'Имя',
                            'phone' => 'Телефон',
                            'message' => 'Текст сообщения',
                        );
                        /////////////////////////////////
                        $email_to = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'FEEDBACK_EMAIL_TO');
                        $mail_title = 'Новое сообщение из формы "Оставьте данные для звонка" (сайт "'.$_SERVER['SERVER_NAME'].'")';
                        $mail_html = '<p>Новое сообщение из формы "Оставьте данные для звонка" на сайте "'.$_SERVER['SERVER_NAME'].'"</p><hr>';
                        /////////////////////////////////
                        // Отправка
                        $el_id = tools\feedback::send(
                            array(
                                'email_to' => $email_to,
                                'iblock_code' => $iblock_code,
                                'iblock_name' => $iblock_name,
                                'settings' => $settings,
                                'mail_title' => $mail_title,
                                'mail_html' => $mail_html,
                            )
                        );
                        if( intval($el_id) > 0 ){

                            $amo_contact_id = project\amo_crm::addContact(
                                strip_tags(trim($arFields["name"])),
                                false,
                                false,
                                false,
                                strip_tags(trim($arFields["phone"]))
                            );
                            if( intval($amo_contact_id) > 0 ){
                                $sdelka_id = project\amo_crm::addSdelka($amo_contact_id, 'Запрос на обратный звонок №'.$el_id);
                                if( intval($sdelka_id) > 0 ){
                                    $task_id = project\amo_crm::addTask($sdelka_id, strip_tags(trim($arFields["message"])));
                                }
                            }

                            echo json_encode( array("status" => 'ok') ); return;
                        } else {
                            echo json_encode( array("status" => 'error', 'text' => 'Ошибка отправки') ); return;
                        }

                    } else {
                        // Ответ
                        echo json_encode(array("status" => 'error', 'text' => 'Ошибка проверки пользователя'));
                        return;
                    }

                } else {
                    // Ответ
                    echo json_encode(array("status" => 'error', 'text' => 'Ошибка проверки пользователя'));
                    return;
                }

            } catch (RequestException $ex) {
                // Ответ
                echo json_encode(array("status" => 'error', 'text' => 'Ошибка проверки пользователя'));
                return;
            }

        } else {

            // Ответ
            echo json_encode(array("status" => 'error', 'text' => 'Пройдите, пожалуйста, проверку пользователя!'));
            return;

        }
    }




    // Обратная связь
	if ($_POST['action'] == "feedback"){

        require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if (strlen($arFields['g-recaptcha-response']) > 0){

            $client = new Client(array('timeout' => 10));

            try {

                $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', array('form_params' => array('secret' => \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'GOOGLE_RECAPTCHA2_SECRET_KEY'), 'response' => strip_tags($arFields['g-recaptcha-response']), 'remoteip' => $_SERVER['REMOTE_ADDR'])));

                if ($response->getStatusCode() == 200){

                    $body = $response->getBody();
                    $json = $body->getContents();
                    $res = tools\funcs::json_to_array($json);

                    if ($res['success'] == true){

                        /////////////////////////////////
                        $iblock_code = 'feedback_cafeto';
                        $iblock_name = 'Форма обратной связи';
                        ////////////////////////////////
                        $settings = array(
                            'name' => 'Имя',
                            'phone' => 'Телефон',
                            'message' => 'Текст сообщения',
                        );
                        /////////////////////////////////
                        $email_to = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'FEEDBACK_EMAIL_TO');
                        $mail_title = 'Новое сообщение из формы "Обратная связь" (сайт "'.$_SERVER['SERVER_NAME'].'")';
                        $mail_html = '<p>Новое сообщение из формы "Обратная связь" на сайте "'.$_SERVER['SERVER_NAME'].'"</p><hr>';
                        /////////////////////////////////
                        // Отправка
                        $el_id = tools\feedback::send(
                            array(
                                'email_to' => $email_to,
                                'iblock_code' => $iblock_code,
                                'iblock_name' => $iblock_name,
                                'settings' => $settings,
                                'mail_title' => $mail_title,
                                'mail_html' => $mail_html,
                            )
                        );
                        if( intval($el_id) > 0 ){

                            $arFields = Array();
                            parse_str($_POST["form_data"], $arFields);

                            $amo_contact_id = project\amo_crm::addContact(
                                strip_tags(trim($arFields["name"])),
                                false,
                                false,
                                false,
                                strip_tags(trim($arFields["phone"]))
                            );
                            if( intval($amo_contact_id) > 0 ){
                                $sdelka_id = project\amo_crm::addSdelka($amo_contact_id, 'Запрос на обратный звонок №'.$el_id);
                                if( intval($sdelka_id) > 0 ){
                                    $task_id = project\amo_crm::addTask($sdelka_id, strip_tags(trim($arFields["message"])));
                                }
                            }

                            ob_start();
                            	$APPLICATION->IncludeComponent(
                            	    "aoptima:feedback_form", "default",
                                    ['IS_AJAX' => 'Y']
                                );
                            	$html = ob_get_contents();
                            ob_end_clean();

                            echo json_encode( array("status" => 'ok', 'feedback_form_html' => $html) ); return;
                        } else {
                            echo json_encode( array("status" => 'error', 'text' => 'Ошибка отправки') ); return;
                        }

                    } else {
                        // Ответ
                        echo json_encode(array("status" => 'error', 'text' => 'Ошибка проверки пользователя'));
                        return;
                    }

                } else {
                    // Ответ
                    echo json_encode(array("status" => 'error', 'text' => 'Ошибка проверки пользователя'));
                    return;
                }

            } catch (RequestException $ex) {
                // Ответ
                echo json_encode(array("status" => 'error', 'text' => 'Ошибка проверки пользователя'));
                return;
            }

        } else {

            // Ответ
            echo json_encode(array("status" => 'error', 'text' => 'Пройдите, пожалуйста, проверку пользователя!'));
            return;

        }

	}



}
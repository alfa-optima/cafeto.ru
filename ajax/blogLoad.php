<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    global $APPLICATION;

    ob_start();

        $blogSections = project\blog::sections();

        if( is_array($_POST['stop_ids']) ){
            $GLOBALS['blog_list_load']['!ID'] = $_POST['stop_ids'];
        }
        if( intval($_POST['section_id']) > 0 ){
            $section = tools\section::info($_POST['section_id']);
            $GLOBALS['blog_list_load']['SECTION_ID'] = $_POST['section_id'];
        }

        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "blog_list",
            array(
                "IS_AJAX" => "Y",
                "blogSections" => $blogSections,
                "COMPONENT_TEMPLATE" => "default",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => project\blog::IBLOCK_ID,
                "NEWS_COUNT" => project\blog::MAX_CNT+1,
                "SORT_BY1" => 'PROPERTY_SORT_DATE',
                "SORT_ORDER1" => 'DESC',
                "SORT_BY2" => 'NAME',
                "SORT_ORDER2" => 'ASC',
                "FILTER_NAME" => "blog_list_load",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('SORT_DATE', 'AUTHOR_NAME', 'AUTHOR_PHOTO'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => "undefined",
                "SET_LAST_MODIFIED" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ),
            false
        );
        $html = ob_get_contents();
    ob_end_clean();

    $breadcrumbs = '<div class="breadcrumbs template__breadcrumbs catalog__breadcrumbs"><a href="/" class="link breadcrumbs__item breadcrumb__link">Главная</a><p class="breadcrumbs__item breadcrumbs__sep">/</p><a href="/blog/" class="link breadcrumbs__item breadcrumb__link">Блог</a>';
    if( intval($section) > 0 ){
        $breadcrumbs .= '<p class="breadcrumbs__item breadcrumbs__sep">/</p><a class="link breadcrumbs__item breadcrumb__link">'.$section['NAME'].'</a>';
    }
    $breadcrumbs .= '</div>';

    $arResult = ["status" => "ok", 'html' => $html, 'breadcrumbs' => $breadcrumbs];
    if( intval($section) > 0 ){    $arResult['section'] = $section;    }
	echo json_encode($arResult);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
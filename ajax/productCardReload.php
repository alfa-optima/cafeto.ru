<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $item_id = strip_tags($_POST['item_id']);
    $item_code = strip_tags($_POST['item_code']);
    $sku_id = strip_tags($_POST['sku_id']);

    ob_start();
        $params = $_SESSION['catalog_element_params'];
        $params['IS_AJAX'] = 'Y';
        $params['sku_id'] = $sku_id;
        $params['ELEMENT_ID'] = $item_id;
        $params['ELEMENT_CODE'] = $item_code;
        $APPLICATION->IncludeComponent( 'bitrix:catalog.element', 'catalog_product', $params );
        $html = ob_get_contents();
    ob_end_clean();


	// Ответ
	echo json_encode(Array( "status" => "ok", "html" => $html ));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
/* share42.com | 22.08.2016 | (c) Dimox */
(function($){$(function(){$('div.share42init').each(function(idx){var el=$(this),u=el.attr('data-url'),t=el.attr('data-title'),i=el.attr('data-image'),d=el.attr('data-description'),f=el.attr('data-path'),fn=el.attr('data-icons-file'),z=el.attr("data-zero-counter");if(!u)u=location.href;if(!fn)fn='icons.png';if(!z)z=0;if(!f){function path(name){var sc=document.getElementsByTagName('script'),sr=new RegExp('^(.*/|)('+name+')([#?]|$)');for(var p=0,scL=sc.length;p<scL;p++){var m=String(sc[p].src).match(sr);if(m){if(m[1].match(/^((https?|file)\:\/{2,}|\w:[\/\\])/))return m[1];if(m[1].indexOf("/")==0)return m[1];b=document.getElementsByTagName('base');if(b[0]&&b[0].href)return b[0].href+m[1];else return document.location.pathname.match(/(.*[\/\\])/)[0]+m[1];}}return null;}f=path('share42.js');}if(!t)t=document.title;

if(!d){
    var meta=$('meta[name="description"]').attr('content');
    if(meta!==undefined)d=meta;else d='';
}

u=encodeURIComponent(u);
t=encodeURIComponent(t);
t=t.replace(/\'/g,'%27');
i=encodeURIComponent(i);
d=encodeURIComponent(d);

d=d.replace(/\'/g,'%27');
var vkImage='';
if(i!='null'&&i!='')vkImage='&image='+i;


var hrefs = {
    vk: '"#" data-count="vk" onclick="window.open(\'//vk.com/share.php?url='+u+'&title='+t+vkImage+'&description='+d+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться В Контакте"',
    fb: '"#" data-count="fb" onclick="window.open(\'//www.facebook.com/sharer/sharer.php?u='+u+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться в Facebook"',
    //gplus: '"#" data-count="gplus" onclick="window.open(\'//plus.google.com/share?url='+u+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться в Google+"',
};

var links = {
    vk: '<a href="#" class="share_vk share__link"><svg width="40px" height="40px" viewBox="0 0 48.03 48.03"><path fill="#4d729a" d="M56,32A24,24,0,1,1,32,8,24,24,0,0,1,56,32Zm0,0" transform="translate(-7.98 -7.98)" /><path fill="#fff" fill-rule="evenodd" d="M31.09,42.53H33a1.59,1.59,0,0,0,.86-.37,1.45,1.45,0,0,0,.26-.83s0-2.53,1.13-2.9,2.65,2.44,4.22,3.52a3,3,0,0,0,2.1.64l4.21-.06s2.2-.13,1.16-1.87A14.28,14.28,0,0,0,43.79,37c-2.64-2.45-2.29-2,.89-6.29,1.94-2.58,2.71-4.16,2.47-4.83s-1.65-.48-1.65-.48l-4.75,0a1,1,0,0,0-.61.11,1.26,1.26,0,0,0-.42.51A27,27,0,0,1,38,29.78c-2.11,3.59-3,3.78-3.3,3.56-.8-.52-.6-2.09-.6-3.2,0-3.47.52-4.92-1-5.3a8.53,8.53,0,0,0-2.21-.22,10.36,10.36,0,0,0-3.94.4c-.54.27-.95.86-.7.89a2.09,2.09,0,0,1,1.4.71,4.51,4.51,0,0,1,.47,2.14s.28,4.09-.65,4.6c-.64.35-1.52-.37-3.4-3.62a30.52,30.52,0,0,1-1.69-3.5,1.38,1.38,0,0,0-.39-.53,1.93,1.93,0,0,0-.73-.3l-4.51,0a1.51,1.51,0,0,0-.92.32c-.22.26,0,.8,0,.8S19.28,34.81,23.27,39a10.82,10.82,0,0,0,7.82,3.56Zm0,0" transform="translate(-7.98 -7.98)" /></svg></a>',
    fb: '<a href="#" class="share_fb share__link"><svg width="40px" height="40px" viewBox="0 0 47.72 47.72"><path fill="#3b5690" d="M31.64,8.17A23.86,23.86,0,1,1,7.78,32,23.86,23.86,0,0,1,31.64,8.17Zm0,0" transform="translate(-7.78 -8.17)" /><path fill="#fff" d="M34.54,24.6h3.08V20.06H34c-4.38.16-5.28,2.62-5.36,5.21h0v2.26h-3V32h3V43.92h4.49V32h3.68l.71-4.45H33.13V26.17a1.46,1.46,0,0,1,1.41-1.57Zm0,0" transform="translate(-7.78 -8.17)"/></svg></a>',
    //gplus: '<a href="#" class="share_gplus share__link"><svg width="40px" height="40px" viewBox="0 0 89.76 89.76"><path fill="#dc4e41" d="M76.76,31.88A44.88,44.88,0,1,1,31.88-13,44.89,44.89,0,0,1,76.76,31.88Zm0,0" transform="translate(13 13)" /><path fill="#dc4e41" d="M2.62,33.89A18.49,18.49,0,0,0,15.1,51.38C22,53.7,30.46,52,35.05,46.06c3.35-4.13,4.11-9.65,3.75-14.79-5.91-.06-11.81,0-17.7,0,0,2.1,0,4.19,0,6.3,3.53.09,7.07.05,10.6.12a10.24,10.24,0,0,1-6,7c-5.95,2.62-13.55-.81-15.42-7.05a11.71,11.71,0,0,1,7.55-14.94c3.79-1.34,7.7.16,10.94,2.13C30.41,23.28,32,21.63,33.46,20a18.55,18.55,0,0,0-13.29-4.56c-9.55.12-17.91,8.93-17.55,18.48Zm0,0" transform="translate(13 13)" /><path fill="#dc4e41" d="M50.28,25.93c0,1.77,0,3.53,0,5.29l-5.29,0v5.27l5.29,0c0,1.75,0,3.51,0,5.28,1.75,0,3.51,0,5.27,0,0-1.77,0-3.53,0-5.29l5.28,0V31.25l-5.28,0,0-5.28Zm0,0" transform="translate(13 13)" /><path fill="#fff" d="M2.62,33.89c-.36-9.55,8-18.36,17.55-18.48A18.55,18.55,0,0,1,33.46,20c-1.51,1.66-3.05,3.31-4.69,4.84-3.25-2-7.15-3.47-10.94-2.13a11.68,11.68,0,1,0,7.87,22,10.21,10.21,0,0,0,6-7c-3.53-.07-7.07,0-10.6-.12,0-2.11,0-4.2,0-6.3,5.89,0,11.79,0,17.7,0,.36,5.15-.4,10.67-3.75,14.8C30.46,52,22,53.7,15.1,51.38A18.49,18.49,0,0,1,2.62,33.89Zm0,0" transform="translate(13 13)" /><path fill="#fff" d="M50.28,25.93h5.26l0,5.29,5.28,0v5.27l-5.28,0c0,1.76,0,3.52,0,5.29-1.76,0-3.52,0-5.27,0,0-1.77,0-3.53,0-5.28l-5.29,0V31.25l5.29,0c0-1.76,0-3.52,0-5.29Zm0,0" transform="translate(13 13)" /></svg></a>'
};


var html = '';
for( key in links ){
    var link_html = links[key];
    link_html = link_html.replace(new RegExp('href="#"','g'), 'href='+hrefs[key])
    html += link_html;
}
el.html('<p class="share__name">Поделиться:</p><div class="share__list">'+html+'</div>');


})})})(jQuery);
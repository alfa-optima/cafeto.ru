<?
require( $_SERVER[ "DOCUMENT_ROOT" ] . "/bitrix/header.php" );
$APPLICATION->SetTitle("Условия использования");
?>

    <p style="text-align: justify;">
	Основные понятия
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>
	<p style="text-align: justify;">
		 Посетитель Сайта — лицо, посетившее <a href="http://www.cafeto.ru">http://www.cafeto.ru</a> без оформления Заказа.<br>
		 Сайт —&nbsp;<a href="http://www.cafeto.ru/">http://www.cafeto.ru</a>
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Пользователь Сайта (далее «Пользователь») — физическое лицо, посетитель Сайта, принимающий условия настоящего Соглашения и желающий разместить Заказы в Интернет-магазине CAFETO.
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Покупатель — Пользователь, разместивший Заказ в Интернет-магазине CAFETO.
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 CAFETO — Общество с ограниченной ответственностью «КАФЕТО» (ИНН 7726410514, КПП 772501001, ОГРН 1177746932802, юр. адрес: 115191, г. Москва, ул. Большая Тульская, д.2, этаж антресоль 1, пом. IIВ, комната 1).
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Продавец — компания CAFETO, которая реализует собственную продукцию и продукцию сторонних производителей, представленную на сайте <a href="http://www.cafeto.ru/">http://www.cafeto.ru</a>
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Интернет-магазин — Интернет-сайт, принадлежащий CAFETO, расположенный в сети интернет по адресу <a href="http://www.cafeto.ru">http://www.cafeto.ru</a>, где представлены Товары, предлагаемые для приобретения, а также условия оплаты и доставки Товаров Покупателям.
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Товар&nbsp;— материальный объект купли-продажи, не изъятый из гражданского оборота, исчерпывающая информация о котором размещена на Сайте, доступный для заказа Пользователем.
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Заказ — оформленный запрос Покупателя на приобретение и доставку по указанному Покупателем адресу / посредством самовывоза Товаров, выбранных на Сайте.
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Личный кабинет&nbsp;— персональная страница Пользователя, зарегистрированного в соответствии с Пользовательским соглашением. С помощью Личного кабинета Пользователь получает доступ к истории и состоянию своих Заказов, возможность проверить текущее состояние Заказа, просмотреть или изменить личную информацию, а также подписаться на новости, рекламные и другие информационные рассылки.
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Менеджер&nbsp;— сотрудник, уполномоченный совершать действия от имени ООО «КАФЕТО».
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 Контент&nbsp;Сайта (далее —&nbsp;«Контент») — любые сведения, размещенные на Сайте, в том числе, в текстовом, графическом, фото-, видеоформате, об ООО «КАФЕТО» о товарах, предлагаемых к розничной или оптовой продаже дистанционным способом, их свойствах, условиях заказа, продажи, покупки, иная информация, предусмотренная Правилами продажи товаров дистанционным способом, утв. Постановлением Правительства РФ от 27.09.2007 № 612.
	</p>
 </li>
	<li><span style="text-align: justify;">Публичная оферта&nbsp;—&nbsp;содержащее все существенные условия договора предложение, из которого усматривается воля лица, делающего предложение, заключить Соглашение на указанных в предложении условиях с любым, кто отзовется.</span><br>
 </li>
</ul>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Акцепт&nbsp;— конкретные действия Пользователя по выполнению условий, изложенных в Соглашении, указывающие на полное и безоговорочное принятие Соглашения.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1. Общие положения
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.1. Продавец осуществляет продажу Товаров через Интернет-магазин по адресу <a href="http://www.cafeto.ru/">http://www.cafeto.ru</a>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.2. CAFETO предоставляет Пользователю возможность:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>
	<p style="text-align: justify;">
		 пользоваться Контентом на бесплатной основе, с правом его просмотра и скачивания;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 регистрироваться и авторизоваться на Сайте;
	</p>
 </li>
	<li><span style="text-align: justify;">получать доступ к&nbsp;средствам поиска и&nbsp;навигации Сайта;</span></li>
	<li>
	<p style="text-align: justify;">
		 размещать сообщения, комментарии,&nbsp;отзывы и фотоматериалы;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 получить доступ к&nbsp;информации о&nbsp;Товаре, его свойствах, условиях приобретения;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 выбрать и&nbsp;заказать Товар, размещенный на Сайте;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 выбрать и&nbsp;заказать Товар из&nbsp;каталогов;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 заказать доставку Товара;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 оплатить Заказ через Сайт, либо иным доступным Пользователю способом;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 создать Личный кабинет;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 получать информацию об акциях при продаже Товара;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 получать новостные и рекламные рассылки;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 обращаться за помощью в службу клиентской и технической поддержки Сайта;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 оформлять подписку на обновление информации;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 использовать иной функционал Сайта.
	</p>
 </li>
</ul>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.3. Заказывая Товары через Интернет-магазин, Пользователь соглашается с условиями продажи Товаров, изложенными ниже (далее — Условия продажи товаров). В случае несогласия с настоящим Пользовательским соглашением (далее — Соглашение /Публичная оферта) Пользователь обязан немедленно прекратить использование сервиса и покинуть сайт <a href="http://www.cafeto.ru">http://www.cafeto.ru</a>.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.4. Настоящие Условия продажи товаров, а также информация о Товаре, представленная на Сайте, являются публичной офертой в соответствии со ст.435 и п.2 ст.437 Гражданского кодекса Российской Федерации.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.5. Соглашение может быть изменено Продавцом в одностороннем порядке без уведомления Пользователя/Покупателя. Новая редакция Соглашения вступает в силу по истечении 10 (Десяти) календарных дней с момента ее опубликования на Сайте, если иное не предусмотрено условиями настоящего Соглашения.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.6. Публичная оферта признается акцептованной Посетителем Сайта / Покупателем с момента регистрации Посетителя на Сайте, оформления Покупателем Заказа без авторизации на Сайте, а также с момента принятия от Покупателя Заказа по телефону +7 (800) 600-71-41.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.7. Договор розничной купли-продажи считается заключенным с момента выдачи Продавцом Покупателю кассового или товарного чека либо иного документа, подтверждающего оплату товара. Сообщая Продавцу свой e-mail и номер телефона, Посетитель Сайта/Пользователь/Покупатель дает согласие на использование указанных средств связи Продавцом, а также третьими лицами, привлекаемыми им для целей выполнения обязательств перед Посетителями Сайта/Пользователями/Покупателями, в целях осуществления рассылок рекламного и информационного характера, содержащих информацию о скидках, предстоящих и действующих акциях и других мероприятиях Продавца, о передаче заказа в доставку, а также иную информацию, непосредственно связанную с выполнением обязательств Покупателем в рамках настоящей Публичной оферты.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.8. Осуществляя Заказ, Пользователь/Покупатель соглашается с тем, что Продавец может поручить исполнение Договора третьему лицу, при этом оставаясь ответственным за его исполнение.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1.9. В рамках Заказа CAFETO предоставляет Пользователю информационное сопровождение заключенного Пользователем с Продавцом Договора.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2. Предмет соглашения
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2.1. Предметом настоящего Соглашения является предоставление возможности Пользователю приобретать для личных, семейных, домашних и иных нужд, не связанных с осуществлением предпринимательской деятельности, Товары, представленные в каталоге Интернет-магазина по адресу <a href="http://www.cafeto.ru">http://www.cafeto.ru</a>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2.2. Данное Соглашение распространяется на все виды Товаров и услуг, представленных на Сайте, пока такие предложения с описанием присутствуют в каталоге Интернет-магазина.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2.3. Пользователь, приступая к использованию Сайта, в том числе, к любой из его функциональных возможностей, указанных в п.1.2 настоящего Соглашения, подтверждает, что он:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2.3.1. Ознакомился с условиями настоящего Соглашения в полном объеме, до начала использования Сайта;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2.3.2. Принимает все условия настоящего Соглашения, без каких-либо изъятий, исключений и оговорок с его стороны, обязуется беспрекословно соблюдать их, или, в противном случае, прекратить использование Сайта;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2.3.3. Предоставил точную и актуальную информацию о себе при заполнении полей форм обратной связи, Регистрации и Авторизации.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3. Регистрация на сайте
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.1. Регистрация на Сайте осуществляется с помощью всплывающего окна «Регистрация».
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.2. Регистрация на Сайте не является обязательной для оформления Заказа.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.3. CAFETO не несет ответственности за точность и правильность информации, предоставляемой Пользователем при регистрации.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.4. Пользователь обязуется не сообщать третьим лицам логин и пароль, указанные Пользователем при регистрации. В случае возникновения у Пользователя подозрений относительно безопасности его логина и пароля или возможности их несанкционированного использования третьими лицами, Пользователь обязуется незамедлительно уведомить об этом CAFETO, направив соответствующее электронное письмо по адресу: <a href="mailto:info@cafeto.ru">info@cafeto.ru</a>.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.5. Общение Пользователя/Покупателя с операторами Call-центра / менеджерами и иными представителями CAFETO и должно строиться на принципах общепринятой морали и коммуникационного этикета. Строго запрещено использование нецензурных слов, брани, оскорбительных выражений, а также угроз и шантажа, в независимости от того, в каком виде и кому они были адресованы.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.6. Персональные данные Пользователя обрабатываются CAFETO в соответствии с Политикой конфиденциальности, размещенной в свободном доступе на Сайте.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.7. Пользователь немедленно уведомляет CAFETO о&nbsp;любом случае неавторизованного (неразрешенного Пользователем) доступа к своему Личному кабинету, под его логином и&nbsp;паролем, а также, о любом другом нарушении безопасности,&nbsp;наличии неисправностей в&nbsp;системе защиты Сайта.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3.8. Если Пользователем не доказано обратное, любые действия, совершенные с использованием его логина и пароля, считаются совершенными этим Пользователем.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4. Товар и порядок совершения покупки
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.1. В случае отсутствия заказанных Покупателем Товаров на складе Продавца, последний вправе исключить указанный Товар из Заказа / отменить Заказ Покупателя, уведомив об этом Покупателя путем направления соответствующего электронного сообщения по адресу, указанному Покупателем при регистрации (либо звонком менеджера CAFETO).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.2. В случае отмены предоплаченного Заказа стоимость Товара возвращается Продавцом Покупателю способом, которым Товар был оплачен.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.3. Покупатель несет полную ответственность за предоставление неверных сведений, повлекшее за собой невозможность надлежащего исполнения Продавцом своих обязательств перед Покупателем.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.4. После оформления Заказа на Сайте Покупателю предоставляется информация о предполагаемой дате доставки путем направления электронного сообщения по адресу, указанному Покупателем при регистрации, или по телефону. Менеджер, обслуживающий данный Заказ, уточняет детали Заказа, согласовывает дату доставки, наличия заказанных Товаров на складе Продавца и времени, необходимого для обработки и доставки Заказа.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.5. Ожидаемая дата передачи Заказа в Службу доставки сообщается Покупателю менеджером, обслуживающим Заказ, по электронной почте или при контрольном звонке Покупателю. Дата передачи Товара может быть изменена Продавцом в одностороннем порядке в случае наличия объективных, по мнению Продавца, причин.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.6. Товар имитируют фотообразцы, размещенные на Сайте. Реальный вид Товара может не совпадать с изображением, представленным на Сайте. Каждый фотообразец сопровождается текстовой информацией: артикулом, ценой и описанием Товара.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.7. Цена Товара, указанная на Сайте, может быть изменена CAFETO в одностороннем порядке, до момента оплаты Товара Пользователем.&nbsp;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 4.8. Полная стоимость Заказа состоит из каталожной цены Товара, которая формируется из итоговой стоимости Товара и стоимости доставки.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5. Доставка заказа
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.1. Способы, а также примерные сроки доставки реализуемых Товаров, указаны на Сайте в разделе «Доставка и оплата» по адресу <a href="https://cafeto.ru/delivery_and_payment/">https://cafeto.ru/delivery_and_payment/</a>. Конкретные сроки доставки могут быть согласованы Покупателем менеджером при подтверждении заказа.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.2. Территория доставки Товаров, представленных на Сайте, ограничена пределами Российской Федерации.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.3. Задержки в доставке возможны ввиду непредвиденных обстоятельств, произошедших не по вине Продавца.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.4. При доставке Заказ вручается Покупателю либо третьему лицу, указанному в Заказе в качестве получателя (далее Покупатель и третье лицо именуются «Получатель»). При невозможности получения Заказа, оплаченного посредством наличного расчета, указанными выше лицами, Заказ может быть вручен лицу, который может предоставить сведения о Заказе (номер отправления и/или ФИО Получателя), а также оплатить стоимость Заказа в полном объеме лицу, осуществляющему доставку Заказа.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.5. Во избежание случаев мошенничества, а также для выполнения взятых на себя обязательств, указанных в пункте 5. настоящего Соглашения, при вручении предоплаченного Заказа лицо, осуществляющее доставку Заказа, вправе затребовать документ, удостоверяющий личность Получателя, а также указать тип и номер предоставленного Получателем документа на квитанции к Заказу. Продавец гарантирует конфиденциальность и защиту персональных данных Получателя (п.9.3.).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.6. Обязанность Продавца передать товар Покупателю считается исполненной в момент вручения курьером Товара Получателю или получения Товара Получателем в отделении почтовой связи либо в заранее оговоренном месте выдачи Заказа (в т.ч. в пункте самовывоза).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.7. При принятии Заказа от курьера, Получатель обязан осмотреть доставленный Товар и проверить его на соответствие заявленному количеству, ассортименту и комплектности Товара, а также проверить срок службы доставленного Товара и целостность упаковки. В случае отсутствия претензий к доставленному Товару Получатель расписывается в документе, предоставляемом курьером, и оплачивает Заказ (в отсутствие 100%-ной предоплаты). Подпись в доставочных документах свидетельствует о том, что претензий к Товару Получателем не заявлено и Продавец полностью и надлежащим образом выполнил свою обязанность по передаче Товара.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.8. Товар, представленный на Сайте, по качеству и упаковке соответствует ГОСТу и ТУ, что подтверждается соответствующими документами (сертификатами и т.д.).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 5.9. Пользователь понимает и соглашается с тем, что: осуществление доставки — отдельная услуга, не являющаяся неотъемлемой частью приобретаемого Покупателем Товара, выполнение которой заканчивается в момент получения Получателем Товара и осуществления платежа за него. Претензии к качеству приобретенного Товара, возникшие после получения и оплаты Товара, рассматриваются в соответствии с Законом РФ «О защите прав потребителей».
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6. Оплата товара
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6.1. Цена товара, реализуемого в Интернет-магазине, указывается в рублях Российской Федерации.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6.2. Цена Товара на Сайте может быть изменена Продавцом в одностороннем порядке. При этом цена на заказанный Покупателем Товар изменению не подлежит. Цена Товара может дифференцироваться по регионам.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6.3. Особенности оплаты Товара с помощью банковских карт:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6.3.1. В соответствии с положением ЦБ РФ «Об эмиссии банковских карт и об операциях, совершаемых с использованием платежных карт» от 24.12.2004 № 266-П операции по банковским картам совершаются держателем карты либо уполномоченным им лицом.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6.3.2. Авторизация операций по банковским картам осуществляется банком. Если у банка есть основания полагать, что операция носит мошеннический характер, то банк вправе отказать в осуществлении данной операции. Мошеннические операции с банковскими картами попадают под действие статьи 159 УК РФ.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 6.3.3. Во избежание случаев различного рода неправомерного использования банковских карт при оплате все Заказы, оформленные на Сайте и предоплаченные банковской картой, проверяются Продавцом. В целях проверки личности владельца и его правомочности на использование карты Продавец вправе потребовать от Покупателя, оформившего такой заказ, предъявления документа, удостоверяющего личность.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7. Возврат товара и денежных средств
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.1. Возврат Товара, реализуемого CAFETO, осуществляется в соответствии с «Условиями возврата», указанными на Сайте по адресу <a href="https://cafeto.ru/delivery_and_payment/">https://cafeto.ru/delivery_and_payment/</a>.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.2. Возврат Товара надлежащего качества.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.2.1. При отказе Покупателя от Товара Продавец возвращает ему стоимость возвращенного Товара, за исключением расходов Продавца, связанных с доставкой возвращенного Покупателем Товара, в течение 10 дней с даты поступления возвращенного Товара на склад Продавца вместе с заполненным Покупателем заявлением на возврат.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.2.2. Если на момент обращения Покупателя аналогичный товар отсутствует в продаже у Продавца, Покупатель вправе отказаться от исполнения настоящего Соглашения и потребовать возврата уплаченной за указанный Товар денежной суммы. Продавец обязан вернуть уплаченную за возвращенный товар денежную сумму в течение 10 дней со дня возврата Товара.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.3. Возврат Товара ненадлежащего качества:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.3.1. Под товаром ненадлежащего качества подразумевается товар, который неисправен и не может обеспечить исполнение своих функциональных качеств. Полученный Товар должен соответствовать описанию на Сайте.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.3.2. Внешний вид и комплектность Товара, а также комплектность всего Заказа должны быть проверены Получателем в момент доставки Товара.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.3.3. При доставке Товара Покупатель ставит свою подпись в квитанции о доставке в графе: «Заказ принял, претензий к количеству и качеству товара не имею».
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.3.4. Если Покупателю был передан Товар ненадлежащего качества и оное не было заранее оговорено Продавцом, Покупатель вправе воспользоваться положениями ст.18 «Права потребителя при обнаружении в товаре недостатков» Закона о защите прав потребителей.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.3.5. Требования о возврате уплаченной за товар денежной суммы подлежат удовлетворению в течение 10 дней со дня предъявления соответствующего требования (ст. 22 Закона РФ «О защите прав потребителей»).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 7.4. Возврат денежных средств осуществляется посредством возврата стоимости оплаченного Товара на банковскую карту или почтовым переводом. Способ должен быть указан в соответствующем поле заявления на возврат Товара.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 8. Ответственность
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 8.1. Продавец не несет ответственности за ущерб, причиненный Покупателю вследствие ненадлежащего использования Товаров, приобретенных в Интернет-магазине.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 8.2. Продавец не несет ответственности за содержание и функционирование внешних сайтов.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9. Конфиденциальность и защита информации
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.1. Персональные данные Пользователя/Покупателя обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.2. При регистрации на Сайте Пользователь предоставляет следующую информацию: Фамилия, Имя, Отчество, контактный номер телефона, адрес электронной почты, адрес доставки товара.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.3. Предоставляя свои персональные данные Продавцу, Посетитель Сайта/Пользователь/Покупатель соглашается на их обработку Продавцом, в том числе в целях выполнения Продавцом обязательств перед Посетителем Сайта/Пользователем/Покупателем в рамках настоящей Публичной оферты. Также, персональные данные могут быть использованы Продавцом для продвижения товаров и услуг, проведения электронных и sms опросов, контроля результатов маркетинговых акций, клиентской поддержки, организации доставки товара Покупателям, проведение розыгрышей призов среди Посетителей Сайта/Пользователей/ Покупателей, контроля удовлетворенности Посетителя Сайта/Пользователя/Покупателя, качества услуг, оказываемых Продавцом.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.4. Под обработкой персональных данных понимается любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение) извлечение, использование, обезличивание, блокирование, удаление, уничтожение персональных данных.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.5. Продавец имеет право отправлять информационные, в том числе рекламные сообщения, на электронную почту и мобильный телефон Пользователя/Покупателя с его согласия, выраженного посредством совершения им действий, однозначно идентифицирующих этого абонента и позволяющих достоверно установить его волеизъявление на получение сообщения. Пользователь/Покупатель вправе отказаться от получения рекламной и другой информации без объяснения причин отказа путем информирования CAFETO о своем отказе по телефону +7 (800) 600-71-41 либо посредством направления соответствующего заявления на электронный адрес Продавца: <a href="mailto:info@cafeto.ru">info@cafeto.ru</a>. Сервисные сообщения, информирующие Пользователя/Покупателя о заказе и этапах его обработки, отправляются автоматически и не могут быть отклонены Пользователем/Покупателем.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.6. Продавец вправе использовать технологию «cookies». «Cookies» не содержат конфиденциальную информацию. Посетитель / Пользователь / Покупатель настоящим дает согласие на сбор, анализ и использование cookies, в том числе третьими лицами для целей формирования статистики и оптимизации рекламных сообщений.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.7. Продавец получает информацию об ip-адресе посетителя Сайта <a href="http://www.cafeto.ru">www.cafeto.ru</a>. Данная информация не используется для установления личности посетителя.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 9.8. Продавец вправе осуществлять записи телефонных разговоров с Пользователем/Покупателем. При этом Продавец обязуется: предотвращать попытки несанкционированного доступа к информации, полученной в ходе телефонных переговоров, и/или передачу ее третьим лицам, не имеющим непосредственного отношения к исполнению Заказов, в соответствии с п. 4 ст. 16 Федерального закона «Об информации, информационных технологиях и о защите информации».
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10. Заключительные положения
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10.1. Настоящая Публичная оферта вступает в силу с момента ее акцепта Посетителем Сайта/Покупателем, и действует до момента отзыва акцепта Публичной оферты.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10.2. Настоящее Соглашение, взаимоотношения CAFETO и Пользователя регулируются и толкуются в соответствии с действующим законодательством РФ. Споры, неурегулированные настоящим Соглашением, разрешаются путем проведения переговоров между CAFETO и Пользователем, либо в соответствии с законодательством РФ.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10.3. Настоящее Соглашение заключается на неопределенный срок, распространяет свое действие на всех Пользователей, осуществляющих доступ Контенту (в том числе, статьям, описанию Товаров, постам, фото-, аудио-, видеоматериалам), функциональным возможностям Сайта и их использованию, как до, так и после даты размещения Соглашения на Сайте.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10.4. При обращении с претензией к CAFETO, Пользователь должен представить письменный вариант претензии и документы, подтверждающие ее обоснованность.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10.5. Действующая редакция настоящего Соглашения размещена в свободном доступе на Сайте.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 10.6. Настоящее Соглашение изменяется или прекращается CAFETO в одностороннем порядке без предварительного уведомления Пользователя и без выплаты ему какой-либо компенсации. Новая редакция настоящего Соглашения вступает в силу с момента ее размещения на Сайте, либо с момента доведения ее содержания до сведения Пользователя в другой удобной форме, если иное не предусмотрено новой редакцией настоящего Соглашения. Пользователь обязан регулярно отслеживать изменения в настоящем Соглашении.
</p><? require( $_SERVER[ "DOCUMENT_ROOT" ] . "/bitrix/footer.php" ); ?>
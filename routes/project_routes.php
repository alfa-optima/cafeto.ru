<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use AOptima\ProjectCafeto as project;
use AOptima\ToolsCafeto as tools;

if(
    \Bitrix\Main\Loader::includeModule('aoptima.projectcafeto')
    &&
    \Bitrix\Main\Loader::includeModule('aoptima.toolscafeto')
){

    $arURI = tools\funcs::arURI(tools\funcs::pureURL());

    $catalog_1_level_sections = project\catalog::first_level_sections();
    
    // Каталог
    if( $catalog_1_level_sections[$arURI[1]] ){

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/catalog/catalog.php" );

    } else if( $arURI[1] == 'brewing' ){

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/content/brewing.php" );

    } else {

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/local/templates/main/include/404include.php" );
    }

}
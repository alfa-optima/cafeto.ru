<? require( $_SERVER[ "DOCUMENT_ROOT" ] . "/bitrix/header.php" );

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$arURI = tools\funcs::arURI(tools\funcs::pureURL());

if( strlen($arURI[3]) > 0 ){

    include( $_SERVER[ "DOCUMENT_ROOT" ] . "/local/templates/main/include/404include.php" );

} else if( strlen($arURI[2]) > 0 ){

    // brewing_detail
    $APPLICATION->IncludeComponent(
        "aoptima:brewing_detail", "",
        ['code' => $arURI[2]]
    );

} else {

    // brewing_list
    $APPLICATION->IncludeComponent(
        "aoptima:brewing_list", ""
    );

}


require( $_SERVER[ "DOCUMENT_ROOT" ] . "/bitrix/footer.php" );
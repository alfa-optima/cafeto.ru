<?php

use Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();

if( \Bitrix\Main\Loader::includeModule('aoptima.projectcafeto') ){

    EventManager::getInstance()->addEventHandler(
        "catalog",
        "\Bitrix\Catalog\Price::OnAfterAdd",
        array(
            "AOptima\\ProjectCafeto\\handlers",
            "OnPriceAdd"
        )
    );
    EventManager::getInstance()->addEventHandler(
        "catalog",
        "\Bitrix\Catalog\Price::OnAfterUpdate",
        array(
            "AOptima\\ProjectCafeto\\handlers",
            "OnPriceUpdate"
        )
    );
    EventManager::getInstance()->addEventHandler(
        "catalog",
        "\Bitrix\Catalog\Price::OnBeforeDelete",
        array(
            "AOptima\\ProjectCafeto\\handlers",
            "OnBeforePriceDelete"
        )
    );

}


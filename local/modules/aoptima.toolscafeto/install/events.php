<?php

// Массив событий модуля.

 
$MODULE_EVENTS = array(



    array(
        'iblock',
        'OnAfterIBlockElementAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockElementDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockElementDelete',
        100,
    ),
	
	
    array(
        'iblock',
        'OnAfterIBlockSectionAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockSectionDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockSectionDelete',
        100,
    ),
	




    array(
        'iblock',
        'OnAfterIBlockAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnAfterIBlockUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnIBlockDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnIBlockDelete',
        100
    ),

    array(
        'iblock',
        'OnBeforeIBlockAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeIBlockDelete',
        100
    ),



	
	
    array(
        'main',
        'OnAfterUserAdd',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterUserAdd',
        100,
    ),
    array(
        'main',
        'OnAfterUserUpdate',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnAfterUserUpdate',
        100,
    ),
    array(
        'main',
        'OnBeforeUserDelete',
        'aoptima.toolscafeto',
        '\AOptima\ToolsCafeto\handlers',
        'OnBeforeUserDelete',
        100,
    ),








	
	
);


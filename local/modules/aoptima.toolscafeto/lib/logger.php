<? namespace AOptima\ToolsCafeto;
use AOptima\ToolsCafeto as tools;



class logger {


    const LOG_PATH = '/local/php_interface/aoptima_log/';





    static function addLog($message){

        $log_path = static::LOG_PATH;
        $file_name = 'logs.log';

        if( !file_exists($_SERVER['DOCUMENT_ROOT'].$log_path) ){
            mkdir($_SERVER['DOCUMENT_ROOT'].$log_path, 0700);
        }
        $message = '['.date("Y-m-d H:i:s").'] '.$message."\n";
        $file_path = $_SERVER["DOCUMENT_ROOT"].$log_path.$file_name;

        $f = fopen($file_path, "a");
        fwrite($f, $message);
        fclose($f);

        // Если превышает 2 МБ
        if( filesize($file_path)/1024/1024 > 2  ){

            // Архивируем файл
            $time = time();
            $tmpPath = $_SERVER["DOCUMENT_ROOT"].$log_path."logs_".$time."/";
            if( mkdir($tmpPath, 0700) ) {
                copy($file_path, $tmpPath.$file_name);
                $arPackFiles[] = $tmpPath.$file_name;
            }
            $packarc = \CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"].$log_path."/logs_".$time.".zip");
            $pRes = $packarc->Pack($arPackFiles);

            // Удаляем что уже не нужно
            unlink($tmpPath.$file_name);
            unlink($file_path);
            rmdir($tmpPath);

        }

    }


	
	static function addError($message){

        $log_path = static::LOG_PATH;
		$file_name = 'errors.log';

		if( !file_exists($_SERVER['DOCUMENT_ROOT'].$log_path) ){
			mkdir($_SERVER['DOCUMENT_ROOT'].$log_path, 0700);
		}
		$message = '['.date("Y-m-d H:i:s").'] '.$message."\n";
		$file_path = $_SERVER["DOCUMENT_ROOT"].$log_path.$file_name;

		$f = fopen($file_path, "a");
		fwrite($f, $message);
		fclose($f);
		
		// Если превышает 2 МБ
		if( filesize($file_path)/1024/1024 > 2  ){

			// Архивируем файл
			$time = time();
			$tmpPath = $_SERVER["DOCUMENT_ROOT"].$log_path."logs_".$time."/";
			if( mkdir($tmpPath, 0700) ) {
				copy($file_path, $tmpPath.$file_name);
				$arPackFiles[] = $tmpPath.$file_name;
			}
			$packarc = \CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"].$log_path."/logs_".$time.".zip");
			$pRes = $packarc->Pack($arPackFiles);
			
			// Удаляем что уже не нужно
			unlink($tmpPath.$file_name);
			unlink($file_path);
			rmdir($tmpPath);
			
		}
		
	}


	
}
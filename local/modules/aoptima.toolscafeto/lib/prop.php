<? namespace AOptima\ToolsCafeto;
use AOptima\ToolsCafeto as tools;



class prop {
	

	
	// Код свойства по ID
	static function get_code( $prop_id ){
		\Bitrix\Main\Loader::includeModule('iblock');
		$prop_res = \CIBlockProperty::GetByID( $prop_id );
		if($el_prop = $prop_res->GetNext()){
			$prop_code = $el_prop['CODE'];
			return $prop_code;
		}
		return false;
	}




    static function getList( $iblock_id ){
		\Bitrix\Main\Loader::includeModule('iblock');
	    $list = [];
        $properties = \CIBlockProperty::GetList(
            array("sort"=>"asc", "name"=>"asc"),
            array(
                "IBLOCK_ID" => $iblock_id
            )
        );
        while ($prop = $properties->GetNext()){
            $list[$prop['ID']] = $prop;
        }
        return $list;
    }



	
}
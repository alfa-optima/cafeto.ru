<?php


namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;


use Bitrix\Main\Entity;

class SeopageTable extends Entity\DataManager {

    public static function getTableName(){
        return 'aoptima_seo_pages';
    }

    public static function getMap(){
        return array(
            new Entity\IntegerField( 'ID', array(
                'primary' => true,
                'autocomplete' => true,
            )),
            new Entity\StringField( 'NAME', array(
                'required' => true
            )),
            new Entity\StringField( 'FILTER_URL', array(
                'required' => true,
            )),
            new Entity\StringField( 'H1' ),
            new Entity\StringField( 'TITLE' ),
            new Entity\StringField( 'DESCRIPTION' ),
            new Entity\StringField( 'KEYWORDS' ),
            new Entity\TextField( 'TEXT' ),
        );
    }


    public static function checkTable(){
        $addTableSQL = static::getEntity()->compileDbTableStructureDump();
        if( is_array($addTableSQL) ){
            foreach ( $addTableSQL as $key => $sql ){
                $sql = str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $sql);
                global $DB;
                $res = $DB->Query( $sql );
            }
        }
    }



    public static function addItem( $fields ){
        $result = static::add($fields);
        if ($result->isSuccess()){
            $id = $result->getId();
            return $id;
        }
        return false;
    }


    public static function updateItem( $id, $fields ){
        $result = static::update( $id, $fields );
        return $result;
    }



    public static function getByCode( $code ){
        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ '*' ],
            'filter' => [ 'CODE' => $code ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            return $element;
        }
        return false;
    }


    public static function getByID( $id ){
        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ '*' ],
            'filter' => [ 'ID' => $id ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            return $element;
        }
        return false;
    }


    public static function getByFilterPath( $section, $smart_filter_path ){
        // Ищём запись по символьному коду
        $all_list = static::allList();
        foreach ( $all_list as $key => $seo_page ){
            if(
                substr_count(
                    $seo_page['FILTER_URL'],
                    $section['SECTION_PAGE_URL']."filter/".$smart_filter_path."/apply/"
                )
            ){   return static::getByID($seo_page['ID']);   }
        }
        return false;
    }



    public static function allList(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_seo_pages';
        $cache_path = '/all_seo_pages/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $seo_pagesss = project\SeopageTable::getList([
                'select' => [ 'ID', 'FILTER_URL', 'NAME' ],
                'order' => array('ID' => 'DESC'),
            ]);
            while( $seo_page = $seo_pagesss->fetch() ){
                $list[] = [
                    'ID' => $seo_page['ID'],
                    'NAME' => $seo_page['NAME'],
                    'FILTER_URL' => $seo_page['FILTER_URL']
                ];
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




}
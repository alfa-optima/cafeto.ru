<? namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;



class handlers {



    static function OnAfterIBlockAdd( $arFields ){}
    static function OnAfterIBlockUpdate( $arFields ){}
    static function OnIBlockDelete( $ID ){}


    static function OnBeforeIBlockAdd( &$arFields ){}
    static function OnBeforeIBlockUpdate( &$arFields ){}





    static function OnAfterIBlockElementAdd( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){
            static::setSortPrice( $arFields['ID'] );
            static::clearElCaches();
        }
        // методы заваривания
        if( $arFields['IBLOCK_ID'] == project\catalog::METHODS_IBLOCK_ID ){
            BXClearCache(true, "/all_methods/");
        }
        // регионы
        if( $arFields['IBLOCK_ID'] == project\catalog::REGIONS_IBLOCK_ID ){
            BXClearCache(true, "/all_regions/");
        }
        // Методы заваривания
        if( $arFields['IBLOCK_ID'] == project\brewing::IBLOCK_ID ){
            BXClearCache(true, "/brewing_method_steps/");
        }
    }
    static function OnAfterIBlockElementUpdate( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){
            static::setSortPrice( $arFields['ID'] );
            static::clearElCaches();
        }
        // методы заваривания
        if( $arFields['IBLOCK_ID'] == project\catalog::METHODS_IBLOCK_ID ){
            BXClearCache(true, "/all_methods/");
        }
        // регионы
        if( $arFields['IBLOCK_ID'] == project\catalog::REGIONS_IBLOCK_ID ){
            BXClearCache(true, "/all_regions/");
        }
        // Методы заваривания
        if( $arFields['IBLOCK_ID'] == project\brewing::IBLOCK_ID ){
            BXClearCache(true, "/brewing_method_steps/");
        }
    }
    static function OnAfterIBlockElementDelete(){}

    static function OnBeforeIBlockElementAdd( &$arFields ){}
    static function OnBeforeIBlockElementUpdate( &$arFields ){}
    static function OnBeforeIBlockElementDelete( $ID ){
        $iblock_id = tools\el::getIblock($ID);
        // методы заваривания
        if( $iblock_id == project\catalog::METHODS_IBLOCK_ID ){
            BXClearCache(true, "/all_methods/");
        }
        // регионы
        if( $iblock_id == project\catalog::REGIONS_IBLOCK_ID ){
            BXClearCache(true, "/all_regions/");
        }
        // Методы заваривания
        if( $iblock_id == project\brewing::IBLOCK_ID ){
            BXClearCache(true, "/brewing_method_steps/");
        }
    }






    static function OnAfterIBlockSectionAdd( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){
            BXClearCache(true, "/catalog_1_level_sections/");
        }
        // Верх.меню
        if( $arFields['IBLOCK_ID'] == project\top_menu::IBLOCK_ID ){
            BXClearCache(true, "/all_top_menu_items/");
        }
        // Блог
        if( $arFields['IBLOCK_ID'] == project\blog::IBLOCK_ID ){
            BXClearCache(true, "/blog_sections/");
        }
        // Методы заваривания
        if( $arFields['IBLOCK_ID'] == project\brewing::IBLOCK_ID ){
            BXClearCache(true, "/brewing_sections/");
        }
    }
    static function OnAfterIBlockSectionUpdate( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){
            BXClearCache(true, "/catalog_1_level_sections/");
        }
        // Верх.меню
        if( $arFields['IBLOCK_ID'] == project\top_menu::IBLOCK_ID ){
            BXClearCache(true, "/all_top_menu_items/");
        }
        // Блог
        if( $arFields['IBLOCK_ID'] == project\blog::IBLOCK_ID ){
            BXClearCache(true, "/blog_sections/");
        }
        // Методы заваривания
        if( $arFields['IBLOCK_ID'] == project\brewing::IBLOCK_ID ){
            BXClearCache(true, "/brewing_sections/");
        }
    }
    static function OnAfterIBlockSectionDelete(){}


    static function OnBeforeIBlockSectionAdd( &$arFields ){}
    static function OnBeforeIBlockSectionUpdate( &$arFields ){}
    static function OnBeforeIBlockSectionDelete( $ID ){
        $iblock_id = tools\section::getIblock($ID);
        // Каталог
        if( $iblock_id == project\catalog::IBLOCK_ID ){
            BXClearCache(true, "/catalog_1_level_sections/");
        }
        // Верх.меню
        if( $iblock_id == project\top_menu::IBLOCK_ID ){
            BXClearCache(true, "/all_top_menu_items/");
        }
        // Блог
        if( $iblock_id == project\blog::IBLOCK_ID ){
            BXClearCache(true, "/blog_sections/");
        }
        // Методы заваривания
        if( $iblock_id == project\brewing::IBLOCK_ID ){
            BXClearCache(true, "/brewing_sections/");
        }
    }




    static function OnSuccessCatalogImport1C( $arParams, $filePath ){}





    static function OnAfterUserAdd(&$arFields){}
    static function OnAfterUserUpdate(&$arFields){}
    static function OnBeforeUserDelete($ID){
        if( $ID == project\order::SERVICE_USER_ID ){
            global $APPLICATION;
            $APPLICATION->throwException("Анонимный пользователь не может быть удалён");
            return false;
        }
    }





    static function clearElCaches(){
        $cache_name = \AOptima\ToolsCafeto\el::EL_CACHE_NAME;
        BXClearCache(true, "/".$cache_name."/");
        BXClearCache(true, "/".$cache_name."_by_code/");
    }
    static function setSortPrice( $product_id ){
        $min_price = false;   $offers_cnt = 0;
        $filter = Array(
            "IBLOCK_ID" => project\catalog::TP_IBLOCK_ID,
            "ACTIVE" => "Y",  "PROPERTY_CML2_LINK" => $product_id
        );
        $fields = Array( "ID", "PROPERTY_CML2_LINK", "CATALOG_GROUP_".project\catalog::PRICE_ID );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $offers_cnt++;
            if(
                $element["CATALOG_PRICE_".project\catalog::PRICE_ID]
                &&
                (
                    !$min_price
                    ||
                    $element["CATALOG_PRICE_".project\catalog::PRICE_ID] < $min_price
                )
            ){    $min_price = $element["CATALOG_PRICE_".project\catalog::PRICE_ID];    }
        }
        if( $offers_cnt == 0 ){
            $filter = Array(
                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                "ACTIVE" => "Y",  "ID" => $product_id
            );
            $fields = Array( "ID", "CATALOG_GROUP_".project\catalog::PRICE_ID );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
            );
            if ( $element = $dbElements->GetNext() ){
                if( $element["CATALOG_PRICE_".project\catalog::PRICE_ID] ){
                    $min_price = $element["CATALOG_PRICE_".project\catalog::PRICE_ID];
                }
            }
        }
        if( $min_price ){
            $set_prop = array( "SORT_PRICE" => $min_price );
            \CIBlockElement::SetPropertyValuesEx($product_id, project\catalog::IBLOCK_ID, $set_prop);
        }
    }
    static function OnPriceAdd( $event ){
        $params = $event->getParameters();
        $fields = $params['fields'];
        $arPrice = \CPrice::GetByID( $params['id']['ID'] );
        static::setSortPrice( $arPrice['PRODUCT_ID'] );
        static::clearElCaches();
    }
    static function OnPriceUpdate( $event ){
        $params = $event->getParameters();
        $fields = $params['fields'];
        $arPrice = \CPrice::GetByID( $params['id']['ID'] );
        static::setSortPrice( $arPrice['PRODUCT_ID'] );
        static::clearElCaches();
    }
    static function OnBeforePriceDelete( $event ){
        $params = $event->getParameters();
        $fields = $params['fields'];
        $arPrice = \CPrice::GetByID( $params['id']['ID'] );
        static::setSortPrice( $arPrice['PRODUCT_ID'] );
        static::clearElCaches();
    }





}
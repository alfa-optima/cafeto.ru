<? namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,    
Bitrix\Main\Application,
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,
Bitrix\Sale\Basket, 
\Bitrix\Sale\Discount,    
\Bitrix\Sale\Result,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context,
Bitrix\Main\Web\Json,
Bitrix\Sale\PersonType,
Bitrix\Sale\Shipment,
Bitrix\Sale\Payment,
Bitrix\Sale\Location\LocationTable,
Bitrix\Sale\Services\Company,
Bitrix\Sale\Location\GeoIp;



class user_basket {


	const DEFAULT_DELIVERY_ID = 2;


	


	// Получение текущей корзины
	static function info( 
		$delivery_price = false,
		$ds_id = false,
		$ps_id = false,
		$person_type = 1 //
	){
		
		if( !$ds_id ){    $ds_id = static::DEFAULT_DELIVERY_ID;    }
		
		$basketUserID = Sale\Fuser::getId();
		$siteID = \Bitrix\Main\Context::getCurrent()->getSite();
		
		global $USER;
		$rounding = project\catalog::PRICE_ROUND;
		if( !$rounding ){   $rounding = 2;   }
		
		$calcInfo = array();
		$basket_cnt = 0;			$basket_sum = 0;
		$basket_disc_sum = 0;		$arBasket = array();
		
		$order = \Bitrix\Sale\Order::create( $siteID, $USER->GetID() );
		$basket = Basket::create( $siteID );

		// Получаем корзину
		$userBasket = Sale\Basket::loadItemsForFUser( $basketUserID, $siteID );
		foreach ( $userBasket as $key => $basketItem ){
			// Инфо об элементе
			$el = tools\el::info( $basketItem->getProductId() );
			if (
			    intval($el['ID']) > 0
                //////////
                &&
                project\catalog::isAvailableProduct( $el['ID'] )
                //////////
            ){
				$item = $basket->createItem( 'catalog', $el['ID'] );
				$iblocks = \CIBlock::GetByID( $el['IBLOCK_ID'] );
				if( $iblock = $iblocks->GetNext() ){
					$item->setFields([
						'QUANTITY' => $basketItem->getField('QUANTITY'),
						'CURRENCY' => 'RUB',   'LID' => 's2',
						'PRODUCT_XML_ID' => $el['XML_ID'],
						'CATALOG_XML_ID' => $iblock['XML_ID'],
						'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
					]);
				}
				$basket_cnt += $basketItem->getField('QUANTITY');
			} else {
				$basketItem->delete();
				$userBasket->save();
			}
		}
		$userBasket = Sale\Basket::loadItemsForFUser( $basketUserID, $siteID );
		$order->setBasket($basket);
		$order->setPersonTypeId($person_type);
		if( intval( $ps_id ) > 0 ){
			// Оплата
			$paymentCollection = $order->getPaymentCollection();
			$payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
		}
		// Доставка
		$shipmentCollection = $order->getShipmentCollection();
		$shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
		$shipmentItemCollection = $shipment->getShipmentItemCollection();
		foreach ( $basket as $basketItem ){
			$item = $shipmentItemCollection->createItem($basketItem);
			$item->setQuantity($basketItem->getQuantity());
		}
		$orderBasket = $order->getBasket()->createClone();
		$calcInfo['BASKET_DISC_SUM'] = round($orderBasket->getPrice(), $rounding);
		$calcInfo['BASKET_DISC_SUM_FORMAT'] = number_format($calcInfo['BASKET_DISC_SUM'], $rounding, ",", " ");
		$calcInfo['BASKET_SUM'] = round($orderBasket->getBasePrice(), $rounding);
        $calcInfo['BASKET_SUM_FORMAT'] = number_format($calcInfo['BASKET_SUM'], $rounding, ",", " ");
		$calcInfo['ORDER_DISCOUNT'] = round($order->getDiscountPrice(), $rounding);
        $calcInfo['ORDER_DISCOUNT_FORMAT'] = number_format($calcInfo['ORDER_DISCOUNT'], $rounding, ",", " ");
		$calcInfo['TOTAL_DISCOUNT'] = $order->getDiscountPrice() + $calcInfo['BASKET_SUM'] - $calcInfo['BASKET_DISC_SUM'];
        $calcInfo['TOTAL_DISCOUNT_FORMAT'] = number_format($calcInfo['TOTAL_DISCOUNT'], $rounding, ",", " ");

		if( $delivery_price ){
			$shipment->setBasePriceDelivery($delivery_price);
			$calcInfo['DELIVERY_DISC_PRICE'] = round($shipment->getPrice(), $rounding);
            $calcInfo['DELIVERY_DISC_PRICE_FORMAT'] = number_format($calcInfo['DELIVERY_DISC_PRICE'], $rounding, ",", " ");
			$calcInfo['DELIVERY_PRICE'] = round($delivery_price, $rounding);
            $calcInfo['DELIVERY_PRICE_FORMAT'] = number_format($calcInfo['DELIVERY_PRICE'], $rounding, ",", " ");
		} else {
			$calcInfo['DELIVERY_DISC_PRICE'] = round($order->getDeliveryPrice(), $rounding);
            $calcInfo['DELIVERY_DISC_PRICE_FORMAT'] = number_format($calcInfo['DELIVERY_DISC_PRICE'], $rounding, ",", " ");
			$calcInfo['DELIVERY_PRICE'] = round($order->getDeliveryPrice(), $rounding);
            $calcInfo['DELIVERY_PRICE_FORMAT'] = number_format($calcInfo['DELIVERY_PRICE'], $rounding, ",", " ");
		}

		$calcInfo['ORDER_DISCOUNT_DATA'] = $order->getDiscount()->getApplyResult();

		foreach ($orderBasket as $key => $basketItem){
			$el = tools\el::info( $basketItem->getField('PRODUCT_ID') );
			$basketItem->el = $el;
			$result = \CCatalogSku::GetProductInfo( $el['ID'] );
			$isSKU = $result['ID'];
			$basketItem->isSKU = $isSKU;
			if ( $isSKU ){
				$basketItem->product = tools\el::info($isSKU);
			} else {
				$basketItem->product = $el;
			}
			$basketItem->id = $userBasket[$key]->getId();
			$basketItem->price = $basketItem->getBasePrice();
			$basketItem->priceFormat = number_format($basketItem->price, $rounding, ",", " ");
			$basketItem->discPrice = $basketItem->getPrice();
			$basketItem->discPriceFormat = number_format($basketItem->discPrice, $rounding, ",", " ");
			$basketItem->sum = $basketItem->price * $basketItem->getField('QUANTITY');
			$basketItem->sumFormat = number_format($basketItem->sum, $rounding, ",", " ");
			$basketItem->discSum = $basketItem->discPrice * $basketItem->getField('QUANTITY');
			$basketItem->discSumFormat = number_format($basketItem->discSum, $rounding, ",", " ");
			$arBasket[] = $basketItem;
		}

		$res = array(
			'calcInfo' => $calcInfo,
			'basket_sum' => $calcInfo['BASKET_SUM'],
			'basket_sum_format' => number_format($calcInfo['BASKET_SUM'], $rounding, ",", " "),
			'basket_disc_sum' => $calcInfo['BASKET_DISC_SUM'],
			'basket_disc_sum_format' => number_format($calcInfo['BASKET_DISC_SUM'], $rounding, ",", " "),
            'basket_discount' => $calcInfo['BASKET_SUM'] - $calcInfo['BASKET_DISC_SUM'],
            'basket_discount_format' => number_format($calcInfo['BASKET_SUM'] - $calcInfo['BASKET_DISC_SUM'], $rounding, ",", " "),
			'basket_cnt' => round($basket_cnt, 0),
			'arBasket' => $arBasket,
			'orderBasket' => $orderBasket,
			'userBasket' => $userBasket,
		);
		return $res;
	}







	
}
<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

use AOptima\ToolsCafeto as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class ds_pickpoint_pvz extends ds_pickpoint {

    const DELIVERY_MODE = 'Standard';

    static $PVZ_ADDRESS_FIELD_NAME = 'pickpoint_address';
    static $PVZ_POINT_NUMBER_FIELD_NAME = 'pickpoint_point_number';
    static $PVZ_POINT_ID_FIELD_NAME = 'pickpoint_point_id';



    static function hasPVZ(){
        return true;
    }



    public function getPrice( $basketInfo, $point_number ){

        $price = false;

        $auth_res = $this->auth();

        if( strlen($auth_res['SessionId']) > 0 ){

            if( $point_number ){

                $basketParams = project\ds::getBasketParams($basketInfo);

                require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
                \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');

                $hash = md5( json_encode($basketParams).$point_number );

                // Кеширование
                $obCache = new \CPHPCache();
                $cache_time = 60*60;
                $cache_id = 'ds_pickpoint_pvz_price_'.$hash;
                $cache_path = '/ds_pickpoint_pvz_price/'.$hash.'/';
                if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
                	$vars = $obCache->GetVars();   extract($vars);
                } elseif($obCache->StartDataCache()){

                    $client = new Client(array( 'timeout' => 15 ));

                    try {

                        $arRequest = [
                            'SessionId' => $auth_res['SessionId'],
                            'IKN' => static::IKN,
                            'FromCity'=> static::FROM_CITY,
                            'PTNumber' => $point_number,
                            // length (см)
                            'Length' => round($basketParams['places'][0]['length']/10, 0),
                            // height (см)
                            'Depth' => round($basketParams['places'][0]['height']/10, 0),
                            // width (см)
                            'Width' => round($basketParams['places'][0]['width']/10, 0),
                            // weight (кг)
                            'Weight' => round($basketParams['places'][0]['weight']/1000, 3),
                        ];

                        $response = $client->request(
                            'POST',
                            static::URL.'calctariff',
                            array(
                                'headers' => array(
                                    'Content-Type' => 'application/json'
                                ),
                                'body' => json_encode($arRequest)
                            )
                        );

                        $code = $response->getStatusCode();

                        if( $code == 200 ){

                            $body = $response->getBody();
                            $content = $body->getContents();

                            $res = tools\funcs::json_to_array($content);
                            
                            if(  is_array($res['Services']) ){

                                $deliv_price = 0;

                                foreach ( $res['Services'] as $service ){
                                    if( $service['DeliveryMode'] == static::DELIVERY_MODE ){
                                        $deliv_price += $service['Tariff'];
                                        $deliv_price += $service['NDS'];
                                    }
                                }

                                return $deliv_price;

                            } else {

                                tools\logger::addError( 'pickpoint - Ошибка расчёта цены - '.$res['ErrorMessage'].' ['.$res['ErrorCode'].']' );
                            }

                        } else {
                            tools\logger::addError( 'pickpoint - ответ сервера - '.$code );
                        }

                    } catch(RequestException $ex){

                        tools\logger::addError( 'pickpoint - Ошибка расчёта цены - '.$ex->getMessage() );
                    }

                $obCache->EndDataCache(array('price' => $price));
                }

                if( !$price ){
                    BXClearCache(true, '/ds_pickpoint_pvz_price/'.$hash.'/');
                }

            }

        }

        return $price;
    }


    
}
<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;


class product_vote {

    const HIBLOCK_ID = 2;



    function __construct(){
        \CModule::IncludeModule('highloadblock');
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( static::HIBLOCK_ID )->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $this->hlblock );
        $this->entity = $entity;
        $this->entity_data_class = $entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }




    public function getList(
        $product_id = false,
        $user_id = false
    ){
        $list = [];
        $arFilter = array();
        if( intval($product_id) > 0 ){    $arFilter['UF_PRODUCT'] = $product_id;    }
        if( intval($user_id) > 0 ){    $arFilter['UF_USER'] = $user_id;    }
        $arSelect = array( "ID", "UF_USER", "UF_PRODUCT", "UF_VOTE" );
        $arOrder = array( "ID" => "DESC" );
        $entity_data_class = $this->entity_data_class;
        $rsData = $entity_data_class::getList([
            "select" => $arSelect,
            "filter" => $arFilter,
            //"limit" => '1',
            "order" => $arOrder
        ]);
        $rsData = new \CDBResult($rsData, $this->sTableID);
        while( $el = $rsData->Fetch() ){
            $list[] = $el;
        }
        return $list;
    }




    public function add( $product_id, $user_id, $vote ){
        $arData = Array(
            'UF_USER' => $user_id,
            'UF_PRODUCT' => $product_id,
            'UF_VOTE' => $vote
        );
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            \AOptima\Tools\logger::addError( implode(', ', $result->getErrors()) );
            return false;
        }
    }



    public function delete( $vote_id ){
        $this->entity_data_class::delete($vote_id);
        return true;
    }





    // Инфо по элементу
    public function info($item_id){
        $item = false;
        if ( intval($item_id) > 0 ){
            $arFilter = array("ID" => $item_id);
            $arSelect = array( "ID" );
            $arOrder = array("ID" => "DESC");
            // Кешируем
            $obCache = new \CPHPCache();
            $cache_time = static::HEL_CACHE_TIME;
            $cache_id = static::HEL_CACHE_NAME.'_'.$item_id;
            $cache_path = '/'.static::HEL_CACHE_NAME.'/'.$item_id.'/';
            if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
                $vars = $obCache->GetVars();   extract($vars);
            } elseif($obCache->StartDataCache()){
                $entity_data_class = $this->entity_data_class;
                $rsData = $entity_data_class::getList(array(
                    "select" => $arSelect,
                    "filter" => $arFilter,
                    "limit" => '1',
                    "order" => $arOrder
                ));
                $rsData = new \CDBResult($rsData, $this->sTableID);
                if($el = $rsData->Fetch()){
                    $item = tools\funcs::clean_array($el);
                }
                $obCache->EndDataCache(array('item' => $item));
            }
        }
        return $item;
    }





}
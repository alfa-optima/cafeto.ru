<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

use Bitrix\Main,
Bitrix\Main\Application,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Order,
Bitrix\Sale\Basket,
\Bitrix\Sale\Discount,
\Bitrix\Sale\Result,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context,
Bitrix\Main\Web\Json,
Bitrix\Sale\PersonType,
Bitrix\Sale\Shipment,
Bitrix\Sale\Payment,
Bitrix\Sale\Location\LocationTable,
Bitrix\Sale\Services\Company,
Bitrix\Sale\Location\GeoIp;
use Bitrix\Sale\Fuser;



class accum_discount {



    static function get(){
        global $USER;
        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = [ "IBLOCK_ID" => project\catalog::TP_IBLOCK_ID, "ACTIVE" => "Y" ];
        $fields = [ 'ID', 'ACTIVE' ];
        $products = \CIBlockElement::GetList(
            ["SORT"=>"ASC"], $filter, false, ["nTopCount"=>1], $fields
        );
        if ( $product = $products->GetNext() ){

            $basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
            $basketPseudo = $basket->copy();
            foreach ($basketPseudo as $basketItem) {   $basketItem->delete();   }
            $item = $basketPseudo->createItem('catalog', $product['ID']);
            $item->setFields(array(
                'QUANTITY' => 1,
                'CURRENCY' => 'RUB',
                'LID' => 's2',
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            ));
            $order = \Bitrix\Sale\Order::create( 's2', $USER->GetID() );
            $order->setBasket($basketPseudo);
            $order->setPersonTypeId( 1 );
            $result = $order->getDiscount()->getApplyResult();
            $discList = $result['DISCOUNT_LIST'];
            if( is_array($discList) ){
                foreach ( $discList as $discItem ){
                    $discDescr = strtolower(trim($discItem['ACTIONS_DESCR']['BASKET']));
                    $discData = $discItem['ACTIONS_DESCR_DATA']['BASKET'];
                    $discData = $discData[array_keys($discData)[0]];
                    if(
                        strlen($discDescr) > 0
                        &&
                        substr_count($discDescr, 'накопительная скидка')
                        &&
                        $discData['VALUE']
                    ){
                        if( $discData['VALUE_TYPE'] == 'P' ){
                            return $discData['VALUE'].'%';
                        } else if( $discData['VALUE_TYPE'] == 'C' ){
                            return $discData['VALUE'].' '.$discData['VALUE_UNIT'];
                        }
                    }
                }
            }
        }
        return false;
    }



}
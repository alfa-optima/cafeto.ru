<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;


class catalog {

    const PRICE_CODE = 'BASE';
    const PRICE_ID = 1;
    const PRICE_ROUND = 2;

    const IBLOCK_TYPE = 'catalog';
    const IBLOCK_ID = 9;
    const TP_IBLOCK_ID = 10;

    const MAX_CNT = 12;
    const FILTER_NAME = 'catalog_goods';

    const METHODS_IBLOCK_ID = 12;
    const REGIONS_IBLOCK_ID = 13;
    const METHOD_PROP_CODE = 'METOD';
    const REGION_PROP_CODE = 'REGION';
    const COFFEE_TYPE_PROP_CODE = 'COFFEE_TYPE';

    const BESPLAT_DOSTAVKA_SUM = 3000;



    static function getSortVariants(){
        return [
            'NAME|ASC' => [
                'NAME' => 'По названию А-Я',
                'CODE' => 'NAME|ASC',
            ],
            'NAME|DESC' => [
                'NAME' => 'По названию Я-А',
                'CODE' => 'NAME|DESC',
            ],
            'PROPERTY_SORT_PRICE|ASC' => [
                'NAME' => 'По цене (сначала дешевые)',
                'CODE' => 'PROPERTY_SORT_PRICE|ASC',
            ],
            'PROPERTY_SORT_PRICE|DESC' => [
                'NAME' => 'По цене (сначала дорогие)',
                'CODE' => 'PROPERTY_SORT_PRICE|DESC',
            ],
        ];
    }





    static function first_level_sections(){
        $list = [];
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'catalog_1_level_sections';
        $cache_path = '/catalog_1_level_sections/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "ACTIVE" => "Y",
                "DEPTH_LEVEL" => 1,
                "IBLOCK_ID" => static::IBLOCK_ID
            );
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"),
                $filter, false, array('UF_*')
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['CODE']] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    static function all_methods(){
        $list = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_methods';
        $cache_path = '/all_methods/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = Array(
                "IBLOCK_ID" => static::METHODS_IBLOCK_ID,
                "ACTIVE" => "Y"
            );
            $fields = Array( "ID", "CODE",  "NAME", "PREVIEW_TEXT" );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['CODE']] = $element;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    static function all_regions(){
        $list = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_regions';
        $cache_path = '/all_regions/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = Array(
                "IBLOCK_ID" => static::REGIONS_IBLOCK_ID,
                "ACTIVE" => "Y"
            );
            $fields = Array( "ID", "CODE", "NAME", "PREVIEW_TEXT" );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['CODE']] = $element;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    static function productInfo( $arResult, $sku_id = false ){
        $price = false;  $discPrice = false;
        $pictures = [];   $big_pictures = [];
        $middle_pictures = [];   $small_pictures = [];
        $basketElID = false;
        $offers = [];
        if( count($arResult['OFFERS']) > 0 ){
            foreach( $arResult['OFFERS'] as $key => $offer ){ $cnt++;
                $offers[$offer['PROPERTY_WEIGHT_VALUE']] = $offer;
            }
            ksort($offers);
            $cnt = 0;
            foreach( $offers as $key => $offer ){ $cnt++;
                if(
                    ( !$sku_id && $cnt == 1 )
                    ||
                    ( intval($sku_id) > 0 && $sku_id == $offer['ID'] )
                ){
                    $basketElID = $offer['ID'];
                    $price = $offer['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT'];
                    $discPrice = $offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'];
                    if( intval($offer['DETAIL_PICTURE']['ID']) > 0 ){
                        $pictures[] = $offer['DETAIL_PICTURE']['ID'];
                    }
                    if( is_array($offer['PROPERTY_MORE_PHOTOS_VALUE']) ){
                        foreach ( $offer['PROPERTY_MORE_PHOTOS_VALUE'] as $key => $pic_id ){
                            if( intval($pic_id) > 0 ){    $pictures[] = $pic_id;    }
                        }
                    } else if( is_array($offer['PROPERTIES']['MORE_PHOTOS']['VALUE']) ){
                        foreach ( $offer['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $key => $pic_id ){
                            if( intval($pic_id) > 0 ){    $pictures[] = $pic_id;    }
                        }
                    }
                }
            }
        } else {
            $basketElID = $arResult['ID'];
            $price = $arResult['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT'];
            $discPrice = $arResult['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'];
            if( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){
                $pictures[] = $arResult['DETAIL_PICTURE']['ID'];
            }
            if( is_array($arResult['PROPERTY_MORE_PHOTOS_VALUE']) ){
                foreach ( $arResult['PROPERTY_MORE_PHOTOS_VALUE'] as $key => $pic_id ){
                    if( intval($pic_id) > 0 ){    $pictures[] = $pic_id;    }
                }
            } else if( is_array($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE']) ){
                foreach ( $arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $key => $pic_id ){
                    if( intval($pic_id) > 0 ){    $pictures[] = $pic_id;    }
                }
            }
        }
        if( count($pictures) > 0 ){
            foreach ( $pictures as $key => $picture_id ){
                $big_pictures[$key] = \AOptima\ToolsCafeto\funcs::rIMGG($picture_id, 4, 800, 800);
                $middle_pictures[$key] = \AOptima\ToolsCafeto\funcs::rIMGG($picture_id, 4, 405, 405);
                $small_pictures[$key] = \AOptima\ToolsCafeto\funcs::rIMGG($picture_id, 4, 48, 48);
            }
        }
        return [
            'offers' => $offers,
            'price' => $price,
            'discPrice' => $discPrice,
            'pictures' => $pictures,
            'big_pictures' => $big_pictures,
            'middle_pictures' => $middle_pictures,
            'small_pictures' => $small_pictures,
            'basketElID' => $basketElID,
        ];
    }




    // Категори каталога для отслеживания наличия остатков
    static $availableCategories = [ 'accessories', 'food_supply' ];


    // Проверка доступности товара
    static function isAvailableProduct( $product ){

        \Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');

        if( is_array($product) ){

            if(
                intval( $product['ID'] ) > 0
                &&
                $product['ACTIVE'] == 'Y'
            ){

                // Если это ТП
                if( $product['IBLOCK_ID'] == project\catalog::TP_IBLOCK_ID ){

                    return true;

                // Если это товар
                } else if( $product['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){

                    if(
                        isset( $product['OFFERS'] )
                        &&
                        count( $product['OFFERS'] ) > 0
                    ){

                        return true;

                    } else {

                        // Получим категорию 1 уровня для товара
                        $sectID = \AOptima\ToolsCafeto\el::sections( $product['ID'] )[0]['ID'];
                        $chain = \AOptima\ToolsCafeto\section::chain( $sectID );
                        $sect1Level = $chain[0];
                        $sect1LevelCode = $sect1Level['CODE'];
                        // Если категория входит в перечень отслеживаемых по остаткам на складе
                        if( in_array( $sect1LevelCode, static::$availableCategories ) ){
                            if( $product['CATALOG_QUANTITY'] > 0 ){
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return true;
                        }
                    }

                }

            } else {
                return false;
            }

        } else if( intval($product) > 0 ){

            $product_id = intval($product);
            $product = \AOptima\ToolsCafeto\el::info( $product_id );

            if(
                intval( $product['ID'] ) > 0
                &&
                $product['ACTIVE'] == 'Y'
            ){

                // Если это ТП
                if( $product['IBLOCK_ID'] == project\catalog::TP_IBLOCK_ID ){

                    return true;

                // Если это товар
                } else if( $product['IBLOCK_ID'] == project\catalog::IBLOCK_ID ){

                    $offers = [];

                    \Bitrix\Main\Loader::includeModule('iblock');
                    $filter = [
                        "IBLOCK_ID" => project\catalog::TP_IBLOCK_ID,
                        "ACTIVE" => "Y",
                        "PROPERTY_CML2_LINK" => $product['ID']
                    ];
                    $fields = [ "ID", "PROPERTY_CML2_LINK" ];
                    $dbOffers = \CIBlockElement::GetList(
                        [ "SORT" => "ASC" ], $filter, false, false, $fields
                    );
                    while ($offer = $dbOffers->GetNext()){
                        $offers[] = $offer['ID'];
                    }

                    if(
                        isset( $offers )
                        &&
                        count( $offers ) > 0
                    ){

                        return true;

                    } else {

                        // Получим категорию 1 уровня для товара
                        $sectID = \AOptima\ToolsCafeto\el::sections( $product['ID'] )[0]['ID'];
                        $chain = \AOptima\ToolsCafeto\section::chain( $sectID );
                        $sect1Level = $chain[0];
                        $sect1LevelCode = $sect1Level['CODE'];
                        // Если категория входит в перечень отслеживаемых по остаткам на складе
                        if( in_array( $sect1LevelCode, static::$availableCategories ) ){

                            \Bitrix\Main\Loader::includeModule('catalog');

                            $products = \CCatalogProduct::GetList([], [ "ID" => $product_id ]);
                            if ( $arProduct = $products->GetNext() ){
                                if( $arProduct['QUANTITY'] > 0 ){
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        } else {
                            return true;
                        }
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }





}
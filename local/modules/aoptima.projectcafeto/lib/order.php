<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;


class order {

    const SERVICE_USER_ID = 6;




    // Инфо по свойству
    static function getPropValue( $propValues, $propCode ){
        $propValue = false;
        foreach ( $propValues as $key => $prop ){
            $value = $prop['VALUE'][0];
            if( strlen($value) > 0 ){
                if( $prop['CODE'] == $propCode ){   $propValue = $value;   }
            }
        }
        return $propValue;
    }



    // Значения свойств заказа
    static function getOrderPropValues( $obOrder ){
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'orderPropValues_'.$obOrder->getID();
        $cache_path = '/orderPropValues/'.$obOrder->getID().'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $propertyCollection = $obOrder->getPropertyCollection();
            $propValues = $propertyCollection->getArray()['properties'];
            $obCache->EndDataCache(array('propValues' => $propValues));
        }
        return $propValues;
    }



    // setPropValue
    static function setPropValue( $obOrder, $propCode, $propValue ){
        $propertyCollection = $obOrder->getPropertyCollection();
        $propValues = $propertyCollection->getArray()['properties'];
        foreach ( $propValues as $key => $prop ){
            if( $prop['CODE'] == $propCode ){
                $obProp = $propertyCollection->getItemByOrderPropertyId($prop['ID']);
                $obProp->setValue( $propValue );
                $obOrder->save();
                BXClearCache(true, "/orderPropValues/".$obOrder->getID."/");
            }
        }
    }





    static function saveDeliveryInfo( $user_id, $ds, $arFields ){
        if( intval($user_id) > 0 && is_array($arFields) ){
            $user = tools\user::info($user_id);
            if( intval($user['ID']) > 0 ){

                $ds_class = '\AOptima\ProjectCafeto\ds_'.$ds['XML_ID'];
                if( isset($ds_class) && class_exists($ds_class) ){
                    $dsObj = new $ds_class();
                }

                if( strlen($user['UF_DELIV_INFO']) > 0 ){
                    $deliv_info = tools\funcs::json_to_array($user['UF_DELIV_INFO']);
                } else {
                    $deliv_info = [ 'np_info' => [], 'pvz_info' => [], 'other_info' => [] ];
                }
                if( $arFields['np'][$ds['ID']] ){
                    $deliv_info['np_info'][$ds['XML_ID']]['np'] = $arFields['np'][$ds['ID']];
                }
                if( $arFields['np_id'][$ds['ID']] ){
                    $deliv_info['np_info'][$ds['XML_ID']]['np_id'] = $arFields['np_id'][$ds['ID']];
                }
                if( isset($arFields[$ds_class::$PVZ_ADDRESS_FIELD_NAME][$ds['ID']]) ){
                    $deliv_info['pvz_info'][$ds['XML_ID']]['address'] = $arFields[$ds_class::$PVZ_ADDRESS_FIELD_NAME][$ds['ID']];
                }
                if( isset($arFields[$ds_class::$PVZ_POINT_ID_FIELD_NAME][$ds['ID']]) ){
                    $deliv_info['pvz_info'][$ds['XML_ID']]['point_id'] = $arFields[$ds_class::$PVZ_POINT_ID_FIELD_NAME][$ds['ID']];
                }
                if( isset($arFields[$ds_class::$PVZ_POINT_NUMBER_FIELD_NAME][$ds['ID']]) ){
                    $deliv_info['pvz_info'][$ds['XML_ID']]['point_id'] = $arFields[$ds_class::$PVZ_POINT_NUMBER_FIELD_NAME][$ds['ID']];
                }
                if( $arFields['zip_code'][$ds['ID']] ){
                    $deliv_info['other_info']['zip_code'] = $arFields['zip_code'][$ds['ID']];
                }
                if( $arFields['street'][$ds['ID']] ){
                    $deliv_info['other_info']['street'] = $arFields['street'][$ds['ID']];
                }
                if( $arFields['house'][$ds['ID']] ){
                    $deliv_info['other_info']['house'] = $arFields['house'][$ds['ID']];
                }
                if( $arFields['kvartira'][$ds['ID']] ){
                    $deliv_info['other_info']['kvartira'] = $arFields['kvartira'][$ds['ID']];
                }
                $obUser = new \CUser;
                $obUser->Update($user['ID'], ["UF_DELIV_INFO" => json_encode($deliv_info)]);
            }
        }
    }





}
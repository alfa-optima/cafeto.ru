<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;



class ps {



    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $filter = Array( "ACTIVE"=>"Y", "ID" => $id );
        $db_ptype = \CSalePaySystem::GetList(
            Array(
                "SORT"=>"ASC",
                "PSA_NAME"=>"ASC"
            ),
            $filter, false, false,
            array('ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION')
        );
        while ($ps = $db_ptype->GetNext()){
            return $ps;
        }
        return false;
    }



    // Перечень вариантов оплаты
    static function getList( $person_type_id = 1 ){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $filter = Array( "ACTIVE"=>"Y", "!ID" => 1 );
        if( intval($person_type_id) > 0 ){
            $filter['PSA_PERSON_TYPE_ID'] = $person_type_id;
        }
        $db_ptype = \CSalePaySystem::GetList(
            Array(
                "SORT"=>"ASC",
                "PSA_NAME"=>"ASC"
            ),
            $filter, false, false,
            array('ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION')
        );
        while ($ps = $db_ptype->GetNext()){
            $list[$ps['ID']] = $ps;
        }
        return  $list;
    }




    static function getFilteredtList( $ds_id, $person_type_id = false ){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $all_pss = static::getList( $person_type_id );
        $filtered_ps = [];
        $res = \CSaleDelivery::GetDelivery2PaySystem();
        while ($item = $res->GetNext()){
            $filtered_ps[] = $item;
        }
        foreach ( $all_pss as $ps ){
            foreach ( $filtered_ps as $item ){
                if(
                    $ds_id == $item['DELIVERY_ID']
                    &&
                    $ps['ID'] == $item['PAYSYSTEM_ID']
                ){    $list[$ps['ID']] = $ps;    }
            }
        }
        return  $list;
    }







}
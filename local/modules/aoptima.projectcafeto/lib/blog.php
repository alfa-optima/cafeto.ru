<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;


class blog {

    const IBLOCK_ID = 14;
    const MAX_CNT = 6;




    static function sections(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'blog_sections';
        $cache_path = '/blog_sections/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => static::IBLOCK_ID,
                "DEPTH_LEVEL" => 1
            );
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"), $filter, false
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['ID']] = $section;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




}
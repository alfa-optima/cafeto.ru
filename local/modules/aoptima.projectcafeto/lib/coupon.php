<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Main;
use Bitrix\Sale;
use Bitrix\Sale\DiscountCouponsManager;



class coupon {


	// Проверка существование купона
	static function check($coupon){
		DiscountCouponsManager::init();
		$res = DiscountCouponsManager::getData($coupon);
		return (intval($res['ID']) > 0 && $res['ACTIVE'] != 'N');
	}

	
	// Установка купона
	static function set($coupon){
		DiscountCouponsManager::init();
		DiscountCouponsManager::add($coupon);
	}
	
	
	// Удаления купона из куки
	static function clear(){
		DiscountCouponsManager::init();
		$arCoupons = DiscountCouponsManager::get(true, array(), true, true);
		foreach($arCoupons as $coupon => $ar){
			DiscountCouponsManager::delete($coupon);
		}
	}
	
	
	// Запрос активного купона
	static function get_active_coupon(){
        DiscountCouponsManager::init();
        $arCoupons = DiscountCouponsManager::get(true, array(), true, true);
	    foreach ( $arCoupons as $coupon => $arCoupon ){
            return $arCoupon;
	    }
	    return false;
	}
	
	
	// Получение ID купона
	static function get_coupon_id($coupon){
		DiscountCouponsManager::init();
		$arCoupons = DiscountCouponsManager::get(true, array(), true, true);
		foreach($arCoupons as $c => $ar){
			if( $c == $coupon ){
				return $ar['ID'];
			}
		}
		return false;
	}
	
	
	// Деактивация купона в БД
	static function deactivate($coupon){
		\CCatalogDiscountCoupon::Update(static::get_coupon_id($coupon), array('ACTIVE' => 'N')); 
	}
	
	
	// Удаление купона в БД
	static function delete($coupon){
		\CCatalogDiscountCoupon::Delete(static::get_coupon_id($coupon));  
	}
	
	
}
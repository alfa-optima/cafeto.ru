<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

use AOptima\ToolsCafeto as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class smsc {

    const LOGIN = 'cafeto';
    const PASSWORD = 'Abcd1234';



    static function send( $phone, $message ){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
        \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');

        $client = new Client(array( 'timeout' => 10 ));

        try {

            $response = $client->request(
                'GET',
                'https://smsc.ru/sys/send.php?login='.static::LOGIN.'&psw='.static::PASSWORD.'&phones='.$phone.'&mes='.$message.'&fmt=3'
            );

            $code = $response->getStatusCode();

            if( $code == 200 ){

                $body = $response->getBody();
                $content = $body->getContents();

                $res = tools\funcs::json_to_array($content);

                if( intval($res['id']) > 0 ){

                    return $res['id'];

                }

            } else {
                tools\logger::addError( 'smsc.ru - ответ сервера - '.$code );
            }

        } catch(RequestException $ex){

            tools\logger::addError( 'smsc.ru - ошибка отправки SMS - '.$ex->getMessage() );
        }

        return false;
    }



}
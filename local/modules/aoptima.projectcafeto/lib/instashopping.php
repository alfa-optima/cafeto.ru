<? namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

class instashopping {

    const YML_PATH = '/bitrix/catalog_export/instashopping/';
    const YML_FILE_NAME = 'catalog.php';


    static function createCatalogYML(){

        \Bitrix\Main\Loader::includeModule('iblock');

        $tps = [];
        $products_ids = [];   $products = [];
        $categories_ids = [];   $categories = [];
        
        // Получим все ТП
        $filter = Array(
        	"IBLOCK_ID" => project\catalog::TP_IBLOCK_ID,
        	"ACTIVE" => "Y"
        );
        $fields = Array(
            "ID", "NAME", "PROPERTY_CML2_LINK",
            "DETAIL_PICTURE", "CATALOG_GROUP_".project\catalog::PRICE_ID
        );
        $obTPS = \CIBlockElement::GetList(
        	array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($tp = $obTPS->GetNext()){
            $tps[$tp['ID']] = $tp;
            $products_ids[$tp['PROPERTY_CML2_LINK_VALUE']] = $tp['PROPERTY_CML2_LINK_VALUE'];
        }

        if( count($products_ids) > 0 ){
            $filter = Array(
                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                "ACTIVE" => "Y",
                "ID" => $products_ids
            );
            $fields = Array( "ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL" );
            $obProducts = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($product = $obProducts->GetNext()){
                $products[$product['ID']] = $product;
                $categories_ids[$product['IBLOCK_SECTION_ID']] = $product['IBLOCK_SECTION_ID'];
            }
        }

        // Получим категории каталога
        if( count($categories_ids) > 0 ){
            $filter = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                "ID" => $categories_ids
            );
            $dbSections = \CIBlockSection::GetList( ["SORT"=>"ASC"], $filter, false, array("ID", 'NAME') );
            while ($section = $dbSections->GetNext()){
                $categories[$section['ID']] = $section;
            }
        }

        $yml = "<"."? header(\"Content-Type: text/xml; charset=UTF-8\"); ";
        $yml .= "echo \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?>\";"."?>";
        $yml .= "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">"."\n";

        $yml .= "<yml_catalog date=\"2017-02-05 17:22\">"."\n";
            $yml .= "<shop>"."\n";

                $yml .= "<name>cafeto.ru</name>"."\n";
                $yml .= "<company>cafeto.ru</company>"."\n";
                $yml .= "<url>https://cafeto.ru/</url>"."\n";

                $yml .= "<currencies>"."\n";
                    $yml .= "<currency id=\"RUR\" rate=\"1\"/>"."\n";
                $yml .= "</currencies>"."\n";

                if( count($categories) > 0 ){
                    $yml .= "<categories>"."\n";
                    foreach ( $categories as $key => $category ){
                        $yml .= "<category id=\"".$category['ID']."\">".$category['NAME']."</category>"."\n";
                    }
                    $yml .= "</categories>"."\n";
                }

                if( count($categories) > 0 ){

                    $yml .= "<offers>"."\n";

                        foreach ( $tps as $tp ){
                            $product = $products[$tp['PROPERTY_CML2_LINK_VALUE']];
                            if( intval($product['ID']) > 0 ){

                                $yml .= "<offer id=\"".$tp['ID']."\">"."\n";
                                    $yml .= "<name>".$tp['NAME']."</name>"."\n";
                                    $yml .= "<url>https://".$_SERVER['SERVER_NAME'].$product['DETAIL_PAGE_URL']."</url>"."\n";
                                    $yml .= "<price>".$tp['CATALOG_PRICE_'.project\catalog::PRICE_ID]."</price>"."\n";
                                    $yml .= "<currencyId>RUR</currencyId>"."\n";
                                    $yml .= "<categoryId>".$product['IBLOCK_SECTION_ID']."</categoryId>"."\n";
                                    if( intval($tp['DETAIL_PICTURE']) > 0 ){
                                        $yml .= "<picture>https://".$_SERVER['SERVER_NAME'].\CFile::GetPath($tp['DETAIL_PICTURE'])."</picture>"."\n";
                                    }
                                     $yml .= "<country_of_origin>Россия</country_of_origin>"."\n";
                                 $yml .= "</offer>"."\n";

                            }
                        }

                    $yml .= "</offers>"."\n";

                }

            $yml .= "</shop>"."\n";
        $yml .= "</yml_catalog>";

        if( !file_exists($_SERVER['DOCUMENT_ROOT'].static::YML_PATH) ){
            mkdir($_SERVER['DOCUMENT_ROOT'].static::YML_PATH);
        }

        $file_path = $_SERVER['DOCUMENT_ROOT'].static::YML_PATH.static::YML_FILE_NAME;
        $file = fopen($file_path, "w");
        $res = fwrite($file,  $yml);
        fclose($file);

        return "\AOptima\ProjectCafeto\instashopping::createCatalogYML();";

    }







}
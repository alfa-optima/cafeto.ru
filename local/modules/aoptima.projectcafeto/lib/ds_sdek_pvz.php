<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;



class ds_sdek_pvz extends ds_sdek{

    const SDEK_INFO_PATH = '/local/php_interface/sdek_info';

    
    static $PVZ_ADDRESS_FIELD_NAME = 'sdek_address';
    static $PVZ_POINT_ID_FIELD_NAME = 'sdek_point_id';
    


    static function hasPVZ(){
        return true;
    }


    // Получение пути к XML с ПВЗ
    static function getXmlFilePath(){
        $dir = $_SERVER['DOCUMENT_ROOT'].static::SDEK_INFO_PATH;
        if( !file_exists( $dir ) ){    mkdir( $dir, 0770 );    }
        $file_path = $dir.'/pvz_list.xml';
        return $file_path;
    }


    // Скачивание XML-файла с ПВЗ
    static function downloadXmlFile(){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $logPrefix = 'СДЭК - получение списка ПВЗ - ';

        $file_path = static::getXmlFilePath();

        $xml_url = 'https://integration.cdek.ru/pvzlist/v1/xml';

        $client = new Client([]);
        try {
            $response = $client->request( 'GET', $xml_url );
            $code = $response->getStatusCode();
            if( $code == 200 ){
                $body = $response->getBody();

                $file = fopen($file_path, "w");
                $res = fwrite($file,  $body->getContents());
                fclose($file);

                BXClearCache(true, "/sdek_pvz_list/");

                static::getPVZList();

            } else {
                tools\logger::addError( $logPrefix.' - ответ сервера - '.$code );
            }
        } catch(RequestException $ex){
            tools\logger::addError( $logPrefix.$ex->getMessage() );
        } catch(ClientException $ex){
            tools\logger::addError( $logPrefix.$ex->getMessage() );
        } catch(ConnectException $ex){
            tools\logger::addError( $logPrefix.$ex->getMessage() );
        }

        return file_exists( $file_path );
    }


    static function getPVZList(  ){

        require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php');
        \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');

        $list = [];

        $file_path = static::getXmlFilePath();

        if( file_exists( $file_path ) ){

            // Кеширование
            $obCache = new \CPHPCache();
            $cache_time = 24*60*60;
            $cache_id = 'sdek_pvz_list';
            $cache_path = '/sdek_pvz_list/';
            if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            	$vars = $obCache->GetVars();   extract($vars);
            } elseif($obCache->StartDataCache()){

                $content = file_get_contents( $file_path );

                $xml = new \CDataXML();
                $xml->LoadString($content);
                if ($PvzListBlock = $xml->SelectNodes('/PvzList')) {
                    $PvzList = $PvzListBlock->children();
                    foreach ( $PvzList as $pvz ){
                        $pvz_attrs = $pvz->getAttributes();
                        $pvz_id = false;
                        foreach ( $pvz_attrs as $pvz_attr ){
                            if( $pvz_attr->name == 'Code' ){
                                $pvz_id = $pvz_attr->content;
                            }
                        }
                        if( $pvz_id ){
                            foreach ( $pvz_attrs as $pvz_attr ){
                                $list[$pvz_id][$pvz_attr->name] = $pvz_attr->content;
                            }
                        }
                        /*$PvzItems = $pvz->children();
                        foreach ( $PvzItems as $PvzItem ){
                            $PvzItem_attrs = $PvzItem->getAttributes();
                            foreach ( $PvzItem_attrs as $attr ){}
                        }*/
                    }
                }

            $obCache->EndDataCache([ 'list' => $list ]);
            }
        }

        return $list;
    }




}
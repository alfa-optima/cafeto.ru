<?

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;



class product_subscribe {

    const HIBLOCK_ID = 5;



    function __construct(){
        \CModule::IncludeModule('highloadblock');
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( static::HIBLOCK_ID )->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $this->hlblock );
        $this->entity = $entity;
        $this->entity_data_class = $entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    public function getList(
        $product_id = false,
        $email = false
    ){
        $list = [];
        $arFilter = array();
        if( intval($product_id) > 0 ){    $arFilter['UF_PRODUCT_ID'] = $product_id;    }
        if( strlen($email) > 0 ){    $arFilter['UF_EMAIL'] = $email;    }
        $arSelect = array( "ID", "UF_PRODUCT_ID", "UF_EMAIL" );
        $arOrder = array( "ID" => "DESC" );
        $entity_data_class = $this->entity_data_class;
        $rsData = $entity_data_class::getList([
            "select" => $arSelect,
            "filter" => $arFilter,
            //"limit" => $limit,
            "order" => $arOrder
        ]);
        $rsData = new \CDBResult($rsData, $this->sTableID);
        while( $el = $rsData->Fetch() ){    $list[] = $el;    }
        return $list;
    }



    public function add( $arData ){
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            \AOptima\ToolsCafeto\logger::addError( implode(', ', $result->getErrors()) );
            return false;
        }
    }




    public function delete( $item_id ){
        $this->entity_data_class::delete($item_id);
        return true;
    }





    static function sendNotifications(){

        \Bitrix\Main\Loader::includeModule('catalog');

        $product_subscribe = new static;
        $notes = $product_subscribe->getList();

        foreach ( $notes as $note ){

            $el = tools\el::info($note['UF_PRODUCT_ID']);

            if( intval($el['ID']) > 0 ){

                $products = \CCatalogProduct::GetList([], [ "ID" => $el['ID'] ]);
                if ( $arProduct = $products->GetNext() ){
                    if( $arProduct['QUANTITY'] > 0 ){

                        $el = tools\el::info($note['UF_PRODUCT_ID']);

                        ob_start();
                            global $APPLICATION;
                            $APPLICATION->IncludeComponent(
                                "aoptima:mailProductSubscribe", "default",
                                [ 'email' => $note['UF_EMAIL'], 'product' => $el ]
                            );
                            $mail_html = ob_get_contents();
                        ob_end_clean();

                        $mail_title = 'Поступление товара (подписка на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';

                        tools\feedback::sendMainEvent( $mail_title, $mail_html, $note['UF_EMAIL'] );

                        // Удалим запись
                        $product_subscribe->delete($note['ID']);

                    }
                }

            }

        }

        return '\AOptima\ProjectCafeto\product_subscribe::sendNotifications();';
    }





}
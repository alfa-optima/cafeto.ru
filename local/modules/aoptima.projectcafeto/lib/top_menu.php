<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;


class top_menu {

    const IBLOCK_ID = 11;




    static function all_items(){
        $list = [];
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_top_menu_items';
        $cache_path = '/all_top_menu_items/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => static::IBLOCK_ID
            );
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"),
                $filter, false, array('UF_*')
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['ID']] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }





    static function tree(){
        $tree = [];
        $all_items = static::all_items();
        foreach ( $all_items as $key => $sect ){
            if( $sect['DEPTH_LEVEL'] == 1 ){
                $tree[$sect['ID']] = [
                    'ID' => $sect['ID'],
                    'NAME' => $sect['NAME'],
                    'URL' => $sect['UF_LINK'],
                ];
                unset($all_items[$key]);
            }
        }
        foreach ( $all_items as $key => $sect ){
            if( $sect['DEPTH_LEVEL'] == 2 ){
                $chain = tools\section::chain($sect['ID']);
                $tree[$chain[0]['ID']]['SUBSECTIONS'][$sect['ID']] = [
                    'ID' => $sect['ID'],
                    'NAME' => $sect['NAME'],
                    'URL' => $sect['UF_LINK'],
                ];
                unset($all_items[$key]);
            }
        }
        foreach ( $all_items as $key => $sect ){
            if( $sect['DEPTH_LEVEL'] == 3 ){
                $chain = tools\section::chain($sect['ID']);
                $tree[$chain[0]['ID']]['SUBSECTIONS'][$chain[1]['ID']]['SUBSECTIONS'][$sect['ID']] = [
                    'ID' => $sect['ID'],
                    'NAME' => $sect['NAME'],
                    'URL' => $sect['UF_LINK'],
                ];
                unset($all_items[$key]);
            }
        }
        return $tree;
    }






}
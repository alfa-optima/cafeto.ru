<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;



class ds {


    static $PVZ_ADDRESS_FIELD_NAME = null;
    static $PVZ_POINT_NUMBER_FIELD_NAME = null;
    static $PVZ_POINT_ID_FIELD_NAME = null;



    static function hasPVZ(){
        return false;
    }



    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
                "ID" => $id
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            return $ds;
        }
        return false;
    }



    // Варианты доставки
    static function getList(){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
                "!ID" => 1
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            $list[$ds['ID']] = $ds;
        }
        return $list;
    }




    static function getBasketParams( $basketInfo ){
        $arBasket = $basketInfo['arBasket'];

        $volume = 0;   $weight = 0;
        foreach ( $arBasket as $bItem ){
            $el = $bItem->el;
            if(
                $el['PROPERTY_WIDTH_VALUE'] > 0
                &&
                $el['PROPERTY_LENGTH_VALUE'] > 0
                &&
                $el['PROPERTY_HEIGHT_VALUE'] > 0
            ){
                $volume += $el['PROPERTY_WIDTH_VALUE'] * $el['PROPERTY_LENGTH_VALUE'] * $el['PROPERTY_HEIGHT_VALUE'];
                $weight += $el['PROPERTY_WEIGHT_VALUE'] * round($bItem->getField('QUANTITY'),0);
            }
        }

        $basketParams = [
            'places' => [
                [
                    'width'     => pow($volume, 1/3), // мм
                    'length'    => pow($volume, 1/3), // мм
                    'height'    => pow($volume, 1/3), // мм
                    'weight'    => $weight    // гр
                ]
            ],
            'sum' => $basketInfo['basket_disc_sum']
        ];

        return $basketParams;
    }




}
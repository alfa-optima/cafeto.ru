<? namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;



class funcs {


    static function isContentPage(){
        $arURI = tools\funcs::arURI( tools\funcs::pureURL() );
        if(
            in_array( $arURI[1], [
                'personal', 'basket', 'blog', 'contacts', 'password_recovery', 'favorites',
                'search', 'register_confirm', 'order_success', 'about', 'product', 'brewing'
            ])
            ||
            ERROR_404 == 'Y'
        ){
            return false;
        } else {
            $catalog_1_level_sections = project\catalog::first_level_sections();
            foreach ( $catalog_1_level_sections as $key => $sect ){
                if( $sect['CODE'] == $arURI[1] ){  return false;  }
            }
        }
        return true;
    }



	
}
<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Cookie\CookieJar;


\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;


class amo_crm {

    const ACCOUNT = 'afanasevcafetoru';
    const LOGIN = 'afanasev@cafeto.ru';
    const USER_HASH = '52e44d243a994be18b98f8b590daf0b582380c50';


    static function getAccount(){
        $account = trim(\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'AMO_ACCOUNT'));
        return $account;
    }

    static function getLogin(){
        $login = trim(\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'AMO_LOGIN'));
        return $login;
    }

    static function getUserHash(){
        $user_hash = trim(\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'AMO_USER_HASH'));
        return $user_hash;
    }


    // Авторизация
    static function auth(){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $reqParams = [
            'method' => 'POST',
            'url' => 'https://'.static::getAccount().'.amocrm.ru/private/api/auth.php?type=json',
            'params' => [
                'form_params' => [
                    'USER_LOGIN' => static::getLogin(),
                    'USER_HASH' => static::getUserHash(),
                ],
            ],
        ];
        
        $client = new Client(array( 'timeout' => 5 ));

        try {

            $response = $client->request(
                $reqParams['method'],
                $reqParams['url'],
                $reqParams['params']
            );

            $code = $response->getStatusCode();

            if( $code == 200 ){

                $headers = $response->getHeaders();
                $body = $response->getBody();
                $content = $body->getContents();

                $result = tools\funcs::json_to_array($content);
                
                if( $result['response']['auth'] ){

                    return $headers['Set-Cookie'];

                } else if( strlen($result['response']['error']) > 0 ){

                    tools\logger::addError( 'Ошибка авторизации в AMOCRM - '.$result['response']['error'].' [error_code - '.$result['response']['error_code'].']' );
                    project\guzzle_request::log( $reqParams );
                }

            } else {
                tools\logger::addError( 'Ошибка авторизации в AMOCRM - код ответа '.$code );
                project\guzzle_request::log( $reqParams );
            }

        } catch(RequestException $ex){

            tools\logger::addError( 'Ошибка авторизации в AMOCRM - '.$ex->getMessage() );
            project\guzzle_request::log( $reqParams );

        } catch(ClientException $ex){

            tools\logger::addError( 'Ошибка авторизации в AMOCRM - '.$ex->getMessage() );
            project\guzzle_request::log( $reqParams );

        } catch(ConnectException $ex){

            tools\logger::addError( 'Ошибка авторизации в AMOCRM - '.$ex->getMessage() );
            project\guzzle_request::log( $reqParams );
        }
        return false;
    }



    // Новый контакт
    static function addContact( $name, $last_name, $second_name = false, $email = false, $phone = false ){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $authCookies = static::auth();

        if( $authCookies ){

            $fio = $last_name.' '.$name;
            if( $second_name ){   $fio .= ' '.$second_name;   }

            $arResult = [
                'add' => [
                    [
                        'name' => $fio,
                        'custom_fields' => []
                    ]
                ]
            ];

            if( $email ){
                // Email
                $arResult['add'][0]['custom_fields'][] = [
                    'id' => 482911,
                    'values' => [
                        [
                            'value' => $email,
                            'enum' => 'WORK'
                        ]
                    ]
                ];
            }

            if( $phone ){
                // Phone
                $arResult['add'][0]['custom_fields'][] = [
                    'id' => 482909,
                    'values' => [
                        [
                            'value' => $phone,
                            'enum' => 'WORK'
                        ]
                    ]
                ];
            }

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'POST',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v2/contacts?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($arResult)
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    if( intval($result['_embedded']['items'][0]['id']) > 0 ){

                        return $result['_embedded']['items'][0]['id'];

                    } else {

                        tools\logger::addError( 'Ошибка добавления контакта в AMOCRM - не удалось получить id добавленного контакта' );
                        project\guzzle_request::log( $reqParams, $authCookies );
                    }

                } else {
                    tools\logger::addError( 'Ошибка добавления контакта в AMOCRM - код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'Ошибка добавления контакта в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ClientException $ex){

                tools\logger::addError( 'Ошибка добавления контакта в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( 'Ошибка добавления контакта в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }




    // Запрос контакта по ID
    static function getContactByID( $amo_contact_id ){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $authCookies = static::auth();

        if( $authCookies ){

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'GET',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v4/contacts/'.$amo_contact_id.'?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    return $result;

                } else {
                    tools\logger::addError( 'Ошибка запроса контакта в AMOCRM - код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'Ошибка запроса контакта в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ClientException $ex){

                tools\logger::addError( 'Ошибка запроса контакта в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( 'Ошибка запроса контакта в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }



    // Перечень контактов
    static function getContactList(){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $authCookies = static::auth();

        if( $authCookies ){

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'GET',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v2/contacts?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    return $result;

                } else {
                    tools\logger::addError( 'Ошибка запроса списка контактов в AMOCRM - код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'Ошибка запроса списка контактов в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ClientException $ex){

                tools\logger::addError( 'Ошибка запроса списка контактов в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( 'Ошибка запроса списка контактов в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }



    // Заказ
    static function addOrder(
        $order_id,
        $sum,
        $user_amo_id,
        $delivery_sum,
        $ds_name,
        $comment,
        $phone,
        $basket_disc_sum,
        $address = false
    ){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $authCookies = project\amo_crm::auth();

        if( $authCookies ){

            $arResult = [
                'add' => [
                    [
                        'name' => 'Заказ №'.$order_id,
                        'sale' => $sum,
                        'contacts_id' => [ $user_amo_id ],
                        'status_id' => "29328319",
                        'custom_fields' => [
                            // Стоимость товаров
                            [
                                'id' => 663109,
                                'values' => [
                                    [ 'value' => $basket_disc_sum ]
                                ],
                            ],
                            // Стоимость доставки
                            [
                                'id' => 685181,
                                'values' => [
                                    [ 'value' => $delivery_sum ]
                                ],
                            ],
                            // Вариант доставки
                            [
                                'id' => 663099,
                                'values' => [
                                    [ 'value' => $ds_name ]
                                ],
                            ],
                            // Адрес
                            [
                                'id' => 663251,
                                'values' => [
                                    [ 'value' => $address ]
                                ],
                            ],
                            // Контактный телефон
                            [
                                'id' => 665145,
                                'values' => [
                                    [ 'value' => $phone ]
                                ],
                            ],
                            // Комментарий к заказу
                            [
                                'id' => 486449,
                                'values' => [
                                    [ 'value' => $comment ]
                                ],
                            ],
                        ]
                    ]
                ]
            ];

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'POST',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v2/leads?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($arResult)
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    $sdelka_id = $result['_embedded']['items'][0]['id'];

                    if( intval($sdelka_id) > 0 ){

                        return $sdelka_id;

                    } else {

                        tools\logger::addError( 'Ошибка отправки заказа в AMOCRM - не удалось получить id добавленного заказа' );
                        project\guzzle_request::log( $reqParams, $authCookies );
                    }

                } else {
                    tools\logger::addError( 'Ошибка отправки заказа в AMOCRM - код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'Ошибка отправки заказа в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ClientException $ex){

                tools\logger::addError( 'Ошибка отправки заказа в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( 'Ошибка отправки заказа в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }



    // Примечание к заказу
    static function sendPrim( $sdelka_id, $amo_prim ){
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $authCookies = project\amo_crm::auth();

        if( $authCookies ){

            $arResult = [
                'add' => [
                    [
                        'element_id' => $sdelka_id,
                        'element_type' => 2,
                        'note_type' => 4,
                        'text' => $amo_prim
                    ]
                ]
            ];

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'POST',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v2/notes?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($arResult)
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    $id = $result['_embedded']['items'][0]['id'];

                    if( intval($id) > 0 ){
                        return $id;
                    } else {

                        tools\logger::addError( 'Ошибка отправки примечания в AMOCRM - не удалось получить id добавленного примечания' );
                        project\guzzle_request::log( $reqParams, $authCookies );
                    }

                } else {
                    tools\logger::addError( 'Ошибка отправки примечания в AMOCRM - код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'Ошибка отправки примечания в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ClientException $ex){

                tools\logger::addError( 'Ошибка отправки примечания в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( 'Ошибка отправки примечания в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }



    // Новая сделка
    static function addSdelka( $contact_id, $title ){

        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $log_prefix = 'Создание сделки в AMO CRM - ';

        $authCookies = project\amo_crm::auth();

        if( $authCookies ){

            $arResult = [
                'add' => [
                    [
                        'name' => $title,
                        'responsible_user_id' => 3276526,
                        'status_id' => 29718985,
                        'contacts_id' => [$contact_id]
                    ]
                ]
            ];

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'POST',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v2/leads?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($arResult)
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    $id = $result['_embedded']['items'][0]['id'];

                    if( intval($id) > 0 ){

                        return $id;

                    } else {

                        tools\logger::addError( $log_prefix.'не удалось получить id добавленной сделки' );
                        project\guzzle_request::log( $reqParams, $authCookies );
                    }

                } else {
                    tools\logger::addError( $log_prefix.'код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(ClientException $ex){

                tools\logger::addError( $log_prefix.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(RequestException $ex){

                tools\logger::addError( $log_prefix.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( $log_prefix.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }



    // Новая задача
    static function addTask( $sdelka_id, $comment = false ){
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $authCookies = project\amo_crm::auth();

        if( $authCookies ){

            $arResult = [
                'add' => [
                    [
                        'element_id' => $sdelka_id,
                        'element_type' => 2,
                        'complete_till' => MakeTimeStamp(date('d.m.Y').' 23:59:59', "DD.MM.YYYY HH:MI:SS"),
                        'task_type' => 1,
                        'text' => $comment
                    ]
                ]
            ];

            $cookies = [];
            $str = implode(';', $authCookies);
            $ar = explode(';', $str);
            foreach ( $ar as $key => $value ){
                $ar2 = explode('=', $value);
                $cookies[trim($ar2[0])] = trim($ar2[1]);
            }

            $reqParams = [
                'method' => 'POST',
                'url' => 'https://'.static::getAccount().'.amocrm.ru/api/v2/tasks?type=json&login='.static::getLogin().'&api_key='.static::getUserHash(),
                'params' => [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($arResult)
                ],
            ];

            $jar = CookieJar::fromArray($cookies, $_SERVER['HTTP_HOST']);

            $client = new Client(array(
                'timeout' => 5,
                'cookies' => $jar
            ));

            try {

                $response = $client->request(
                    $reqParams['method'],
                    $reqParams['url'],
                    $reqParams['params']
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $result = tools\funcs::json_to_array($content);

                    $id = $result['_embedded']['items'][0]['id'];

                    if( intval($id) > 0 ){

                        return $id;

                    } else {

                        tools\logger::addError( 'Ошибка отправки задачи в AMOCRM - не удалось получить id добавленной задачи' );
                        project\guzzle_request::log( $reqParams, $authCookies );
                    }

                } else {
                    tools\logger::addError( 'Ошибка отправки задачи в AMOCRM - код ответа '.$code );
                    project\guzzle_request::log( $reqParams, $authCookies );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'Ошибка отправки задачи в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ClientException $ex){

                tools\logger::addError( 'Ошибка отправки задачи в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );

            } catch(ConnectException $ex){

                tools\logger::addError( 'Ошибка отправки задачи в AMOCRM - '.$ex->getMessage() );
                project\guzzle_request::log( $reqParams, $authCookies );
            }

            return false;
        }

    }





    

}
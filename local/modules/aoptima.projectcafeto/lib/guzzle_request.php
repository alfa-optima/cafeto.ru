<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;


class guzzle_request {



    static function log( $reqParams, $cookies = null ){
        \Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
        $log_prefix = 'guzzle_request - ';
        if( isset($cookies) ){
            $reqParams['cookies'] = $cookies;
        }
        ob_start();
            print_r($reqParams);
            $string = ob_get_contents();
        ob_end_clean();
        $string .= "\n"."-----------------------------";
        \AOptima\ToolsCafeto\logger::addLog( $log_prefix.$string );
    }



}
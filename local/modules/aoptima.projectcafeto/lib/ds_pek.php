<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

use AOptima\ToolsCafeto as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class ds_pek extends ds {

    const API_KEY = '0D0C8F593587109A94E9E06FF8A0223DCA35E89A';
    const POINT_FROM = '-446';




    public function getLocations(){
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
        \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'pek_locations';
        $cache_path = '/pek_locations/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $list = [];

            $client = new Client(array( 'timeout' => 10 ));

            try {

                $response = $client->request(
                    'GET',
                    'https://pecom.ru/ru/calc/towns.php'
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $list = tools\funcs::json_to_array($content);

                } else {
                    tools\logger::addError( 'ПЕК - ответ сервера - '.$code );
                }

            } catch(RequestException $ex){

                tools\logger::addError( $ex->getMessage() );

            }

        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    public function getPrice( $basketInfo, $point_to_id = false ){

        if( $point_to_id ){

            $basketParams = project\ds::getBasketParams($basketInfo);

            require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
            \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');

            $client = new Client(array( 'timeout' => 10 ));

            try {

                $arRequest = [];

                $key = -1;
                foreach ( $basketParams['places'] as $basketParam ){ $key++;

                    // Ширина, м
                    $arRequest['places'][$key][] = round($basketParam['width']/1000, 3);
                    // Длина, м
                    $arRequest['places'][$key][] = round($basketParam['length']/1000, 3);
                    // Высота, м
                    $arRequest['places'][$key][] = round($basketParam['height']/1000, 3);
                    // Объём
                    $arRequest['places'][$key][] = false;
                    // Вес, кг
                    $arRequest['places'][$key][] = round($basketParam['weight']/100, 3);
                    // Негабарит
                    $arRequest['places'][$key][] = 0;
                    // ЗУ
                    $arRequest['places'][$key][] = 0;

                }

                $arRequest['take']['town'] = static::POINT_FROM;
                $arRequest['take']['tent'] = 0;
                $arRequest['take']['gidro'] = 0;
                $arRequest['take']['manip'] = 0;
                $arRequest['take']['speed'] = 0;
                $arRequest['take']['moscow'] = 0;

                $arRequest['deliver']['town'] = $point_to_id;
                $arRequest['deliver']['tent'] = 0;
                $arRequest['deliver']['gidro'] = 0;
                $arRequest['deliver']['manip'] = 0;
                $arRequest['deliver']['speed'] = 0;
                $arRequest['deliver']['moscow'] = 0;

                $arRequest['plombir'] = 0;
                $arRequest['strah'] = 0;
                $arRequest['ashan'] = 0;
                $arRequest['night'] = 0;
                $arRequest['pal'] = 0;
                $arRequest['pallets'] = 0;
                
                $getString = tools\funcs::array_to_get_string($arRequest);

                $response = $client->request(
                    'GET',
                    'http://calc.pecom.ru/bitrix/components/pecom/calc/ajax.php'.$getString
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $res = tools\funcs::json_to_array($content);
     
                    $price = 0;
                    $price += $res['take'][2]?$res['take'][2]:0;
                    $price += $res['auto'][2]?$res['auto'][2]:0;
                    $price += $res['deliver'][2]?$res['deliver'][2]:0;
                    $price += $res['ADD_1'][2]?$res['ADD_1'][2]:0;
                    $price += $res['ADD_2'][2]?$res['ADD_2'][2]:0;
                    $price += $res['ADD_3'][2]?$res['ADD_3'][2]:0;
                    $price += $res['ADD_4'][2]?$res['ADD_4'][2]:0;

                    return $price;

                } else {
                    tools\logger::addError( 'ПЕК - ответ сервера - '.$code );
                }

            } catch(RequestException $ex){

                tools\logger::addError( $ex->getMessage() );
            }
        }
        return false;
    }



    
}
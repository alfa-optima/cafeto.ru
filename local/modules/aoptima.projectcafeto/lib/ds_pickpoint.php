<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;

use AOptima\ToolsCafeto as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class ds_pickpoint extends ds {

    const LOGIN = '9Cf5h8';
    const PASSWORD = '0Je24Rs24Bo1';
    const IKN = '9990677212';
    const URL = 'https://e-solution.pickpoint.ru/api/';
    const FROM_CITY = 'Москва'; // 992



    public function auth(){
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
        \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');
        $result = false;
        $client = new Client(array( 'timeout' => 10 ));
        $send_body = json_encode([
            'Login' => static::LOGIN,
            'Password' => static::PASSWORD,
        ]);
        try {

            $response = $client->request(
                'POST',
                static::URL.'login',
                array(
                    'headers' => array(
                        'Content-Type' => 'application/json'
                    ),
                    'body' => $send_body
                )
            );

            $code = $response->getStatusCode();

            if( $code == 200 ){

                $body = $response->getBody();
                $content = $body->getContents();

                $result = tools\funcs::json_to_array($content);

                if( strlen($result['SessionId']) > 0 ){} else {

                    tools\logger::addError( 'pickpoint - авторизация - '.$result['ErrorMessage'].' ['.$result['ErrorCode'].']' );

                }

            } else {

                tools\logger::addError( 'pickpoint - ответ сервера - '.$code );
            }

        } catch(RequestException $ex){

            tools\logger::addError( 'pickpoint - авторизация - '.$ex->getMessage() );
        }
        return $result;
    }



    public function getLocations(){
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
        \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'pickpoint_locations';
        $cache_path = '/pickpoint_locations/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $list = [];

            $client = new Client(array( 'timeout' => 10 ));

            try {

                $response = $client->request(
                    'GET',
                    static::URL.'citylist',
                    array(
                        'headers' => array(
                            'Content-Type' => 'application/json'
                        ),
                    )
                );

                $code = $response->getStatusCode();

                if( $code == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $list = tools\funcs::json_to_array($content);

                } else {

                    tools\logger::addError( 'pickpoint - ответ сервера - '.$code );
                }

            } catch(RequestException $ex){

                tools\logger::addError( 'pickpoint - получение списка городов - '.$ex->getMessage() );
            }

        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    public function getTerminals(){
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
        \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 4*60*60;
        $cache_id = 'pickpoint_terminals';
        $cache_path = '/pickpoint_terminals/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $client = new Client(array( 'timeout' => 10 ));
            try {
                $response = $client->request(
                    'GET',
                    static::URL.'postamatlist',
                    array(
                        'headers' => array(
                            'Content-Type' => 'application/json'
                        ),
                    )
                );
                $code = $response->getStatusCode();
                if( $code == 200 ){
                    $body = $response->getBody();
                    $content = $body->getContents();
                    $list = tools\funcs::json_to_array($content);
                } else {
                    tools\logger::addError( 'pickpoint - ответ сервера - '.$code );
                }
            } catch(RequestException $ex){
                tools\logger::addError( 'pickpoint - получение списка терминалов - '.$ex->getMessage() );
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    public function getPrice( $basketInfo, $point_to_id = false ){

        $auth_res = $this->auth();

        if( strlen($auth_res['SessionId']) > 0 ){

            if( $point_to_id ){

                $basketParams = project\ds::getBasketParams($basketInfo);

                require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
                \Bitrix\Main\Loader::includeModule('aoptima.tools_cafe');

                $client = new Client(array( 'timeout' => 10 ));

                try {

                    $arRequest = [];

                    $key = -1;
                    foreach ( $basketParams['places'] as $basketParam ){ $key++;

                        // Ширина, м
                        $arRequest['places'][$key][] = round($basketParam['width']/1000, 3);
                        // Длина, м
                        $arRequest['places'][$key][] = round($basketParam['length']/1000, 3);
                        // Высота, м
                        $arRequest['places'][$key][] = round($basketParam['height']/1000, 3);
                        // Объём
                        $arRequest['places'][$key][] = false;
                        // Вес, кг
                        $arRequest['places'][$key][] = round($basketParam['weight']/100, 3);
                        // Негабарит
                        $arRequest['places'][$key][] = 0;
                        // ЗУ
                        $arRequest['places'][$key][] = 0;

                    }

                    $arRequest['take']['town'] = static::POINT_FROM;
                    $arRequest['take']['tent'] = 0;
                    $arRequest['take']['gidro'] = 0;
                    $arRequest['take']['manip'] = 0;
                    $arRequest['take']['speed'] = 0;
                    $arRequest['take']['moscow'] = 0;

                    $arRequest['deliver']['town'] = $point_to_id;
                    $arRequest['deliver']['tent'] = 0;
                    $arRequest['deliver']['gidro'] = 0;
                    $arRequest['deliver']['manip'] = 0;
                    $arRequest['deliver']['speed'] = 0;
                    $arRequest['deliver']['moscow'] = 0;

                    $arRequest['plombir'] = 0;
                    $arRequest['strah'] = 0;
                    $arRequest['ashan'] = 0;
                    $arRequest['night'] = 0;
                    $arRequest['pal'] = 0;
                    $arRequest['pallets'] = 0;

                    $getString = tools\funcs::array_to_get_string($arRequest);

                    $response = $client->request(
                        'GET',
                        'http://calc.pecom.ru/bitrix/components/pecom/calc/ajax.php'.$getString
                    );

                    $code = $response->getStatusCode();

                    if( $code == 200 ){

                        $body = $response->getBody();
                        $content = $body->getContents();

                        $res = tools\funcs::json_to_array($content);

                        $price = 0;
                        $price += $res['take'][2]?$res['take'][2]:0;
                        $price += $res['auto'][2]?$res['auto'][2]:0;
                        $price += $res['deliver'][2]?$res['deliver'][2]:0;
                        $price += $res['ADD_1'][2]?$res['ADD_1'][2]:0;
                        $price += $res['ADD_2'][2]?$res['ADD_2'][2]:0;
                        $price += $res['ADD_3'][2]?$res['ADD_3'][2]:0;
                        $price += $res['ADD_4'][2]?$res['ADD_4'][2]:0;

                        return $price;

                    } else {
                        tools\logger::addError( 'pickpoint - ответ сервера - '.$code );
                    }

                } catch(RequestException $ex){

                    tools\logger::addError( 'pickpoint - Ошибка расчёта цены - '.$ex->getMessage() );
                }
            }

        }

        return false;
    }


    
}
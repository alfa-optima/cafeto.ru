<?

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;


class product_review {

    const HIBLOCK_ID = 1;
    const MAX_CNT = 5;



    function __construct(){
        \CModule::IncludeModule('highloadblock');
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( static::HIBLOCK_ID )->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $this->hlblock );
        $this->entity = $entity;
        $this->entity_data_class = $entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    public function getList(
        $product_id = false,
        $user_id = false,
        $limit = 10,
        $stop_ids = [],
        $only_active = true
    ){
        $list = [];
        $arFilter = array();
        if( $only_active ){   $arFilter['UF_ACTIVE'] = 1;   }
        if( intval($product_id) > 0 ){    $arFilter['UF_PRODUCT'] = $product_id;    }
        if( intval($user_id) > 0 ){    $arFilter['UF_USER'] = $user_id;    }
        if ( is_array($stop_ids) && count($stop_ids) > 0 ){
            $arFilter['!ID'] = $stop_ids;
        }
        $arSelect = array( "ID", "UF_USER", "UF_PRODUCT", "UF_VOTE", "UF_REVIEW_TEXT", "UF_TIMESTAMP", "UF_ACTIVE" );
        $arOrder = array( "ID" => "DESC" );
        $entity_data_class = $this->entity_data_class;
        $rsData = $entity_data_class::getList([
            "select" => $arSelect,
            "filter" => $arFilter,
            "limit" => $limit,
            "order" => $arOrder
        ]);
        $rsData = new \CDBResult($rsData, $this->sTableID);
        while( $el = $rsData->Fetch() ){    $list[] = $el;    }

        return $list;
    }




    public function add( $arData ){
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            \AOptima\ToolsCafeto\logger::addError( implode(', ', $result->getErrors()) );
            return false;
        }
    }




    public function delete( $review_id ){
        $this->entity_data_class::delete($review_id);
        return true;
    }





}
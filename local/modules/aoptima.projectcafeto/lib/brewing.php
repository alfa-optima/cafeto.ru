<?php

namespace AOptima\ProjectCafeto;
use AOptima\ProjectCafeto as project;



class brewing {

    const IBLOCK_ID = 15;



    static function sections(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'brewing_sections';
        $cache_path = '/brewing_sections/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => static::IBLOCK_ID,
                "DEPTH_LEVEL" => 1
            );
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"), $filter, false
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['ID']] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    static function methodSteps( $method_id ){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'brewing_method_steps_'.$method_id;
        $cache_path = '/brewing_method_steps/'.$method_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = Array(
            	"IBLOCK_ID" => static::IBLOCK_ID,
            	"ACTIVE" => "Y",
            	"SECTION_ID" => $method_id
            );
            $fields = Array( "ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE" );
            $dbElements = \CIBlockElement::GetList(
            	array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




}
var keyupInterval;


$(document).ready(function(){



    $(document).on('keyup', '.add___table .number___input', function(){
        var reg = /[^\d]/g;
        var val = $(this).val();
        $(this).val( val.replace( reg , '' ) );
    })



    // Переход на страницу добавления записи
    $(document).on('click', '.to_add_page_button', function(){
        window.location.href = "/bitrix/admin/aoptima.projectcafeto_seo_pages.php?type=add";
    })
    // Переход на страницу добавления категории
    $(document).on('click', '.to_add_category_button', function(){
        window.location.href = "/bitrix/admin/aoptima.project_seo_pages_categories.php?type=add";
    })

    // Создание новой страницы
    $(document).on('click', '.seo_page_add___button', function(){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var NAME = $(this_form).find("input[name=NAME]").val();
            var FILTER_URL = $(this_form).find("input[name=FILTER_URL]").val();
            var H1 = $(this_form).find("input[name=H1]").val();
            var TITLE = $(this_form).find("input[name=TITLE]").val();
            var DESCRIPTION = $(this_form).find("input[name=DESCRIPTION]").val();
            var KEYWORDS = $(this_form).find("input[name=KEYWORDS]").val();
            var TEXT = $(this_form).find("textarea[name=TEXT]").val();
            var error = false;
            // Проверки
            if (NAME.length == 0){
                error = ['NAME', 'Введите, пожалуйста, название страницы!'];
            } else if (FILTER_URL.length == 0){
                error = ['FILTER_URL', 'Укажите, пожалуйста, URL фильтра!'];
            } else if (H1.length == 0){
                error = ['H1', 'Укажите, пожалуйста, заголовок H1 страницы!'];
            } else if (TITLE.length == 0){
                error = ['TITLE', 'Укажите, пожалуйста, Title страницы!'];
            } else if (TEXT.length == 0){
                error = ['TEXT', 'Введите, пожалуйста, текст для страницы!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var postParams = {
                    action: 'add',
                    form_data: $(this_form).serialize(),
                };
                $.post("/local/modules/aoptima.projectcafeto/install/admin/seo_pages/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        if( $(button).hasClass('prim') ){
                            window.location.href = "/bitrix/admin/aoptima.projectcafeto_seo_pages.php?type=update&id="+data.id;
                        } else {
                            window.location.href = "/bitrix/admin/aoptima.projectcafeto_seo_pages.php";
                        }
                    } else if (data.status == 'error'){
                        $('.error___p').html( data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    $('.error___p').html("Ошибка запроса");
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                $('.error___p').html(error[1]);
            }
        }
    });


    // Редактирование записи
    $(document).on('click', '.seo_page_update___button', function(){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var NAME = $(this_form).find("input[name=NAME]").val();
            var FILTER_URL = $(this_form).find("input[name=FILTER_URL]").val();
            var H1 = $(this_form).find("input[name=H1]").val();
            var TITLE = $(this_form).find("input[name=TITLE]").val();
            var DESCRIPTION = $(this_form).find("input[name=DESCRIPTION]").val();
            var KEYWORDS = $(this_form).find("input[name=KEYWORDS]").val();
            var TEXT = $(this_form).find("textarea[name=TEXT]").val();
            var error = false;
            // Проверки
            if (NAME.length == 0){
                error = ['NAME', 'Введите, пожалуйста, название страницы!'];
            } else if (FILTER_URL.length == 0){
                error = ['FILTER_URL', 'Укажите, пожалуйста, URL фильтра!'];
            } else if (H1.length == 0){
                error = ['H1', 'Укажите, пожалуйста, заголовок H1 страницы!'];
            } else if (TITLE.length == 0){
                error = ['TITLE', 'Укажите, пожалуйста, Title страницы!'];
            } else if (TEXT.length == 0){
                error = ['TEXT', 'Введите, пожалуйста, текст для страницы!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var postParams = {
                    action: 'update',
                    form_data: $(this_form).serialize(),
                };
                $.post("/local/modules/aoptima.projectcafeto/install/admin/seo_pages/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        if( $(button).hasClass('prim') ){
                            window.location.href = document.URL;
                        } else {
                            window.location.href = "/bitrix/admin/aoptima.projectcafeto_seo_pages.php";
                        }
                    } else if (data.status == 'error'){
                        $('.error___p').html( data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    $('.error___p').html("Ошибка запроса");
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                $('.error___p').html(error[1]);
            }
        }
    });

    // Удаление элемента
    $(document).on('click', '.delete___seo_page_button', function(){
        var button = $(this);
        if( !is_process(button) ){

            var item_id = $(button).attr('item_id');

            if (confirm("Вы действительно хотите удалить данный элемент?")) {

                process(true);
                var postParams = {
                    action: 'delete',
                    item_id: item_id,
                };
                process(true);
                $.post("/local/modules/aoptima.projectcafeto/install/admin/seo_pages/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){

                        window.location.href = document.URL;

                    } else if (data.status == 'error'){
                        alert( data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    alert( 'Ошибка запроса' );
                })
                .always(function(data, status, xhr){
                    process(false);
                })

            }

        }

    })



})
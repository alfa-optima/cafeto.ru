<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;



if( $USER->IsAdmin() ){



    // Создание страницы
    if( $_POST['action'] == 'add' ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arFields['FILTER_URL'] = trim($arFields['FILTER_URL']);

        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ 'ID', 'FILTER_URL' ],
            'filter' => [ 'FILTER_URL' => $arFields['FILTER_URL'] ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            // Ответ
            echo json_encode(["status" => "error", "text" => "Страница с таким URL уже создана ранее"]);
            return;
        }
        
        $id = project\SeopageTable::addItem($arFields);
        if( intval($id) > 0 ){

            BXClearCache(true, "/all_seo_pages/");

            // Ответ
            echo json_encode(["status" => "ok", "id" => $id]);
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }

    }


    // Редактирование
    if( $_POST['action'] == 'update' ){
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arFields['FILTER_URL'] = trim($arFields['FILTER_URL']);

        $item_id = $arFields['item_id'];
        unset($arFields['item_id']);

        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ 'ID', 'FILTER_URL' ],
            'filter' => [ 'FILTER_URL' => $arFields['FILTER_URL'], '!ID' => $item_id ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            // Ответ
            echo json_encode(["status" => "error", "text" => "Страница с таким URL уже создана ранее"]);
            return;
        }

        $res = project\SeopageTable::updateItem($item_id, $arFields);
        if( $res ){

            BXClearCache(true, "/all_seo_pages/");

            // Ответ
            echo json_encode(["status" => "ok"]);
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }
    }



    // Удаление
    if( $_POST['action'] == 'delete' ){

        $item_id = strip_tags($_POST['item_id']);

        if ( intval($item_id) > 0 ){
            $result = project\SeopageTable::delete($item_id);
        }

        if( intval($item_id) > 0 && $result ){

            BXClearCache(true, "/all_seo_pages/");

            // Ответ
            echo json_encode(["status" => "ok"]);
            return;

        } else {

            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка удаления"]);
            return;

        }

    }







    // Ответ
    echo json_encode(Array("status" => "error", "text" => "Ошибка данных"));
    return;


}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
return;
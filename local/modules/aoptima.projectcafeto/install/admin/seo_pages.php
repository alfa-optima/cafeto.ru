<? use Bitrix\Main,  Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$CURRENCY_RIGHT = $APPLICATION->GetGroupRight("aoptima.projectcafeto");

if ($CURRENCY_RIGHT=="D") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('iblock');


// Автосоздание таблиц в БД
project\SeopageTable::checkTable(); ?>


<link rel="stylesheet" href="/local/modules/aoptima.projectcafeto/install/admin/seo_pages/styles.css?v=<?=time()?>">


<? // Новая запись
if( $_GET['type'] == 'add' ){

    $APPLICATION->SetTitle('Создание SEO-страницы'); ?>

    <form onsubmit="return false;">
        <table class="add___table">
            <tr>
                <th class="first_td">Поле</th>
                <th>Значение</th>
            </tr>
            <tr>
                <td class="first_td">Название страницы</td>
                <td>
                    <div class="inputs_block">
                        <input type="text" name="NAME">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="first_td">URL фильтра</td>
                <td>
                    <div class="inputs_block">
                        <input type="text" name="FILTER_URL">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="first_td">H1</td>
                <td>
                    <div class="inputs_block">
                        <input type="text" name="H1">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="first_td">Title</td>
                <td>
                    <div class="inputs_block">
                        <input type="text" name="TITLE">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="first_td">Description</td>
                <td>
                    <div class="inputs_block">
                        <input type="text" name="DESCRIPTION">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="first_td">Keywords</td>
                <td>
                    <div class="inputs_block">
                        <input type="text" name="KEYWORDS">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="first_td">Текст страницы</td>
                <td>
                    <div class="inputs_block">
                        <textarea name="TEXT"></textarea>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2"><p class="error___p"></p></td>
            </tr>
            <tr>
                <td colspan="2" class="buttons_td">
                    <input class="adm-btn-save seo_page_add___button to___process" type="button" value="Сохранить">
                    <input class="prim seo_page_add___button to___process" type="button" value="Применить">
                    <input type="button" value="Отмена" onclick="window.location.href = '/bitrix/admin/aoptima.project_seo_pages.php';">
                </td>
            </tr>
        </table>
    </form>

<? // Редактирование записи
} else if(
    $_GET['type'] == 'update'
    &&
    intval($_GET['id']) > 0
){


    $APPLICATION->SetTitle('Редактирование SEO-страницы');

    // Ищём запись по ID
    $res = project\SeopageTable::getList([
        'select' => [
            'ID', 'FILTER_URL', 'NAME', 'H1', 'TITLE', 'DESCRIPTION', 'KEYWORDS', 'TEXT'
        ],
        'filter' => [ 'ID' => $_GET['id'] ],
        'limit' => 1,
    ]);

    // Если запись найдена
    if( $element = $res->fetch() ){ ?>

        <form onsubmit="return false;">
            <table class="add___table">
                <tr>
                    <th class="first_td">Поле</th>
                    <th>Значение</th>
                </tr>
                <tr>
                    <td class="first_td">Название страницы</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="NAME" value="<?=$element['NAME']?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">URL фильтра</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="FILTER_URL" value="<?=$element['FILTER_URL']?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">H1</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="H1" value="<?=$element['H1']?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Title</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="TITLE" value="<?=$element['TITLE']?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Description</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="DESCRIPTION" value="<?=$element['DESCRIPTION']?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Keywords</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="KEYWORDS" value="<?=$element['KEYWORDS']?>">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Текст страницы</td>
                    <td>
                        <div class="inputs_block">
                            <textarea name="TEXT"><?=$element['TEXT']?></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><p class="error___p"></p></td>
                </tr>
                <tr>
                    <td colspan="2" class="buttons_td">
                        <input class="adm-btn-save seo_page_update___button to___process" type="button" value="Сохранить">
                        <input class="prim seo_page_update___button to___process" type="button" value="Применить">
                        <input type="button" value="Отмена" onclick="window.location.href = '/bitrix/admin/aoptima.project_seo_pages.php';">
                    </td>
                </tr>
            </table>

            <input type="hidden" name="item_id" value="<?=$element['ID']?>">

        </form>

    <? } else {

        echo BeginNote();
        echo "<b>Запись не найдена</b>";
        echo EndNote();

    } ?>


<? // Список записей
} else {


    if( !isset($_GET['PAGEN_1']) ){
        LocalRedirect( '/bitrix/admin/aoptima.projectcafeto_seo_pages.php?PAGEN_1=1' );
    }

    $APPLICATION->SetTitle('SEO-страницы');

    $list = [];
    $elements = project\SeopageTable::getList([
        'select' => [
            'ID', 'FILTER_URL', 'NAME'
        ],
        'order' => array('ID' => 'DESC'),
    ]);
    while( $element = $elements->fetch() ){
        $list[$element['ID']] =  $element;
    } ?>

    <div style="margin-bottom: 20px;">
        <input class="adm-btn-save to_add_page_button" type="button" value="Добавить страницу">
    </div>

    <? if( count($list) > 0 ){

        $r_elements = new CDBResult;
        $r_elements->InitFromArray($list);
        $r_elements->NavStart(20);
        $NAV_STRING = $r_elements->GetPageNavStringEx($navComponentObject, false, false); ?>

        <table class="list___table">

            <tr>
                <th class="first_td">ID</th>
                <th>Название страницы</th>
                <th>URL фильтра</th>
                <th class="buttons_td"></th>
                <th class="buttons_td"></th>
            </tr>

            <? while ( $item = $r_elements->Fetch() ){ ?>

                <tr class="iblock_tr" iblock_id="<?=$iblock['ID']?>">
                    <td class="first_td">
                        <?=$item['ID']?>
                    </td>
                    <td>
                        <?=$item['NAME']?>
                    </td>
                    <td>
                        <?=$item['FILTER_URL']?>
                    </td>
                    <td class="buttons_td">
                        <a href="?type=update&id=<?=$item['ID']?>">Редактировать</a>
                    </td>
                    <td class="buttons_td">
                        <a class="delete___seo_page_button to___process" style="cursor: pointer" item_id="<?=$item['ID']?>">Удалить</a>
                    </td>
                </tr>

            <? } ?>

        </table>


        <? echo $NAV_STRING; ?>


    <? } else {

        echo BeginNote();
        echo "<b>На данный момент не создано ни одной SEO-страницы</b>";
        echo EndNote();

    } ?>

<? } ?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="/local/modules/aoptima.projectcafeto/install/admin/seo_pages/funcs.js?v=<?=time()?>"></script>
<script type="text/javascript" src="/local/modules/aoptima.projectcafeto/install/admin/seo_pages/scripts.js?v=<?=time()?>"></script>



<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
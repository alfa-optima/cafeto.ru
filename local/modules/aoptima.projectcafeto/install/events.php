<?php

// Массив событий модуля.


$MODULE_EVENTS = array(

    array(
        'iblock',
        'OnAfterIBlockAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnAfterIBlockUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnIBlockDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnIBlockDelete',
        100
    ),

    array(
        'iblock',
        'OnBeforeIBlockAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockAdd',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockUpdate',
        100
    ),
    array(
        'iblock',
        'OnBeforeIBlockDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockDelete',
        100
    ),





    array(
        'iblock',
        'OnAfterIBlockElementAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockElementDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockElementDelete',
        100,
    ),


    array(
        'iblock',
        'OnAfterIBlockSectionAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterIBlockSectionDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeIBlockSectionDelete',
        100,
    ),



    array(
        'catalog',
        'OnSuccessCatalogImport1C',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnSuccessCatalogImport1C',
        100,
    ),



    array(
        'main',
        'OnAfterUserAdd',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterUserAdd',
        100,
    ),
    array(
        'main',
        'OnAfterUserUpdate',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnAfterUserUpdate',
        100,
    ),
    array(
        'main',
        'OnBeforeUserDelete',
        'aoptima.projectcafeto',
        '\AOptima\ProjectCafeto\handlers',
        'OnBeforeUserDelete',
        100,
    ),





);


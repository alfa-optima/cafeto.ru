<?php

$aoptima_project_default_option = array(
	'COMPANY_NAME' => 'ООО "Компания"',
	'MAP_POINT_LAT' => '',
	'MAP_POINT_LNG' => '',
	'CONTACTS_PHONES' => '',
	'CONTACTS_ADDRESS' => '',
	'CONTACTS_EMAIL' => '',
	'FEEDBACK_EMAIL_TO' => '',
	'SITE_PHONE' => '',
	'SITE_ADDRESS' => '',
	'SITE_EMAIL' => '',
	'VK_LINK' => '',
	'FB_LINK' => '',
	'TW_LINK' => '',
	'INSTA_LINK' => '',
	'GOOGLE_RECAPTCHA2_KEY' => '6LcH1rcUAAAAAETY5BCdKaPQy9880DqHGjJ5ZUTI',
	'GOOGLE_RECAPTCHA2_SECRET_KEY' => '6LcH1rcUAAAAAJ8fz2s0DRyDmjxLilP3QPyBE_Rw',
	'ORDER_EMAIL' => 'sales@cafeto.ru',
	'DELIVERY_PRICE_DISCOUNT' => 0,

	'AMO_ACCOUNT' => '',
	'AMO_LOGIN' => '',
	'AMO_USER_HASH' => '',

);
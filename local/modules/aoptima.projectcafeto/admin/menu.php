<?
/** @global CMain$APPLICATION */
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


if ($APPLICATION->GetGroupRight('currency') == 'D')
    return false;



return array(
    "parent_menu" => "global_menu_content",
    "section" => "aoptima_project",
    "sort" => 1,
    "text" => 'SEO-страницы (cafeto.ru)',
    "title" => 'SEO-страницы (cafeto.ru)',
    // url - при необходимости
    //"url" => "aoptima.tools_test.php",
    "items_id" => "menu_aoptima_project",
    "url" => "aoptima.projectcafeto_seo_pages.php"
);

<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'aoptima.projectcafeto';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ( $APPLICATION->GetGroupRight($module_id) < "S" ){
    $APPLICATION->AuthForm('Access denied');
}

\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    
	array(
        'DIV' => 'edit1',
        'TAB' => 'Основное',
        'OPTIONS' => array(
            array(
				'SITE_PHONE',
				'Телефон на сайте',
                '',
                array('text', 38)
			),
            array(
                'SITE_ADDRESS',
                'Адрес на сайте',
                '',
                array('text', 38)
            ),
            array(
                'SITE_EMAIL',
                'Email на сайте',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit2',
        'TAB' => "Соц. сети",
        'OPTIONS' => array(
            array(
                'VK_LINK',
                'группа Вконтакте (ссылка)',
                '',
                array('text', 38)
            ),
            array(
                'FB_LINK',
                'группа Facebook (ссылка)',
                '',
                array('text', 38)
            ),
            array(
                'TW_LINK',
                'группа Twitter (ссылка)',
                '',
                array('text', 38)
            ),
            array(
                'INSTA_LINK',
                'группа Instagram (ссылка)',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit3',
        'TAB' => "Контакты",
        'OPTIONS' => array(
            array(
                'MAP_POINT_LAT',
                'Точка на карте (широта)',
                '',
                array('text', 38)
            ),
            array(
                'MAP_POINT_LNG',
                'Точка на карте (долгота)',
                '',
                array('text', 38)
            ),
            array(
                'CONTACTS_PHONES',
                'Телефоны',
                '',
                array('text', 38)
            ),
            array(
                'CONTACTS_ADDRESS',
                'Адрес',
                '',
                array('text', 38)
            ),
            array(
                'CONTACTS_EMAIL',
                'Email',
                '',
                array('text', 38)
            ),
            array(
                'FEEDBACK_EMAIL_TO',
                'Email для сообщений из формы обратной связи',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit4',
        'TAB' => "Google",
        'OPTIONS' => array(
            array(
                'GOOGLE_RECAPTCHA2_KEY',
                'Ключ для Google Recaptcha2',
                '6LcH1rcUAAAAAETY5BCdKaPQy9880DqHGjJ5ZUTI',
                array('text', 50)
            ),
            array(
                'GOOGLE_RECAPTCHA2_SECRET_KEY',
                'Секрет. ключ для Google Recaptcha2',
                '6LcH1rcUAAAAAJ8fz2s0DRyDmjxLilP3QPyBE_Rw',
                array('text', 50)
            ),
        )
    ),

    array(
        'DIV' => 'edit5',
        'TAB' => "Заказ",
        'OPTIONS' => array(
            array(
                'ORDER_EMAIL',
                'Email для заказов',
                'sales@cafeto.ru',
                array('text', 50)
            ),
            array(
                'DELIVERY_PRICE_DISCOUNT',
                'Скидка на стоимость доставки, %',
                0,
                array('text', 50)
            ),
        )
    ),

    array(
        'DIV' => 'edit6',
        'TAB' => "Amo CRM",
        'OPTIONS' => array(
            array(
                'AMO_ACCOUNT',
                'Account',
                '',
                array('text', 50)
            ),
            array(
                'AMO_LOGIN',
                'Login',
                '',
                array('text', 50)
            ),
            array(
                'AMO_USER_HASH',
                'User hash',
                '',
                array('text', 50)
            ),
        )
    ),

);

if ($APPLICATION->GetGroupRight($module_id)=="W"){
	
	$aTabs[] = array(
        "DIV" => "edit100",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    );
	
}


#Сохранение
if ( 
	$request->isPost()
	&&
	$request['Update']
	&&
	check_bitrix_sessid()
){

    foreach ($aTabs as $aTab){
		
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption){
			
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
			
        }
    }
	
}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>

<? $tabControl->Begin(); ?>

<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='AOPTIMA_PROJECT_settings'>

    <? foreach ($aTabs as $aTab):
		if($aTab['OPTIONS']):?>
			<? $tabControl->BeginNextTab(); ?>
			<? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>
		<? endif;
     endforeach; ?>

    <? $tabControl->BeginNextTab();
	
	if ($APPLICATION->GetGroupRight($module_id)=="W"){
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
	}


    $tabControl->Buttons(); ?>

    <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
	
    <?=bitrix_sessid_post();?>
	
</form>

<? $tabControl->End(); ?>


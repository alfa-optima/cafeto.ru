<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<form class="form feedback___form" onsubmit="return false;">
    <div class="row align-items-stretch">
        <div class="col-12 col-lg-6">
            <div class="form__row">
                <input type="text" class="form__input" name="name" placeholder="Имя">
            </div>
            <div class="form__row">
                <input type="tel" class="form__input" name="phone" placeholder="Телефон">
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form__row form__row_s">
                <textarea class="form__input form__textarea form__textarea_s" name="message" placeholder="Комментарий"></textarea>
            </div>
        </div>
    </div>

    <div class="form__row">

        <label class="sort__value form___label" style="white-space: unset;">
            <input type="checkbox" checked value="Y" name="agree" class="form___checkbox">
            <p class="sort__value-link link sort__value-link_s">Я принимаю <a href="/oferta/" target="_blank">Условия использования</a></p>
        </label>

    </div>

    <div class="recaptcha___block">
        <div class="g-recaptcha" data-sitekey="<?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'GOOGLE_RECAPTCHA2_KEY')?>"></div>
    </div>

    <p class="error___p"></p>

    <button type="button" class="btn contacts__form-btn feedback___button to___process">Отправить</button>

</form>


<script src="https://www.google.com/recaptcha/api.js"></script>

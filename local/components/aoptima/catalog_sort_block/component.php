<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$arResult['ITEMS'] = project\catalog::getSortVariants();

global $APPLICATION;
$arResult['CURRENT_SORT'] = $APPLICATION->get_cookie('sort');

if(
    strlen($arResult['CURRENT_SORT']) > 0
    &&
    $arResult['ITEMS'][$arResult['CURRENT_SORT']]
){} else {
    $arResult['CURRENT_SORT'] = array_keys($arResult['ITEMS'])[0];
}


$arResult['SORT_FIELD'] = explode('|', $arResult['CURRENT_SORT'])[0];
$arResult['SORT_ORDER'] = explode('|', $arResult['CURRENT_SORT'])[1];


$this->IncludeComponentTemplate();

return [
    'SORT_FIELD' => $arResult['SORT_FIELD'],
    'SORT_ORDER' => $arResult['SORT_ORDER'],
];
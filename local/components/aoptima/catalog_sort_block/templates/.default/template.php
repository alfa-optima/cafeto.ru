<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<div class="sort__grid sortBlock">

    <p class="sort__name">Сортировка<span><?=$arResult['ITEMS'][$arResult['CURRENT_SORT']]['NAME']?></span></p>

    <div class="sort__list">

        <div class="sort__list-wrapper">

            <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

                <a style="cursor: pointer" class="link sort__value sortButton to___process" code="<?=$arItem['CODE']?>"><?=$arItem['NAME']?></a>

            <? } ?>

        </div>

    </div>

</div>
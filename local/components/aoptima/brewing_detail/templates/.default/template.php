<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools; ?>


<div class="template default-template method">

    <div class="container">
        <div class="row">

            <? // Хлебные крошки
            $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb", "page",
                Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
            ); ?>

        </div>
    </div>

    <div class="template__grid">

        <? if( intval($arResult['DETAIL_PICTURE']) > 0 ){ ?>

            <header class="method__header"
                    style="background: url(<?=\CFile::GetPath($arResult['DETAIL_PICTURE'])?>) no-repeat 50%; background-size: cover;">
                <h1 class="title template__title method__title">Аэропресс</h1>
            </header>

        <? } ?>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="method__grid">
                        <div class="row justify-content-center">
                            <? if( strlen($arResult['UF_TIME']) > 0 ){ ?>
                                <div class="col-6 col-lg-4">
                                    <div class="method__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/icons/icon-method-1.png" class="method__item-icon" alt="alt">
                                        <p class="method__item-title">Время приготовления:</p>
                                        <p class="method__item-value">2 мин 30 с</p>
                                    </div>
                                </div>
                            <? } ?>
                            <? if(
                                is_array($arResult['UF_NEED'])
                                &&
                                count($arResult['UF_NEED']) > 0
                            ){ ?>
                                <div class="col-6 col-lg-4">
                                    <div class="method__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/icons/icon-method-2.png" class="method__item-icon" alt="alt">
                                        <p class="method__item-title">Вам понадобится:</p>
                                        <? foreach( $arResult['UF_NEED'] as $need ){ ?>
                                            <p class="method__item-value"><?=$need?></p>
                                        <? } ?>
                                    </div>
                                </div>
                            <? } ?>
                        </div>
                    </div>

                    <? if( count($arResult['STEPS']) > 0 ){ ?>

                        <div class="method__steps">

                            <? $cnt = 0;
                            foreach( $arResult['STEPS'] as $key => $step ){ $cnt++;

                                if( $cnt%2 == 1 ){ ?>

                                    <div class="method__step">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-lg-6">
                                                <img src="<?=tools\funcs::rIMGG($step['PREVIEW_PICTURE'], 5, 800, 600)?>" class="method__step-img" alt="alt">
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="method__step-info">
                                                    <p class="method__step-name"><?=$step['NAME']?></p>
                                                    <p class="method__step-text"><?=$step['PREVIEW_TEXT']?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <? } else { ?>

                                    <div class="method__step">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-lg-6">
                                                <div class="method__step-info method__step-info_s">
                                                    <p class="method__step-name"><?=$step['NAME']?></p>
                                                    <p class="method__step-text"><?=$step['PREVIEW_TEXT']?></p>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <img src="<?=tools\funcs::rIMGG($step['PREVIEW_PICTURE'], 5, 800, 600)?>" class="method__step-img" alt="alt">
                                            </div>
                                        </div>
                                    </div>

                                <? }

                            } ?>

                        </div>

                    <? } ?>

                </div>
            </div>
        </div>

    </div>

</div>
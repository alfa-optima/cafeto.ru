<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$code = strip_tags($arParams['code']);

$arResult = tools\section::info_by_code($code, project\brewing::IBLOCK_ID);

if( intval($arResult['ID']) > 0 ){

    $APPLICATION->SetPageProperty("title", $arResult['NAME']);
    $APPLICATION->AddChainItem('Методы заваривания', '/brewing/');
    $APPLICATION->AddChainItem($arResult['NAME'], '/brewing/'.$arResult['CODE'].'/');

    $arResult['STEPS'] = project\brewing::methodSteps($arResult['ID']);




    $this->IncludeComponentTemplate();

} else {

    include( $_SERVER[ "DOCUMENT_ROOT" ] . "/local/templates/main/include/404include.php" );

}





<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<div class="template page personal">

    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "catalog",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

            </div>
        </div>
    </div>

    <div class="personal__grid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="personal__wrapper">
                        <div class="row">

                             <div class="col-12 col-lg-9 col-xl-8">
                                <div class="personal__content password_update_block">

                                    <h1 class="title personal__title">Восстановление пароля</h1>

                                    <? if( !$arResult['ERROR'] ){ ?>

                                        <div class="personal__list" style="margin-bottom: 45px;">
                                            <form class="edit-personal-form" onsubmit="return false;">

                                                <input name="PASSWORD" type="password" class="personal__list-item" placeholder="Новый пароль">

                                                <input name="CONFIRM_PASSWORD" type="password" class="personal__list-item" placeholder="Подтверждение пароля">

                                                <input type="hidden" name="code" value="<?=$arResult['KOD']?>">

                                                <p class="error___p"></p>

                                                <button type="button" class="btn edit-personal-form__btn password___recovery_save_button to___process">Сохранить</button>

                                            </form>
                                        </div>

                                    <? } else { ?>

                                        <p class="error___p"><?=$arResult['ERROR']?></p>

                                    <? } ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
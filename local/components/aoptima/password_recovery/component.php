<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if( $USER->IsAuthorized() ){    LocalRedirect('/personal/');    }

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';
$arResult['IS_ADMIN'] = $USER->IsAdmin()?'Y':'N';

$arResult['KOD'] = strip_tags($_GET['code']);

if( strlen($arResult['KOD']) > 0 ){

    $user_id = false;

    // Ищем пользователя с таким кодом
    $filter = Array("=UF_RECOVERY_CODE" => $arResult['KOD']);
    $rsUsers = CUser::GetList(($by = "id"), ($order = "desc"), $filter, array('FIELDS' => array('ID'), 'SELECT' => array('UF_*')));
    if ($arUser = $rsUsers->GetNext()) {    $user_id = $arUser['ID'];    }

    if( !$user_id ){
        $arResult['ERROR'] = 'Ссылка ошибочная либо устарела';
    }

} else {

    $arResult['ERROR'] = 'Ошибочная ссылка';
}





$this->IncludeComponentTemplate();
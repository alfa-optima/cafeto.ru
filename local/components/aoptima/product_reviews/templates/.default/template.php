<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$maxCNT = project\product_review::MAX_CNT;

if( $arParams['IS_AJAX'] == 'Y' ){

    if( count($arResult['REVIEWS']) > 0 ){

        $cnt = 0;
        foreach( $arResult['REVIEWS'] as $review ){ $cnt++;
            if( $cnt <= $maxCNT ){ ?>

                <div class="testimonial-item review___item" item_id="<?=$review['ID']?>">

                    <div class="stars testimonial-item__stars">
                        <div class="stars__grid">
                            <div class="stars__wrapper">
                                <? for( $v = 1; $v <= 5; $v++ ){ ?>
                                    <div class="stars__icon <? if( $review['UF_VOTE'] >= $v ){ echo 'selected'; } ?>">
                                        <svg viewBox="0 0 47.94 47.94" width="15" height="15">
                                            <path fill="#ddd" d="M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757 c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042 c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685 c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528 c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956 C22.602,0.567,25.338,0.567,26.285,2.486z" />
                                        </svg>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>

                    <p class="testimonial-item__name"><?=$review['USER']['NAME']?></p>

                    <? if( intval($review['UF_TIMESTAMP']) > 0 ){ ?>
                        <p class="testimonial-item__date"><?=date("d.m.Y H:i", $review['UF_TIMESTAMP'])?></p>
                    <? } ?>

                    <div class="testimonial-item__text text">
                        <p><?=$review['UF_REVIEW_TEXT']?></p>
                    </div>

                </div>

            <? } else { echo '<ost></ost>'; }
        }

    } ?>

<? } else { ?>

    <div class="testimonials">

        <? if( count($arResult['REVIEWS']) > 0 ){

            $cnt = 0;
            foreach( $arResult['REVIEWS'] as $review ){ $cnt++;
                if( $cnt <= $maxCNT ){ ?>

                    <div class="testimonial-item review___item" item_id="<?=$review['ID']?>">

                        <div class="stars testimonial-item__stars">
                            <div class="stars__grid">
                                <div class="stars__wrapper">
                                    <? for( $v = 1; $v <= 5; $v++ ){ ?>
                                        <div class="stars__icon <? if( $review['UF_VOTE'] >= $v ){ echo 'selected'; } ?>">
                                            <svg viewBox="0 0 47.94 47.94" width="15" height="15">
                                                <path fill="#ddd" d="M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757 c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042 c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685 c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528 c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956 C22.602,0.567,25.338,0.567,26.285,2.486z" />
                                            </svg>
                                        </div>
                                    <? } ?>
                                </div>
                            </div>
                        </div>

                        <p class="testimonial-item__name"><?=$review['USER']['NAME']?></p>

                        <? if( intval($review['UF_TIMESTAMP']) > 0 ){ ?>
                            <p class="testimonial-item__date"><?=date("d.m.Y H:i", $review['UF_TIMESTAMP'])?></p>
                        <? } ?>

                        <div class="testimonial-item__text text">
                            <p><?=$review['UF_REVIEW_TEXT']?></p>
                        </div>

                    </div>

                <? }
            } ?>

            <div class="reviewsLoadBlock" <? if( $cnt <= $maxCNT ){ ?>style="display:none;"<? } ?>>
                <button type="button" class="btn productReviewsLoadButton to___process">Показать ещё</button>
            </div>

        <? } else { ?>

            <div class="testimonial-item">
                <div class="testimonial-item__text text">
                    <p style="color:gray; font-style: italic;">Отзывов пока нет</p>
                </div>
            </div>

        <? } ?>

        <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

            <button type="button" data-modal="#testimonial" class="btn testimonials__btn">Оставить отзыв</button>

        <? } ?>

    </div>

<? } ?>
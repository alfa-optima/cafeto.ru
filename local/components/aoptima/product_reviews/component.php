<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';

$product_review = new project\product_review();
$arResult['REVIEWS'] = $product_review->getList(
    $arParams['product_id'], false,
    project\product_review::MAX_CNT + 1, $_POST['stop_ids']
);

foreach ( $arResult['REVIEWS'] as $key => $review ){
    if( intval($review['UF_USER']) > 0 ){
        $arResult['REVIEWS'][$key]['USER'] = tools\user::info($review['UF_USER']);
    }
}


$this->IncludeComponentTemplate();
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<div class="template page personal">

    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "catalog",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

            </div>
        </div>
    </div>

    <div class="personal__grid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="personal__wrapper">
                        <div class="row">

                            <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

                                <div class="col-12 col-lg-3 col-xl-2 offset-xl-1">
                                    <aside class="personal-menu">
                                        <nav class="personal-menu__grid">
                                            <? // personal
                                            $APPLICATION->IncludeComponent(
                                                "bitrix:menu",  "personal", // Шаблон меню
                                                Array(
                                                    "ROOT_MENU_TYPE" => "personal", // Тип меню
                                                    "MENU_CACHE_TYPE" => "A",
                                                    "MENU_CACHE_TIME" => "3600",
                                                    "MENU_CACHE_USE_GROUPS" => "N",
                                                    "CACHE_SELECTED_ITEMS" => "N",
                                                    "MENU_CACHE_GET_VARS" => array(""),
                                                    "MAX_LEVEL" => "1",
                                                    "CHILD_MENU_TYPE" => "left",
                                                    "USE_EXT" => "N",
                                                    "DELAY" => "N",
                                                    "ALLOW_MULTI_SELECT" => "N"
                                                )
                                            ); ?>
                                            <a href="/out.php?to=<?=$_SERVER['REQUEST_URI']?>" class="personal-menu__link">Выход</a>
                                        </nav>
                                    </aside>
                                </div>

                                <div class="col-12 col-lg-9 col-xl-8">
                                    <div class="personal__content">

                                        <h1 class="title personal__title">Личный кабинет</h1>

                                        <div class="personal__list">

                                            <p class="personal__list-item"><?=$arResult['USER']['NAME']?></p>

                                            <? if( $arResult['USER']['PERSONAL_PHONE'] ){ ?>
                                                <p class="personal__list-item"><?=$arResult['USER']['PERSONAL_PHONE']?></p>
                                            <? } ?>

                                            <p class="personal__list-item"><?=$arResult['USER']['EMAIL']?></p>

                                            <!--<p class="personal__list-item">Минск</p>-->

                                            <? if( $arResult['ACCUM_DISCOUNT'] ){ ?>

                                                <p class="personal__list-item">Персональная скидка: <?=$arResult['ACCUM_DISCOUNT']?></p>

                                            <? } else { ?>

                                                <p class="personal__list-item">Персональная скидка: нет</p>

                                            <? } ?>


                                            <!--<p class="personal__list-item">Подписка на рассылку <a href="#">Изменить</a></p>-->

                                        </div>
                                    </div>
                                </div>


                            <? } else {

                                include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_auth_form.php';

                            } ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
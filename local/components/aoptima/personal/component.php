<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;
\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';
$arResult['IS_ADMIN'] = $USER->IsAdmin()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){

    $arResult['USER'] = tools\user::info( $USER->GetID() );

    $arResult['ACCUM_DISCOUNT'] = project\accum_discount::get();



}






$this->IncludeComponentTemplate();
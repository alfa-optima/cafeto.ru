<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project; ?>


<? if( in_array( $arResult['PS']['CODE'], [ 'card' ] ) ){ ?>


    <div class="template page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="template__grid payment-loader">
                        <svg width="150px" height="150px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-eclipse"
                             style="background-image: none; background-position: initial initial; background-repeat: initial initial;">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M5 50A45 45 0 0 0 95 50A45 48 0 0 1 5 50" fill="#ffcc00">
                                <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51.5;360 50 51.5" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform>
                            </path>
                        </svg>
                        <p class="payment-loader__text">Переходим к оплате, ожидайте...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pay___block"></div>

    <script>
        $(document).ready(function(){
            getPaymentButton(<?=$arResult['ORDER_ID']?>);
        })
    </script>


<? } else { ?>


    <div class="template page success-order payment-success">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                
                <div class="col-12 col-sm-7 col-lg-4">
                    <div class="template__grid success-order__grid payment-success__grid">
                        <p>Ваш заказ <b>№<?=$arResult['ORDER_ID']?></b> успешно оформлен</p>
                        <p>Мы выслали детали заказа на Вашу почту.</p>
                        <p>Теперь Вы можете перейти на <a href="/">главную</a></p>
                    </div>
                </div>
                
                <div class="col-12 col-sm-5 col-lg-4">
                    <img class="payment-success__icon" src="<?=SITE_TEMPLATE_PATH?>/img/payment-success-icon.jpg" alt="alt">
                </div>
                
                <div class="col-12">

                    <? if(
                        $arResult['vk_link']
                        &&
                        $arResult['fb_link']
                        &&
                        $arResult['insta_link']
                    ){ ?>

                        <div class="payment-success__social">
                            <p class="title payment-success__social-title">Подпишитесь на наши соцсети, чтобы быть в
                                курсе новинок</p>
                            <div class="row">

                                <? if( $arResult['vk_link'] ){ ?>

                                    <div class="col-12 col-lg-4">
                                        <a href="<?=$arResult['vk_link']?>" target="_blank" class="payment-success__social-item">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="20"
                                                 height="20">
                                                <path style="fill:#436EAB;" d="M440.649,295.361c16.984,16.582,34.909,32.182,50.142,50.436 c6.729,8.112,13.099,16.482,17.973,25.896c6.906,13.382,0.651,28.108-11.348,28.907l-74.59-0.034 c-19.238,1.596-34.585-6.148-47.489-19.302c-10.327-10.519-19.891-21.714-29.821-32.588c-4.071-4.444-8.332-8.626-13.422-11.932 c-10.182-6.609-19.021-4.586-24.84,6.034c-5.926,10.802-7.271,22.762-7.853,34.8c-0.799,17.564-6.108,22.182-23.751,22.986 c-37.705,1.778-73.489-3.926-106.732-22.947c-29.308-16.768-52.034-40.441-71.816-67.24 C58.589,258.194,29.094,200.852,2.586,141.904c-5.967-13.281-1.603-20.41,13.051-20.663c24.333-0.473,48.663-0.439,73.025-0.034 c9.89,0.145,16.437,5.817,20.256,15.16c13.165,32.371,29.274,63.169,49.494,91.716c5.385,7.6,10.876,15.201,18.694,20.55 c8.65,5.923,15.236,3.96,19.305-5.676c2.582-6.11,3.713-12.691,4.295-19.234c1.928-22.513,2.182-44.988-1.199-67.422 c-2.076-14.001-9.962-23.065-23.933-25.714c-7.129-1.351-6.068-4.004-2.616-8.073c5.995-7.018,11.634-11.387,22.875-11.387h84.298 c13.271,2.619,16.218,8.581,18.035,21.934l0.072,93.637c-0.145,5.169,2.582,20.51,11.893,23.931 c7.452,2.436,12.364-3.526,16.836-8.251c20.183-21.421,34.588-46.737,47.457-72.951c5.711-11.527,10.622-23.497,15.381-35.458 c3.526-8.875,9.059-13.242,19.056-13.049l81.132,0.072c2.406,0,4.84,0.035,7.17,0.434c13.671,2.33,17.418,8.211,13.195,21.561 c-6.653,20.945-19.598,38.4-32.255,55.935c-13.53,18.721-28.001,36.802-41.418,55.634 C424.357,271.756,425.336,280.424,440.649,295.361L440.649,295.361z" />
                                            </svg>
                                            <span class="link">Группа КАФЕТО Вконтакте</span>
                                        </a>
                                    </div>

                                <? } ?>

                                <? if( $arResult['fb_link'] ){ ?>

                                    <div class="col-12 col-lg-4">
                                        <a href="<?=$arResult['fb_link']?>" target="_blank" class="payment-success__social-item">
                                            <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 12.01 20.01">
                                                <path fill="#3c578e" d="M37.5,21.19a19.88,19.88,0,0,0-3.86-.69c-3.9,0-5,2-5,5.11v2.57H25.49V32h3.17v8.47h4V32h3.47l.64-3.86H32.64V26.11c0-1,.41-1.63,1.93-1.63a12.62,12.62,0,0,1,2.11.22Z" transform="translate(-25.49 -20.5)"></path>
                                            </svg>
                                            <span class="link">Страница «Cafeto» на Facebook</span>
                                        </a>
                                    </div>

                                <? } ?>

                                <? if( $arResult['insta_link'] ){ ?>

                                    <div class="col-12 col-lg-4">
                                        <a href="<?=$arResult['insta_link']?>" target="_blank" class="payment-success__social-item">
                                            <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 512 512">
                                                <linearGradient id="SVGID_111_" gradientUnits="userSpaceOnUse" x1="-46.0041" y1="634.1208" x2="-32.9334" y2="647.1917" gradientTransform="matrix(32 0 0 -32 1519 20757)">
                                                    <stop offset="0" style="stop-color:#FFC107" />
                                                    <stop offset="0.507" style="stop-color:#F44336" />
                                                    <stop offset="0.99" style="stop-color:#9C27B0" />
                                                </linearGradient>
                                                <path style="fill:url(#SVGID_111_);" d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192 c88.352,0,160-71.648,160-160V160C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112 V160C48,98.24,98.24,48,160,48h192c61.76,0,112,50.24,112,112V352z" />
                                                <linearGradient id="SVGID_112_" gradientUnits="userSpaceOnUse" x1="-42.2971" y1="637.8279" x2="-36.6404" y2="643.4846" gradientTransform="matrix(32 0 0 -32 1519 20757)">
                                                    <stop offset="0" style="stop-color:#FFC107" />
                                                    <stop offset="0.507" style="stop-color:#F44336" />
                                                    <stop offset="0.99" style="stop-color:#9C27B0" />
                                                </linearGradient>
                                                <path style="fill:url(#SVGID_112_);" d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128 S326.688,128,256,128z M256,336c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80 C336,300.096,300.096,336,256,336z" />
                                                <linearGradient id="SVGID_113_" gradientUnits="userSpaceOnUse" x1="-35.5456" y1="644.5793" x2="-34.7919" y2="645.3331" gradientTransform="matrix(32 0 0 -32 1519 20757)">
                                                    <stop offset="0" style="stop-color:#FFC107" />
                                                    <stop offset="0.507" style="stop-color:#F44336" />
                                                    <stop offset="0.99" style="stop-color:#9C27B0" />
                                                </linearGradient>
                                                <circle style="fill:url(#SVGID_113_);" cx="393.6" cy="118.4" r="17.056" />
                                            </svg>
                                            <span class="link">Аккаунт cafetoru в Instagram</span>
                                        </a>
                                    </div>

                                <? } ?>

                            </div>
                        </div>

                    <? } ?>

                    <? // order_success_articles
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list", "order_success_articles",
                        array(
                            "COMPONENT_TEMPLATE" => "order_success_articles",
                            "IBLOCK_TYPE" => "content",
                            "IBLOCK_ID" => project\blog::IBLOCK_ID,
                            "NEWS_COUNT" => "3",
                            "SORT_BY1" => "PROPERTY_SORT_DATE",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "NAME",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array('SORT_DATE', 'AUTHOR_NAME', ''),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "PAGER_TEMPLATE" => ".default",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "undefined",
                            "SET_LAST_MODIFIED" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "SHOW_404" => "N",
                            "MESSAGE_404" => ""
                        ),
                        false
                    ); ?>

                </div>
                
            </div>
        </div>
    </div>


<? } ?>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

if( intval($_POST['order_id']) > 0 ){

    $APPLICATION->SetPageProperty("title", 'Заказ №'.intval($_POST['order_id']).' успешно оформлен!');


    $arResult['ORDER_ID'] = strip_tags($_POST['order_id']);
    $arResult['PS_ID'] = strip_tags($_POST['ps_id']);

    $arResult['PS'] = project\ps::getByID($arResult['PS_ID']);



    $arResult['vk_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'VK_LINK');
    $arResult['fb_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'FB_LINK');
    $arResult['tw_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'TW_LINK');
    $arResult['insta_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'INSTA_LINK');



} else {

    LocalRedirect('/' , false, '301 Moved permanently');

}








$this->IncludeComponentTemplate();
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools; ?>

<div class="item catalog___item" item_id="<?=$arResult['ID']?>">

    <? if( $arParams['is_favorites'] == 'Y' ){ ?>

        <button type="button" class="item__remove-btn favRemoveButton to___process" item_id="<?=$arResult['ID']?>">
            <svg width="12" height="12" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.971 47.971">
                <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z" />
            </svg>
        </button>

    <? } ?>

    <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="item__layer">

        <picture class="item__img">
            <? if( intval($arResult['PREVIEW_PICTURE']['ID']) > 0 ){ ?>
                <source srcset="<?=tools\funcs::rIMGG($arResult['PREVIEW_PICTURE']['ID'], 4, 191, 227)?> 1x, <?=tools\funcs::rIMGG($arResult['PREVIEW_PICTURE']['ID'], 4, 382, 454)?> 2x">
                <img src="<?=tools\funcs::rIMGG($arResult['PREVIEW_PICTURE']['ID'], 4, 191, 227)?>">
            <? } else { ?>
                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png, <?=SITE_TEMPLATE_PATH?>/img/no-photo-big.png">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
            <? } ?>
        </picture>

        <? if( $arResult['DISCOUNT_POCENT'] > 0 ){ ?>
            <p class="item__label item__label_2">- <?=$arResult['DISCOUNT_POCENT']?>%</p>
        <? } else if( $arResult["PROPERTIES"]['IS_NEW']['VALUE'] ){ ?>
            <p class="item__label item__label_1">new</p>
        <? } else if( $arResult["PROPERTIES"]['IS_WEEK_SORT']['VALUE'] ){ ?>
            <p class="item__label item__label_4">сорт недели</p>
        <? } else if( $arResult["PROPERTIES"]['IS_MIKRO']['VALUE'] ){ ?>
            <p class="item__label item__label_3">микролот</p>
        <? } ?>

        <div class="item__options">
            <button type="button" style="cursor: pointer" class="item__option catalog_item_modal_link to___process" item_id="<?=$arResult['ID']?>">
                <svg width="23.97px" height="14px" viewBox="0 0 23.97 14">
                    <path fill="#262424" d="M43.31,30a6.82,6.82,0,0,0-2.71-3.25,16,16,0,0,0-9.09-3.27,16,16,0,0,0-9.09,3.27A6.82,6.82,0,0,0,19.71,30a.73.73,0,0,0,0,1,6.82,6.82,0,0,0,2.71,3.25,16,16,0,0,0,9.09,3.27,16,16,0,0,0,9.09-3.27A6.82,6.82,0,0,0,43.31,31,.73.73,0,0,0,43.31,30Zm-3.59,3a14.85,14.85,0,0,1-8.21,3,14.85,14.85,0,0,1-8.21-3c-1.4-1-1.47-2-2-2.56.56-.57.63-1.57,2-2.56a14.85,14.85,0,0,1,8.21-3,14.85,14.85,0,0,1,8.21,3c1.4,1,1.47,2,2,2.56C41.19,31.07,41.12,32.07,39.72,33.06Zm-4.39-3.29a.74.74,0,0,0-.74.73,3.07,3.07,0,1,1-1.05-2.3.73.73,0,1,0,1-1.09,4.59,4.59,0,0,0-3-1.12,4.51,4.51,0,1,0,4.55,4.51A.73.73,0,0,0,35.33,29.77ZM30,30.5a1.47,1.47,0,1,0,1.47-1.45A1.46,1.46,0,0,0,30,30.5Z" transform="translate(-19.53 -23.5)" />
                </svg>
            </button>
            <button type="button" style="cursor: pointer" class="item__option fav___button to___process add-to-fav" item_id="<?=$arResult['ID']?>">
                <svg viewBox="0 0 20 19" width="20px" height="19px">
                    <path fill="#030303" stroke="#030303" stroke-width=".5" stroke-miterlimit="10" d="M18.5,6l-4.9-0.4c-0.1,0-0.2-0.1-0.3-0.2L11.5,1c-0.4-0.8-1.3-1.2-2.1-0.9C9,0.3,8.6,0.6,8.5,1L6.6,5.5 c0,0.1-0.1,0.2-0.2,0.2L1.5,6C0.6,6.1-0.1,6.8,0,7.7c0,0.4,0.2,0.9,0.6,1.2L4.3,12c0.1,0.1,0.1,0.2,0.1,0.3L3.3,17 c-0.1,0.5,0,1,0.3,1.4C3.8,18.8,4.3,19,4.8,19c0.3,0,0.6-0.1,0.9-0.3l4.1-2.6c0.1,0,0.2,0,0.3,0l4.2,2.5c0.3,0.2,0.6,0.3,0.9,0.3 c0.5,0,0.9-0.2,1.2-0.6c0.3-0.4,0.4-0.9,0.3-1.4l-1.2-4.7c0-0.1,0-0.2,0.1-0.3l3.8-3.1c0.5-0.4,0.7-1.1,0.5-1.8 C19.7,6.5,19.1,6.1,18.5,6z M18.8,8.1L15,11.2c-0.4,0.3-0.5,0.8-0.4,1.3l1.2,4.7c0,0.2,0,0.4-0.1,0.6c-0.1,0.1-0.3,0.2-0.5,0.2 c-0.1,0-0.2,0-0.3-0.1l-4.2-2.6c-0.2-0.1-0.4-0.2-0.7-0.2c-0.2,0-0.5,0.1-0.7,0.2l-4.1,2.6c-0.3,0.2-0.6,0.1-0.8-0.2 c-0.1-0.2-0.2-0.3-0.1-0.5l1.2-4.7c0.1-0.5-0.1-1-0.4-1.3L1.2,8.1C1,8,1,7.7,1,7.5c0.1-0.2,0.3-0.4,0.6-0.4l4.9-0.4 c0.5,0,0.9-0.3,1.1-0.8l1.9-4.5c0.1-0.3,0.5-0.5,0.8-0.4c0.2,0.1,0.3,0.2,0.4,0.4l1.9,4.5c0.2,0.4,0.6,0.7,1.1,0.8l4.9,0.4 c0.3,0,0.6,0.3,0.6,0.7C19,7.9,18.9,8.1,18.8,8.1L18.8,8.1z" />
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.94 49.94" width="20px" height="20px">
                    <path d="M48.856,22.73c0.983-0.958,1.33-2.364,0.906-3.671c-0.425-1.307-1.532-2.24-2.892-2.438l-12.092-1.757 c-0.515-0.075-0.96-0.398-1.19-0.865L28.182,3.043c-0.607-1.231-1.839-1.996-3.212-1.996c-1.372,0-2.604,0.765-3.211,1.996 L16.352,14c-0.23,0.467-0.676,0.79-1.191,0.865L3.069,16.622c-1.359,0.197-2.467,1.131-2.892,2.438 c-0.424,1.307-0.077,2.713,0.906,3.671l8.749,8.528c0.373,0.364,0.544,0.888,0.456,1.4L8.224,44.701 c-0.183,1.06,0.095,2.091,0.781,2.904c1.066,1.267,2.927,1.653,4.415,0.871l10.814-5.686c0.452-0.237,1.021-0.235,1.472,0 l10.815,5.686c0.526,0.277,1.087,0.417,1.666,0.417c1.057,0,2.059-0.47,2.748-1.288c0.687-0.813,0.964-1.846,0.781-2.904 l-2.065-12.042c-0.088-0.513,0.083-1.036,0.456-1.4L48.856,22.73z" />
                </svg>
            </button>
        </div>
    </a>

    <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="item__name"><?=$arResult['NAME']?></a>

    <?php if( project\catalog::isAvailableProduct( $arResult ) ){ ?>

        <p class="item__price">
            <? if( $arResult['MIN_PRICE'] ){
                echo 'от '.$arResult['MIN_PRICE'].' RUB';
            } else {
                echo $arResult['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'].' RUB';
            } ?>
        </p>

    <?php } else { ?>

        <p style="text-align: center;"><i>Нет в наличии</i></p>

    <?php } ?>

</div>
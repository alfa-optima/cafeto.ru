<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$arResult = $arParams['arItem'];

$arResult['MIN_PRICE'] = false;
$arResult['MAX_PRICE'] = false;

$arResult['DISCOUNT_POCENT'] = 0;
if(
    is_array($arResult['OFFERS'])
    &&
    count($arResult['OFFERS']) > 0
){
    $arResult['DISCOUNT_POCENT'] = round(($arResult['OFFERS'][0]['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT'] - $arResult['OFFERS'][0]['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'])/$arResult['OFFERS'][0]['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT']*100, 0);
} else {
    $arResult['DISCOUNT_POCENT'] = round(($arResult['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT'] - $arResult['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'])/$arResult['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT']*100, 0);
}

foreach ( $arResult['OFFERS'] as $offer ){
    if( $offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'] ){
        if(
            !$arResult['MIN_PRICE']
            ||
            $offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'] < $arResult['MIN_PRICE']
        ){    $arResult['MIN_PRICE'] = $offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'];    }
        if(
            !$arResult['MAX_PRICE']
            ||
            $offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'] > $arResult['MAX_PRICE']
        ){    $arResult['MAX_PRICE'] = $offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT'];    }
    }
}



$arResult['isAvailableProduct'] = project\catalog::isAvailableProduct( $arResult );



$this->IncludeComponentTemplate();
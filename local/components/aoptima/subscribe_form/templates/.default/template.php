<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <h5 class="newsletter__title">Получайте интересные истории про кофе и информацию об акциях и
                    скидках<span>Подпишитесь на рассылку, чтобы быть в курсе новостей</span></h5>

                <div class="newsletter__grid">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-8">
                            <form class="form newsletter__form subscribe___form" onsubmit="return false;">
                                <div class="newsletter__input">
                                    <input type="text" placeholder="Email адрес" name="email">
                                </div>
                                <button type="button" class="btn newsletter__btn subscribe___button to___process">Подписаться</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
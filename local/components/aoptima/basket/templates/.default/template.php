<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if( $arParams['IS_AJAX'] != 'Y' ){ ?>

<div class="template cart">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "catalog",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

                <? } ?>



                <div class="cart__grid basket_inner_block">

                    <form class="form cart-form order___form" onsubmit="return false;" action="/order_success/" method="post">

                        <? if( count($arResult['basketInfo']['arBasket']) > 0 ){ ?>

                            <div class="cart-table">
                                <table>
                                    <tr>
                                        <th>Товар</th>
                                        <th>Цена</th>
                                        <th>Количество</th>
                                        <th>Итого</th>
                                    </tr>

                                    <? foreach( $arResult['basketInfo']['arBasket'] as $key => $basketItem ){ ?>

                                        <tr>
                                            <td>
                                                <div class="cart-table__product">
                                                    <a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>" class="cart-table__img">
                                                        <picture>
                                                            <? if( intval($basketItem->product['PREVIEW_PICTURE']) > 0 ){ ?>
                                                                <source srcset="<?=tools\funcs::rIMGG($basketItem->product['PREVIEW_PICTURE'], 4, 120, 120)?>, <?=tools\funcs::rIMGG($arResult['PREVIEW_PICTURE'], 4, 382, 454)?>">
                                                                <img src="<?=tools\funcs::rIMGG($basketItem->product['PREVIEW_PICTURE'], 4, 120, 120)?>">
                                                            <? } else { ?>
                                                                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png, <?=SITE_TEMPLATE_PATH?>/img/no-photo-big.png">
                                                                <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                                                            <? } ?>
                                                        </picture>
                                                    </a>
                                                    <div class="cart-table__info">
                                                        <a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>" class="link cart-table__name"><?=$basketItem->product['NAME']?></a>
                                                        <? if( strlen($basketItem->el['PROPERTY_WEIGHT_VALUE']) > 0 ){ ?>
                                                            <p class="cart-table__text"><?=$basketItem->el['PROPERTY_WEIGHT_VALUE']?> гр</p>
                                                        <? } ?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="cart-table__price"><?=$basketItem->discPriceFormat?> RUB</p>
                                            </td>
                                            <td>
                                                <div class="quantity quantity___block">

                                                    <button type="button" class="quantity__btn quantity__btn-minus basket_button___minus basket_quantity_button to___process" item_id="<?=$arResult['basketInfo']['userBasket'][$key]->getField('ID')?>">
                                                        <svg width="8.98" height="0.99" viewBox="0 0 8.98 0.99">
                                                            <rect fill="#000" class="cls-1" width="8.98" height="0.99" />
                                                        </svg>
                                                    </button>

                                                    <input type="text" name="quantity" value="<?=$basketItem->getQuantity()?>" class="quantity__input" item_id="<?=$arResult['basketInfo']['userBasket'][$key]->getField('ID')?>">

                                                    <button type="button" class="quantity__btn quantity__btn-plus basket_button___plus basket_quantity_button to___process" item_id="<?=$arResult['basketInfo']['userBasket'][$key]->getField('ID')?>">
                                                        <svg width="8.99px" height="9.02px" viewBox="0 0 8.99 9.02">
                                                            <polygon fill="#000" points="8.99 3.96 5.23 3.96 5.23 0 3.98 0 3.98 3.96 0 3.96 0 5.08 3.98 5.08 3.98 9.02 5.23 9.02 5.23 5.08 8.99 5.08 8.99 3.96" />
                                                        </svg>
                                                    </button>

                                                </div>
                                            </td>
                                            <td>
                                                <div class="cart-table__options">

                                                    <p class="cart-table__price"><?=$basketItem->discSumFormat?> RUB</p>

                                                    <a style="cursor: pointer" class="cart-table__remove basketRemoveButton to___process" item_id="<?=$arResult['basketInfo']['userBasket'][$key]->getField('ID')?>">
                                                        <svg viewBox="0 0 47.971 47.971" width="12px" height="12px">
                                                            <path fill="#707070" d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z" />
                                                        </svg>
                                                    </a>

                                                </div>
                                            </td>
                                        </tr>

                                    <? } ?>

                                </table>
                            </div>

                            <div class="cart-form__info">
                                <div class="row">

                                    <div class="col-12 col-lg-5">
                                        <div class="promocode">
                                            <div class="promocode__input">
                                                <input type="text" name="coupon" placeholder="Введите промокод" value="<?=$arResult['ACTIVE_COUPON']['COUPON']?>">
                                            </div>
                                            <button type="button" class="btn promocode__btn saveCouponButton to___process">Применить</button>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-7">
                                        <div class="cart-form__total">
                                            <p>Стоимость товаров <? if( $arResult['basketInfo']['basket_discount'] > 0 ){ ?> (с учётом скидки)<? } ?>: <span><?=$arResult['basketInfo']['basket_disc_sum_format']?> RUB</span></p>
                                            <? if( $arResult['basketInfo']['basket_discount'] > 0 ){ ?>
                                                <p>Сумма скидки: <span><?=$arResult['basketInfo']['basket_discount_format']?> RUB</span></p>
                                            <? } ?>
                                            <p>Стоимость доставки: <span><?=$arResult['DELIVERY_PRICE_FORMAT']?> <? if( $arResult['DELIVERY_PRICE_FORMAT'] != 'не опред.' && $arResult['DELIVERY_PRICE_FORMAT'] != 'бесплатно' ){ ?>RUB<? } ?></span></p>
                                            <p>Итого, к оплате: <span><?=$arResult['TOTAL_SUM_FORMAT']?> RUB</span></p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cart-form__options">
                                <div class="row">

                                    <div class="col-12 col-lg-4 cart-form__row">
                                        <div class="cart-form__col">
                                            <p class="cart-form__col-name">Контактные данные:</p>
                                            <div class="form__row">
                                                <input type="text" name="name" class="form__input" placeholder="Ваше имя" value="<?=$arResult['form_data']['name']?$arResult['form_data']['name']:$arResult['USER']['NAME']?>">
                                            </div>
                                            <div class="form__row">
                                                <input type="text" name="second_name" class="form__input" placeholder="Ваше отчество" value="<?=$arResult['form_data']['second_name']?$arResult['form_data']['second_name']:$arResult['USER']['SECOND_NAME']?>">
                                            </div>
                                            <div class="form__row">
                                                <input type="text" name="last_name" class="form__input" placeholder="Ваша фамилия" value="<?=$arResult['form_data']['last_name']?$arResult['form_data']['last_name']:$arResult['USER']['LAST_NAME']?>">
                                            </div>
                                            <div class="form__row">
                                                <input type="tel" name="phone" class="form__input" placeholder="Номер телефона" value="<?=$arResult['form_data']['phone']?$arResult['form_data']['phone']:$arResult['USER']['PERSONAL_PHONE']?>">
                                            </div>
                                            <div class="form__row order_email___block">
                                                <input type="email" name="email" class="form__input" placeholder="Электронный адрес" value="<?=$arResult['form_data']['email']?$arResult['form_data']['email']:$arResult['USER']['EMAIL']?>">
                                            </div>
                                            <div class="form__row">
                                                <textarea name="comment" class="form__input form__textarea" placeholder="Комментарий"><?=$arResult['form_data']['comment']?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-4 cart-form__row">
                                        <div class="cart-form__col">

                                            <p class="cart-form__col-name">Доставка:</p>
                                            <!--                                            <div class="form__row">-->
                                            <!--                                                <input type="text" name="city" class="form__input" id="city-autocomplete" placeholder="Город">-->
                                            <!--                                            </div>-->

                                            <div class="delivery-options">

<? $cnt = 0;
foreach( $arResult['DS_LIST'] as $key => $ds ){

    $hasPVZ = 'N';
    $ds_class = '\AOptima\ProjectCafeto\ds_'.$ds['XML_ID'];
    if( isset($ds_class) && class_exists($ds_class) ){
        $dsObj = new $ds_class();
        $hasPVZ = $dsObj->hasPVZ()?'Y':'N';
    } ?>

    <input type="hidden" name="ds_has_pvz[<?=$ds['ID']?>]" value="<?=$hasPVZ?>">
    <input type="hidden" name="ds_class[<?=$ds['ID']?>]" value="<?=$ds_class?>">

    <? // Самовывоз
    if( $ds['XML_ID'] == 'samovyvoz' ){ $cnt++; ?>

        <div class="delivery-options__item <? if( $arResult['form_data']['ds_id'] && $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>delivery-options__item_opened<? } else if($cnt==1 && !$arResult['form_data']['ds_id']){ ?>delivery-options__item_opened<? } ?>">
            <label class="radio delivery-options__radio">
                <input <? if( $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>checked<? } ?> type="radio" name="ds_id" value="<?=$ds['ID']?>" class="radio__input">
                <p class="radio__name"><?=$ds['NAME']?></p>
            </label>
            <p class="delivery-options__text delivery-options__value"><?=$ds['DESCRIPTION']?></p>
        </div>

    <? // ПЭК
    } else if( $ds['XML_ID'] == 'pek' ){ $cnt++; ?>

        <div class="delivery-options__item <? if( $arResult['form_data']['ds_id'] && $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>delivery-options__item_opened<? } ?>">

            <label class="radio delivery-options__radio">
                <input <? if( $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>checked<? } ?> type="radio" name="ds_id" value="<?=$ds['ID']?>" class="radio__input <?=$ds['XML_ID']?>_np">
                <p class="radio__name"><?=$ds['NAME']?></p>
            </label>

            <div class="delivery-options__text">
                <div class="form__row">
                    <input
                        type="text"
                        name="np[<?=$ds['ID']?>]"
                        id="np_<?=$ds['XML_ID']?>_input"
                        code="<?=$ds['XML_ID']?>"
                        class="form__input np___input"
                        placeholder="Населённый пункт"
                        onfocus="npFocus($(this));"
                        onkeyup="npKeyup($(this));"
                        onfocusout="npFocusOut($(this));"
                        np_name=""
                        value="<?=$arResult['form_data']['np'][$ds['ID']]?>"
                    >
                    <input type="hidden" name="np_id[<?=$ds['ID']?>]" value="<?=$arResult['form_data']['np_id'][$ds['ID']]?>">
                </div>
                <script>
                    $(document).ready(function(){
                        $('#np_<?=$ds['XML_ID']?>_input').autocomplete({
                            source: '/ajax/deliveryLocations_<?=$ds['XML_ID']?>.php',
                            minLength: 2,
                            delay: 700,
                            select: npSelect
                        })
                    })
                </script>
                <div class="form__row">
                    <input type="text" name="street[<?=$ds['ID']?>]" class="form__input street___input" placeholder="Улица" value="<?=$arResult['form_data']['street'][$ds['ID']]?>">
                </div>
                <div class="form__row form__row_double">
                    <input type="text" name="house[<?=$ds['ID']?>]" class="form__input house___input" placeholder="Дом / корпус" value="<?=$arResult['form_data']['house'][$ds['ID']]?>">
                    <input type="text" name="kvartira[<?=$ds['ID']?>]" class="form__input" placeholder="Квартира" value="<?=$arResult['form_data']['kvartira'][$ds['ID']]?>">
                </div>
            </div>

        </div>

    <? // СДЭК
    } else if( $ds['XML_ID'] == 'sdek' ){ $cnt++; ?>

        <div class="delivery-options__item <? if( $arResult['form_data']['ds_id'] && $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>delivery-options__item_opened<? } ?>">

            <label class="radio delivery-options__radio">
                <input <? if( $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>checked<? } ?> type="radio" name="ds_id" value="<?=$ds['ID']?>" class="radio__input <?=$ds['XML_ID']?>_np">
                <p class="radio__name"><?=$ds['NAME']?></p>
            </label>

            <div class="delivery-options__text">
                <div class="form__row">
                    <input
                        type="text"
                        name="np[<?=$ds['ID']?>]"
                        id="np_<?=$ds['XML_ID']?>_input"
                        code="<?=$ds['XML_ID']?>"
                        class="form__input np___input"
                        placeholder="Населённый пункт"
                        onfocus="npFocus($(this));"
                        onkeyup="npKeyup($(this));"
                        onfocusout="npFocusOut($(this));"
                        np_name=""
                        value="<?=$arResult['form_data']['np'][$ds['ID']]?>"
                    >
                    <input type="hidden" name="np_id[<?=$ds['ID']?>]" value="<?=$arResult['form_data']['np_id'][$ds['ID']]?>">
                </div>
                <script>
                    $(document).ready(function(){
                        $('#np_<?=$ds['XML_ID']?>_input').autocomplete({
                            source: '/ajax/deliveryLocations_<?=$ds['XML_ID']?>.php',
                            minLength: 2,
                            delay: 700,
                            select: npSelect
                        })
                    })
                </script>
                <div class="form__row">
                    <input type="text" name="street[<?=$ds['ID']?>]" class="form__input street___input" placeholder="Улица" value="<?=$arResult['form_data']['street'][$ds['ID']]?>">
                </div>
                <div class="form__row form__row_double">
                    <input type="text" name="house[<?=$ds['ID']?>]" class="form__input house___input" placeholder="Дом / корпус" value="<?=$arResult['form_data']['house'][$ds['ID']]?>">
                    <input type="text" name="kvartira[<?=$ds['ID']?>]" class="form__input" placeholder="Квартира" value="<?=$arResult['form_data']['kvartira'][$ds['ID']]?>">
                </div>
                <div class="form__row">
                    <textarea name="ds_comment[<?=$ds['ID']?>]" class="form__input form__textarea" placeholder="Комментарий для курьера"></textarea>
                </div>
            </div>

        </div>

    <? // СДЭК (ПВЗ)
    } else if( $ds['XML_ID'] == 'sdek_pvz' ){ $cnt++; ?>

        <div class="delivery-options__item <? if( $arResult['form_data']['ds_id'] && $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>delivery-options__item_opened<? } else if( $cnt==1 && !$arResult['form_data']['ds_id'] ){ ?>delivery-options__item_opened<? } ?>">

            <label class="radio delivery-options__radio">
                <input <? if( $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>checked<? } ?> type="radio" name="ds_id" value="<?=$ds['ID']?>" class="radio__input <?=$ds['XML_ID']?>_np">
                <p class="radio__name"><?=$ds['NAME']?></p>
            </label>

            <?php if( $hasPVZ == 'Y' ){ ?>

                <a href="#sdek_pvz_window" rel="modal:open" class="delivery-options__text link delivery-options__link">Выбрать пункт</a>

                <p class="delivery-options__text delivery-options__value <?=$ds_class::$PVZ_ADDRESS_FIELD_NAME?>" ><?=$arResult['form_data'][$ds_class::$PVZ_ADDRESS_FIELD_NAME][$ds['ID']]?></p>

                <input type="hidden" class="<?=$ds_class::$PVZ_ADDRESS_FIELD_NAME?>_input" name="<?=$ds_class::$PVZ_ADDRESS_FIELD_NAME?>[<?=$ds['ID']?>]" value="<?=$arResult['form_data'][$ds_class::$PVZ_ADDRESS_FIELD_NAME][$ds['ID']]?>">
                <input type="hidden" class="sdek_np_input" name="np[<?=$ds['ID']?>]" value="<?=$arResult['form_data']['np'][$ds['ID']]?>">
                <input type="hidden" class="sdek_np_id_input" name="np_id[<?=$ds['ID']?>]" value="<?=$arResult['form_data']['np_id'][$ds['ID']]?>">
                <input type="hidden" class="<?=$ds_class::$PVZ_POINT_ID_FIELD_NAME?>_input" name="<?=$ds_class::$PVZ_POINT_ID_FIELD_NAME?>[<?=$ds['ID']?>]" value="<?=$arResult['form_data'][$ds_class::$PVZ_POINT_ID_FIELD_NAME][$ds['ID']]?>">

            <?php } ?>

        </div>

    <? // Pickpoint (ПВЗ)
    } else if( $ds['XML_ID'] == 'pickpoint_pvz' ){ $cnt++; ?>

        <div class="delivery-options__item <? if( $arResult['form_data']['ds_id'] && $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>delivery-options__item_opened<? } else if( $cnt==1 && !$arResult['form_data']['ds_id'] ){ ?>delivery-options__item_opened<? } ?>">

            <label class="radio delivery-options__radio">
                <input <? if( $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>checked<? } ?> type="radio" name="ds_id" value="<?=$ds['ID']?>" class="radio__input <?=$ds['XML_ID']?>_np">
                <p class="radio__name"><?=$ds['NAME']?></p>
            </label>

            <?php if( $hasPVZ == 'Y' ){ ?>

                <a href="#pickpoint_pvz_window" rel="modal:open" class="delivery-options__text link delivery-options__link">Выбрать пункт</a>

                <p class="delivery-options__text delivery-options__value <?=$ds_class::$PVZ_ADDRESS_FIELD_NAME?>" ><?=$arResult['form_data'][$ds_class::$PVZ_ADDRESS_FIELD_NAME][$ds['ID']]?></p>
                <input type="hidden" class="<?=$ds_class::$PVZ_ADDRESS_FIELD_NAME?>_input" name="<?=$ds_class::$PVZ_ADDRESS_FIELD_NAME?>[<?=$ds['ID']?>]" value="<?=$arResult['form_data'][$ds_class::$PVZ_ADDRESS_FIELD_NAME][$ds['ID']]?>">
                <input type="hidden" class="pickpoint_city_input" name="pickpoint_city[<?=$ds['ID']?>]" value="<?=$arResult['form_data']['pickpoint_city'][$ds['ID']]?>">
                <input type="hidden" class="<?=$ds_class::$PVZ_POINT_ID_FIELD_NAME?>_input" name="<?=$ds_class::$PVZ_POINT_ID_FIELD_NAME?>[<?=$ds['ID']?>]" value="<?=$arResult['form_data'][$ds_class::$PVZ_POINT_ID_FIELD_NAME][$ds['ID']]?>">
                <input type="hidden" class="<?=$ds_class::$PVZ_POINT_NUMBER_FIELD_NAME?>_input" name="<?=$ds_class::$PVZ_POINT_NUMBER_FIELD_NAME?>[<?=$ds['ID']?>]" value="<?=$arResult['form_data'][$ds_class::$PVZ_POINT_NUMBER_FIELD_NAME][$ds['ID']]?>">

            <? } ?>

        </div>

    <? // Почта России
    } else if( $ds['XML_ID'] == 'pochta' ){ $cnt++; ?>

        <div class="delivery-options__item <? if( $arResult['form_data']['ds_id'] && $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>delivery-options__item_opened<? } else if( $cnt==1 && !$arResult['form_data']['ds_id'] ){ ?>delivery-options__item_opened<? } ?>">

            <label class="radio delivery-options__radio">
                <input <? if( $arResult['form_data']['ds_id'] == $ds['ID'] ){ ?>checked<? } ?> type="radio" name="ds_id" value="<?=$ds['ID']?>" class="radio__input <?=$ds['XML_ID']?>_np">
                <p class="radio__name"><?=$ds['NAME']?></p>
            </label>

            <div class="delivery-options__text">
                <div class="form__row">
                    <input type="text" name="zip_code[<?=$ds['ID']?>]" class="form__input zip___input" placeholder="Почтовый индекс" value="<?=$arResult['form_data']['zip_code'][$ds['ID']]?>">
                </div>
            </div>

        </div>

    <? }

} ?>

                                            </div>

                                        </div>
                                    </div>

                                    <? if( count($arResult['PS_LIST']) > 0 ){ ?>

                                        <div class="col-12 col-lg-4 cart-form__row">
                                            <div class="cart-form__col">

                                                <p class="cart-form__col-name">Оплата:</p>

                                                <? foreach( $arResult['PS_LIST'] as $key => $ps ){ ?>
                                                    <label class="radio cart-form__radio">
                                                        <input <?=$ps['ID']==$arResult['form_data']['ps_id']?'checked':''?> type="radio" name="ps_id" value="<?=$ps['ID']?>" class="radio__input">
                                                        <p class="radio__name"><?=$ps['NAME']?></p>
                                                    </label>
                                                <? } ?>

                                            </div>
                                        </div>

                                    <? } ?>

                                </div>
                            </div>

                            <p class="error___p"></p>

                            <div class="row">
                                <div class="col-12 offset-lg-8 col-lg-4">
                                    <footer class="cart-form__footer">

                                        <div class="callback cart-form__callback">
                                            <p class="callback__title">Нужен звонок оператора?</p>
                                            <label class="radio callback__radio">
                                                <input type="radio" <? if( $arResult['form_data']['callback'] == 'Да' || !$arResult['form_data'] ){ echo 'checked'; } ?> name="callback" value="Да" class="radio__input">
                                                <p class="radio__name">Да, я хочу уточнить детали</p>
                                            </label>
                                            <label class="radio callback__radio">
                                                <input type="radio" <? if( $arResult['form_data']['callback'] == 'Нет' ){ echo 'checked'; } ?> name="callback" value="Нет" class="radio__input">
                                                <p class="radio__name">Нет, достаточно SMS/Email оповещения</p>
                                            </label>
                                        </div>

                                        <button type="button" class="btn cart-form__btn order___button to___process">Оформить заказ</button>

                                        <label class="checkbox privacy-checkbox">

                                            <input type="checkbox" <? if( $arResult['form_data']['agree'] == 'Y' || !$arResult['form_data'] ){ echo 'checked'; } ?> value="Y" name="agree" class="checkbox__input">

                                            <p class="checkbox__name privacy-checkbox__name">Я принимаю <a href="/oferta/" target="_blank" class="link">Условия использования</a> и соглашаюсь с <a href="/privacy_policy/" target="_blank" class="link">Политикой конфиденциальности</a></p>

                                        </label>

                                    </footer>
                                </div>
                            </div>

                        <? } else { ?>

                            <h1>Корзина</h1>
                            <p>Корзина пуста</p>

                        <? } ?>

                    </form>

                    <div class="cart__gesture-icon">
                        <svg width="25px" height="25px" viewBox="0 0 554.625 554.625">
                            <path fill="#ffcc00" d="M478.125,229.5c-11.475,0-21.037,3.825-28.688,9.562l0,0c0-26.775-21.037-47.812-47.812-47.812 c-11.475,0-22.95,3.825-32.513,11.475c-7.649-19.125-24.862-30.6-43.987-30.6c-11.475,0-21.037,3.825-28.688,9.562V86.062 c0-26.775-21.037-47.812-47.812-47.812c-22.95,0-42.075,17.212-45.9,38.25H66.938l61.2-61.2L114.75,0L28.688,86.062l86.062,86.062 l15.3-15.3l-63.112-61.2h133.875v160.65c-38.25-34.425-80.325-61.2-107.1-34.425c-38.25,38.25,42.075,112.837,103.275,225.675 c43.988,78.412,105.188,107.1,166.388,107.1c89.888,0,162.562-72.675,162.562-162.562v-114.75 C525.938,250.538,504.9,229.5,478.125,229.5z M506.812,319.388v72.675c0,74.588-65.025,143.438-143.438,143.438 c-72.675,0-114.75-40.162-149.175-95.625c-74.588-126.225-128.138-183.6-107.1-204.638c21.038-21.037,72.675,24.863,112.837,66.938 V86.062c0-15.3,13.388-28.688,28.688-28.688s28.688,13.388,28.688,28.688v200.812h19.125v-66.938 c0-15.3,13.388-28.688,28.688-28.688s28.688,13.388,28.688,28.688v47.812h19.125v-28.688c0-15.3,13.388-28.688,28.688-28.688 s28.688,13.388,28.688,28.688v47.812h19.125v-9.562c0-15.3,13.388-28.688,28.688-28.688s28.688,13.388,28.688,28.688V319.388z" />
                        </svg>

                    </div>

                </div>



                <? if( $arParams['IS_AJAX'] != 'Y' ){ ?>
            </div></div></div></div>
<? } ?>

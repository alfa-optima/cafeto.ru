<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if( $USER->IsAuthorized() ){
    $arResult['USER'] = tools\user::info($USER->GetID());
    $cur_deliv_info = [];
    if( strlen($arResult['USER']["UF_DELIV_INFO"]) > 0 ){
        $cur_deliv_info = tools\funcs::json_to_array($arResult['USER']["UF_DELIV_INFO"]);
    }
}




if( $arParams['basketInfo'] ){
    $arResult['basketInfo'] = $arParams['basketInfo'];
} else {
    $arResult['basketInfo'] = project\user_basket::info();
}

$arResult['ACTIVE_COUPON'] = project\coupon::get_active_coupon();

$arResult['form_data'] = [];
parse_str($_POST["form_data"], $arResult['form_data']);


$arResult['DS_LIST'] = project\ds::getList();
foreach ( $arResult['DS_LIST'] as $key => $ds ){
    $class_name = '\AOptima\ProjectCafeto\ds_'.$ds['XML_ID'];
    if( class_exists($class_name) ){
        $dsObj = new $class_name();
        if ( method_exists($dsObj, 'getPVZList') ){
            $pvzList = $dsObj->getPVZList();
            if( count($pvzList) == 0 ){
                unset($arResult['DS_LIST'][$key]);
            }
            unset($pvzList);
        }
        if ( method_exists($dsObj, 'getTerminals') ){
            unset($arResult['DS_LIST'][$key]);
        }
    }
}

if( intval($arResult['form_data']['ds_id']) > 0 ){
    $arResult['PS_LIST'] = project\ps::getFilteredtList($arResult['form_data']['ds_id']);
} else {
    $arResult['PS_LIST'] = [];
}


$arResult['DELIVERY_PRICE'] = 0;
$arResult['DS'] = $arResult['DS_LIST'][$arResult['form_data']['ds_id']];

if( intval($arResult['DS']['ID']) > 0 ){

    $arResult['CLASS_NAME'] = '\AOptima\ProjectCafeto\ds_'.$arResult['DS']['XML_ID'];

    if( !$arResult['form_data']['np'][$arResult['DS']['ID']] ){
        $arResult['form_data']['np'][$arResult['DS']['ID']] = $cur_deliv_info['np_info'][$arResult['DS']['XML_ID']]['np'];
    }
    if( !$arResult['form_data']['np_id'][$arResult['DS']['ID']] ){
        $arResult['form_data']['np_id'][$arResult['DS']['ID']] = $cur_deliv_info['np_info'][$arResult['DS']['XML_ID']]['np_id'];
    }
    if( !$arResult['form_data']['sdek_address'][$arResult['DS']['ID']] ){
        $arResult['form_data']['sdek_address'][$arResult['DS']['ID']] = $cur_deliv_info['pvz_info'][$arResult['DS']['XML_ID']]['address'];
    }
    if( !$arResult['form_data']['pickpoint_address'][$arResult['DS']['ID']] ){
        $arResult['form_data']['pickpoint_address'][$arResult['DS']['ID']] = $cur_deliv_info['pvz_info'][$arResult['DS']['XML_ID']]['address'];
    }
    if( !$arResult['form_data']['pickpoint_point_number'][$arResult['DS']['ID']] ){
        $arResult['form_data']['pickpoint_point_number'][$arResult['DS']['ID']] = $cur_deliv_info['pvz_info'][$arResult['DS']['XML_ID']]['point_id'];
    }
    if( !$arResult['form_data']['street'][$arResult['DS']['ID']] ){
        $arResult['form_data']['street'][$arResult['DS']['ID']] = $cur_deliv_info['other_info']['street'];
    }
    if( !$arResult['form_data']['house'][$arResult['DS']['ID']] ){
        $arResult['form_data']['house'][$arResult['DS']['ID']] = $cur_deliv_info['other_info']['house'];
    }
    if( !$arResult['form_data']['kvartira'][$arResult['DS']['ID']] ){
        $arResult['form_data']['kvartira'][$arResult['DS']['ID']] = $cur_deliv_info['other_info']['kvartira'];
    }
    if( !$arResult['form_data']['zip_code'][$arResult['DS']['ID']] ){
        $arResult['form_data']['zip_code'][$arResult['DS']['ID']] = $cur_deliv_info['other_info']['zip_code'];
    }
}


if(
    intval($arResult['form_data']['ds_id']) > 0
    &&
    strlen($arResult['CLASS_NAME']) > 0
    &&
    class_exists($arResult['CLASS_NAME'])
){
    $dsObj = new $arResult['CLASS_NAME']();
    if( $arResult['DS']['XML_ID'] == 'pochta' ){
        $zip_code = $arResult['form_data']['zip_code'][$arResult['form_data']['ds_id']];
        if( $zip_code ){
            $arResult['DELIVERY_PRICE'] = $dsObj->getPrice(
                $arResult['basketInfo'], $zip_code
            );
        }
    } else if( $arResult['DS']['XML_ID'] == 'pickpoint_pvz' ){
        $pickpoint_point_number = $arResult['form_data']['pickpoint_point_number'][$arResult['form_data']['ds_id']];
        if( $pickpoint_point_number ){
            $arResult['DELIVERY_PRICE'] = $dsObj->getPrice(
                $arResult['basketInfo'], $pickpoint_point_number
            );
        }
    } else {
        $np_id = $arResult['form_data']['np_id'][$arResult['form_data']['ds_id']];
        if( $np_id ){
            $arResult['DELIVERY_PRICE'] = $dsObj->getPrice(
                $arResult['basketInfo'], $np_id
            );
        }
    }
}

// Скидка на стоимость доставки - при наличии
$discount_procent = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'DELIVERY_PRICE_DISCOUNT');

if( $discount_procent > 0 ){
    $arResult['DELIVERY_PRICE'] = round($arResult['DELIVERY_PRICE'] * (100 - $discount_procent)/100, 2);
}

if( $arResult['DELIVERY_PRICE'] ){
    $arResult['DELIVERY_PRICE_FORMAT'] = number_format($arResult['DELIVERY_PRICE'], project\catalog::PRICE_ROUND, ",", " ");
} else {
    $arResult['DELIVERY_PRICE_FORMAT'] = 'не опред.';
}

if( $arResult['basketInfo']['basket_disc_sum'] >= project\catalog::BESPLAT_DOSTAVKA_SUM ){
    $arResult['DELIVERY_PRICE'] = 0;
    $arResult['DELIVERY_PRICE_FORMAT'] = 'бесплатно';
}

$_SESSION['DELIVERY_PRICE'] = $arResult['DELIVERY_PRICE'];

$arResult['TOTAL_SUM'] = $arResult['basketInfo']['basket_disc_sum'] + $arResult['DELIVERY_PRICE'];
$arResult['TOTAL_SUM_FORMAT'] = number_format($arResult['TOTAL_SUM'], project\catalog::PRICE_ROUND, ",", " ");


$this->IncludeComponentTemplate();
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Order,
Bitrix\Sale\Basket,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('sale');

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;


if( intval($arParams['ORDER_ID']) > 0 ){

    $order = Sale\Order::load(intval($arParams['ORDER_ID']));
    $basket = Sale\Order::load(intval($arParams['ORDER_ID']))->getBasket();
    $arResult['arBasket'] = [];
    foreach ( $basket as $key => $bItem ){
        $bItem->el = tools\el::info($bItem->getProductId());
        $arResult['arBasket'][$key] = $bItem;
    }

    $arResult['ORDER_ID'] = intval($arParams['ORDER_ID']);

    $arResult['ORDER_DATE'] = ConvertDateTime($order->getDateInsert(), "DD", "ru").' '.tools\funcs::getMonthName(ConvertDateTime($order->getDateInsert(), "MM", "ru")).' '.ConvertDateTime($order->getDateInsert(), "YYYY", "ru").ConvertDateTime($order->getDateInsert(), " HH:MI", "ru");

    $arResult['DELIVERY_PRICE'] = $order->getDeliveryPrice();
    $arResult['PAY_SUM'] = $order->getPrice();
    $arResult['BASKET_SUM'] = $arResult['PAY_SUM'] - $arResult['DELIVERY_PRICE'];

    $paymentCollection = $order->getPaymentCollection();
    $shipmentCollection = $order->getShipmentCollection();
    $arResult['PS_ID'] = $paymentCollection[0]->getPaymentSystemId();
    $arResult['PS'] = project\ps::getByID($arResult['PS_ID']);
    $arResult['DS_ID'] = $shipmentCollection[0]->getDeliveryId();
    $arResult['DS'] = project\ds::getByID($arResult['DS_ID']);

    $propValues = project\order::getOrderPropValues($order);
    $arResult['NAME'] = project\order::getPropValue($propValues, 'NAME');
    $arResult['LAST_NAME'] = project\order::getPropValue($propValues, 'LAST_NAME');
    $arResult['SECOND_NAME'] = project\order::getPropValue($propValues, 'SECOND_NAME');
    $arResult['FIO'] = [];
    if( strlen($arResult['LAST_NAME']) > 0 ){  $arResult['FIO'][] = $arResult['LAST_NAME'];  }
    if( strlen($arResult['NAME']) > 0 ){  $arResult['FIO'][] = $arResult['NAME'];  }
    if( strlen($arResult['SECOND_NAME']) > 0 ){  $arResult['FIO'][] = $arResult['SECOND_NAME'];  }
    $arResult['FIO'] = implode(' ', $arResult['FIO']);

    $arResult['PHONE'] = project\order::getPropValue($propValues, 'PHONE');
    $arResult['EMAIL'] = project\order::getPropValue($propValues, 'EMAIL');

    $arResult['ZIP_CODE'] = project\order::getPropValue($propValues, 'ZIP_CODE');
    $arResult['NP'] = project\order::getPropValue($propValues, 'NP');
    $arResult['STREET'] = project\order::getPropValue($propValues, 'STREET');
    $arResult['HOUSE'] = project\order::getPropValue($propValues, 'HOUSE');
    $arResult['KV'] = project\order::getPropValue($propValues, 'KV');
    $arResult['ADDRESS'] = [];
    if( strlen($arResult['ZIP_CODE']) > 0 ){  $arResult['ADDRESS'][] = $arResult['ZIP_CODE'];  }
    if( strlen($arResult['NP']) > 0 ){  $arResult['ADDRESS'][] = $arResult['NP'];  }
    if( strlen($arResult['STREET']) > 0 ){
        $arResult['ADDRESS'][] = 'ул.'.$arResult['STREET'];
    }
    if( strlen($arResult['HOUSE']) > 0 ){
        $arResult['ADDRESS'][] = 'д.'.$arResult['HOUSE'];
    }
    if( strlen($arResult['KV']) > 0 ){
        $arResult['ADDRESS'][] = 'кв.'.$arResult['KV'];
    }
    $arResult['ADDRESS'] = implode(', ', $arResult['ADDRESS']);
    $arResult['ADDRESS'] = preg_replace('/\s*\[[^\[\]]+\]/', '', $arResult['ADDRESS']);

    $arResult['PVZ_POINT'] = project\order::getPropValue($propValues, 'PVZ_POINT');

    $arResult['COMMENT'] = project\order::getPropValue($propValues, 'COMMENT');


    $arResult['vk_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'VK_LINK');
    $arResult['fb_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'FB_LINK');
    $arResult['tw_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'TW_LINK');
    $arResult['insta_link'] = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'INSTA_LINK');
    $arResult['server_name'] = \Bitrix\Main\Config\Option::get('main', 'server_name');



    //echo "<pre>"; print_r( $arResult ); echo "</pre>";

}






$this->IncludeComponentTemplate();
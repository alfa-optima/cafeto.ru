<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if( intval($arResult['ORDER_ID']) > 0 ){ ?>

    <!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html
        style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta name="x-apple-disable-message-reformatting">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="telephone=no" name="format-detection">
        <title>Новое письмо</title>
        <!--[if (mso 16)]>
        <style type="text/css">
            a {text-decoration: none;}
        </style>
        <![endif]-->
        <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
        <!--[if !mso]><!-- -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
        <!--<![endif]-->
        <style type="text/css">
            @media only screen and (max-width:600px) {

                *[class="gmail-fix"] {
                    display: none !important
                }

                /* .es-adaptive table,
                .es-btn-fw,
                .es-btn-fw-brdr,
                .es-left,
                .es-right {
                  width: 100% !important
                } */

                .es-content table,
                .es-header table,
                .es-footer table,
                .es-content,
                .es-footer,
                .es-header {
                    width: 100% !important;
                    max-width: 600px !important
                }

                .es-adapt-td {
                    display: block !important;
                    width: 100% !important
                }

                .adapt-img {
                    width: 100% !important;
                    height: auto !important
                }

                .es-m-p0 {
                    padding: 0px !important
                }

                .es-m-p0r {
                    padding-right: 0px !important
                }

                .es-m-p0l {
                    padding-left: 0px !important
                }

                .es-m-p0t {
                    padding-top: 0px !important
                }

                .es-m-p0b {
                    padding-bottom: 0 !important
                }

                .es-m-p20b {
                    padding-bottom: 20px !important
                }

                .es-mobile-hidden,
                .es-hidden {
                    display: none !important
                }

                .es-desk-hidden {
                    display: table-row !important;
                    width: auto !important;
                    overflow: visible !important;
                    float: none !important;
                    max-height: inherit !important;
                    line-height: inherit !important
                }

                .es-desk-menu-hidden {
                    display: table-cell !important
                }

                table.es-table-not-adapt,
                .esd-block-html table {
                    width: auto !important
                }

                table.es-social {
                    display: inline-block !important
                }

                table.es-social td {
                    display: inline-block !important
                }
            }

            #outlook a {
                padding: 0;
            }

            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .es-button {
                mso-style-priority: 100 !important;
                text-decoration: none !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            .es-desk-hidden {
                display: none;
                float: left;
                overflow: hidden;
                width: 0;
                max-height: 0;
                line-height: 0;
                mso-hide: all;
            }
        </style>
        <style type="text/css">
            #outlook a {
                padding: 0;
            }

            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .es-button {
                mso-style-priority: 100 !important;
                text-decoration: none !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            .es-desk-hidden {
                display: none;
                float: left;
                overflow: hidden;
                width: 0;
                max-height: 0;
                line-height: 0;
                mso-hide: all;
            }
        </style>
    </head>

    <body
        style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;overflow: auto">
    <div class="es-wrapper-color" style="background-color:#F2F2F2;">
        <!--[if gte mso 9]>
        <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
            <v:fill type="tile" color="#f2f2f2"></v:fill>
        </v:background>
        <![endif]-->
        <table class="es-wrapper" width="600" cellspacing="0" cellpadding="0"
               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;max-width:600px;margin: 0 auto; background-repeat:repeat;background-position:center top;">
            <tr class="gmail-fix">
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                        <tr>
                            <td cellpadding="0" cellspacing="0" border="0" height="1" ; style="line-height: 1px; min-width: 600px;">
                                <img src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/spacer.gif" width="600" height="1"
                                     style="display: block; max-height: 1px; min-height: 1px; min-width: 600px; width: 600px;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-collapse:collapse;">
                <td valign="top" style="padding:0;Margin:0;">
                    <table cellpadding="0" cellspacing="0" class="" align="center"
                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
                        <tr style="border-collapse:collapse;">
                            <td align="center" style="padding:0;Margin:0;">
                                <table bgcolor="#ffffff" class="" align="center" cellpadding="0" cellspacing="0" width="600"
                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;table-layout: fixed;">
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-top:15px;padding-bottom:5px;padding-left:20px;padding-right:20px;">
                                            <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="170" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="170" class="es-m-p20b" align="left" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;padding-top: 5px;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;font-size: 13px;">
                                                                        Номер заказа: <span
                                                                            style="display: inline-block; padding:2px 4px;background-color: #ffcc00;"><?=$arResult['ORDER_ID']?></span>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="210" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="210" align="center" class="es-m-p20b" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <img style="width: 125px;" src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/logo.png" alt="logo">
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="0"></td><td width="180" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-right" align="right"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" align="left" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="right" style="padding:0;Margin:0;padding-top: 5px;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#1c130c;;font-size: 13px;;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;">
                                                                        <?=$arResult['ORDER_DATE']?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </td>
                                    </tr>
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:16px;padding-right:16px;padding-top:10px;padding-bottom:25px;border-bottom: 1px solid #ffcc00">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="center" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size: 26px;color: #1c130c;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        Спасибо за ваш заказ!</p>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:20px;padding-right:20px;padding-top:23px;padding-bottom:5px;">
                                            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="200" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color: #1c130c;">
                                                                        Личные данные</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="180" valign="top"><![endif]-->

                                            <!--[if mso]></td><td width="20"></td><td width="180" valign="top"><![endif]-->

                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </td>
                                    </tr>
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:16px;padding-right:16px;padding-top:5px;padding-bottom:0;">
                                            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="200" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" align="center" style="padding:0;Margin:0;max-width: 180px;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Ф.И.О:</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="180" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" align="center" style="padding:0;Margin:0;max-width: 180px;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Контактный телефон:</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="20"></td><td width="180" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-right"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" align="center" style="padding:0;Margin:0;max-width: 180px;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Электронная почта:</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </td>
                                    </tr>
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:16px;padding-right:16px;padding-top:0;padding-bottom:25px;border-bottom: 1px solid #f2f2f2">
                                            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="200" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        <?=$arResult['FIO']?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="es-hidden" width="20" style="padding:0;Margin:0;"></td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="180" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" class="es-m-p20b" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        <?=$arResult['PHONE']?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="20"></td><td width="180" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-right" align="right"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="180" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        <?=$arResult['EMAIL']?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </td>
                                    </tr>

                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:20px;padding-right:20px;padding-top:25px;padding-bottom:14px;">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        Информация о заказе</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:5px;">
                                            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="150" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="275" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Название товара</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="es-hidden" width="5" style="padding:0;Margin:0;"></td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="150" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="110" class="es-m-p20b" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="right" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Цена за ед., руб.</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="es-hidden" width="5" style="padding:0;Margin:0;"></td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="130" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="60" class="es-m-p20b" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="right" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Кол-во</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td><td width="20"></td><td width="130" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-right" align="right"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="85" align="center" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="right" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Сумма, руб.</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </td>
                                    </tr>

<? $cnt = 0;
foreach( $arResult['arBasket'] as $bItem ){ $cnt++; ?>

    <tr style="border-collapse:collapse;">
        <td align="left" <? if( $cnt < count($arResult['arBasket']) ){ ?>style="Margin:0;padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:15px;"<? } else { ?>style="Margin:0;padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:38px;border-bottom: 1px solid #f2f2f2;"<? } ?>
            valign="middle">
            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="120" valign="top"><![endif]-->

            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                <tr style="border-collapse:collapse;">
                    <td width="40" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;" align="center" valign="center">
                                <img src="https://<?=$arResult['server_name']?><?=tools\funcs::rIMGG($bItem->el['DETAIL_PICTURE'], 4, 40, 40)?>">
                            </tr>
                        </table>
                    </td>
                    <td class="es-hidden" width="5" style="padding:0;Margin:0;padding-top: 12px;"></td>
                </tr>
            </table>
            <!--[if mso]></td><td width="120" valign="top"><![endif]-->
            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                <tr style="border-collapse:collapse;">
                    <td width="240" class="es-m-p20b" align="center" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="left" style="padding:0;Margin:0;padding-top: 12px;">
                                    <p
                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                        <?=$bItem->el['NAME']?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="es-hidden" width="5" style="padding:0;Margin:0;"></td>
                </tr>
            </table>
            <!--[if mso]></td><td width="120" valign="top"><![endif]-->
            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                <tr style="border-collapse:collapse;">
                    <td width="100" class="es-m-p20b" align="center" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="right" style="padding:0;Margin:0;padding-top: 12px;">
                                    <p
                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                        <?=number_format($bItem->getPrice(), 2, ",", " ")?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="es-hidden" width="5" style="padding:0;Margin:0;"></td>
                </tr>
            </table>
            <!--[if mso]></td><td width="100" valign="top"><![endif]-->
            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                <tr style="border-collapse:collapse;">
                    <td width="60" align="center" class="es-m-p20b" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="right" style="padding:0;Margin:0;padding-top: 12px;">
                                    <p
                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                        <?=number_format($bItem->getQuantity(), 0, ",", " ")?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if mso]></td><td width="20"></td><td width="100" valign="top"><![endif]-->
            <table cellpadding="0" cellspacing="0" class="es-right" align="right"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
                <tr style="border-collapse:collapse;">
                    <td width="85" align="left" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="right" style="padding:0;Margin:0;padding-top: 12px;">
                                    <p
                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                        <?=number_format($bItem->getPrice() * $bItem->getQuantity(), 2, ",", " ")?></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if mso]></td></tr></table><![endif]-->
        </td>

    </tr>

<? } ?>

<tr style="border-collapse:collapse;">
    <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-top:25px;padding-bottom:25px;border-bottom: 1px solid #f2f2f2">

        <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="280" valign="top"><![endif]-->

        <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
            <tr style="border-collapse:collapse;">
                <td width="280" class="es-m-p20b" align="left" style="padding:0;Margin:0;">
                    <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                        <tr style="border-collapse:collapse;">
                            <td align="right" style="padding:0;Margin:0;padding-right: 10px;border-right: 1px solid #F2F2F2">
                                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">Стоимость: <?=number_format($arResult['BASKET_SUM'], 2, ",", " ")?> Руб</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--[if mso]></td><td width="20"></td><td width="280" valign="top"><![endif]-->
        <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
            <tr style="border-collapse:collapse;">
                <td width="280" align="left" style="padding:0;Margin:0;">
                    <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                        <tr style="border-collapse:collapse;">
                            <td align="left" style="padding:0;Margin:0;padding-left: 10px;;">
                                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                    Стоимость доставки: <?=number_format($arResult['DELIVERY_PRICE'], 2, ",", " ")?> Руб</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--[if mso]></td></tr></table><![endif]-->
    </td>
</tr>


                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:20px;padding-right:20px;padding-top:25px;padding-bottom:15px;">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="568" align="center" valign="top" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        Оплата и доставка</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:15px;">
                                            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="280" valign="top"><![endif]-->
                                            <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="210" class="es-m-p20b" align="left" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td width="45" align="left" style="padding:0;Margin:0;padding-right: 2px;">
                                                                    <img style="width: 35px;" src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/delivery.png" alt="">
                                                                </td>
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Вариант доставки</p>
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        <?=$arResult['DS']['NAME']?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>

                                    <tr style="border-collapse:collapse;">
                                        <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-top:5px;padding-bottom:15px;">

                                            <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="280" valign="top"><![endif]-->

                                            <!--[if mso]></td><td width="20"></td><td width="280" valign="top"><![endif]-->
                                            
                                            <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="350" align="left" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px; float:left;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td width="45" align="center" style="padding:0;Margin:0;padding-right: 2px;">
                                                                    <img style="width: 28px;" src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/pay.png" alt="">
                                                                </td>
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Вариант оплаты</p>
                                                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        <?=$arResult['PS']['NAME']?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </td>
                                    </tr>
                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:16px;padding-right:16px;padding-top:15px;padding-bottom:15px;">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td width="45" align="center" style="padding:0;Margin:0;padding-right: 2px;">
                                                                    <img style="width: 20px;" src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/pickpoint.png" alt="">
                                                                </td>
                                                                <td align="left" style="padding:0;Margin:0;">

<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">

    <? if( strlen($arResult['PVZ_POINT']) > 0 ){
        echo 'Точка самовывоза:';
    } else if( strlen($arResult['ADDRESS']) > 0 ){
        echo 'Адрес доставки:';
    } ?>

</p>

<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">

    <? if( strlen($arResult['PVZ_POINT']) > 0 ){
        echo $arResult['PVZ_POINT'];
    } else if( strlen($arResult['ADDRESS']) > 0 ){
        echo $arResult['ADDRESS'];
    } ?>

</p>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="Margin:0;padding-left:16px;padding-right:16px;padding-top:15px;padding-bottom:30px;border-bottom: 1px solid #f2f2f2;">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0;padding-right: 2px;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td width="45" align="center" style="padding:0;Margin:0;">
                                                                    <img style="width: 25px;" src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/bubble.png" alt="">
                                                                </td>
                                                                <td align="left" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">
                                                                        Комментарий</p>
                                                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        <?=strlen($arResult['COMMENT']) > 0?$arResult['COMMENT']:'-'?></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                    <tr style="border-collapse:collapse;">
                                        <td align="left" style="Margin:0;padding-left:16px;padding-right:16px;padding-top:30px;padding-bottom:30px;">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="center" style="padding:0;Margin:0;">
                                                                    <p
                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:26px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1c130c;">
                                                                        Итого к оплате: <?=number_format($arResult['PAY_SUM'], 2, ",", " ")?> Руб</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border-collapse:collapse;">
                <td valign="top" style="padding:0;Margin:0;">
                    <table cellpadding="0" cellspacing="0" class="" align="center"
                           style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
                        <tr style="border-collapse:collapse;">
                            <td align="center" style="padding:0;Margin:0;">
                                <table bgcolor="#f9f9f9" class="es-content-body" align="center" cellpadding="0" cellspacing="0"
                                       width="600"
                                       style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#F9F9F9;">
                                    <tr style="border-collapse:collapse;">
                                        <td align="left" style="padding:0;Margin:0;padding-top:28px;padding-left:20px;padding-right:20px;">
                                            <table width="100%" cellspacing="0" cellpadding="0"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td class="es-m-p0r es-m-p20b" width="560" valign="top" align="center"
                                                        style="padding:0;Margin:0;">
                                                        <table width="100%" cellspacing="0" cellpadding="0"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <img src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/coffe.png" alt="coffe" style="width: 50px;">
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <? // mailPhrase
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:news.list", "mailPhrase",
                                        array(
                                            "COMPONENT_TEMPLATE" => "mailPhrase",
                                            "IBLOCK_TYPE" => "content",
                                            "IBLOCK_ID" => "20",
                                            "NEWS_COUNT" => "1",
                                            "SORT_BY1" => "SORT",
                                            "SORT_ORDER1" => "ASC",
                                            "SORT_BY2" => "RAND",
                                            "SORT_ORDER2" => "ASC",
                                            "FILTER_NAME" => "",
                                            "FIELD_CODE" => array(),
                                            "PROPERTY_CODE" => array(), "CHECK_DATES" => "Y", "DETAIL_URL" => "",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "10",
                                            "CACHE_FILTER" => "Y",
                                            "CACHE_GROUPS" => "Y",
                                            "PREVIEW_TRUNCATE_LEN" => "",
                                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                            "SET_TITLE" => "N",
                                            "SET_BROWSER_TITLE" => "N",
                                            "SET_META_KEYWORDS" => "N",
                                            "SET_META_DESCRIPTION" => "N",
                                            "SET_STATUS_404" => "N",
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                            "PARENT_SECTION" => "",
                                            "PARENT_SECTION_CODE" => "",
                                            "INCLUDE_SUBSECTIONS" => "Y",
                                            "DISPLAY_DATE" => "Y",
                                            "DISPLAY_NAME" => "Y",
                                            "DISPLAY_PICTURE" => "Y",
                                            "DISPLAY_PREVIEW_TEXT" => "Y",
                                            "PAGER_TEMPLATE" => ".default",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "N",
                                            "PAGER_TITLE" => "Новости",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "N",
                                            "AJAX_OPTION_ADDITIONAL" => "undefined",
                                            "SET_LAST_MODIFIED" => "N",
                                            "PAGER_BASE_LINK_ENABLE" => "N",
                                            "SHOW_404" => "N",
                                            "MESSAGE_404" => ""
                                        ),
                                        false
                                    ); ?>



                <tr style="border-collapse:collapse;">

                    <td align="center" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;">

                    <!--[if mso]>
                    <table width="560" cellpadding="0" cellspacing="0"><tr>
                    <![endif]-->

                    <? if( $arResult['fb_link'] ){ ?>

                        <!--[if mso]>
                        <td width="194" valign="top">
                        <![endif]-->

                            <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;margin-left: 210px;">
                                <tr style="border-collapse:collapse;">
                                    <td width="41px" height="41px" class="es-m-p0r es-m-p20b" align="center" style="padding:0;Margin:0;">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                            <tr style="border-collapse:collapse;">
                                                <a href="<?=$arResult['fb_link']?>" style="display: block; padding: 7px;">
                                                    <img src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/fb.png" alt="facebook" style="width: 25px; height: 25px">
                                                </a>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        <!--[if mso]>
                        </td>
                        <![endif]-->

                    <? } ?>

                    <? if( $arResult['vk_link'] ){ ?>

                        <!--[if mso]>
                        <td width="173" valign="top">
                        <![endif]-->

                            <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;margin: 0 5px;">
                                <tr style="border-collapse:collapse;">
                                    <td width="41px" height="41px" class="es-m-p20b" align="center" style="padding:0;Margin:0;">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                            <tr style="border-collapse:collapse;">
                                                <a href="<?=$arResult['vk_link']?>" style="display: block; padding: 7px;">
                                                    <img src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/vk.png" alt="vk" style="width: 25px; height: 25px">
                                                </a>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        <!--[if mso]>
                        </td>
                        <![endif]-->

                    <? } ?>

                    <? if( $arResult['insta_link'] ){ ?>

                        <!--[if mso]>
                        <td width="20"></td>
                        <td width="173" valign="top">
                        <![endif]-->

                            <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
                                <tr style="border-collapse:collapse;">
                                    <td width="41px" height="41px" align="center" style="padding:0;Margin:0;">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                            <tr style="border-collapse:collapse;">
                                                <a href="<?=$arResult['insta_link']?>" style="display: block; padding: 7px;">
                                                    <img src="https://<?=$arResult['server_name']?>/local/components/aoptima/mailOrder/templates/default/images/insta.png" alt="instagram" style="width: 25px; height: 25px">
                                                </a>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        <!--[if mso]>
                        </td>
                        <![endif]-->

                    <? } ?>



                <!--[if mso]></tr></table><![endif]-->


                    </td>
                </tr>

                                    <tr style="border-collapse:collapse;">
                                        <td align="left"
                                            style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;padding-bottom: 20px;">
                                            <table cellpadding="0" cellspacing="0" width="100%"
                                                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                <tr style="border-collapse:collapse;">
                                                    <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                                            <tr style="border-collapse:collapse;">
                                                                <td align="center" style="padding:0;Margin:0;">
<!--                                                                    <p-->
<!--                                                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:11px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#838383;">-->
<!--                                                                        Не хотите больше получать такие письма? Вы можете <a href="#"-->
<!--                                                                                                                             style="color:inherit; text-decoration: none; display: inline-block; line-height: normal;;border-bottom:1px solid #ffcc00">отписаться</a>-->
<!--                                                                        от этой-->
<!--                                                                        рассылки.</p>-->
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>



                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </body>

    </html>

<? } ?>
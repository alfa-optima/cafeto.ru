<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');  use AOptima\ToolsCafeto as tools;
\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');  use AOptima\ProjectCafeto as project;

$arResult['q'] = trim(strip_tags($_GET['q']));

$arResult['SEARCH_CATALOG_ITEMS'] = [];  $arResult['SEARCH_BLOG_ITEMS'] = [];

if( strlen($arResult['q']) > 0 ){

    \Bitrix\Main\Loader::includeModule('search');

    // ищем в каталоге
    $obSearch = new \CSearch;
    $obSearch->Search(array( "QUERY" => $arResult['q'], "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => project\catalog::IBLOCK_ID))), array("TITLE_RANK"=>"DESC"), array('STEMMING' => true));
    if ($obSearch->errorno != 0){} else {
        while($item = $obSearch->GetNext()){
            $el = tools\el::info($item['ITEM_ID']);
            if ( intval($el['ID']) > 0 && $el['ACTIVE'] == 'Y' ){
                $arResult['SEARCH_CATALOG_ITEMS'][$el['ID']] = $el['ID'];
            }
        }
    }

    // ищем в блоге
    $obSearch = new \CSearch;
    $obSearch->Search(array( "QUERY" => $arResult['q'], "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => project\blog::IBLOCK_ID))), array("TITLE_RANK"=>"DESC"), array('STEMMING' => true));
    if ($obSearch->errorno != 0){} else {
        while($item = $obSearch->GetNext()){
            $el = tools\el::info($item['ITEM_ID']);
            if ( intval($el['ID']) > 0 && $el['ACTIVE'] == 'Y' ){
                $arResult['SEARCH_BLOG_ITEMS'][$el['ID']] = $el['ID'];
            }
        }
    }
    
    $_SESSION['SEARCH_CATALOG_ITEMS'] = $arResult['SEARCH_CATALOG_ITEMS'];
    $_SESSION['SEARCH_BLOG_ITEMS'] = $arResult['SEARCH_BLOG_ITEMS'];

}




$this->IncludeComponentTemplate();
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project; ?>


<div class="template page search-template">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "catalog",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

                <div class="template__grid">

                    <? if(
                        count($arResult['SEARCH_CATALOG_ITEMS']) > 0
                        ||
                        count($arResult['SEARCH_BLOG_ITEMS']) > 0
                    ){ ?>

                        <h1 class="title template__title page__title">По запросу "<?=$arResult['q']?>" найдено:</h1>

                        <div class="page__grid">
                            <div class="custom-tabs">

                                <header class="custom-tabs__header">

                                    <? if( count($arResult['SEARCH_CATALOG_ITEMS']) > 0 ){ ?>
                                        <button type="button" class="custom-tabs__btn custom-tabs__btn_active">Каталог</button>
                                    <? } ?>

                                    <? if( count($arResult['SEARCH_BLOG_ITEMS']) > 0 ){ ?>
                                        <button type="button" class="custom-tabs__btn <? if( count($arResult['SEARCH_CATALOG_ITEMS']) == 0 ){ ?>custom-tabs__btn_active<? } ?>">Блог</button>
                                    <? } ?>

                                </header>

                                <div class="custom-tabs__grid">

                                    <? // Товары каталога
                                    if( count($arResult['SEARCH_CATALOG_ITEMS']) > 0 ){

                                        $GLOBALS['search_goods']['ID'] = $arResult['SEARCH_CATALOG_ITEMS'];
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:catalog.section", "search_goods",
                                            Array(
                                                "IDS" => $arResult['SEARCH_CATALOG_ITEMS'],
                                                "STOP_IDS" => [],
                                                "MAX_CNT" => project\catalog::MAX_CNT,
                                                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                                                "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                                                "SECTION_USER_FIELDS" => array(),
                                                "ELEMENT_SORT_FIELD" => 'NAME',
                                                "ELEMENT_SORT_ORDER" => 'ASC',
                                                "ELEMENT_SORT_FIELD2" => 'NAME',
                                                "ELEMENT_SORT_ORDER2" => 'ASC',
                                                "FILTER_NAME" => 'search_goods',
                                                "HIDE_NOT_AVAILABLE" => "N",
                                                "PAGE_ELEMENT_COUNT" => count($arResult['SEARCH_CATALOG_ITEMS']),
                                                "LINE_ELEMENT_COUNT" => "3",
                                                "PROPERTY_CODE" => array(),
                                                "OFFERS_LIMIT" => 0,
                                                "TEMPLATE_THEME" => "",
                                                "PRODUCT_SUBSCRIPTION" => "N",
                                                "SHOW_DISCOUNT_PERCENT" => "N",
                                                "SHOW_OLD_PRICE" => "N",
                                                "MESS_BTN_BUY" => "Купить",
                                                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                                "MESS_BTN_DETAIL" => "Подробнее",
                                                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                                "SECTION_URL" => "",
                                                "DETAIL_URL" => "",
                                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                                "AJAX_MODE" => "N",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N",
                                                "CACHE_TYPE" => 'A',
                                                "CACHE_TIME" => '36000',
                                                "CACHE_GROUPS" => "Y",
                                                "SET_META_KEYWORDS" => "N",
                                                "META_KEYWORDS" => "",
                                                "SET_META_DESCRIPTION" => "N",
                                                "META_DESCRIPTION" => "",
                                                "BROWSER_TITLE" => "-",
                                                "ADD_SECTIONS_CHAIN" => "N",
                                                "DISPLAY_COMPARE" => "N",
                                                "SET_TITLE" => "N",
                                                "SET_STATUS_404" => "N",
                                                "CACHE_FILTER" => "Y",
                                                "PROPERTY_CODE" => array(),
                                                "PRICE_CODE" => array(
                                                    project\catalog::PRICE_CODE
                                                ),
                                                "USE_PRICE_COUNT" => "N",
                                                "SHOW_PRICE_COUNT" => "1",
                                                "PRICE_VAT_INCLUDE" => "Y",
                                                "CONVERT_CURRENCY" => "N",
                                                "BASKET_URL" => "/personal/basket.php",
                                                "ACTION_VARIABLE" => "action",
                                                "PRODUCT_ID_VARIABLE" => "id",
                                                "USE_PRODUCT_QUANTITY" => "N",
                                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                                "PRODUCT_PROPS_VARIABLE" => "prop",
                                                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                                "PRODUCT_PROPERTIES" => "",
                                                "PAGER_TEMPLATE" => "",
                                                "DISPLAY_TOP_PAGER" => "N",
                                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                                "PAGER_TITLE" => "Товары",
                                                "PAGER_SHOW_ALWAYS" => "N",
                                                "PAGER_DESC_NUMBERING" => "N",
                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                "PAGER_SHOW_ALL" => "Y",
                                                "ADD_PICT_PROP" => "-",
                                                "LABEL_PROP" => "-",
                                                'INCLUDE_SUBSECTIONS' => "Y",
                                                'SHOW_ALL_WO_SECTION' => "Y"
                                            )
                                        );
                                    }

                                    // Статьи блога
                                    if( count($arResult['SEARCH_BLOG_ITEMS']) > 0 ){

                                        $GLOBALS['search_blog_list']['ID'] = $_SESSION['SEARCH_BLOG_ITEMS'];
                                        $APPLICATION->IncludeComponent(
                                            "bitrix:news.list", "search_blog_list",
                                            array(
                                                "IDS" => $arResult['SEARCH_BLOG_ITEMS'],
                                                "OPENED" => count($_SESSION['SEARCH_CATALOG_ITEMS'])==0?'Y':'N',
                                                "COMPONENT_TEMPLATE" => "default",
                                                "IBLOCK_TYPE" => "content",
                                                "IBLOCK_ID" => project\blog::IBLOCK_ID,
                                                "MAX_COUNT" => project\blog::MAX_CNT,
                                                "NEWS_COUNT" => count($_SESSION['SEARCH_BLOG_ITEMS']),
                                                "SORT_BY1" => 'PROPERTY_SORT_DATE',
                                                "SORT_ORDER1" => 'DESC',
                                                "SORT_BY2" => 'NAME',
                                                "SORT_ORDER2" => 'ASC',
                                                "FILTER_NAME" => "search_blog_list",
                                                "FIELD_CODE" => array(),
                                                "PROPERTY_CODE" => array('SORT_DATE', 'AUTHOR_NAME', 'AUTHOR_PHOTO'),
                                                "CHECK_DATES" => "Y",
                                                "DETAIL_URL" => "",
                                                "AJAX_MODE" => "N",
                                                "AJAX_OPTION_JUMP" => "N",
                                                "AJAX_OPTION_STYLE" => "Y",
                                                "AJAX_OPTION_HISTORY" => "N",
                                                "CACHE_TYPE" => "A",
                                                "CACHE_TIME" => "36000000",
                                                "CACHE_FILTER" => "Y",
                                                "CACHE_GROUPS" => "Y",
                                                "PREVIEW_TRUNCATE_LEN" => "",
                                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                                "SET_TITLE" => "N",
                                                "SET_BROWSER_TITLE" => "N",
                                                "SET_META_KEYWORDS" => "N",
                                                "SET_META_DESCRIPTION" => "N",
                                                "SET_STATUS_404" => "N",
                                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                                "ADD_SECTIONS_CHAIN" => "N",
                                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                                "PARENT_SECTION" => "",
                                                "PARENT_SECTION_CODE" => "",
                                                "INCLUDE_SUBSECTIONS" => "Y",
                                                "DISPLAY_DATE" => "Y",
                                                "DISPLAY_NAME" => "Y",
                                                "DISPLAY_PICTURE" => "Y",
                                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                                "PAGER_TEMPLATE" => ".default",
                                                "DISPLAY_TOP_PAGER" => "N",
                                                "DISPLAY_BOTTOM_PAGER" => "N",
                                                "PAGER_TITLE" => "Новости",
                                                "PAGER_SHOW_ALWAYS" => "N",
                                                "PAGER_DESC_NUMBERING" => "N",
                                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                                "PAGER_SHOW_ALL" => "N",
                                                "AJAX_OPTION_ADDITIONAL" => "undefined",
                                                "SET_LAST_MODIFIED" => "N",
                                                "PAGER_BASE_LINK_ENABLE" => "N",
                                                "SHOW_404" => "N",
                                                "MESSAGE_404" => ""
                                            ),
                                            false
                                        );

                                    } ?>

                                </div>
                            </div>
                        </div>

                    <? } else if( strlen($arResult['q']) > 0 ){ ?>

                        <h1 class="title template__title page__title">По запросу "<?=$arResult['q']?>" ничего не найдено</h1>

                    <? } else { ?>

                        <h1 class="title template__title page__title">Пустой поисковый запрос</h1>

                    <? } ?>

                </div>

            </div>
        </div>
    </div>
</div>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools; ?>


<div class="template page">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "page",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

                <div class="template__grid">
                    <h1 class="title template__title page__title">Методы заваривания</h1>
                    <div class="page__grid brew">
                        <div class="row">

                            <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

                                <div class="col-6 col-lg-4 brew__col">
                                    <a href="/brewing/<?=$arItem['CODE']?>/" class="brew__item" style="background: url(<?=tools\funcs::rIMGG($arItem['PICTURE'], 5, 700, 480)?>) no-repeat 50%; background-size: cover;">
                                        <p class="brew__name"><?=$arItem['NAME']?></p>
                                    </a>
                                </div>

                            <? } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
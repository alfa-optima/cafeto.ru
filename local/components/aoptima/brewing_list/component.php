<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$APPLICATION->SetPageProperty("title", 'Методы заваривания');
$APPLICATION->AddChainItem('Методы заваривания', '/brewing/');


$arResult['ITEMS'] = project\brewing::sections();






$this->IncludeComponentTemplate();
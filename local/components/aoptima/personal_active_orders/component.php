<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale\Internals;
use Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';
$arResult['IS_ADMIN'] = $USER->IsAdmin()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){

    $arResult['ITEMS'] = [];

    $arResult['MAX_CNT'] = 10;
    $filter_array = array(
        'USER_ID' => $USER->GetID(),
        array(
            "LOGIC" => "OR",
            array( '!STATUS_ID' => 'F' ),
            array( '!CANCELED' => 'Y' )
        ),
    );
    if( is_array($_POST['stop_ids']) && count($_POST['stop_ids']) > 0 ){
        $filter_array['!ID'] = $_POST['stop_ids'];
    }
    $orders = \Bitrix\Sale\OrderTable::getList(array(
        'filter' => $filter_array,
        'select' => array('*'),
        'order' => array('ID' => 'DESC'),
        'limit' => $arResult['MAX_CNT']+1
    ));
    while ($order = $orders->fetch()){

        $order['object'] = Sale\Order::load($order['ID']);
        $order['basket'] = $order['object']->getBasket();
        foreach( $order['basket'] as $key => $bItem ){
            $order['basket'][$key]->el = tools\el::info($bItem->getProductID());
        }

        $arResult['ITEMS'][$order['ID']] = $order;
    }

}







$this->IncludeComponentTemplate();
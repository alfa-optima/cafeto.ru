<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

if( $arParams['IS_AJAX'] != 'Y' ){ ?>

    <div class="template page personal">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs template__breadcrumbs personal__breadcrumbs">
                        <a href="#" class="link breadcrumbs__item breadcrumb__link">Главная</a>
                        <p class="breadcrumbs__item breadcrumbs__sep">/</p>
                        <p class="breadcrumbs__item breadcrumbs__name">Корзина</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="personal__grid">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="personal__wrapper">
                            <div class="row">
                                <div class="col-12 col-lg-3 col-xl-2 offset-xl-1">
                                    <aside class="personal-menu">
                                        <nav class="personal-menu__grid">
                                            <a href="#" class="personal-menu__link">Личный кабинет</a>
                                            <a href="#" class="personal-menu__link">Изменить данные</a>
                                            <a href="#" class="personal-menu__link">История
                                                покупок</a>
                                            <a href="#"
                                               class="personal-menu__link personal-menu__link_active">Активные
                                                заказы</a>
                                            <a href="#" class="personal-menu__link">Выход</a>
                                        </nav>
                                    </aside>
                                </div>
                                <div class="col-12 col-lg-9 col-xl-8">
                                    <div class="personal__content">

                                        <h1 class="title personal__title">Активные заказы</h1>

<div class="personal__list history-page">
    <div class="history-page__table">

        <? if( count($arResult['ITEMS']) > 0 ){ ?>

            <table>

                <tr>
                    <th>Заказ</th>
                    <th>Статус</th>
                    <th>Сумма заказа</th>
                </tr>

                <? $cnt = 0;
                foreach( $arResult['ITEMS'] as $order_id => $order ){ $cnt++;
                    if( $cnt <= $arResult['MAX_CNT'] ){ ?>

                        <tr>
                            <td>
                                <p class="history-page__item">Заказ №<?=$order['ID']?> от <?=ConvertDateTime($order['DATE_INSERT'], "DD.MM.YYYY", "ru")?></p>
                            </td>
                            <td>
                                <p class="history-page__item">Оплачен</p>
                            </td>
                            <td>
                                <p class="history-page__item">2.500 р</p>
                            </td>
                        </tr>

                    <? }
                } ?>

            </table>

        <? } else { ?>

            <p class="no___count">У вас пока нет заказов</p>

        <? } ?>

    </div>
</div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? } else {

    $cnt = 0;
    foreach( $arResult['ITEMS'] as $order_id => $order ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){ ?>

            <tr class="order___item" item_id="<?=$order_id?>">
                <td>
                    <p class="history-page__item">Заказ 01.02.2019 11:34</p>
                </td>
                <td>
                    <p class="history-page__item">Собирается</p>
                </td>
                <td>
                    <p class="history-page__item">2.500 р</p>
                </td>
            </tr>

        <? } else {
            echo '<tr class="ost"></tr>';
        }
    }

} ?>

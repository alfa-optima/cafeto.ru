<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

if( $arParams['basketInfo'] ){
    $arResult['basketInfo'] = $arParams['basketInfo'];
} else {
    $arResult['basketInfo'] = project\user_basket::info();
}



$this->IncludeComponentTemplate();
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools; ?>

<button type="button" class="basket header__basket basket_small_1">
    <svg width="16px" height="20px" viewBox="0 0 16 20">
        <path fill="#706f70" d="M30.72,22a2,2,0,0,1,2,2.09h1.42a3.4,3.4,0,1,0-6.78,0h1.41A2,2,0,0,1,30.72,22Zm8,17.67-.93-14.14a.74.74,0,0,0-.71-.71H24.37a.73.73,0,0,0-.71.71l-.94,14.18a.81.81,0,0,0,.19.56.71.71,0,0,0,.51.23H38a.73.73,0,0,0,.71-.75A.22.22,0,0,0,38.71,39.67ZM29.33,27.2a.9.9,0,1,1-1.79,0v-.54a.9.9,0,1,1,1.79,0Zm4.56,0a.89.89,0,1,1-1.78,0v-.54a.89.89,0,1,1,1.78,0Z" transform="translate(-22.72 -20.5)" />
    </svg>
    <? if( $arResult['basketInfo']['basket_cnt'] > 0 ){ ?>
        <p class="basket__count"><?=$arResult['basketInfo']['basket_cnt']?></p>
    <? } ?>
</button>

<div class="basket-sidebar basket_small_2">
    <div class="overlay basket-sidebar__overlay"></div>
    <div class="basket-sidebar__grid">
        <div class="basket-sidebar__wrapper">

            <header class="basket-sidebar__header">
                <? if( $arResult['basketInfo']['basket_cnt'] > 0 ){ ?>
                    <p class="basket-sidebar__title">В корзине:</p>
                <? } else { ?>
                    <p class="basket-sidebar__title">Корзина</p>
                <? } ?>
                <button type="button" class="basket-sidebar__close">
                    <svg width="18.5px" height="18.5px" viewBox="0 0 18.5 18.5">
                        <path fill="#000" d="M40.85,24.13l-.92-.91-8.35,8.36-7.66-7.66-.89.89,7.66,7.66-8.34,8.34.91.91,8.34-8.34,8,8,.89-.89-8-8Z" transform="translate(-22.35 -23.22)" />
                    </svg>
                </button>
            </header>

            <? if( $arResult['basketInfo']['basket_cnt'] > 0 ){ ?>

                <div class="basket-sidebar__list">

                    <? foreach( $arResult['basketInfo']['arBasket'] as $key => $basketItem ){ ?>

                        <div class="basket-item small_basket_item">
                            <a style="cursor: pointer" class="basket-item__remove basketRemoveButton to___process" item_id="<?=$arResult['basketInfo']['userBasket'][$key]->getField('ID')?>">
                                <svg width="14px" height="16px" viewBox="0 0 14 16">
                                    <path fill="#b9b9b9" d="M39.06,26.8H35.79v-.54a1.75,1.75,0,0,0-1.73-1.76H30.94a1.75,1.75,0,0,0-1.73,1.76v.54H25.94a.45.45,0,0,0,0,.89h.79V38.12a2.36,2.36,0,0,0,2.33,2.38h6.88a2.36,2.36,0,0,0,2.33-2.38V27.69h.79a.45.45,0,0,0,0-.89Zm-9-.54a.86.86,0,0,1,.85-.87h3.12a.86.86,0,0,1,.85.87v.54H30.09ZM37.4,38.12h0a1.48,1.48,0,0,1-1.46,1.49H29.06a1.48,1.48,0,0,1-1.46-1.49V27.69h9.8ZM32.5,38a.44.44,0,0,0,.44-.44V29.73a.44.44,0,1,0-.88,0v7.84A.45.45,0,0,0,32.5,38Zm-2.85-.49a.44.44,0,0,0,.44-.44V30.21a.44.44,0,0,0-.88,0v6.88A.44.44,0,0,0,29.65,37.53Zm5.7,0a.44.44,0,0,0,.44-.44V30.21a.44.44,0,0,0-.88,0v6.88A.44.44,0,0,0,35.35,37.53Z" transform="translate(-25.5 -24.5)" />
                                </svg>
                            </a>
                            <a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>" class="basket-item__img">
                                <? if( intval($basketItem->product['PREVIEW_PICTURE']['ID']) > 0 ){ ?>
                                    <img src="<?=tools\funcs::rIMGG($basketItem->product['PREVIEW_PICTURE'], 4, 85, 85)?>">
                                <? } else { ?>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                                <? } ?>
                            </a>
                            <div class="basket-item__info">
                                <a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>" class="basket-item__name"><?=$basketItem->product['NAME']?></a>
                                <? if( strlen($basketItem->el['PROPERTY_WEIGHT_VALUE']) > 0 ){ ?>
                                    <p class="basket-item__option"><?=$basketItem->el['PROPERTY_WEIGHT_VALUE']?> гр x <?=$basketItem->getQuantity()?> шт</p>
                                <? } else { ?>
                                    <p class="basket-item__option"><?=$basketItem->getQuantity()?> шт</p>
                                <? } ?>
                                <p class="basket-item__price"><?=$basketItem->discPriceFormat?> RUB</p>
                            </div>
                        </div>

                    <? } ?>

                </div>

                <footer class="basket-sidebar__footer">
                    <p class="total basket-sidebar__total">Итого: <span><?=$arResult['basketInfo']['basket_disc_sum_format']?> RUB</span></p>
                    <a href="/basket/" class="btn basket-sidebar__btn">Оформить заказ</a>
                </footer>

            <? } else { ?>

                <div class="basket-sidebar__list">

                    <p>Корзина пуста</p>

                </div>

            <? } ?>

        </div>
    </div>
</div>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<? foreach( $arResult['TREE'] as $key => $sect_1 ){ ?>

    <div class="menu__item <? if( $sect_1['SUBSECTIONS'] ){ ?>has-children<? } ?>">

        <a <? if( strlen($sect_1['URL']) > 0 ){ ?>href="<?=$sect_1['URL']?>"<? } ?> class="menu__link"><?=$sect_1['NAME']?></a>

        <? if( $sect_1['SUBSECTIONS'] ){

            $has3levelSects = false;
            foreach( $sect_1['SUBSECTIONS'] as $sect_2 ){
                if( $sect_2['SUBSECTIONS'] ){   $has3levelSects = true;   }
            } ?>

            <div class="sub-menu">

                <? if( $has3levelSects ){ ?>

                    <div class="sub-menu__wrapper sub-menu__wrapper_s">

                        <? foreach( $sect_1['SUBSECTIONS'] as $sect_2 ){ ?>

                            <div class="sub-menu__col">

                                <p class="sub-menu__name">
                                    <? if( strlen($sect_2['URL']) > 0 ){ ?>
                                        <a href="<?=$sect_1['URL']?>" class="menu__link"><?=$sect_2['NAME']?></a>
                                    <? } else { ?>
                                        <?=$sect_2['NAME']?>
                                    <? } ?>
                                </p>

                                <? if( $sect_2['SUBSECTIONS'] ){
                                    foreach ( $sect_2['SUBSECTIONS'] as $sect_3 ){ ?>

                                        <a href="<?=$sect_3['URL']?>" class="link sub-menu__link"><?=$sect_3['NAME']?></a>

                                    <? }
                                } ?>

                            </div>

                        <? } ?>

                    </div>

                <? } else { ?>

                    <div class="sub-menu__wrapper">

                        <? foreach( $sect_1['SUBSECTIONS'] as $sect_2 ){ ?>

                            <a <? if( strlen($sect_2['URL']) > 0 ){ ?>href="<?=$sect_2['URL']?>"<? } ?> class="link sub-menu__link"><?=$sect_2['NAME']?></a>

                        <? } ?>

                    </div>

                <? } ?>

            </div>

        <? } ?>

    </div>

<? } ?>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;


$arResult['TREE'] = project\top_menu::tree();

foreach ( $arResult['TREE'] as $key => $sect_1 ){
    if( trim(strtolower($sect_1['NAME'])) == 'каталог' ){

        $catalogTree = [];
        $catalogFirstLevelSections = project\catalog::first_level_sections();
        foreach ( $catalogFirstLevelSections as $catalogSect_1 ){

            $catalogCategory = [
                'ID' => $catalogSect_1['ID'],
                'NAME' => $catalogSect_1['NAME'],
                'URL' => $catalogSect_1['SECTION_PAGE_URL'],
            ];

            $sub_sects = tools\section::sub_sects($catalogSect_1['ID']);
            if( count($sub_sects) > 0 ){
                foreach ( $sub_sects as $sub_sect ){
                    $catalogCategory['SUBSECTIONS'][$sub_sect['ID']] = [
                        'ID' => $sub_sect['ID'],
                        'NAME' => $sub_sect['NAME'],
                        'URL' => $sub_sect['SECTION_PAGE_URL'],
                    ];
                }
            }

            $catalogTree[$catalogSect_1['ID']] = $catalogCategory;
        }

        if( count($catalogTree) > 0 ){
            $arResult['TREE'][$key]['SUBSECTIONS'] = $catalogTree;
        }
        
    }
}






$this->IncludeComponentTemplate();
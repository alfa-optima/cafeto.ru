<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

if( $arParams['IS_AJAX'] != 'Y' ){ ?>

    <div class="template page personal">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <? // Хлебные крошки
                    $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb", "personal",
                        Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                    ); ?>

                </div>
            </div>
        </div>

        <div <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>class="personal__grid"<? } ?>>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="personal__wrapper">
                            <div class="row">

                                <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

                                    <div class="col-12 col-lg-3 col-xl-2 offset-xl-1">
                                        <aside class="personal-menu">
                                            <nav class="personal-menu__grid">
                                                <? // personal
                                                $APPLICATION->IncludeComponent(
                                                    "bitrix:menu",  "personal", // Шаблон меню
                                                    Array(
                                                        "ROOT_MENU_TYPE" => "personal", // Тип меню
                                                        "MENU_CACHE_TYPE" => "A",
                                                        "MENU_CACHE_TIME" => "3600",
                                                        "MENU_CACHE_USE_GROUPS" => "N",
                                                        "CACHE_SELECTED_ITEMS" => "N",
                                                        "MENU_CACHE_GET_VARS" => array(""),
                                                        "MAX_LEVEL" => "1",
                                                        "CHILD_MENU_TYPE" => "left",
                                                        "USE_EXT" => "N",
                                                        "DELAY" => "N",
                                                        "ALLOW_MULTI_SELECT" => "N"
                                                    )
                                                ); ?>
                                                <a href="/out.php?to=<?=$_SERVER['REQUEST_URI']?>" class="personal-menu__link">Выход</a>
                                            </nav>
                                        </aside>
                                    </div>

                                    <div class="col-12 col-lg-9 col-xl-8">
                                        <div class="personal__content">

                                            <h1 class="title personal__title"><?$APPLICATION->ShowTitle()?></h1>

                                            <div class="personal__list history-page">

                                                <? if( 1==2 ){ ?>
                                                    <p class="history-page__sale">Персональная скидка:
                                                        <span>
                                                            <svg width="20px" height="19.97px" viewBox="0 0 20 19.97">
                                                                <path fill="#c82850" d="M30.11,28.76a.85.85,0,0,0-.85-.84.84.84,0,1,0,0,1.67A.84.84,0,0,0,30.11,28.76Zm4.57,2.55a.84.84,0,1,0,0,1.67.84.84,0,1,0,0-1.67Zm7,.25-.31-.41a1.29,1.29,0,0,1,0-1.52l.3-.42a1.29,1.29,0,0,0-.46-1.9l-.46-.24a1.28,1.28,0,0,1-.68-1.36l.09-.51a1.31,1.31,0,0,0-1.26-1.51l-.52,0a1.33,1.33,0,0,1-1.22-.94L37,22.24a1.33,1.33,0,0,0-1.8-.83l-.48.2a1.35,1.35,0,0,1-1.52-.33l-.34-.38a1.34,1.34,0,0,0-2,0l-.35.38a1.35,1.35,0,0,1-1.51.35l-.48-.2a1.34,1.34,0,0,0-1.79.86l-.14.5a1.32,1.32,0,0,1-1.21,1l-.52,0a1.32,1.32,0,0,0-1.23,1.54l.1.5a1.29,1.29,0,0,1-.66,1.37l-.46.25a1.27,1.27,0,0,0-.42,1.9l.3.42a1.26,1.26,0,0,1,0,1.51l-.3.42a1.29,1.29,0,0,0,.46,1.9l.46.24a1.3,1.3,0,0,1,.68,1.37l-.09.5A1.31,1.31,0,0,0,25,37.22l.52,0a1.33,1.33,0,0,1,1.22.94l.15.49a1.32,1.32,0,0,0,1.8.83l.48-.2a1.34,1.34,0,0,1,1.51.33l.35.38a1.34,1.34,0,0,0,2,0l.34-.38a1.36,1.36,0,0,1,1.51-.35l.49.2a1.34,1.34,0,0,0,1.79-.86l.14-.5a1.31,1.31,0,0,1,1.2-1l.53,0a1.3,1.3,0,0,0,1.22-1.53l-.09-.51a1.27,1.27,0,0,1,.66-1.37l.46-.25A1.28,1.28,0,0,0,41.71,31.56Zm-14.52-2.8a2.06,2.06,0,1,1,2.06,2A2,2,0,0,1,27.19,28.76ZM29.65,34a.52.52,0,0,1-.71.07.48.48,0,0,1-.06-.69L34.28,27a.5.5,0,0,1,.7-.06.48.48,0,0,1,.07.69Zm5,.21a2,2,0,1,1,2-2A2,2,0,0,1,34.68,34.16Z" transform="translate(-21.97 -20.47)" />
                                                            </svg>
                                                            15%
                                                        </span>
                                                    </p>
                                                <? } ?>

        <div class="history-page__table">

            <? if( count($arResult['ITEMS']) > 0 ){ ?>

                <table class="orders___table">
                    <tr>
                        <th>Заказ</th>
                        <th>Стоимость</th>
                        <th>Статус</th>
                        <th></th>
                    </tr>

                    <? $cnt = 0;
                    foreach( $arResult['ITEMS'] as $order_id => $order ){ $cnt++;
                        if( $cnt <= $arResult['MAX_CNT'] ){ ?>

                            <tr class="order___item" item_id="<?=$order_id?>">

                                <td>

                                    <p class="history-page__item js-item"><b>№<?=$order['ID']?> от <?=ConvertDateTime($order['DATE_INSERT'], "DD.MM.YYYY", "ru")?></b></p>

                                    <? foreach( $order['basket'] as $bItem ){ ?>

                                        <p class="history-page__item history-page__item_s"><?=$bItem->el['NAME']?></p>

                                    <? } ?>

                                    <p class="history-page__item history-page__item_s">Стоимость доставки</p>

                                </td>

                                <td>

                                    <p class="history-page__item js-item">
                                        <b><?=number_format($order['object']->getPrice(), project\catalog::PRICE_ROUND, ",", " ")?> р</b>
                                    </p>

                                    <? foreach( $order['basket'] as $bItem ){ ?>

                                        <p class="history-page__item history-page__item_s"><?=$bItem->getQuantity()?> x <?=number_format($bItem->getPrice(), project\catalog::PRICE_ROUND, ",", " ")?> р</p>

                                    <? } ?>

                                    <p class="history-page__item history-page__item_s"><?=number_format($order['object']->getDeliveryPrice(), project\catalog::PRICE_ROUND, ",", " ")?></p>

                                </td>

                                <td>
                                    <p class="history-page__item"><?=$order['STATUS']?></p>
                                </td>

                                <td>

                                    <? if( intval($order['PS']['ID']) > 0 ){ ?>

                                        <? if( $order['object']->isPaid() || $order['PS']['CODE'] != 'card' ){ ?>

                                            <a style="cursor: pointer" class="history-page__link order_repeat_link to___process" item_id="<?=$order['ID']?>">Повторить</a>

                                        <? } else { ?>

                                            <a style="cursor: pointer" class="history-page__link order_pay_link to___process" item_id="<?=$order['ID']?>">Оплатить</a>

                                        <? } ?>

                                    <? } ?>

                                </td>

                            </tr>

                        <? }
                    } ?>

                </table>

            <? } else { ?>

                <p class="no___count">У вас пока нет заказов</p>

            <? } ?>

            <div class="more_button_block" <? if( $cnt <= $arResult['MAX_CNT'] ){ ?>style="display:none;"<? } ?>>
                <button type="button" class="btn order_history_load_button to___process">Показать ещё</button>
            </div>

        </div>

                                            </div>
                                        </div>
                                    </div>

                                <? } else {

                                    include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_auth_form.php';

                                } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? } else {

    $cnt = 0;
    foreach( $arResult['ITEMS'] as $order_id => $order ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){ ?>

            <tr class="order___item" item_id="<?=$order_id?>">
                <td>

                    <p class="history-page__item js-item"><b>№<?=$order['ID']?> от <?=ConvertDateTime($order['DATE_INSERT'], "DD.MM.YYYY", "ru")?></b></p>

                    <? foreach( $order['basket'] as $bItem ){ ?>

                        <p class="history-page__item history-page__item_s"><?=$bItem->el['NAME']?></p>

                    <? } ?>

                    <p class="history-page__item history-page__item_s">Стоимость доставки</p>

                </td>
                <td>

                    <p class="history-page__item js-item">
                        <b><?=number_format($order['object']->getPrice(), project\catalog::PRICE_ROUND, ",", " ")?> р</b>
                    </p>

                    <? foreach( $order['basket'] as $bItem ){ ?>

                        <p class="history-page__item history-page__item_s"><?=$bItem->getQuantity()?> x <?=number_format($bItem->getPrice(), project\catalog::PRICE_ROUND, ",", " ")?> р</p>

                    <? } ?>

                    <p class="history-page__item history-page__item_s"><?=number_format($order['object']->getDeliveryPrice(), project\catalog::PRICE_ROUND, ",", " ")?></p>

                </td>
                <td>
                    <p class="history-page__item"><?=$order['STATUS']?></p>
                </td>
                <td>

                    <? if( $order['object']->isPaid() || $order['PS']['CODE'] != 'card' ){ ?>

                        <a style="cursor: pointer" class="history-page__link order_repeat_link to___process" item_id="<?=$order['ID']?>">Повторить</a>

                    <? } else { ?>

                        <a style="cursor: pointer" class="history-page__link order_pay_link to___process" item_id="<?=$order['ID']?>">Оплатить</a>

                    <? } ?>

                </td>
            </tr>

        <? } else {
            echo '<tr class="ost"></tr>';
        }
    }

} ?>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale\Internals;
use Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';
$arResult['IS_ADMIN'] = $USER->IsAdmin()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){

    $arResult['ITEMS'] = [];

    $arResult['MAX_CNT'] = 10;
    $filter_array = array(
        'USER_ID' => $USER->GetID()
    );
    if( is_array($_POST['stop_ids']) && count($_POST['stop_ids']) > 0 ){
        $filter_array['!ID'] = $_POST['stop_ids'];
    }
    $orders = \Bitrix\Sale\OrderTable::getList(array(
        'filter' => $filter_array,
        'select' => array('*'),
        'order' => array('ID' => 'DESC'),
        'limit' => $arResult['MAX_CNT']+1
    ));
    while ($order = $orders->fetch()){

        $order['object'] = Sale\Order::load($order['ID']);

        $ps_id = false;
        $order['PS'] = false;
        $paymentCollection = $order['object']->getPaymentCollection();
        if( count($paymentCollection) > 0 ){
            $ps_id = $paymentCollection[0]->getPaymentSystemId();
            $order['PS'] = project\ps::getByID($ps_id);
        }

        $order['basket'] = $order['object']->getBasket();
        foreach( $order['basket'] as $key => $bItem ){
            $order['basket'][$key]->el = tools\el::info($bItem->getProductID());
        }

        if( intval($order['PS']['ID']) > 0 && $order['PS']['CODE'] == 'card' ){
            $order['STATUS'] = 'Принят, не оплачен';
            if( $order['object']->isPaid() ){
                $order['STATUS'] = 'Принят, оплачен';
            }
        } else {
            $order['STATUS'] = 'Принят';
        }

        if( $order['object']->isAllowDelivery() ){
            $order['STATUS'] = 'Выполняется';
        }
        if( $order['object']->isShipped() ){
            $order['STATUS'] = 'Передан в доставку';
        }
        if( $order['STATUS_ID'] == 'F' ){
            $order['STATUS'] = 'Выполнен';
        }
        if( $order['object']->isCanceled() ){
            $order['STATUS'] = 'Отменён';
        }

        $arResult['ITEMS'][$order['ID']] = $order;
    }

}







$this->IncludeComponentTemplate();
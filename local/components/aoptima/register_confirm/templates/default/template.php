<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="template page personal">

    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "catalog",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

            </div>
        </div>
    </div>

    <div class="personal__grid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="personal__wrapper">
                        <div class="row">

                            <div class="col-12 col-lg-9 col-xl-8">
                                <div class="personal__content">

                                    <h1 class="title personal__title">Подтверждение регистрации</h1>

                                    <? if( $arResult['ERROR'] ){ ?>

                                        <p class="error___p"><?=$arResult['ERROR']?></p>

                                    <? } else if( $arResult['SUCCESS'] ){ ?>

                                        <p class="success___p"><?=$arResult['SUCCESS']?></p>

                                    <? } ?>

                                    <p class="success___p">
                                        <a href="#log-in" rel="modal:open" class="link sub-menu__link">Авторизоваться</a>
                                    </p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if( $USER->IsAuthorized() ){    LocalRedirect('/personal/');    }

$arResult['ERROR'] = false;
$arResult['SUCCESS'] = false;

$code = strip_tags($_GET['code']);

if( strlen($code) > 0 ){

    // Поиск пользователя с таким кодом подтверждения
    $filter = Array( "UF_REG_CODE" => $code, "UF_REG_CONFIRMED" => false );
    $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
    if ($user = $rsUsers->GetNext()){

        // Активируем профиль
        $obUser = new \CUser;
        $res = $obUser->Update($user['ID'], array(
            'UF_REG_CONFIRMED' => true,
            'UF_REG_CODE' => ''
        ));

        // Отправка рег. информации
        tools\user::sendRegInfo($user['ID']);

        $arResult['SUCCESS'] = 'Регистрация успешно подтверждена!';


    } else {

        $arResult['ERROR'] = 'Ссылка ошибочная либо устарела';
    }

} else {

    $arResult['ERROR'] = 'Ошибочная ссылка';
}












$this->IncludeComponentTemplate();
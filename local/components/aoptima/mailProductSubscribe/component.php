<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$arResult['server_name'] = \Bitrix\Main\Config\Option::get('main', 'server_name');

$arResult['EMAIL'] = $arParams['email'];
$arResult['PRODUCT'] = $arParams['product'];




$arResult['TITLE'] = '<p>Информация о подписке на товар!</p>';

$arResult['MESSAGE'][] = '';
$arResult['MESSAGE'][] = 'Здравствуйте!';
$arResult['MESSAGE'][] = 'Товар <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$arResult['server_name'].$arResult['PRODUCT']['DETAIL_PAGE_URL'].'">'.$arResult['PRODUCT']['NAME'].'</a> уже в наличии!';




$this->IncludeComponentTemplate();
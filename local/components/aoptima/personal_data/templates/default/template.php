<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="template page personal">

    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "catalog",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

            </div>
        </div>
    </div>

    <div <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>class="personal__grid"<? } ?>>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="personal__wrapper">
                        <div class="row">

                            <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

                                <div class="col-12 col-lg-3 col-xl-2 offset-xl-1">
                                    <aside class="personal-menu">

                                        <nav class="personal-menu__grid">

                                            <? // personal
                                            $APPLICATION->IncludeComponent(
                                                "bitrix:menu",  "personal", // Шаблон меню
                                                Array(
                                                    "ROOT_MENU_TYPE" => "personal", // Тип меню
                                                    "MENU_CACHE_TYPE" => "A",
                                                    "MENU_CACHE_TIME" => "3600",
                                                    "MENU_CACHE_USE_GROUPS" => "N",
                                                    "CACHE_SELECTED_ITEMS" => "N",
                                                    "MENU_CACHE_GET_VARS" => array(""),
                                                    "MAX_LEVEL" => "1",
                                                    "CHILD_MENU_TYPE" => "left",
                                                    "USE_EXT" => "N",
                                                    "DELAY" => "N",
                                                    "ALLOW_MULTI_SELECT" => "N"
                                                )
                                            ); ?>

                                            <a href="/out.php?to=<?=$_SERVER['REQUEST_URI']?>" class="personal-menu__link">Выход</a>

                                        </nav>

                                    </aside>
                                </div>

                                <div class="col-12 col-lg-9 col-xl-8">
                                    <div class="personal__content">

<h1 class="title personal__title">Изменить данные</h1>

<div class="personal__list" style="margin-bottom: 45px;">
    <form class="edit-personal-form" onsubmit="return false;">

        <input name="NAME" type="text" class="personal__list-item" value="<?=$arResult['USER']['NAME']?>" placeholder="Имя пользователя">

        <input name="LAST_NAME" type="text" class="personal__list-item" value="<?=$arResult['USER']['LAST_NAME']?>" placeholder="Фамилия пользователя">

        <input name="SECOND_NAME" type="text" class="personal__list-item" value="<?=$arResult['USER']['SECOND_NAME']?>" placeholder="Отчество пользователя">

        <input name="PERSONAL_PHONE" type="tel" class="personal__list-item" value="<?=$arResult['USER']['PERSONAL_PHONE']?>" placeholder="Телефон">

        <? if( $arResult['IS_ADMIN'] == 'Y' ){ ?>

            <input name="LOGIN" type="email" class="personal__list-item" value="<?=$arResult['USER']['LOGIN']?>" placeholder="Логин">

        <? } ?>

        <? if( $arResult['USER']['EXTERNAL_AUTH_ID'] != 'socservices' ){ ?>

            <input name="EMAIL" type="email" class="personal__list-item" value="<?=$arResult['USER']['EMAIL']?>" placeholder="Email">

        <? } ?>

<!--    <input type="text" class="personal__list-item" value="" placeholder="Адрес">-->

<!--        <label class="checkbox edit-personal-form__checkbox">-->
<!--            <input type="checkbox" name="newsletter" class="checkbox__input">-->
<!--            <p class="checkbox__name">Подписаться на рассылку</p>-->
<!--        </label>-->

        <p class="error___p"></p>

        <button type="button" class="btn edit-personal-form__btn personal_data_button to___process">Сохранить</button>

    </form>
</div>

<? if( $arResult['USER']['EXTERNAL_AUTH_ID'] != 'socservices' ){ ?>

    <h1 class="title personal__title">Изменить пароль</h1>

    <div class="personal__list">
        <form class="edit-personal-form" onsubmit="return false;">

            <input name="PASSWORD" type="password" class="personal__list-item" placeholder="Пароль">

            <input name="CONFIRM_PASSWORD" type="password" class="personal__list-item" placeholder="Подтвердите пароль">

            <p class="error___p"></p>

            <button type="button" class="btn edit-personal-form__btn personal_password_button to___process">Сохранить</button>

        </form>
    </div>

<? } ?>

                                    </div>
                                </div>


                            <? } else {

                                include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_auth_form.php';

                            } ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if (count($arResult["ITEMS"]) > 0){ ?>

    <section class="section about__team team">
    
        <div class="section__grid">
            <div class="row">
                <div class="col-12">
                    
                    <div class="team__wrapper team__carousel swiper-container">
                        <div class="team__grid swiper-wrapper">

                            <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>
                            
                                <div class="team__item swiper-slide">
                                    <div class="row align-items-center">
                                        <div class="col-12 col-lg-6 offset-lg-1 order-2 order-lg-1">
                                            <div class="team__item-info">
                                                <p class="team__item-name"><?=$arItem['NAME']?><span><?=$arItem['PROPERTIES']['POST']['VALUE']?></span>
                                                </p>
                                                <div class="team__item-text">
                                                    <p><?=$arItem['~PREVIEW_TEXT']?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4 offset-lg-1 order-1 order-lg-2">
                                            <div class="team__item-img">
                                                <div class="team__item-img-wrapper">
                                                    <picture>
                                                        <source srcset="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 350, 350)?> 1x, <?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 700, 700)?> 2x">
                                                        <img class="about__header-img" src="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 350, 350)?>">
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            <? } ?>
                            
                        </div>
                    </div>
                    
                    <div class="carousel-info">
                        <button type="button" class="carousel-info__btn carousel-info__btn-prev">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.494 31.494" width="15px" height="15px">
                                <path style="fill:#1E201D;" d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554 c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587 c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z" />
                            </svg>
                        </button>
                        <p class="carousel-info__count"><span>1</span>/<span>5</span></p>
                        <button type="button" class="carousel-info__btn carousel-info__btn-next">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 31.49" width="15px" height="15px">
                                <path style="fill:#1E201D;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111 C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587 c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

<? } ?>
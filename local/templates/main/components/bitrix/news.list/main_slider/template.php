<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if (count($arResult["ITEMS"]) > 0){ ?>


    <div class="slider">

        <div class="slider__grid swiper-container">
            <div class="swiper-wrapper">

                <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

                    <div class="swiper-slide slider__item">
                        <picture>
                            <source srcset="<?=\CFile::GetPath($arItem["PROPERTIES"]['PHOTO_1920_650']['VALUE'])?> 1x, <?=\CFile::GetPath($arItem["PROPERTIES"]['PHOTO_3840_1300']['VALUE'])?> 2x" media="(min-width: 576px)">
                            <!-- Для мобильных устройств -->
                            <source srcset="<?=\CFile::GetPath($arItem["PROPERTIES"]['PHOTO_640_800']['VALUE'])?>  1x, <?=\CFile::GetPath($arItem["PROPERTIES"]['PHOTO_1280_1600']['VALUE'])?> 2x" media="(max-width: 575px)">
                            <!-- Для мобильных устройств -->
                            <img src="<?=\CFile::GetPath($arItem["PROPERTIES"]['PHOTO_1920_650']['VALUE'])?>">
                        </picture>
                        <div class="slider__info">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-12">

                                        <h2 class="slider__title"><?=$arItem['NAME']?></h2>

                                        <? if( strlen($arItem['PREVIEW_TEXT']) > 0 ){ ?>
                                            <p class="slider__text"><?=$arItem['PREVIEW_TEXT']?></p>
                                        <? } ?>

                                        <? if( strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0 ){ ?>
                                            <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="btn slider__btn"><?=strlen($arItem['PROPERTIES']['LINK_TITLE']['VALUE'])>0?$arItem['PROPERTIES']['LINK_TITLE']['VALUE']:'Перейти'?></a>
                                        <? } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <? } ?>

            </div>
        </div>

        <button type="button" class="slider__arrow slider__arrow-prev">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175" width="35" height="35">
                <path fill="#fff" d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225
                        c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z" />
            </svg>
        </button>
        <button type="button" class="slider__arrow slider__arrow-next">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175" width="35" height="35">
                <path fill="#fff" d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5
                        c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z
                        " />
            </svg>
        </button>

        <div class="swiper-pagination slider__pagination"></div>

    </div>

<? } ?>
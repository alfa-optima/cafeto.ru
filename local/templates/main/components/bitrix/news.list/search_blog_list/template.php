<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$maxCNT = $arParams['MAX_COUNT'];

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if ( $arParams['IS_AJAX'] == 'Y' ){

    if( count($arResult["ITEMS"]) > 0 ){
        $cnt = 0;
        foreach( $arParams["IDS"] as $id ){
            $item = false;
            foreach( $arResult["ITEMS"] as $key => $arItem ){
                if( $arItem['ID'] == $id ){   $item = $arItem;   }
            }
            if( $item ){

                $cnt++;
                if( $cnt <= $maxCNT ){
                    $sections = tools\el::sections($item['ID']); ?>

                    <div class="col-12 col-sm-6 col-lg-4 blog-results__col article___item" item_id="<?=$item['ID']?>">
                        <article class="blog-item">

                            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="blog-item__img" style="background: url(<?=tools\funcs::rIMGG($item['PREVIEW_PICTURE'], 5, 340, 240)?>) no-repeat 50%; background-size: cover;"></a>

                            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="link blog-item__title"><?=$item['NAME']?></a>

                            <p class="blog-item__text"><?=$item['PREVIEW_TEXT']?></p>

                            <? if( is_array($sections) && count($sections) > 0 ){ ?>
                                <div class="blog-item__tags">
                                    <? foreach( $sections as $section ){ ?>
                                        <a href="/blog/<?=$section['CODE']?>/" style="cursor: pointer" class="tag setBlogSectionButton to___process"><?=$section['NAME']?></a>
                                    <? } ?>
                                </div>
                            <? } ?>

                            <div class="blog-item__author">
                                <? if( strlen($item['PROPERTIES']['AUTHOR_NAME']['VALUE']) > 0 ){ ?>
                                    <? if( intval($item['PROPERTIES']['AUTHOR_PHOTO']['VALUE']) > 0 ){ ?>
                                        <div class="blog-item__author-img" style="background: url(<?=tools\funcs::rIMGG($arItem['PROPERTIES']['AUTHOR_PHOTO']['VALUE'], 5, 35, 35)?>) no-repeat 50%; background-size: cover;"></div>
                                    <? } ?>
                                    <p class="blog-item__author-name"><?=$item['PROPERTIES']['AUTHOR_NAME']['VALUE']?></p>
                                <? } ?>
                                <p class="blog-item__author-date"><?=ConvertDateTime($item['PROPERTIES']['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></p>
                            </div>

                        </article>
                    </div>

                <? } else {
                    echo '<ost></ost>';
                }
            }
        }

    }

} else { ?>

    <div class="custom-tabs__section blog-results <? if( $arParams['OPENED'] == 'Y' ){  echo 'custom-tabs__section_opened';  } ?>">
        <div class="row searchBlogLoadArea">

            <? if( count($arResult["ITEMS"]) > 0 ){
                $cnt = 0;
                foreach( $arParams["IDS"] as $id ){
                    $item = false;
                    foreach( $arResult["ITEMS"] as $key => $arItem ){
                        if( $arItem['ID'] == $id ){   $item = $arItem;   }
                    }
                    if( $item ){

                        $cnt++;
                        if( $cnt <= $maxCNT ){
                            $sections = tools\el::sections($item['ID']); ?>

                            <div class="col-12 col-sm-6 col-lg-4 blog-results__col article___item" item_id="<?=$item['ID']?>">
                                <article class="blog-item">

                                    <a href="<?=$item['DETAIL_PAGE_URL']?>" class="blog-item__img" style="background: url(<?=tools\funcs::rIMGG($item['PREVIEW_PICTURE'], 5, 340, 240)?>) no-repeat 50%; background-size: cover;"></a>

                                    <a href="<?=$item['DETAIL_PAGE_URL']?>" class="link blog-item__title"><?=$item['NAME']?></a>

                                    <p class="blog-item__text"><?=$item['PREVIEW_TEXT']?></p>

                                    <? if( is_array($sections) && count($sections) > 0 ){ ?>
                                        <div class="blog-item__tags">
                                            <? foreach( $sections as $section ){ ?>
                                                <a href="/blog/<?=$section['CODE']?>/" style="cursor: pointer" class="tag setBlogSectionButton to___process"><?=$section['NAME']?></a>
                                            <? } ?>
                                        </div>
                                    <? } ?>

                                    <div class="blog-item__author">
                                        <? if( strlen($item['PROPERTIES']['AUTHOR_NAME']['VALUE']) > 0 ){ ?>
                                            <? if( intval($item['PROPERTIES']['AUTHOR_PHOTO']['VALUE']) > 0 ){ ?>
                                                <div class="blog-item__author-img" style="background: url(<?=tools\funcs::rIMGG($arItem['PROPERTIES']['AUTHOR_PHOTO']['VALUE'], 5, 35, 35)?>) no-repeat 50%; background-size: cover;"></div>
                                            <? } ?>
                                            <p class="blog-item__author-name"><?=$item['PROPERTIES']['AUTHOR_NAME']['VALUE']?></p>
                                        <? } ?>
                                        <p class="blog-item__author-date"><?=ConvertDateTime($item['PROPERTIES']['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></p>
                                    </div>

                                </article>
                            </div>

                        <? }
                    }
                }

            } ?>

        </div>

        <div class="more_button_block" <? if( $cnt <= $maxCNT ){ ?>style="display:none;"<? } ?>>
            <button type="button" class="btn search_blog_load_button to___process">Показать ещё</button>
        </div>

    </div>

<? } ?>

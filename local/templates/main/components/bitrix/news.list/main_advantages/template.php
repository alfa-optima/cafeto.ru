<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if (count($arResult["ITEMS"]) > 0){ ?>

    <section class="section why">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="section__title why__title">9 аргументов в пользу CAFETO</h3>
                    <div class="section__grid row">

                        <? foreach($arResult["ITEMS"] as $arItem){

                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], \CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], \CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>

                            <div class="col-6 col-lg-4 why__item" id='<?=$this->GetEditAreaId($arItem["ID"]);?>'>
                                <div class="why__icon">
                                    <?=$arItem["PROPERTIES"]['SVG']['~VALUE']['TEXT']?>
                                </div>
                                <p class="why__name"><?=$arItem["NAME"]?></p>
                                <p class="why__text"><?=$arItem["PREVIEW_TEXT"]?></p>
                            </div>

                        <? } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

<? } ?>
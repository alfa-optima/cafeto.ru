<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$maxCNT = $arParams['NEWS_COUNT']-1;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if ( $arParams['IS_AJAX'] == 'Y' ){

    if( count($arResult["ITEMS"]) > 0){
        
        $cnt = 0;
        foreach( $arResult["ITEMS"] as $key => $arItem ){ $cnt++;
            if( $cnt <= $maxCNT ){
                $sections = tools\el::sections($arItem['ID']); ?>

                <div class="col-12 col-md-6 col-lg-4 blog__col article___item" item_id="<?=$arItem['ID']?>">
                    <article class="blog-item">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="blog-item__img"
                           style="background: url(<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 340, 240)?>) no-repeat 50%; background-size: cover;"></a>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link blog-item__title"><?=$arItem['NAME']?></a>
                        <p class="blog-item__text"><?=$arItem['PREVIEW_TEXT']?></p>
                        <? if( is_array($sections) && count($sections) > 0 ){ ?>
                            <div class="blog-item__tags">
                                <? foreach( $sections as $section ){ ?>
                                    <a section_id="<?=$section['ID']?>" style="cursor: pointer" class="tag setBlogSectionButton to___process"><?=$section['NAME']?></a>
                                <? } ?>
                            </div>
                        <? } ?>
                        <div class="blog-item__author">
                            <? if( strlen($arItem['PROPERTIES']['AUTHOR_NAME']['VALUE']) > 0 ){ ?>
                                <? if( intval($arItem['PROPERTIES']['AUTHOR_PHOTO']['VALUE']) > 0 ){ ?>
                                    <div class="blog-item__author-img" style="background: url(<?=tools\funcs::rIMGG($arItem['PROPERTIES']['AUTHOR_PHOTO']['VALUE'], 5, 35, 35)?>) no-repeat 50%; background-size: cover;"></div>
                                <? } ?>
                                <p class="blog-item__author-name"><?=$arItem['PROPERTIES']['AUTHOR_NAME']['VALUE']?></p>
                            <? } ?>
                            <p class="blog-item__author-date"><?=ConvertDateTime($arItem['PROPERTIES']['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></p>
                        </div>
                    </article>
                </div>

            <? } else {  echo '<ost></ost>';  }
        }

    }

} else { ?>

    <div class="page__grid">
        <div class="blog__list">
            <div class="row blogLoadArea">

                <? if( count($arResult["ITEMS"]) > 0 ){

                    $cnt = 0;
                    foreach( $arResult["ITEMS"] as $key => $arItem ){ $cnt++;
                        if( $cnt <= $maxCNT ){
                            $sections = tools\el::sections($arItem['ID']); ?>

                            <div class="col-12 col-md-6 col-lg-4 blog__col article___item" item_id="<?=$arItem['ID']?>">
                                <article class="blog-item">
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="blog-item__img"
                                       style="background: url(<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 340, 240)?>) no-repeat 50%; background-size: cover;"></a>
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="link blog-item__title"><?=$arItem['NAME']?></a>
                                    <p class="blog-item__text"><?=$arItem['PREVIEW_TEXT']?></p>
                                    <? if( is_array($sections) && count($sections) > 0 ){ ?>
                                        <div class="blog-item__tags">
                                            <? foreach( $sections as $section ){ ?>
                                                <a section_id="<?=$section['ID']?>" style="cursor: pointer" class="tag setBlogSectionButton to___process"><?=$section['NAME']?></a>
                                            <? } ?>
                                        </div>
                                    <? } ?>
                                    <div class="blog-item__author">
                                        <? if( strlen($arItem['PROPERTIES']['AUTHOR_NAME']['VALUE']) > 0 ){ ?>
                                            <? if( intval($arItem['PROPERTIES']['AUTHOR_PHOTO']['VALUE']) > 0 ){ ?>
                                                <div class="blog-item__author-img" style="background: url(<?=tools\funcs::rIMGG($arItem['PROPERTIES']['AUTHOR_PHOTO']['VALUE'], 5, 35, 35)?>) no-repeat 50%; background-size: cover;"></div>
                                            <? } ?>
                                            <p class="blog-item__author-name"><?=$arItem['PROPERTIES']['AUTHOR_NAME']['VALUE']?></p>
                                        <? } ?>
                                        <p class="blog-item__author-date"><?=ConvertDateTime($arItem['PROPERTIES']['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></p>
                                    </div>
                                </article>
                            </div>

                        <? }
                    }

                } else { ?>

                    <p class="no___count">В данном разделе / по данному фильтру статей нет</p>

                <? } ?>

            </div>
        </div>

        <div class="more_button_block" <? if( $cnt <= $maxCNT ){ ?>style="display:none;"<? } ?>>
            <button type="button" class="btn blog_load_button to___process">Показать ещё</button>
        </div>

    </div>

<? } ?>

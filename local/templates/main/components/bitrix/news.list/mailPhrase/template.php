<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

    <tr style="border-collapse:collapse;">
        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-left:20px;padding-right:20px;">
            <table cellpadding="0" cellspacing="0" width="100%"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                    <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="center" style="padding:0;Margin:0;">
                                    <p
                                            style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1C130C;">
                                        Кстати, а вы знали, что:</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr style="border-collapse:collapse;">
        <td align="left" style="padding:0;Margin:0;padding-top:8px;padding-left:20px;padding-right:20px;">
            <table cellpadding="0" cellspacing="0" width="100%"
                   style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                    <td width="560" align="center" valign="top" style="padding:0;Margin:0;">
                        <table cellpadding="0" cellspacing="0" width="100%"
                               style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="center" style="padding:0;Margin:0;">

                                    <? foreach($arResult["ITEMS"] as $arItem){ ?>

                                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#1C130C;"><?=$arItem['PREVIEW_TEXT']?></p>

                                    <? } ?>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

<? } ?>
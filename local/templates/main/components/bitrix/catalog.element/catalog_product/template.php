<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;
\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$this->setFrameMode(true);

// Инфо о товаре
$pInfo = project\catalog::productInfo($arResult, $arParams['sku_id']); ?>

<div class="product__grid row productCardFirstBlock">
    <div class="col-12 col-lg-5">
        <div class="product__gallery gallery">

            <div class="gallery__wrapper">

                <div class="gallery__main swiper-container product-gallery">
                    <div class="swiper-wrapper">

                        <? foreach( $pInfo['pictures'] as $key => $pic_id ){ ?>

                            <a href="<?=$pInfo['big_pictures'][$key]?>" data-fancybox="main-product" class="swiper-slide gallery__item">
                                <img src="<?=$pInfo['middle_pictures'][$key]?>" class="gallery__img" alt="alt">
                            </a>

                        <? } ?>

                    </div>
                </div>

                <div class="zoom-icon gallery__zoom-icon">
                    <svg width="20px" height="20px" viewBox="0 0 20 20">
                        <path fill="#000301" d="M22.2,28a.69.69,0,0,0,.69-.69v-3l4.49,4.48a.68.68,0,0,0,.49.21.67.67,0,0,0,.49-.21.68.68,0,0,0,0-1l-4.48-4.49h3a.7.7,0,0,0,0-1.39H22.2a.7.7,0,0,0-.7.7v4.65A.69.69,0,0,0,22.2,28ZM40.8,36a.69.69,0,0,0-.69.69v3L35.48,35a.69.69,0,1,0-1,1l4.62,4.63h-3a.7.7,0,0,0,0,1.39H40.8a.7.7,0,0,0,.7-.7V36.65A.69.69,0,0,0,40.8,36ZM27.52,35l-4.63,4.62v-3a.7.7,0,0,0-1.39,0V41.3a.7.7,0,0,0,.7.7h4.65a.7.7,0,0,0,0-1.39h-3L28.5,36a.69.69,0,1,0-1-1ZM40.8,22H36.15a.7.7,0,0,0,0,1.39h3l-4.48,4.49a.68.68,0,0,0,0,1,.68.68,0,0,0,1,0l4.49-4.48v3a.7.7,0,0,0,1.39,0V22.7A.7.7,0,0,0,40.8,22Z" transform="translate(-21.5 -22)" />
                    </svg>
                </div>

                <a style="cursor: pointer" class=" add-to-fav to-favorites gallery__to-favorites fav___button to___process" item_id="<?=$arResult['ID']?>">
                    <svg viewBox="0 0 20 19" width="20px" height="19px">
                        <path fill="#030303" stroke="#030303" stroke-width=".5" stroke-miterlimit="10" d="M18.5,6l-4.9-0.4c-0.1,0-0.2-0.1-0.3-0.2L11.5,1c-0.4-0.8-1.3-1.2-2.1-0.9C9,0.3,8.6,0.6,8.5,1L6.6,5.5 c0,0.1-0.1,0.2-0.2,0.2L1.5,6C0.6,6.1-0.1,6.8,0,7.7c0,0.4,0.2,0.9,0.6,1.2L4.3,12c0.1,0.1,0.1,0.2,0.1,0.3L3.3,17 c-0.1,0.5,0,1,0.3,1.4C3.8,18.8,4.3,19,4.8,19c0.3,0,0.6-0.1,0.9-0.3l4.1-2.6c0.1,0,0.2,0,0.3,0l4.2,2.5c0.3,0.2,0.6,0.3,0.9,0.3 c0.5,0,0.9-0.2,1.2-0.6c0.3-0.4,0.4-0.9,0.3-1.4l-1.2-4.7c0-0.1,0-0.2,0.1-0.3l3.8-3.1c0.5-0.4,0.7-1.1,0.5-1.8 C19.7,6.5,19.1,6.1,18.5,6z M18.8,8.1L15,11.2c-0.4,0.3-0.5,0.8-0.4,1.3l1.2,4.7c0,0.2,0,0.4-0.1,0.6c-0.1,0.1-0.3,0.2-0.5,0.2 c-0.1,0-0.2,0-0.3-0.1l-4.2-2.6c-0.2-0.1-0.4-0.2-0.7-0.2c-0.2,0-0.5,0.1-0.7,0.2l-4.1,2.6c-0.3,0.2-0.6,0.1-0.8-0.2 c-0.1-0.2-0.2-0.3-0.1-0.5l1.2-4.7c0.1-0.5-0.1-1-0.4-1.3L1.2,8.1C1,8,1,7.7,1,7.5c0.1-0.2,0.3-0.4,0.6-0.4l4.9-0.4 c0.5,0,0.9-0.3,1.1-0.8l1.9-4.5c0.1-0.3,0.5-0.5,0.8-0.4c0.2,0.1,0.3,0.2,0.4,0.4l1.9,4.5c0.2,0.4,0.6,0.7,1.1,0.8l4.9,0.4 c0.3,0,0.6,0.3,0.6,0.7C19,7.9,18.9,8.1,18.8,8.1L18.8,8.1z" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.94 49.94" width="20px" height="20px">
                        <path d="M48.856,22.73c0.983-0.958,1.33-2.364,0.906-3.671c-0.425-1.307-1.532-2.24-2.892-2.438l-12.092-1.757 c-0.515-0.075-0.96-0.398-1.19-0.865L28.182,3.043c-0.607-1.231-1.839-1.996-3.212-1.996c-1.372,0-2.604,0.765-3.211,1.996 L16.352,14c-0.23,0.467-0.676,0.79-1.191,0.865L3.069,16.622c-1.359,0.197-2.467,1.131-2.892,2.438 c-0.424,1.307-0.077,2.713,0.906,3.671l8.749,8.528c0.373,0.364,0.544,0.888,0.456,1.4L8.224,44.701 c-0.183,1.06,0.095,2.091,0.781,2.904c1.066,1.267,2.927,1.653,4.415,0.871l10.814-5.686c0.452-0.237,1.021-0.235,1.472,0 l10.815,5.686c0.526,0.277,1.087,0.417,1.666,0.417c1.057,0,2.059-0.47,2.748-1.288c0.687-0.813,0.964-1.846,0.781-2.904 l-2.065-12.042c-0.088-0.513,0.083-1.036,0.456-1.4L48.856,22.73z" />
                    </svg>
                </a>
            </div>

            <div class="gallery__thumbs">

                <div class="thumbs product-thumbs swiper-container">
                    <div class="swiper-wrapper">
                        <? foreach( $pInfo['pictures'] as $key => $pic_id ){ ?>
                            <div class="swiper-slide thumbs__item">
                                <img src="<?=$pInfo['small_pictures'][$key]?>" class="thumbs__img">
                            </div>
                        <? } ?>
                    </div>
                </div>

                <button type="button" class="thumbs-arrow thumbs-arrow-prev product-thumbs-arrow-prev">
                    <svg width="6.02" height="11.02" viewBox="0 0 6.02 11.02">
                        <path fill="#000301" d="M28.86,32.6l4.91,4.95a.56.56,0,0,0,.79,0,.56.56,0,0,0,0-.78L30,32.21l4.53-4.56a.57.57,0,0,0,0-.79.56.56,0,0,0-.79,0l-4.91,4.95A.56.56,0,0,0,28.86,32.6Z" transform="translate(-28.7 -26.7)" />
                    </svg>
                </button>

                <button type="button" class="thumbs-arrow thumbs-arrow-next product-thumbs-arrow-next">
                    <svg width="6.02px" height="11.02px" viewBox="0 0 6.02 11.02">
                        <path fill="#000301" d="M34.56,31.81l-4.92-4.95a.54.54,0,0,0-.78,0,.56.56,0,0,0,0,.79l4.53,4.56-4.53,4.56a.55.55,0,0,0,.78.78l4.92-4.95A.57.57,0,0,0,34.56,31.81Z" transform="translate(-28.7 -26.7)" />
                    </svg>
                </button>

            </div>

        </div>
    </div>

    <div class="col-12 col-lg-7">
        <div class="product__info">

            <p class="title product__title"><?=$arResult['NAME']?></p>

            <? if( strlen($arResult['PREVIEW_TEXT']) > 0 ){ ?>
                <div class="product__text text-overflow text-overflow_s"><? if( !substr_count($arResult['PREVIEW_TEXT'], '<p') ){ echo '<p>'; } ?><?=$arResult['PREVIEW_TEXT']?><? if( !substr_count($arResult['PREVIEW_TEXT'], '<p') ){ echo '</p>'; } ?></div>
                <a href="#tabs" class="show-more">Полное описание</a>
            <? } ?>

            <form class="form product-form product__form">

                <? if( count($pInfo['offers']) > 0 ){ ?>

                    <div class="product-form__options">

                        <? $cnt = 0;
                        foreach( $pInfo['offers'] as $offer ){ $cnt++;

                            $checked = false;
                            if(
                                ( !$arParams['sku_id'] && $cnt == 1 )
                                ||
                                ( intval($arParams['sku_id']) > 0 && $arParams['sku_id'] == $offer['ID'] )
                            ){   $checked = true;   } ?>

                            <label class="radio product-form__radio">
                                <input <? if( $checked ){ echo 'checked'; } ?> type="radio" name="sku_id" value="<?=$offer['ID']?>" item_id="<?=$arResult['ID']?>" item_code="<?=$arResult['CODE']?>" class="radio__input product_card_sku_checkbox" price="<?=$offer['PRICES'][project\catalog::PRICE_CODE]['VALUE_VAT']?>" disc_price="<?=$offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT']?>">
                                <p class="radio__name"><?=$offer['PROPERTY_WEIGHT_VALUE']?>гр — <?=$offer['PRICES'][project\catalog::PRICE_CODE]['DISCOUNT_VALUE_VAT']?> RUB</p>
                            </label>

                        <? } ?>

                    </div>

                <? } ?>

                <?php if( project\catalog::isAvailableProduct( $arResult ) ){ ?>

                    <p class="product__price"><? if( $pInfo['discPrice'] != $pInfo['price'] ){ echo $pInfo['price'].' RUB'; } ?><span><?=$pInfo['discPrice']?> RUB</span></p>

                <?php } else { ?>

                    <p><i>Нет в наличии</i></p>

                <?php } ?>

                <div class="product-form__footer">

                    <?php if( project\catalog::isAvailableProduct( $arResult ) ){ ?>

                        <div class="quantity product-form__quantity quantity___block">
                            <button type="button" class="quantity__btn quantity__btn-minus button___minus">
                                <svg width="8.98" height="0.99" viewBox="0 0 8.98 0.99">
                                    <rect fill="#000" class="cls-1" width="8.98" height="0.99" />
                                </svg>
                            </button>
                            <input type="text" name="quantity" value="1" class="quantity__input">
                            <button type="button" class="quantity__btn quantity__btn-plus button___plus">
                                <svg width="8.99px" height="9.02px" viewBox="0 0 8.99 9.02">
                                    <polygon fill="#000" points="8.99 3.96 5.23 3.96 5.23 0 3.98 0 3.98 3.96 0 3.96 0 5.08 3.98 5.08 3.98 9.02 5.23 9.02 5.23 5.08 8.99 5.08 8.99 3.96" />
                                </svg>
                            </button>
                        </div>

                        <button type="button" class="btn product-form__btn  add_to___basket to___process" item_id="<?=$pInfo['basketElID']?>">в корзину</button>

                    <?php } ?>

                    <div class="share product__share">
                        <button type="button" class="share__icon">
                            <svg width="19px" height="21px" viewBox="0 0 19 21">
                                <path fill="#000301" d="M37.91,35.08a3.81,3.81,0,0,0-3,1.43L30.3,33.82a3.9,3.9,0,0,0,0-2.64l4.63-2.69a3.81,3.81,0,0,0,3,1.43A4,4,0,1,0,34,26a4.17,4.17,0,0,0,.22,1.33L29.63,30a3.84,3.84,0,0,0-3-1.43,4,4,0,0,0,0,7.92,3.84,3.84,0,0,0,3-1.43l4.63,2.68A4.17,4.17,0,0,0,34,39a3.87,3.87,0,1,0,3.87-4Zm0-11.69A2.58,2.58,0,1,1,35.39,26,2.55,2.55,0,0,1,37.91,23.39ZM26.65,35.08a2.58,2.58,0,1,1,2.52-2.58A2.55,2.55,0,0,1,26.65,35.08Zm11.26,6.53A2.58,2.58,0,1,1,40.43,39,2.55,2.55,0,0,1,37.91,41.61Z" transform="translate(-22.78 -22)" />
                            </svg>
                        </button>
                        <div class="share__grid share__grid_s share42init" data-url="<?=$_SERVER['REQUEST_SCHEME']?>://<?=$_SERVER['SERVER_NAME']?><?=$arResult['DETAIL_PAGE_URL']?>" data-title="<?=$arResult['NAME']?>"></div>
                        <script type="text/javascript" src="/share42/share42.js"></script>
                    </div>

                </div>

                <?php if( !project\catalog::isAvailableProduct( $arResult ) ){ ?>

                    <!-- Start Reading -->
                    <div class="reading article__reading">

                        <header class="reading__header">

                            <!-- Необходимо вешать класс reading__btn_disabled когда кнопку необходимо выключить -->
                            <button type="button" class="btn product_subscribe__btn">Уведомить о появлении</button>
                            <!-- Необходимо вешать класс reading__status_active когда текст необходимо включить -->

                            <p class="reading__status">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"
                                     width="20" height="20">
                                    <path fill="#24ac00" d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26 S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" />
                                    <path fill="#24ac00" d="M38.252,15.336l-15.369,17.29l-9.259-7.407c-0.43-0.345-1.061-0.274-1.405,0.156c-0.345,0.432-0.275,1.061,0.156,1.406 l10,8C22.559,34.928,22.78,35,23,35c0.276,0,0.551-0.114,0.748-0.336l16-18c0.367-0.412,0.33-1.045-0.083-1.411 C39.251,14.885,38.62,14.922,38.252,15.336z" />
                                </svg>
                                Подписка на статью успешно отправлена!
                            </p>

                        </header>

                        <div style="display: none;" class="reading__grid">
                            <p class="reading__title">Подписаться на поступление товара</p>
                            <form class="form reading__form">
                                <div class="form__row">
                                    <input name="email" type="email" class="form__input reading__form-input" placeholder="Email">
                                </div>
                                <div class="form__row">
                                    <!-- Captcha -->
                                    <!-- Captcha -->
                                </div>

                                <p class="error___p"></p>

                                <input type="hidden" name="id" value="<?=$arResult['ID']?>">

                                <button type="button" class="btn reading__form-btn subscribe_product_button to___process">Отправить</button>
                            </form>
                        </div>

                    </div>
                    <!-- End Reading -->

                <? } ?>

            </form>
        </div>
    </div>
</div>

<? if( $arParams['IS_AJAX'] != 'Y' ){ ?>

    <div class="product__tabs custom-tabs">

        <header id="tabs" class="custom-tabs__header">
            <button type="button" class="custom-tabs__btn custom-tabs__btn_active">Описание</button>
            <button type="button" class="custom-tabs__btn">Характеристики</button>
            <button type="button" class="custom-tabs__btn">Отзывы</button>
            <? if(
                is_array($arResult["PROPERTIES"]['VIDEO']['VALUE'])
                &&
                count($arResult["PROPERTIES"]['VIDEO']['VALUE']) > 0
            ){ ?>
                <button type="button" class="custom-tabs__btn">Видеоблог</button>
            <? } ?>
        </header>

        <div class="custom-tabs__grid">

            <div class="custom-tabs__section custom-tabs__section_opened text">

                <? if( !substr_count($arResult['DETAIL_TEXT'], '<p') ){ echo '<p>'; } ?><?=$arResult['DETAIL_TEXT']?><? if( !substr_count($arResult['DETAIL_TEXT'], '<p') ){ echo '</p>'; } ?>

            </div>

            <div class="custom-tabs__section">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        
                        <? $props = $arResult["PROPERTIES"];
                        foreach ( $props as $prop_code => $prop ){
                            if(
                                in_array($prop_code, $arParams['stop_props'])
                                ||
                                !isset($prop['VALUE']) || strlen($prop['VALUE']) == 0
                            ){
                                unset($props[$prop_code]);
                            }
                        }
                        if( count($props) > 0 ){ ?>
                            <ul class="product-desc">
                                <? foreach ( $props as $prop_code => $prop ){ ?>
                                    <li class="product-desc__item">
                                        <p><?=$prop['NAME']?>:</p>
                                        <p><?=$prop['VALUE']?></p>
                                    </li>
                                <? } ?>
                            </ul>
                        <? } else { ?>
                            <p><i>Характеристики не указаны</i></p>
                        <? } ?>

                    </div>
                </div>
            </div>

            <div class="custom-tabs__section"><?=$arParams['~product_reviews']?></div>

            <? if(
                is_array($arResult["PROPERTIES"]['VIDEO']['VALUE'])
                &&
                count($arResult["PROPERTIES"]['VIDEO']['VALUE']) > 0
            ){ ?>

                <div class="custom-tabs__section">
                    <div class="row">
                        <div class="col-12 col-lg-7">

                            <? foreach( $arResult["PROPERTIES"]['VIDEO']['VALUE'] as $key => $video_id ){ ?>

                                <iframe width="100%" height="350" src="https://www.youtube-nocookie.com/embed/<?=$video_id?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                            <? } ?>

                        </div>
                    </div>
                </div>

            <? } ?>

        </div>

    </div>

<? } ?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$section = tools\section::info($arResult['VARIABLES']['SECTION_ID']);

if( intval($section['ID']) > 0 ){
    
    $chain = tools\section::chain($section['ID']);
    foreach( $chain as $key => $sect ){
        $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
    }

    $arURI = tools\funcs::arURI( tools\funcs::pureURL() );

    $smart_filter_path = '';
    if( substr_count( tools\funcs::pureURL(), '/filter/' ) ){
        if(preg_match("/\/filter\/(.+)\/apply\//", tools\funcs::pureURL(),$matches)){
            $smart_filter_path = rawurldecode( $matches[1] );
        }
        $filterKey = array_search( 'filter', $arURI );
        foreach ( $arURI as $key => $urlItem ){
            if( $key >= $filterKey ){   unset($arURI[$key]);   }
        }
        $arURI[] = false;
    }

    if( strlen($smart_filter_path) > 0 ){
        // Пытаемся найти SEO-страницу
        $seo_page = project\SeopageTable::getByFilterPath($section, $smart_filter_path);
    }

    if( intval($seo_page['ID']) > 0 ){
        // SEO-поля
        if( strlen($seo_page["TITLE"]) > 0 ){
            $APPLICATION->SetPageProperty("title", $seo_page["TITLE"]);
        }
        if( strlen($seo_page["DESCRIPTION"]) > 0 ){
            $APPLICATION->SetPageProperty("description", $seo_page["DESCRIPTION"]);
        }
        if( strlen($seo_page["KEYWORDS"]) > 0 ){
            $APPLICATION->SetPageProperty("keywords", $seo_page["KEYWORDS"]);
        }
        $APPLICATION->AddChainItem($seo_page["NAME"], '');
    } else {
        // SEO-поля
        $APPLICATION->SetPageProperty("description", $section["UF_DESCRIPTION"]?$section["UF_DESCRIPTION"]:$section["NAME"]);
        $APPLICATION->SetPageProperty("keywords", $section["UF_KEYWORDS"]?$section["UF_KEYWORDS"]:$section["NAME"]);
        $APPLICATION->SetPageProperty("title", $section["UF_TITLE"]?$section["UF_TITLE"]:$section["NAME"]);
    } ?>

    <div class="template catalog">

        <div class="container">
            <div class="row">
                <div class="col-12">

                    <? // Хлебные крошки
                    $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb", "catalog",
                        Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                    ); ?>

                    <div class="catalog__options">

                        <? if( $section['CODE'] != 'accessories' ){ ?>

                            <? // Фильтр
                            $APPLICATION->IncludeComponent(
                                "bitrix:catalog.smart.filter", "catalog_filter",
                                array(
                                    "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                                    "IBLOCK_ID" => project\catalog::IBLOCK_ID,
                                    "SECTION_ID" => $section['ID'],
                                    "FILTER_NAME" => project\catalog::FILTER_NAME,
                                    "PRICE_CODE" => [ project\catalog::PRICE_CODE ],
                                    "CACHE_TYPE" => 'A',
                                    "CACHE_TIME" => 3600,
                                    "CACHE_GROUPS" => 'Y',
                                    "SAVE_IN_SESSION" => "N",
                                    "FILTER_VIEW_MODE" => 'VERTICAL',
                                    "XML_EXPORT" => "N",
                                    "SECTION_TITLE" => "NAME",
                                    "SECTION_DESCRIPTION" => "DESCRIPTION",
                                    'HIDE_NOT_AVAILABLE' => 'N',
                                    "TEMPLATE_THEME" => "",
                                    'CONVERT_CURRENCY' => 'N',
                                    'CURRENCY_ID' => '',
                                    "SEF_MODE" => 'Y',
                                    "SEF_RULE" => $section['SECTION_PAGE_URL'].'filter/#SMART_FILTER_PATH#/apply/',
                                    "SMART_FILTER_PATH" => $smart_filter_path,
                                    "PAGER_PARAMS_NAME" => '',
                                    "INSTANT_RELOAD" => '',
                                ),
                                $component, array('HIDE_ICONS' => 'Y')
                            ); ?>

                        <? } ?>

                        <div class="sort catalog__sort <? if( $section['CODE'] == 'accessories' ){ ?>accessories__sort<? } ?>">

                            <? // catalog_sort_block
                            $sort = $APPLICATION->IncludeComponent(
                                "aoptima:catalog_sort_block", ""
                            ); ?>

                            <button type="reset" class="filter__reset to___process">Сбросить фильтры</button>

                        </div>

                    </div>


                    <? $GLOBALS[project\catalog::FILTER_NAME]['SECTION_ID'] = $section['ID'];
                    $GLOBALS[project\catalog::FILTER_NAME]['INCLUDE_SUBSECTIONS'] = 'Y';

                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section", "catalog_goods",
                        Array(
                            "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "SECTION_USER_FIELDS" => array(),
                            "ELEMENT_SORT_FIELD" => $sort['SORT_FIELD'],
                            "ELEMENT_SORT_ORDER" => $sort['SORT_ORDER'],
                            "ELEMENT_SORT_FIELD2" => 'NAME',
                            "ELEMENT_SORT_ORDER2" => $sort['SORT_ORDER'],
                            "FILTER_NAME" => project\catalog::FILTER_NAME,
                            "HIDE_NOT_AVAILABLE" => "N",
                            "PAGE_ELEMENT_COUNT" => project\catalog::MAX_CNT + 1,
                            "LINE_ELEMENT_COUNT" => "3",
                            "PROPERTY_CODE" => array('REGION'),
                            "FIELDS" => ['PROPERTY_REGION'],
                            "OFFERS_LIMIT" => 0,
                            "TEMPLATE_THEME" => "",
                            "PRODUCT_SUBSCRIPTION" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "SECTION_URL" => "",
                            "DETAIL_URL" => "",
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => 'A',
                            "CACHE_TIME" => 36000000,
                            "CACHE_GROUPS" => "Y",
                            "SET_META_KEYWORDS" => "N",
                            "META_KEYWORDS" => "",
                            "SET_META_DESCRIPTION" => "N",
                            "META_DESCRIPTION" => "",
                            "BROWSER_TITLE" => "-",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "DISPLAY_COMPARE" => "N",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "CACHE_FILTER" => "Y",
                            "PRICE_CODE" => array(
                                project\catalog::PRICE_CODE
                            ),
                            "USE_PRICE_COUNT" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "PRICE_VAT_INCLUDE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "BASKET_URL" => "/personal/basket.php",
                            "ACTION_VARIABLE" => "action",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "USE_PRODUCT_QUANTITY" => "N",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRODUCT_PROPERTIES" => "",
                            "PAGER_TEMPLATE" => "",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Товары",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "Y",
                            "ADD_PICT_PROP" => "-",
                            "LABEL_PROP" => "-",
                            'SHOW_ALL_WO_SECTION' => "Y"
                        )
                    ); ?>

                </div>
            </div>
        </div>
    </div>

    <section class="section seo">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <? if( intval($seo_page['ID']) > 0 ){ ?>

                        <h4 class="section__title seo__title"><?=$seo_page['H1']?></h4>

                        <div class="section__grid text seo___text" <? if( strlen($seo_page['TEXT']) > 0 ){} else { ?>style="display:none;"<? } ?>>
                            <?=$seo_page['TEXT']?>
                        </div>

                    <? } else { ?>

                        <h4 class="section__title seo__title"><?=$section['NAME']?></h4>

                        <div class="section__grid text seo___text" <? if( strlen($section['DESCRIPTION']) > 0 ){} else { ?>style="display:none;"<? } ?>>
                            <?=$section['DESCRIPTION']?>
                        </div>

                    <? } ?>

                </div>
            </div>
        </div>
    </section>


    <input type="hidden" name="section_id" value="<?=$section['ID']?>">


<? } else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");

} ?>

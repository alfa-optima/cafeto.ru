<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

    <div class="col-12 col-lg-2">
        <div class="footer__col">
            <p class="footer__name">Информация</p>

            <? foreach($arResult as $arItem){ ?>

                <a href="<?=$arItem["LINK"]?>" class="link footer__link footer__text"><?=$arItem["TEXT"]?></a>

            <? } ?>

        </div>
    </div>

<? } ?>
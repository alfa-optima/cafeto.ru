<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

if (!empty($arResult)){ ?>
    
    <? //echo "<pre>"; print_r( $arResult ); echo "</pre>"; ?>

    <nav class="menu header__menu">

        <? foreach($arResult as $arItem){

            $level_2 = array();
            foreach($arResult as $arItem2){
                if(
                    $arItem["LINK"] != '/'
                    &&
                    tools\funcs::string_begins_with($arItem["LINK"], $arItem2["LINK"])
                ){
                    $level_2[] = $arItem2;
                }
            } ?>

            <div class="menu__item <? if( count($level_2) > 0 ){ echo 'has-children'; } ?>">
                <a href="<?=$arItem["LINK"]?>" class="menu__link"><?=$arItem["TEXT"]?></a>

                <? if( count($level_2) > 0 ){ ?>

                    <div class="sub-menu">
                        <div class="sub-menu__wrapper sub-menu__wrapper_s">

                            <? foreach ($level_2 as $level_2_sect){
                                $level_3 = array();
                                foreach($arResult as $arItem2){
                                    if(
                                        $arItem["LINK"] != '/'
                                        &&
                                        tools\funcs::string_begins_with($arItem["LINK"], $arItem2["LINK"])
                                    ){
                                        $level_3[] = $arItem2;
                                    }
                                } ?>

                                <div class="sub-menu__col">
                                    <p class="sub-menu__name"><?=$level_2_sect['TEXT']?></p>
                                    <a href="#" class="link sub-menu__link">Все</a>
                                    <a href="#" class="link sub-menu__link">Эспрессо</a>
                                    <a href="#" class="link sub-menu__link">Фильтр</a>
                                    <a href="#" class="link sub-menu__link">Смесь арабики и робусты</a>
                                    <a href="#" class="link sub-menu__link">Арабика</a>
                                    <a href="#" class="link sub-menu__link">Декаф</a>
                                </div>

                            <? } ?>

                        </div>
                    </div>

                <? } ?>

            </div>

        <? } ?>

    </nav>

<? } ?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');  use AOptima\ToolsCafeto as tools;
\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');  use AOptima\ProjectCafeto as project;

// Инфо о статье
$el = tools\el::info_by_code( $arResult['VARIABLES']['ELEMENT_CODE'], $arParams['IBLOCK_ID'] );

if( intval($el['ID']) > 0 ){

    $APPLICATION->SetPageProperty("description", $el["PROPERTY_DESCRIPTION_VALUE"]?$el["PROPERTY_DESCRIPTION_VALUE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("keywords", $el["PROPERTY_KEYWORDS_VALUE"]?$el["PROPERTY_KEYWORDS_VALUE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("title", $el["PROPERTY_TITLE_VALUE"]?$el["PROPERTY_TITLE_VALUE"]:$el["NAME"]);

    $el_sections = tools\el::sections($el['ID']);
    $sect_id = $el_sections[0]['ID'];
    $section_chain = tools\section::chain($sect_id);
    foreach ( $section_chain as $key => $sect ){
        $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
    }
    $APPLICATION->AddChainItem($el["NAME"], $el["DETAIL_PAGE_URL"]);

    $blogSections = project\blog::sections(); ?>


    <div class="template page article">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <? // Хлебные крошки
                    $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb", "catalog",
                        Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                    ); ?>

                    <div class="template__grid">

                        <h1 class="title template__title page__title"><?=$el['NAME']?></h1>

                        <div class="page__grid">

                            <? if( intval($el['DETAIL_PICTURE']) > 0 ){ ?>
                                <img src="<?=tools\funcs::rIMGG($el['DETAIL_PICTURE'], 5, 1080, 370)?>">
                            <? } ?>

                            <div class="row justify-content-center">
                                <div class="col-12 col-lg-10">

                                    <!-- Start Reading -->
                                    <div class="reading article__reading">
                                        <header class="reading__header">

                                            <? if( strlen($el["PROPERTY_READ_TIME_VALUE"]) > 0 ){ ?>

                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 511.992 511.992" width="30" height="30">
                                                    <path id="XMLID_389_" d="M511.005,279.646c-4.597-46.238-25.254-89.829-58.168-122.744    c-28.128-28.127-62.556-46.202-98.782-54.239V77.255c14.796-3.681,25.794-17.074,25.794-32.993c0-18.748-15.252-34-34-34h-72    c-18.748,0-34,15.252-34,34c0,15.918,10.998,29.311,25.793,32.993v25.479c-36.115,8.071-70.429,26.121-98.477,54.169    c-6.138,6.138-11.798,12.577-16.979,19.269c-0.251-0.019-0.502-0.038-0.758-0.038H78.167c-5.522,0-10,4.477-10,10s4.478,10,10,10    h58.412c-7.332,12.275-13.244,25.166-17.744,38.436H10c-5.522,0-10,4.477-10,10s4.478,10,10,10h103.184    c-2.882,12.651-4.536,25.526-4.963,38.437H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h44.54    c0.844,12.944,2.925,25.82,6.244,38.437H50c-5.522,0-10,4.477-10,10s4.478,10,10,10h71.166    c9.81,25.951,25.141,50.274,45.999,71.132c32.946,32.946,76.582,53.608,122.868,58.181c6.606,0.652,13.217,0.975,19.819,0.975    c39.022,0,77.548-11.293,110.238-32.581c4.628-3.014,5.937-9.209,2.923-13.837s-9.209-5.937-13.837-2.923    c-71.557,46.597-167.39,36.522-227.869-23.957c-70.962-70.962-70.962-186.425,0-257.388c70.961-70.961,186.424-70.961,257.387,0    c60.399,60.4,70.529,156.151,24.086,227.673c-3.008,4.632-1.691,10.826,2.94,13.833c4.634,3.008,10.826,1.691,13.833-2.941    C504.367,371.396,515.537,325.241,511.005,279.646z M259.849,44.263c0-7.72,6.28-14,14-14h72c7.72,0,14,6.28,14,14s-6.28,14-14,14    h-1.794h-68.413h-1.793C266.129,58.263,259.849,51.982,259.849,44.263z M285.642,99.296V78.263h48.413v20.997    C317.979,97.348,301.715,97.36,285.642,99.296z" />
                                                    <path id="XMLID_391_" d="M445.77,425.5c-2.64,0-5.21,1.07-7.069,2.93c-1.87,1.86-2.931,4.44-2.931,7.07    c0,2.63,1.061,5.21,2.931,7.07c1.859,1.87,4.43,2.93,7.069,2.93c2.63,0,5.2-1.06,7.07-2.93c1.86-1.86,2.93-4.44,2.93-7.07    c0-2.63-1.069-5.21-2.93-7.07C450.97,426.57,448.399,425.5,445.77,425.5z" />
                                                    <path id="XMLID_394_" d="M310.001,144.609c-85.538,0-155.129,69.59-155.129,155.129s69.591,155.129,155.129,155.129    s155.129-69.59,155.129-155.129S395.539,144.609,310.001,144.609z M310.001,434.867c-74.511,0-135.129-60.619-135.129-135.129    s60.618-135.129,135.129-135.129S445.13,225.228,445.13,299.738S384.512,434.867,310.001,434.867z" />
                                                    <path id="XMLID_397_" d="M373.257,222.34l-49.53,49.529c-4.142-2.048-8.801-3.205-13.726-3.205c-4.926,0-9.584,1.157-13.726,3.205    l-22.167-22.167c-3.906-3.905-10.236-3.905-14.143,0c-3.905,3.905-3.905,10.237,0,14.142l22.167,22.167    c-2.049,4.142-3.205,8.801-3.205,13.726c0,17.134,13.939,31.074,31.074,31.074s31.074-13.94,31.074-31.074    c0-4.925-1.157-9.584-3.205-13.726l48.076-48.076v0l1.453-1.453c3.905-3.905,3.905-10.237,0-14.142    S377.164,218.435,373.257,222.34z M310.001,310.812c-6.106,0-11.074-4.968-11.074-11.074s4.968-11.074,11.074-11.074    s11.074,4.968,11.074,11.074S316.107,310.812,310.001,310.812z" />
                                                    <path id="XMLID_398_" d="M416.92,289.86h-9.265c-5.522,0-10,4.477-10,10s4.478,10,10,10h9.265c5.522,0,10-4.477,10-10    S422.442,289.86,416.92,289.86z" />
                                                    <path id="XMLID_399_" d="M212.346,289.616h-9.264c-5.522,0-10,4.477-10,10s4.478,10,10,10h9.264c5.522,0,10-4.477,10-10    S217.868,289.616,212.346,289.616z" />
                                                    <path id="XMLID_400_" d="M310.123,212.083c5.522,0,10-4.477,10-10v-9.264c0-5.523-4.478-10-10-10s-10,4.477-10,10v9.264    C300.123,207.606,304.601,212.083,310.123,212.083z" />
                                                    <path id="XMLID_424_" d="M309.879,387.393c-5.522,0-10,4.477-10,10v9.264c0,5.523,4.478,10,10,10s10-4.477,10-10v-9.264    C319.879,391.87,315.401,387.393,309.879,387.393z" />
                                                    <path id="XMLID_425_" d="M10,351.44c-2.63,0-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07c0,2.64,1.069,5.21,2.93,7.07    s4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.86-1.86,2.931-4.44,2.931-7.07s-1.07-5.21-2.931-7.07    C15.21,352.51,12.63,351.44,10,351.44z" />
                                                </svg>
                                                <p class="reading__time">Время чтения: <b><?=$el["PROPERTY_READ_TIME_VALUE"]?></b></p>
                                            <? } ?>

                                            <!-- Необходимо вешать класс reading__btn_disabled когда кнопку необходимо выключить -->
                                            <button type="button" class="btn reading__btn">Прочесть позже</button>
                                            <!-- Необходимо вешать класс reading__status_active когда текст необходимо включить -->

                                            <p class="reading__status">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"
                                                     width="20" height="20">
                                                    <path fill="#24ac00" d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26 S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" />
                                                    <path fill="#24ac00" d="M38.252,15.336l-15.369,17.29l-9.259-7.407c-0.43-0.345-1.061-0.274-1.405,0.156c-0.345,0.432-0.275,1.061,0.156,1.406 l10,8C22.559,34.928,22.78,35,23,35c0.276,0,0.551-0.114,0.748-0.336l16-18c0.367-0.412,0.33-1.045-0.083-1.411 C39.251,14.885,38.62,14.922,38.252,15.336z" />
                                                </svg>
                                                Ссылка на статью успешно отправлена!
                                            </p>
                                        </header>
                                        <div style="display: none;" class="reading__grid">
                                            <p class="reading__title">Отправить статью мне на почту</p>
                                            <form class="form reading__form">
                                                <div class="form__row">
                                                    <input name="email" type="email" class="form__input reading__form-input" placeholder="Email">
                                                </div>
                                                <div class="form__row">
                                                    <!-- Captcha -->
                                                    <!-- Captcha -->
                                                </div>

                                                <p class="error___p"></p>

                                                <input type="hidden" name="id" value="<?=$el['ID']?>">

                                                <button type="button" class="btn reading__form-btn send_article_button to___process">Отправить</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- End Reading -->

                                    <div class="article__content text">

                                        <? if( strlen($el['DETAIL_TEXT']) > 0 ){

                                            if( !substr_count($el['DETAIL_TEXT'], '<p>') ){
                                                echo '<p>';
                                            }

                                            echo $el['DETAIL_TEXT'];

                                            if( !substr_count($el['DETAIL_TEXT'], '<p>') ){
                                                echo '</p>';
                                            }

                                        } ?>

                                    </div>

                                    <footer class="article__footer">

                                        <? if( count($el_sections) > 0 ){ ?>
                                            <div class="article__tags">
                                                <? foreach( $el_sections as $key => $sect ){ ?>
                                                    <a href="<?=$blogSections[$sect['ID']]['SECTION_PAGE_URL']?>" class="tag"><?=$sect['NAME']?></a>
                                                <? } ?>
                                            </div>
                                        <? } ?>

                                        <div class="author article__author">

                                            <? if(
                                                strlen($el['PROPERTY_AUTHOR_NAME_VALUE']) > 0
                                                &&
                                                intval($el['PROPERTY_AUTHOR_PHOTO_VALUE']) > 0
                                            ){ ?>
                                                <div class="author__img" style="background: url(<?=tools\funcs::rIMGG($el['PROPERTY_AUTHOR_PHOTO_VALUE'], 5, 60, 60)?>) no-repeat 50%; background-size: cover;"></div>
                                            <? } ?>

                                            <div class="author__info">

                                                <? if( strlen($el['PROPERTY_AUTHOR_NAME_VALUE']) > 0 ){ ?>
                                                    <p class="author__name"><?=$el['PROPERTY_AUTHOR_NAME_VALUE']?></p>
                                                <? } ?>

                                                <p class="author__date"><?=ConvertDateTime($el['PROPERTY_SORT_DATE_VALUE'], "DD.MM.YYYY", "ru")?></p>
                                                
                                            </div>
                                        </div>

                                        <a href="/blog/" class="article__back-link">Назад в блог</a>

                                    </footer>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<? } else {

    include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/include/404include.php";

} ?>

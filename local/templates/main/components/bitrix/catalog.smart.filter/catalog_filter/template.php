<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

$arFilter = $arParams['arFilter'];

$this->setFrameMode(true);

$all_methods = project\catalog::all_methods();
$all_regions = project\catalog::all_regions();


// Не цены
$props = [];
foreach( $arResult['ITEMS'] as $key => $arItem ){
    if( empty($arItem["VALUES"]) || isset($arItem["PRICE"]) ){   continue;   }
    if ( $arItem["DISPLAY_TYPE"] == "A" || $arItem["DISPLAY_TYPE"] == "B" ){   continue;   }
    // Методы заваривания
    if( $arItem['CODE'] == project\catalog::METHOD_PROP_CODE ){
        foreach( $arItem["VALUES"] as $val => $ar ){
            $code = $ar['URL_ID'];
            if( $all_methods[$code] ){
                $arItem["VALUES"][$val]['el'] = $all_methods[$code];
            } else {    unset($arItem["VALUES"][$val]);    }
        }
        if( count($arItem["VALUES"]) > 0 ){    $props[$key] = $arItem;    }
    // Регионы
    } else if( $arItem['CODE'] == project\catalog::REGION_PROP_CODE ){
        foreach( $arItem["VALUES"] as $val => $ar ){
            $code = $ar['URL_ID'];
            if( $all_regions[$code] ){
                $arItem["VALUES"][$val]['el'] = $all_regions[$code];
            } else {    unset($arItem["VALUES"][$val]);    }
        }
        if( count($arItem["VALUES"]) > 0 ){    $props[$key] = $arItem;    }
    }
} ?>


<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter filter">

    <? foreach($arResult["HIDDEN"] as $arItem){ ?>
        <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
    <? } ?>

    <div class="tabs">

        <header class="tabs__header">

            <? $cnt = 0;
            foreach( $props as $key => $arItem ){ $cnt++; ?>
                <button type="button" class="tabs__btn <? if( $cnt==1 ){ echo 'tabs__btn_active'; } ?>"><?=$arItem['NAME']?></button>
            <? } ?>

            <? foreach( $arResult['ITEMS'] as $key => $arItem ){
                if(
                    $arItem['CODE'] == project\catalog::COFFEE_TYPE_PROP_CODE
                    &&
                    is_array($arItem['VALUES']) && count($arItem['VALUES']) > 0
                ){ ?>

                    <div class="b-method tabs__method">

                        <div class="sort">
                            <div class="sort__grid">
                                <p class="sort__name">Бленд и моносорт</p>
                                <div class="sort__list">
                                    <div class="sort__list-wrapper  sort__list-wrapper_s">

                                        <? foreach( $arItem["VALUES"] as $val => $ar ){ ?>

                                            <label class="sort__value">
                                                <input
                                                    type="checkbox"
                                                    value="<?=$ar["HTML_VALUE"]?>"
                                                    name="<?=$ar["CONTROL_NAME"]?>"
                                                    id="<?=$ar["CONTROL_ID"]?>"
                                                    <?=$ar["CHECKED"]?'checked':(in_array($ar['FACET_VALUE'], $arParams['VALUES'])?'checked':''); ?>
                                                    class="sort__value-input"
                                                />
                                                <p class="sort__value-link link sort__value-link_s"><?=$ar['VALUE']?></p>
                                            </label>

                                        <? } ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                <? }
            } ?>

        </header>

        <div class="tabs__grid">

            <? $cnt = 0;
            foreach( $props as $key => $arItem ){ $cnt++; ?>

                <div class="tabs__section <? if( $cnt==1 ){ echo 'tabs__section_opened'; } ?>">

                    <? foreach( $arItem["VALUES"] as $val => $ar ){ ?>

                        <label class="filter-radio tabs__radio">
                            <input
                                class="filter-radio__input"
                                type="checkbox"
                                value="<?=$ar["HTML_VALUE"]?>"
                                name="<?=$ar["CONTROL_NAME"]?>"
                                id="<?=$ar["CONTROL_ID"]?>"
                                <?=$ar["CHECKED"]?'checked':(in_array($ar['FACET_VALUE'], $arParams['VALUES'])?'checked':''); ?>
                            >
                            <div class="filter-radio__layer"></div>
                            <div class="filter-radio__icon"><?=$ar['el']['PREVIEW_TEXT']?></div>
                            <p class="filter-radio__name"><?=$ar['VALUE']?></p>
                        </label>

                    <? } ?>

                </div>

            <? } ?>

        </div>
    </div>

</form>


<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>

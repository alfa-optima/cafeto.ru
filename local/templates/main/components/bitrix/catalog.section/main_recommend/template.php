<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ( count($arResult['ITEMS']) > 0 ){ ?>

    <section class="section new">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <h2 class="section__title new__title">Рекомендуем</h2>

                    <div class="section__grid row">

                        <? foreach( $arResult['ITEMS'] as $arItem ){ ?>

                            <div class="col-12 col-sm-6 col-lg-3 new__col">

                                <? // catalog_item
                                $APPLICATION->IncludeComponent(
                                    "aoptima:catalog_item", "",
                                    array('arItem' => $arItem)
                                ); ?>

                            </div>

                        <? } ?>

                    </div>

                </div>
            </div>
        </div>
    </section>

<? } ?>
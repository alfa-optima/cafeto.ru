<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$maxCNT = $arParams['PAGE_ELEMENT_COUNT']-1;

if( $arParams['IS_AJAX'] == 'Y' ){

    $cnt = 0;
    foreach( $arResult['ITEMS'] as $arItem ){ $cnt++;

        if( $cnt <= $maxCNT ){

            echo '<div class="col-12 col-md-6 col-lg-3 catalog__col">';

                // catalog_item
                $APPLICATION->IncludeComponent(
                    "aoptima:catalog_item", "",
                    array('arItem' => $arItem)
                );

            echo '</div>';

        } else {
            echo '<ost></ost>';
        }

    }

} else {

    if ( count($arResult['ITEMS']) > 0 ){ ?>

        <div class="catalog__list row catalogLoadArea">

            <? $cnt = 0;
            foreach( $arResult['ITEMS'] as $arItem ){ $cnt++;
                
                if( $cnt <= $maxCNT ){

                    echo '<div class="col-12 col-md-6 col-lg-3 catalog__col">';

                        // catalog_item
                        $APPLICATION->IncludeComponent(
                            "aoptima:catalog_item", "",
                            array('arItem' => $arItem)
                        );

                    echo '</div>';

                }

            } ?>

        </div>

        <div class="more_button_block" <? if( $cnt <= $maxCNT ){ ?>style="display:none;"<? } ?>>
            <button type="button" class="btn catalog_load_button to___process">Показать ещё</button>
        </div>

    <? } else { ?>

        <div class="no___count_block">
            <p class="no___count">В данном разделе товаров пока нет</p>
        </div>

    <? }

} ?>
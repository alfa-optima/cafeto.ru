<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$maxCNT = $arParams['MAX_CNT'];

$new_items = [];
foreach ( $arParams['IDS'] as $id ){
    foreach( $arResult['ITEMS'] as $arItem ){
        if(  $arItem['ID'] == $id  &&  !in_array($id, $arParams['STOP_IDS'])  ){
            $new_items[] = $arItem;
        }
    }
}
$arResult['ITEMS'] = $new_items;

if( $arParams['IS_AJAX'] == 'Y' ){

    $cnt = 0;
    foreach( $arResult['ITEMS'] as $arItem ){ $cnt++;

        if( $cnt <= $maxCNT ){

            echo '<div class="col-12 col-md-6 col-lg-3 catalog__col">';

            // catalog_item
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "",
                array('arItem' => $arItem)
            );

            echo '</div>';

        } else {
            echo '<ost></ost>';
        }

    }

} else {

    if ( count($arResult['ITEMS']) > 0 ){ ?>

        <div class="custom-tabs__section catalog-results custom-tabs__section_opened">
            <div class="row searchCatalogLoadArea">

                <? $cnt = 0;
                foreach( $arResult['ITEMS'] as $arItem ){ $cnt++;

                    if( $cnt <= $maxCNT ){

                        echo '<div class="col-12 col-md-6 col-lg-3 catalog__col">';

                        // catalog_item
                        $APPLICATION->IncludeComponent(
                            "aoptima:catalog_item", "",
                            array('arItem' => $arItem)
                        );

                        echo '</div>';

                    }

                } ?>

            </div>

            <div class="more_button_block" <? if( $cnt <= $maxCNT ){ ?>style="display:none;"<? } ?>>
                <button type="button" class="btn search_catalog_load_button to___process">Показать ещё</button>
            </div>

        </div>

    <? }

} ?>
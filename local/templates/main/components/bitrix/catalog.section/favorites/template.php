<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ( count($arResult['ITEMS']) > 0 ){

    foreach( $arResult['ITEMS'] as $arItem ){ $cnt++;

        echo '<div class="col-12 col-md-6 col-lg-3 catalog__col">';

            // catalog_item
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "",
                array(
                    'arItem' => $arItem,
                    'is_favorites' => 'Y'
                )
            );

        echo '</div>';

    }

} else { ?>

    <p class="no___count">Список избранного пуст</p>

<? } ?>

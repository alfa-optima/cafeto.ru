<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include "lang/ru/template.php";

if(empty($arResult))
	return "";
	
$strReturn = '<div class="breadcrumbs template__breadcrumbs personal__breadcrumbs"><a href="/" class="link breadcrumbs__item breadcrumb__link">Главная</a>';
$page_title = $GLOBALS['APPLICATION']->GetTitle();

$itemSize = count($arResult)-1;

for ($index = 0; $index <= $itemSize; $index++) {
	$title = /*htmlspecialcharsex(*/ $arResult[$index]["TITLE"] /*)*/;
	if ($arResult[($index+1)]["TITLE"]!=$arResult[($index)]["TITLE"]){
		if(($arResult[$index]["LINK"] <> "") && $arResult[$index+1]["TITLE"])
			$strReturn .= '<p class="breadcrumbs__item breadcrumbs__sep">/</p><a href="'.$arResult[$index]["LINK"].'" class="link breadcrumbs__item breadcrumb__link">'.$title.'</a>';
		else
			$strReturn .= '<p class="breadcrumbs__item breadcrumbs__sep">/</p><a title="'.$title.'" class="link breadcrumbs__item breadcrumb__link">'.$title.'</a>';
	}
}

$strReturn .= '</div>';

return $strReturn; ?>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
die(); 

$svgs = array(
	'VKontakte' => '<svg width="40px" height="40px" viewBox="0 0 48.03 48.03"><path fill="#4d729a" d="M56,32A24,24,0,1,1,32,8,24,24,0,0,1,56,32Zm0,0" transform="translate(-7.98 -7.98)" /><path fill="#fff" fill-rule="evenodd" d="M31.09,42.53H33a1.59,1.59,0,0,0,.86-.37,1.45,1.45,0,0,0,.26-.83s0-2.53,1.13-2.9,2.65,2.44,4.22,3.52a3,3,0,0,0,2.1.64l4.21-.06s2.2-.13,1.16-1.87A14.28,14.28,0,0,0,43.79,37c-2.64-2.45-2.29-2,.89-6.29,1.94-2.58,2.71-4.16,2.47-4.83s-1.65-.48-1.65-.48l-4.75,0a1,1,0,0,0-.61.11,1.26,1.26,0,0,0-.42.51A27,27,0,0,1,38,29.78c-2.11,3.59-3,3.78-3.3,3.56-.8-.52-.6-2.09-.6-3.2,0-3.47.52-4.92-1-5.3a8.53,8.53,0,0,0-2.21-.22,10.36,10.36,0,0,0-3.94.4c-.54.27-.95.86-.7.89a2.09,2.09,0,0,1,1.4.71,4.51,4.51,0,0,1,.47,2.14s.28,4.09-.65,4.6c-.64.35-1.52-.37-3.4-3.62a30.52,30.52,0,0,1-1.69-3.5,1.38,1.38,0,0,0-.39-.53,1.93,1.93,0,0,0-.73-.3l-4.51,0a1.51,1.51,0,0,0-.92.32c-.22.26,0,.8,0,.8S19.28,34.81,23.27,39a10.82,10.82,0,0,0,7.82,3.56Zm0,0" transform="translate(-7.98 -7.98)"/></svg>',
	'Facebook' => '<svg width="40px" height="40px" viewBox="0 0 47.72 47.72"><path fill="#3b5690" d="M31.64,8.17A23.86,23.86,0,1,1,7.78,32,23.86,23.86,0,0,1,31.64,8.17Zm0,0" transform="translate(-7.78 -8.17)"/><path fill="#fff" d="M34.54,24.6h3.08V20.06H34c-4.38.16-5.28,2.62-5.36,5.21h0v2.26h-3V32h3V43.92h4.49V32h3.68l.71-4.45H33.13V26.17a1.46,1.46,0,0,1,1.41-1.57Zm0,0" transform="translate(-7.78 -8.17)"/></svg>',
    //'Odnoklassniki' => ''
); ?>

<div class="modal__social">
    <p class="modal__social-name">или войти с помощью:</p>
    <div class="modal__social-grid">

        <? foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){ ?>

            <a href="javascript:void(0)" class="modal__social-item" onclick="<?=$service['ONCLICK']?>"><?=$svgs[$service_code]?></a>

        <? } ?>

    </div>
</div>





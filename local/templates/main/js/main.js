// Main scripts for the project
 ! function () {
     $(document).ready(function () {
         //Search
         $('.search-btn').on('click', function () {
             $('.search').addClass('search_opened');
         });
         $('.search__close').on('click', function () {
             $('.search').removeClass('search_opened');
         });
         //More
         /*$(document).on('click', '.show-more', function(e){
             $(this).prev().toggleClass('text-overflow_opened');
             var textValue = $(this).text();
             switch (textValue) {
                 case 'Полное описание':
                     $(this).text('Свернуть');
                     break;
                 case 'Свернуть':
                     $(this).text('Полное описание');
                     break;
                 default:
                     $(this).text('Полное описание');
                     break;
             }
         });*/

         //Scroll
         // Select all links with hashes
         $('a[href*="#"]')
         // Remove links that don't actually link to anything
         .not('[href="#"]')
         .not('[rel="modal:open"]')
         .not('[href="#0"]')
         .click(function (event) {
             // On-page links
             if (
                 location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                 location.hostname == this.hostname
             ) {
                 // Figure out element to scroll to
                 var target = $(this.hash);
                 target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                 // Does a scroll target exist?
                 if (target.length) {
                     // Only prevent default if animation is actually gonna happen
                     event.preventDefault();
                     $('html, body').animate({
                         scrollTop: target.offset().top
                     }, 1000, function () {
                         // Callback after animation
                         // Must change focus!
                         var $target = $(target);
                         $target.focus();
                         if ($target.is(":focus")) { // Checking if the target was focused
                             return false;
                         } else {
                             $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                             $target.focus(); // Set focus again
                         };
                     });
                 }
             }
         });

         //Slider
         if ($('.slider').length !== 0) {
             var slider = new Swiper('.slider__grid', {
                 effect: 'fade',
                 fadeEffect: {
                     crossFade: true
                 },
                 autoplay: {
                     delay: 3000,
                 },
                 pagination: {
                     el: '.slider__pagination',
                     type: 'bullets'
                 },
                 navigation: {
                     nextEl: '.slider__arrow-next',
                     prevEl: '.slider__arrow-prev',
                 }
             });
         }
         /*$('.js-item').on('click', function(){
             $(this).closest('table').find('.history-page__item_s').toggleClass('history-page__item_s-active');
         });*/
         //Share
         $(document).on('click', '.share__icon', function(e){
             $(this).toggleClass('share__icon_active').next().toggleClass('share__grid_opened');
         });
         //Gallery
         var gallerySlider = new Swiper('.product-gallery', {
             simulateTouch: false
         });
         var thumbsCarousel = new Swiper('.product-thumbs', {
             slidesPerView: 5,
             spaceBetween: 15,
             slideToClickedSlide: true,
             preventClicks: false,
             navigation: {
                 nextEl: '.product-thumbs-arrow-next',
                 prevEl: '.product-thumbs-arrow-prev'
             },
             slideActiveClass: 'thumbs__item_active',
             // Responsive breakpoints
             breakpoints: {
                 // when window width is <= 320px
                 320: {
                     slidesPerView: 2
                 },
                 // when window width is <= 480px
                 480: {
                     slidesPerView: 3
                 },
                 // when window width is <= 640px
                 992: {
                     slidesPerView: 4
                 }
             }
         });
         thumbsCarousel.on('slideChangeTransitionStart', function () {
             var index = thumbsCarousel.realIndex;
             gallerySlider.slideTo(index);
         });
         $('.product-thumbs .thumbs__item').on('click', function () {
             var clickedIndex = $('.product-thumbs .thumbs__item').index($(this));
             $('.thumbs__item').removeClass('thumbs__item_active');
             $(this).addClass('thumbs__item_active');
             gallerySlider.slideTo(clickedIndex);
         });
         //Quick gallery
         var quickGallerySlider = new Swiper('.quick-product-gallery', {
             simulateTouch: false
         });
         var quickThumbsCarousel = new Swiper('.quick-product-thumbs', {
             slidesPerView: 5,
             spaceBetween: 17,
             slideToClickedSlide: true,
             preventClicks: false,
             navigation: {
                 nextEl: '.quick-thumbs-arrow-next',
                 prevEl: '.quick-thumbs-arrow-prev'
             },
             slideActiveClass: 'thumbs__item_active',
             // Responsive breakpoints
             breakpoints: {
                 // when window width is <= 320px
                 320: {
                     slidesPerView: 2
                 },
                 // when window width is <= 480px
                 480: {
                     slidesPerView: 3
                 },
                 // when window width is <= 640px
                 992: {
                     slidesPerView: 3
                 }
             }
         });
         quickThumbsCarousel.on('slideChangeTransitionStart', function () {
             var index = quickThumbsCarousel.realIndex;
             quickGallerySlider.slideTo(index);
         });
         $('.quick-product-thumbs .thumbs__item').on('click', function () {
             var clickedIndex = $('.quick-product-thumbs .thumbs__item').index($(this));
             $('.quick-product-thumbs .thumbs__item').removeClass('thumbs__item_active');
             $(this).addClass('thumbs__item_active');
             quickGallerySlider.slideTo(clickedIndex);
         });
         //Modal events
         $('#quick').on($.modal.OPEN, function (event, modal) {
             quickThumbsCarousel.update();
             quickGallerySlider.update();
             setTimeout(function () {
                 quickThumbsCarousel.update();
             }, 500);
         });
         //Click
         $('.item__option').on('click', function (e) {
             e.preventDefault();
         });
         //Basket
         $(document).on('click', '.basket', function(e){
             $('html').addClass('is-locked');
             $('.basket-sidebar').addClass('basket-sidebar_opened');
             $('.basket-sidebar__grid').addClass('basket-sidebar__grid_opened');
         });
         $(document).on('click', '.basket-sidebar__close', function(e){
             $('html').removeClass('is-locked');
             $('.basket-sidebar').removeClass('basket-sidebar_opened');
             $('.basket-sidebar__grid').removeClass('basket-sidebar__grid_opened');
         });
         //Tabs
         $('.tabs__btn').on('click', function () {
             if ($(this).hasClass('tabs__btn_active')) {} else {
                 var tabIndex = $('.tabs__btn').index($(this));
                 $('.tabs__btn').removeClass('tabs__btn_active');
                 $(this).addClass('tabs__btn_active');
                 $('.tabs__section').removeClass('tabs__section_opened').eq(tabIndex).addClass('tabs__section_opened');
             }
         });
         $('.custom-tabs__btn').on('click', function () {
             if ($(this).hasClass('custom-tabs__btn_active')) {} else {
                 var tabIndex = $('.custom-tabs__btn').index($(this));
                 $('.custom-tabs__btn').removeClass('custom-tabs__btn_active');
                 $(this).addClass('custom-tabs__btn_active');
                 $('.custom-tabs__section').removeClass('custom-tabs__section_opened').eq(tabIndex).addClass('custom-tabs__section_opened');
             }
         });
         //Stars
         /* 1. Visualizing things on Hover - See next part for action on click */
         $('.stars__grid li').on('mouseover', function () {
             var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
             // Now highlight all the stars that's not after the current hovered star
             $(this).parent().children('li.stars__item').each(function (e) {
                 if (e < onStar) {
                     $(this).addClass('hover');
                 } else {
                     $(this).removeClass('hover');
                 }
             });
         }).on('mouseleave', function () {
             $(this).parent().children('li.stars__item').each(function (e) {
                 $(this).removeClass('hover');
             });
         });
         /* 2. Action to perform on click */
         $('.stars__grid li').on('click', function () {
             var onStar = parseInt($(this).data('value'), 10); // The star currently selected
             var stars = $(this).parent().children('li.stars__item');
             for (i = 0; i < stars.length; i++) {
                 $(stars[i]).removeClass('selected');
             }
             for (i = 0; i < onStar; i++) {
                 $(stars[i]).addClass('selected');
             }
             // JUST RESPONSE (Not needed)
             var ratingValue = parseInt($('.stars__grid li.selected').last().data('value'), 10);
             if (ratingValue) {
                 $('.stars__value').text(ratingValue + '/5');
             }
         });
         //Mobile
         $('.mobile-menu-btn').on('click', function () {
             $('.menu').toggleClass('menu_opened');
         });
         $('.touchevents .has-children > a').on('click', function (e) {
             e.preventDefault();
             $(this).next().toggleClass('sub-menu_opened');
         });
         //Delivery options
         $('.delivery-options__radio').on('click', function(){
             if ( $(this).find('input').is(':checked') ){}
             else{
                 $('.delivery-options__item').removeClass('delivery-options__item_opened');
                 $(this).parent().addClass('delivery-options__item_opened');
             }
         });

         //Favorites
         /*$(document).on('click', '.add-to-fav', function(e){
             $(this).toggleClass('add-to-fav_active');
         });*/
         //Reading
         $(document).on('click', '.reading__btn', function(e){
             if ($(this).text() == 'Прочесть позже') {
                 $(this).text('Прочту сейчас');
             } else {
                 $(this).text('Прочесть позже');
             }
             $('.reading__grid').slideToggle();
         });

         // Team
         if ($('.team__carousel').length !== 0) {
             var teamCarousel = new Swiper('.team__carousel', {
                 effect: 'fade',
                 fadeEffect: {
                     crossFade: true
                 },
                 navigation: {
                     nextEl: '.carousel-info__btn-next',
                     prevEl: '.carousel-info__btn-prev',
                 }
             });
             $('.carousel-info span:last-child').text(teamCarousel.slides.length);
             teamCarousel.on('slideChangeTransitionStart', function(){
                 $('.carousel-info span:first-child').text(teamCarousel.activeIndex + 1);
             });
         }

         //Switcher
         // $('button[type="reset"]').on('click', function(){
         //     $('.method__switcher').removeClass('method__switcher_r').addClass('method__switcher_l');
         // });
         // $('.method__item').on('click', function(e){
         //     var elIndex = $('.method__item').index($(this));
         //     if ( $(this).find('input').is(':checked') ){}
         //     else{
         //         if ( elIndex == 1 ){
         //             $('.method__switcher').removeClass('method__switcher_l').addClass('method__switcher_r');
         //         }
         //         else{
         //             $('.method__switcher').removeClass('method__switcher_r').addClass('method__switcher_l');
         //         }
         //     }
         // });
         // $('.method__switcher').on('click', function(){
         //     $(this).toggleClass('method__switcher_l method__switcher_r');
         //     $('input[name="method"]').not(':checked').prop('checked', true);
         // });

     });
 }();
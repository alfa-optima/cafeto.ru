$(document).ready(function(){



    // Авторизация
    $(document).on('click', '.auth___button', function(e){
        if( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'login': {  tag: 'input',  name_ru: 'Email / логин',
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email/логин'  },
                'password': {  tag: 'input',  name_ru: 'Пароль',
                    required: true,  required_error: 'Введите, пожалуйста, Ваш пароль'  },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"auth",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $('.close-modal').trigger('click');
                        window.location.href = document.URL;
                    } else if ( response.status == 'error' ){
                        process(false);
                        show_error( this_form, response.text );
                    }
                }, 'json')
                .fail(function(response, status, xhr){
                    show_error( this_form, "Ошибка запроса" );
                    process(false);
                })
                .always(function(response, status, xhr){    })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })


    // Регистрация
    $(document).on('click', '.register___button', function(e){
        if( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'NAME': {
                    tag: 'input',  name_ru: 'Имя',
                    required: true,  required_error: 'Введите, пожалуйста, Ваше имя'
                },
                'LAST_NAME': {
                    tag: 'input',  name_ru: 'Фамилия',
                    required: true,  required_error: 'Введите, пожалуйста, Вашу фамилию'
                },
                'EMAIL': {
                    tag: 'input', is_email: true,  name_ru: 'Email',
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'
                },
                'PASSWORD': {
                    tag: 'input',  name_ru: 'Пароль', min_length: 8,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш пароль'
                },
                'agree': {
                    tag: 'input', type: 'checkbox',
                    name_ru: 'Согласие с условиями использования', required: true
                },
                'oferta_agree': {
                    tag: 'input', type: 'checkbox',
                    name_ru: 'Согласие с условиям публичной оферты', required: true
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"register",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $(this_form).find("input[type=text], input[type=email], input[type=password]").val("");
                        $('.close-modal').trigger('click');
                        show_message( 'Регистрация завершена!<br>На указанный Email отправлена ссылка для подтверждения регистрации', 'success', 'button' );
                    } else if ( response.status == 'error' ){
                        show_message( response.text, 'warning', 'hover' );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {
                show_error( this_form, [checkResult.errors[0]] );
                $('.form___checkbox:visible').each(function(){
                    if( $(this).hasClass('is___error') ){
                        $(this).parent().find('.sort__value-link').addClass('sort__value-link_red');
                    }
                })
            }
        }
    })



    // ЛК - Редактирование личных данных
    $(document).on('click', '.personal_data_button', function(e){
        if( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'NAME': {
                    tag: 'input',  name_ru: 'Имя',
                    required: true,  required_error: 'Введите, пожалуйста, Ваше имя'
                },
                'LAST_NAME': {
                    tag: 'input',  name_ru: 'Фамилия',
                    required: true,  required_error: 'Введите, пожалуйста, Вашу фамилию'
                },
                'PERSONAL_PHONE': { tag: 'input', is_phone: true,  name_ru: 'Телефон' },
                'LOGIN': {
                    tag: 'input',  name_ru: 'Email',
                    required: true,  required_error: 'Введите, пожалуйста, Ваш логин'
                },
                'EMAIL': {
                    tag: 'input', is_email: true,  name_ru: 'Email',
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"personal_data_save",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        show_message( 'Успешное сохранение!', 'success', 'hover' );
                    } else if ( response.status == 'error' ){
                        show_message( response.text, 'warning', 'hover' );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // ЛК - Редактирование пароля
    $(document).on('click', '.personal_password_button', function(e){
        if( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'PASSWORD': {
                    tag: 'input',  name_ru: 'Пароль', min_length: 8,
                    required: true,  required_error: 'Введите, пожалуйста, пароль'
                },
                'CONFIRM_PASSWORD': {
                    tag: 'input', name_ru: 'Подтверждение пароля',
                    required: true,  required_error: 'Введите, пожалуйста, повтор пароля'
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                if( checkResult.values.PASSWORD != checkResult.values.CONFIRM_PASSWORD ){
                    show_error( this_form, 'Пароли отличаются' );
                } else {
                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"personal_password_save",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){
                            $(this_form).find("input[type=password]").val("");
                            show_message( 'Успешное сохранение!', 'success', 'hover' );
                        } else if ( response.status == 'error' ){
                            show_message( response.text, 'warning', 'hover' );
                        }
                    }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                }
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // Восстановление пароля (ЗАПРОС ССЫЛКИ)
    $(document).on('click', '.password___recovery_button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'email': {  tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите Ваш Email'  },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"password_recovery",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $('.close-modal').trigger('click');
                        show_message(
                            'На&nbsp;указанный&nbsp;Email отправлена ссылка для&nbsp;восстановления пароля',
                            'success', 'button'
                        );
                        $(this_form).find("input[type=email]").val("");
                    } else if ( response.status == 'error' ){
                        show_error( this_form, response.text );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // Восстановление пароля (СОХРАНЕНИЕ)
    $(document).on('click', '.password___recovery_save_button', function(){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'PASSWORD': {
                    tag: 'input',  name_ru: 'Пароль', min_length: 8,
                    required: true,  required_error: 'Введите, пожалуйста, новый пароль'
                },
                'CONFIRM_PASSWORD': {
                    tag: 'input', name_ru: 'Подтверждение пароля',
                    required: true,  required_error: 'Введите, пожалуйста, повтор пароля'
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                if( checkResult.values.PASSWORD != checkResult.values.CONFIRM_PASSWORD ){
                    show_error( this_form, 'Пароли отличаются' );
                } else {
                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"password_recovery_save",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){

                            $('.password_update_block').html('<h1 class="title personal__title">Восстановление пароля</h1><p class="success___p">Пароль успешно изменён</p><p class="success___p"><a href="#log-in" rel="modal:open" class="link sub-menu__link">Авторизоваться</a></p>');

                        } else if ( response.status == 'error' ){
                            show_message( response.text, 'warning', 'hover' );
                        }
                    }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                }
                // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    });




// Подгрузка заказов в ЛК
    $(document).on('click', '.order_history_load_button', function(e){
        if( !is_process(this) ){
            var stop_ids = [];
            $('.order___item').each(function(){
                var item_id = $(this).attr('item_id');
                stop_ids.push(item_id);
            })
            var postParams = {
                stop_ids: stop_ids
            };
            process(true);
            $.post("/ajax/order_history_load.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('.orders___table tbody').append(data.html)

                    if( $('tr.ost').length > 0 ){
                        $('.more_button_block').show();
                    } else {
                        $('.more_button_block').hide();
                    }
                    $('tr.ost').remove();

                } else if (data.status == 'error'){
                    show_message( data.text, 'warning', 'hover' );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                show_message( 'Ошибка запроса', 'warning', 'hover' );
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




    
    
    $(document).on('click', '.review___form .stars__item', function(e){
        $('.review___form .error___p').html('');
        $('.review___form input[name=vote]').val( $('.review___form .stars__item.selected').length );
    })

    // Новый отзыв
    $(document).on('click', '.review___button', function(e){
        if( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'review_text': {  tag: 'textarea',  name_ru: 'Текст отзыва',  required: true,  required_error: 'Введите, пожалуйста, текст отзыва'  }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var stars_cnt = $('.review___form .stars__item.selected').length;
                if( stars_cnt == 0 ){
                    $('.review___form .error___p').html('Оцените, пожалуйста, товар по 5-бальной шкале!');
                } else {
                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"new_review",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){
                            show_message(
                                'Отзыв успешно отправлен!<br>Он появится после модерации',
                                'success', 'hover'
                            );
                            $(this_form).find("input[type=text], textarea").val("");
                            $('.review___form .stars__item.selected').removeClass('selected');
                            $('.close-modal ').trigger('click');
                        } else if ( response.status == 'error' ){
                            show_error( this_form, response.text );
                        }
                    }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                }
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })

    // Подгрузка отзывов в товаре
    $(document).on('click', '.productReviewsLoadButton', function(e){
        if( !is_process(this) ){
            var params = { product_id: $('input[name=card_product_id]').val() };
            params.stop_ids = [];
            $('.review___item').each(function(){
                var item_id = $(this).attr('item_id');
                params.stop_ids.push(item_id);
            })
            process(true);
            $.post("/ajax/reviewsLoad.php", params, function(data){
                if (data.status == 'ok'){

                    $('.reviewsLoadBlock').before(data.html);

                    if( $('ost').length > 0 ){
                        $('.reviewsLoadBlock').show();
                    } else {
                        $('.reviewsLoadBlock').hide();
                    }
                    $('ost').remove();

                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




    $(document).on('click', '.order_pay_link', function(e){
        if( !is_process(this) ){
            var order_id = $(this).attr('item_id');
            getPaymentButton(order_id);
        }
    })




})

function blogLoad( params ){
    if( !params ){ var params = {}; }
    var is_filter = true;
    if(
        params.section_id == undefined
        &&
        $('input[name=section_id]').length > 0
    ){     params.section_id = $('input[name=section_id]').val();     }
    if( params.stop_ids != undefined ){   is_filter = false;   }
    process(true);
    $.post("/ajax/blogLoad.php", params, function(data){
        if (data.status == 'ok'){

            if( is_filter ){
                $('.blogLoadArea').html(data.html);
                $('div.breadcrumbs').replaceWith(data.breadcrumbs);
            } else {
                $('.blogLoadArea').append(data.html);
            }

            if( $('ost').length > 0 ){
                $('.more_button_block').show();
            } else {
                $('.more_button_block').hide();
            }
            $('ost').remove();

            if(  params.section_id != undefined ){
                $('input[name=section_id]').val( params.section_id );
            }

            if( data.section != undefined ){
                history.replaceState({}, null, data.section.SECTION_PAGE_URL);
                $('h1.page__title').html(data.section.NAME);
                $('head meta[name=description]').attr( 'content', data.section.UF_DESCRIPTION!=undefined?data.section.UF_DESCRIPTION:data.section.NAME );
                $('head meta[name=keywords]').attr( 'content', data.section.UF_KEYWORDS!=undefined?data.section.UF_KEYWORDS:data.section.NAME );
                $('head title').html( data.section.UF_TITLE!=undefined?data.section.UF_TITLE:data.section.NAME );
                if( $('input[name=section_id]').length == 0 ){
                    $('.template.page.blog').after('<input type="hidden" name="section_id" value="'+data.section.ID+'">');
                }
            } else {
                $('h1.page__title').html('Блог');
                $('head title').html( 'Блог' );
                $('head meta[name=description]').attr( 'content', 'Блог' );
                $('head meta[name=keywords]').attr( 'content', 'Блог' );
                history.replaceState({}, null, '/blog/');
            }

        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
        setFavorites();
    })
}





$(document).ready(function(){

    $("input[name=phone], input[type=tel]").mask("7 (999) 999-99-99");



    // Подписка на товар
    $(document).on('click', '.subscribe_product_button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'email': {  tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'  }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"subscribe_product_quantity",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        show_message(
                            'Подписка успешно оформлена!', 'success', 'hover'
                        );
                        $(this_form).find("input[type=email]").val("");
                        $('.reading__btn').trigger('click');
                    } else if ( response.status == 'error' ){
                        show_message(
                            response.text, 'warning', 'hover'
                        );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })


    $(document).on('click', '.product_subscribe__btn', function(e){
        if ($(this).text() == 'Уведомить о появлении') {
            $(this).text('Не уведомлять');
        } else {
            $(this).text('Уведомить о появлении');
        }
        $('.reading__grid').slideToggle();
    });



    // Прочесть статью позже
    $(document).on('click', '.send_article_button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'email': {  tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'  }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"send_article",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        show_message(
                            'Ссылка успешно отправлена на Email', 'success', 'hover'
                        );
                        $(this_form).find("input[type=email]").val("");
                        $('.reading__btn').trigger('click');
                    } else if ( response.status == 'error' ){
                        show_message(
                            response.text, 'warning', 'hover'
                        );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })


    $(document).on('input', '.subscribe___form input', function(e){
        $.noty.closeAll();
    })

    // Подписка на рассылку
    $(document).on('click', '.subscribe___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'email': {
                    tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'
                }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"subscribe",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        show_message(
                            response.text, 'success', 'hover'
                        );
                        $(this_form).find("input[type=text], input[type=password]").val("");
                    } else if ( response.status == 'error' ){
                        show_message(
                            response.text, 'warning', 'hover'
                        );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]], true );    }
        }
    })



    // Запрос обратного звонка
    $(document).on('click', '.callback___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');

            var gRecaptchaResponse = $(this_form).find("textarea[name=g-recaptcha-response]").val();

            // Настройка полей для проверки
            var fields = {
                'name': {
                    tag: 'input',  name_ru: 'Имя',
                    required: true,  required_error: 'Введите, пожалуйста, Ваше имя'
                },
                'phone': {
                    tag: 'input',  name_ru: 'Телефон', is_phone: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш телефон'
                },
                'agree': {
                    tag: 'input', type: 'checkbox',
                    name_ru: 'Согласие с условиями использования', required: true
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){

                if (gRecaptchaResponse.length == 0) {

                    show_error( this_form, 'Пройдите, пожалуйста, проверку пользователя!' );

                } else {

                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"callback",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){
                            show_message(
                                'Запрос успешно отправлен!', 'success', 'hover'
                            );
                            $(this_form).find("input[type=text], input[type=tel], textarea[name=message]").val("");
                            $('.close-modal').trigger('click');
                        } else if ( response.status == 'error' ){
                            show_message(
                                response.text, 'warning', 'hover'
                            );
                        }
                    }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                }

            // Если есть ошибки
            } else {
                show_error( this_form, [checkResult.errors[0]] );
                $('.form___checkbox').each(function(){
                    if( $(this).hasClass('is___error') ){
                        $(this).parent().find('.sort__value-link').addClass('sort__value-link_red');
                    }
                })
            }
        }
    })



    // Подгрузка в блоге
    $(document).on('click', '.blog_load_button', function(e){
        if( !is_process(this) ){
            var params = {};
            params.stop_ids = [];
            $('.article___item').each(function(){
                var item_id = $(this).attr('item_id');
                params.stop_ids.push(item_id);
            })
            blogLoad( params );
        }
    })

    // Фильтр по разделу в блоге
    $(document).on('change', 'input[name=blog_section]', function(e){
        var section_id = $(this).val();
        var params = { section_id: section_id };
        blogLoad( params );
    })
    $(document).on('click', '.setBlogSectionButton', function(e){
        if( !is_process(this) ){
            var section_id = $(this).attr('section_id');
            $('input.blog_section_input').prop('checked', false);
            $('input.blog_section_input[value='+section_id+']').prop('checked', true);
            $('input.blog_section_input[value='+section_id+']').trigger('change');
        }
    });
    // Сброс фильтра в блоге
    $(document).on('click', '.blog_clear_filter', function(e){
        if( !is_process(this) ){
            $('input[name=blog_section]').prop('checked', false);
            $('input[name=section_id]').remove();
            blogLoad();
        }
    })








    $(document).on('click', '.feedback___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            var gRecaptchaResponse = $(this_form).find("textarea[name=g-recaptcha-response]").val();
            // Настройка полей для проверки
            var fields = {
                'name': {  tag: 'input',  name_ru: 'Имя',
                    required: true,  required_error: 'Введите, пожалуйста, Ваше имя'  },
                'phone': {  tag: 'input',  name_ru: 'Телефон',  is_phone: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш телефон'  },
                'message': {  tag: 'textarea',  name_ru: 'Сообщение',
                    required: true,  required_error: 'Введите, пожалуйста, текст сообщения'  },
                'agree': {
                    tag: 'input', type: 'checkbox',
                    name_ru: 'Согласие с условиями использования', required: true
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){

                if (gRecaptchaResponse.length == 0) {

                    show_error( this_form, 'Пройдите, пожалуйста, проверку пользователя!' );

                } else {

                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"feedback",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){
                            show_message(
                                'Сообщение успешно отправлено',
                                'success', 'hover'
                            );
                            //$(this_form).find("input[type=text], input[type=tel], textarea[name=message]").val("");

                            $('.feedback___form').replaceWith(response.feedback_form_html);

                        } else if ( response.status == 'error' ){
                            show_error( this_form, response.text );
                        }
                    }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                }
			// Если есть ошибки
            } else {
                show_error( this_form, [checkResult.errors[0]] );
                $('.form___checkbox').each(function(){
                    if( $(this).hasClass('is___error') ){
                        $(this).parent().find('.sort__value-link').addClass('sort__value-link_red');
                    }
                })
            }
        }
    })




    $(document).on('change', '.form___checkbox', function(e){
        $(this).parent().find('.sort__value-link').removeClass('sort__value-link_red');
    })




    // Открытие окна запроса обратного звонка
    $(document).on('click', '.openCallbackWindowButton', function(e){
        if ( !is_process(this) ){
            process(true);
            $.post("/ajax/openCallbackWindow.php", {}, function(data){
                if (data.status == 'ok'){
                    $('.сallbackWindowBlock').html(data.html);
                    $('.сallbackWindowButtonHidden').trigger('click');
                }
            }, 'json')
            .fail(function(data, status, xhr){
                show_message( 'Ошибка запроса' );
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })





});
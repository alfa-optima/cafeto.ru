var filterInterval;
var basketProcessInterval;
var zipInterval;

function catalogLoad( params ){
    if( !params ){ var params = {}; }
    params.section_id = $('input[name=section_id]').val();
    params.url = document.URL;
    process(true);
    $.post("/ajax/catalogLoad.php", params, function(data){
        if (data.status == 'ok'){

            if( 'stop_ids' in params ){
                $('.catalogLoadArea').append(data.html);
            } else {
                $('.catalogLoadArea').html(data.html);
            }
            if( $('ost').length > 0 ){
                $('.more_button_block').show();
            } else {
                $('.more_button_block').hide();
            }
            $('ost').remove();

            if( 'sort_code' in params ){
                $.cookie('BITRIX_SM_sort', params.sort_code, { expires: 7, path: '/' });
                var sort_name = false;
                $('.sortBlock .sortButton').each(function () {
                    var sort_code = $(this).attr('code');
                    if( sort_code == params.sort_code ){
                        sort_name = $(this).html();
                    }
                })
                if( sort_name ){
                    $('.sortBlock p.sort__name span').html(sort_name);
                }

            }

            if( 'seo_page' in data ){

                $('title').html(data.seo_page.TITLE);
                $('.seo__title').html(data.seo_page.H1);
                if( data.seo_page.TEXT.length > 0 ){
                    $('.seo___text').show();
                    $('.seo___text').html(data.seo_page.TEXT);
                } else {
                    $('.seo___text').hide();
                }

                var breadcrumbs = '<div class="breadcrumbs template__breadcrumbs catalog__breadcrumbs"><a href="/" class="link breadcrumbs__item breadcrumb__link">Главная</a><p class="breadcrumbs__item breadcrumbs__sep">/</p><a href="'+data.section.SECTION_PAGE_URL+'" class="link breadcrumbs__item breadcrumb__link">'+data.section.NAME+'</a><p class="breadcrumbs__item breadcrumbs__sep">/</p><a title="'+data.seo_page.NAME+'" class="link breadcrumbs__item breadcrumb__link">'+data.seo_page.NAME+'</a></div>';
                $('.breadcrumbs').replaceWith(breadcrumbs);

            } else {

                $('title').html(data.section.UF_TITLE?data.section.UF_TITLE:data.section.NAME);
                $('.seo__title').html(data.section.NAME);
                if( data.section.DESCRIPTION.length > 0 ){
                    $('.seo___text').show();
                    $('.seo___text').html(data.section.DESCRIPTION);
                } else {
                    $('.seo___text').hide();
                }

                var breadcrumbs = '<div class="breadcrumbs template__breadcrumbs catalog__breadcrumbs"><a href="/" class="link breadcrumbs__item breadcrumb__link">Главная</a><p class="breadcrumbs__item breadcrumbs__sep">/</p><a title="'+data.section.NAME+'" class="link breadcrumbs__item breadcrumb__link">'+data.section.NAME+'</a></div>';
                $('.breadcrumbs').replaceWith(breadcrumbs);

            }

        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
        setFavorites();
    })
}


function searchCatalogLoad( params ){
    if( !params ){ var params = {}; }
    process(true);
    $.post("/ajax/searchCatalogLoad.php", params, function(data){
        if (data.status == 'ok'){
            $('.searchCatalogLoadArea').append(data.html);
            if( $('ost').length > 0 ){
                $('.more_button_block').show();
            } else {
                $('.more_button_block').hide();
            }
            $('ost').remove();
        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
        setFavorites();
    })
}

function searchBlogLoad( params ){
    if( !params ){ var params = {}; }
    process(true);
    $.post("/ajax/searchBlogLoad.php", params, function(data){
        if (data.status == 'ok'){
            $('.searchBlogLoadArea').append(data.html);
            if( $('ost').length > 0 ){
                $('.more_button_block').show();
            } else {
                $('.more_button_block').hide();
            }
            $('ost').remove();
        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
        setFavorites();
    })
}



function catalogItemModalLoad( params ){
    process(true);
    $.post("/ajax/catalog_item_modal.php", params, function(data){
        if (data.status == 'ok'){

            $('#catalog_item_modal').html(data.html);
            if( 'sku_id' in params ){} else {
                $('.catalog_item_modal_open').trigger('click');
            }

            //Quick gallery
            var quickGallerySlider = new Swiper('.quick-product-gallery', {
                simulateTouch: false
            });
            var quickThumbsCarousel = new Swiper('.quick-product-thumbs', {
                slidesPerView: 5,
                spaceBetween: 17,
                slideToClickedSlide: true,
                preventClicks: false,
                navigation: {
                    nextEl: '.quick-thumbs-arrow-next',
                    prevEl: '.quick-thumbs-arrow-prev'
                },
                slideActiveClass: 'thumbs__item_active',
                // Responsive breakpoints
                breakpoints: {
                    // when window width is <= 320px
                    320: {
                        slidesPerView: 2
                    },
                    // when window width is <= 480px
                    480: {
                        slidesPerView: 3
                    },
                    // when window width is <= 640px
                    992: {
                        slidesPerView: 3
                    }
                }
            });
            quickThumbsCarousel.on('slideChangeTransitionStart', function () {
                var index = quickThumbsCarousel.realIndex;
                quickGallerySlider.slideTo(index);
            });
            $('.quick-product-thumbs .thumbs__item').on('click', function () {
                var clickedIndex = $('.quick-product-thumbs .thumbs__item').index($(this));
                $('.quick-product-thumbs .thumbs__item').removeClass('thumbs__item_active');
                $(this).addClass('thumbs__item_active');
                quickGallerySlider.slideTo(clickedIndex);
            });

        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
        setFavorites();
    })
}


function productCardReload( params ){
    process(true);
    $.post("/ajax/productCardReload.php", params, function(data){
        if (data.status == 'ok'){

            $('.productCardFirstBlock').replaceWith(data.html);

            //Gallery
            var gallerySlider = new Swiper('.product-gallery', {
                simulateTouch: false
            });
            var thumbsCarousel = new Swiper('.product-thumbs', {
                slidesPerView: 5,
                spaceBetween: 15,
                slideToClickedSlide: true,
                preventClicks: false,
                navigation: {
                    nextEl: '.product-thumbs-arrow-next',
                    prevEl: '.product-thumbs-arrow-prev'
                },
                slideActiveClass: 'thumbs__item_active',
                breakpoints: {
                    320: { slidesPerView: 2 },
                    480: { slidesPerView: 3 },
                    992: { slidesPerView: 4 }
                }
            });
            thumbsCarousel.on('slideChangeTransitionStart', function () {
                var index = thumbsCarousel.realIndex;
                gallerySlider.slideTo(index);
            });
            $('.product-thumbs .thumbs__item').on('click', function () {
                var clickedIndex = $('.product-thumbs .thumbs__item').index($(this));
                $('.thumbs__item').removeClass('thumbs__item_active');
                $(this).addClass('thumbs__item_active');
                gallerySlider.slideTo(clickedIndex);
            });

        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
    })
}


function setFavorites(){
    var cur_favs_str = $.cookie('BITRIX_SM_favorites');
    cur_favs = [];
    if( cur_favs_str != undefined && cur_favs_str.length > 0 ){
        cur_favs = cur_favs_str.split('|');
    }
    if( $('.fav___button').length > 0 ){
        $('.fav___button').each(function(){
            var item_id = $(this).attr('item_id');
            if( in_array(item_id, cur_favs) ){
                $(this).addClass('added');
                $(this).addClass('add-to-fav_active');
                $(this).find('span').eq(0).html('Мне нравится');
            } else {
                $(this).removeClass('added');
                $(this).removeClass('add-to-fav_active');
                $(this).find('span').eq(0).html('В избранное');
            }
        });
    }
    if( cur_favs.length > 0 ){
        $('.header__favorites p').html( cur_favs.length ).show();
    } else {
        $('.header__favorites p').hide();
    }
}



function basketReload( params, bSmallOpen ) {
    if( !params ){  var params = {};  }
    params['form_data'] = $('.order___form').serialize();
    process(true);
    $.post("/ajax/basketReload.php", params, function(data){
        if (data.status == 'ok'){
            $('.basket_small_2').remove();
            $('.basket_small_1').replaceWith( data.top_basket_html );
            $('.basket_inner_block').replaceWith( data.basket_html );
            if( bSmallOpen ){
                $('.basket_small_1').trigger('click');
            }
            $("input[name=phone], input[type=tel]").mask("7 (999) 999-99-99");
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
    })
    .always(function(data, status, xhr){
        process(false);
        $.noty.closeAll();
    })
}



$(document).ready(function(){

    setFavorites();



    // Товар в корзину
    $(document).on('click', '.add_to___basket', function(e){
        if( !is_process(this) ){
            var item_id = $(this).attr('item_id');
            var quantity = $(this).prev().find('input[name=quantity]').val();
            var postParams = {
                action: "add_to_basket",
                item_id: item_id,
                quantity: quantity
            };
            process(true);
            $.post("/ajax/ajax.php", postParams, function(data){
                if (data.status == 'ok'){
                    show_message(
                        'Товар добавлен в корзину!',
                        'success', 'hover'
                    );
                    $('.basket_small_2').remove();
                    $('.basket_small_1').replaceWith( data.html );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })

    // Удаление товара из корзины
    $(document).on('click', '.basketRemoveButton', function(e){
        if( !is_process(this) ){
            var open = $('.basket_small_2').hasClass('basket-sidebar_opened');
            var item_id = $(this).attr('item_id');
            /////////////////////////////////////////////
            var params = { action: "unset_item", item_id: item_id };
            basketReload( params, open );
            /////////////////////////////////////////////
        }
    })

    // Изменение количества в корзине
    $(document).on('click', '.basket_quantity_button', function(e){
        if( !is_process(this) ){
            var item_id = $(this).attr('item_id');
            var quantity_input = $(this).parents('div.quantity___block').find('input[name=quantity]');
            var quantity = Number($(quantity_input).val());
            if( $(this).hasClass('basket_button___plus') ){
                var new_quantity = quantity + 1;
            } else if( $(this).hasClass('basket_button___minus') ){
                var new_quantity = quantity - 1;
            }
            if( new_quantity < 1 ){   new_quantity = 1;   }
            $(quantity_input).val(new_quantity);
            clearInterval(basketProcessInterval);
            basketProcessInterval = setInterval(function(){
                clearInterval(basketProcessInterval);
                /////////////////////////////////////////////
                var params = { action: "recalc_qt", item_id: item_id, quantity:new_quantity };
                basketReload( params );
                /////////////////////////////////////////////
            }, 500);
        }
    })
    $(document).on('keyup', '.basket_inner_block input[name=quantity]', function(e){
        minus_zero( $(this) )
        var item_id = $(this).attr('item_id');
        var quantity = $(this).val();
        clearInterval(basketProcessInterval);
        if( quantity.length > 0 ){
            basketProcessInterval = setInterval(function(){
                clearInterval(basketProcessInterval);
                /////////////////////////////////////////////
                var params = { action: "recalc_qt", item_id: item_id, quantity:quantity };
                basketReload( params );
                /////////////////////////////////////////////
                /////////////////////////////////////////////
            }, 700);
        }
    })
    $(document).on('blur', '.basket_inner_block input[name=quantity]', function(e){
        minus_zero( $(this), true )
        var item_id = $(this).attr('item_id');
        var quantity = $(this).val();
        /////////////////////////////////////////////
        var params = { action: "recalc_qt", item_id: item_id, quantity:quantity };
        basketReload( params );
        /////////////////////////////////////////////
    })

    // Сохранение купона в корзине
    $(document).on('click', '.saveCouponButton', function(e){
        if( !is_process(this) ){
            var params = { action: "save_coupon", coupon: $('input[name=coupon]').val() };
            basketReload( params );
        }
    })




    // Кнопки +- в карточке товара
    $(document).on('click', '.button___plus', function(e){
        var quantity_input = $(this).parents('div.quantity___block').find('input[name=quantity]');
        var quantity = $(quantity_input).val();
        quantity++;
        if( quantity < 1 ){   quantity = 1;   }
        $(quantity_input).val(quantity);
    })
    $(document).on('click', '.button___minus', function(e){
        var quantity_input = $(this).parents('div.quantity___block').find('input[name=quantity]');
        var quantity = $(quantity_input).val();
        quantity--;
        if( quantity < 1 ){   quantity = 1;   }
        $(quantity_input).val(quantity);
    })




    // Кнопка Избранное
    $(document).on('click', '.fav___button', function(e){
        var button = $(this);
        var item_id = $(button).attr('item_id');
        var cur_favs = $.cookie('BITRIX_SM_favorites');
        var postParams = { item_id: item_id };
        if( cur_favs == undefined ){
            postParams['cur_favs'] = '';
        } else {
            postParams['cur_favs'] = cur_favs;
        }
        process(true);
        $.post("/ajax/checkFavorites.php", postParams, function(data){
            if (data.status == 'ok'){
                var new_favs = data.new_favs;
                var cookie_favs = [];
                for( key in new_favs ){
                    var val = new_favs[key];
                    cookie_favs.push(val);
                }
                $.cookie(
                    'BITRIX_SM_favorites',
                    cookie_favs.join('|'),
                    { expires: 90, path: '/' }
                );
                setFavorites();
            } else if (data.status == 'error'){
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
            }
        }, 'json')
        .fail(function(data, status, xhr){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
        })
        .always(function(data, status, xhr){
            process(false);
        })
    })


    // Удаление из избранного
    $(document).on('click', '.favRemoveButton', function(e){
        var item_id = $(this).attr('item_id');
        var cur_favs = $.cookie('BITRIX_SM_favorites');
        if( cur_favs.length > 0 ){
            var favs = cur_favs.split('|');
        } else {
            var favs = [];
        }
        var new_favs = [];
        for( key in favs ){
            fav_id = favs[key];
            if( fav_id != item_id ){
                new_favs.push(fav_id);
            }
        }
        var new_favs_str = new_favs.join('|');
        $.cookie('BITRIX_SM_favorites', new_favs_str, { expires: 7, path: '/' });
        $(this).parent().parent().remove();
        if( $('.catalog___item').length == 0 ){
            $('.catalog__list.row').html('<p class="no___count">Список избранного пуст</p>')
        }
        $('.fav__count').html(new_favs.length);
        if( new_favs.length == 0 ){
            $('.fav__count').hide();
        }
    })



    // ФИЛЬТР - чекбокс
    $(document).on('change', '.filter-radio__input, .sort__value-input', function(e){
        var input = $(this);
        /////////////////////////////
        clearInterval(filterInterval);
        filterInterval = setInterval(function(){
            clearInterval(filterInterval);
            /////////////////////////////
            var filter_form = $('form.smartfilter');
            var form_data = $(filter_form).serialize();
            var curURL = $('input[name=curURL]').val();
            /////////////////////////////
            process(true);
            $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){
                if( data ){

                    data = data.replace(new RegExp("\n",'g'), "");
                    data = data.replace(new RegExp("'",'g'), "\"");

                    var result = JSON.parse( data );
                    var new_url = result.FILTER_URL;
                    new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                    new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");

                    history.replaceState({}, null, new_url);

                    catalogLoad();
                }
            })
            .fail(function(data, status, xhr){})
            .always(function(data, status, xhr){
                process(false);
            })
        }, 500);
    })

    // ФИЛЬТР - чекбокс
    $(document).on('change', '.coffee_type_checkbox', function(e){
        var input = $(this);
        /////////////////////////////
        var filter_form = $('form.smartfilter');
        var form_data = $(filter_form).serialize();
        var curURL = $('input[name=curURL]').val();
        /////////////////////////////
        process(true);
        $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){
            if( data ){

                data = data.replace(new RegExp("\n",'g'), "");
                data = data.replace(new RegExp("'",'g'), "\"");

                var result = JSON.parse( data );
                var new_url = result.FILTER_URL;
                new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");

                history.replaceState({}, null, new_url);

                catalogLoad();
            }
        })
        .fail(function(data, status, xhr){})
        .always(function(data, status, xhr){
            process(false);
        })
    })

    // Сброс фильтра
    $(document).on('click', '.filter__reset', function(e){
        if( !is_process(this) ){
            if( $('form.smartfilter .filter-radio__input:checked').length > 0 ){
                $('form.smartfilter .filter-radio__input').prop('checked', false);
                /////////////////////////////
                var filter_form = $('form.smartfilter');
                var form_data = $(filter_form).serialize();
                var curURL = $('input[name=curURL]').val();
                /////////////////////////////
                process(true);
                $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){
                    if( data ){

                        data = data.replace(new RegExp("\n",'g'), "");
                        data = data.replace(new RegExp("'",'g'), "\"");

                        var result = JSON.parse( data );
                        var new_url = result.FILTER_URL;
                        new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                        new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");

                        history.replaceState({}, null, new_url);

                        catalogLoad();
                    }
                })
                .fail(function(data, status, xhr){})
                .always(function(data, status, xhr){
                    process(false);
                })
            }
        }
    })




    // Подгрузка товаров в каталоге
    $(document).on('click', '.catalog_load_button', function(e){
        if( !is_process(this) ){
            var params = {};
            params.stop_ids = [];
            $('.catalog___item').each(function(){
                var item_id = $(this).attr('item_id');
                params.stop_ids.push(item_id);
            })
            catalogLoad( params );
        }
    })


    // Смена сортировки в каталоге
    $(document).on('click', '.sortButton', function(e){
        if( !is_process(this) ){
            var sort_code = $(this).attr('code');
            var params = { sort_code:sort_code };
            catalogLoad( params );
        }
    })



    // Подгрузка товаров в поиске
    $(document).on('click', '.search_catalog_load_button', function(e){
        if( !is_process(this) ){
            var params = {};
            params.stop_ids = [];
            $('.searchCatalogLoadArea .catalog___item').each(function(){
                var item_id = $(this).attr('item_id');
                params.stop_ids.push(item_id);
            })
            searchCatalogLoad( params );
        }
    })


    // Подгрузка статей в поиске
    $(document).on('click', '.search_blog_load_button', function(e){
        if( !is_process(this) ){
            var params = {};
            params.stop_ids = [];
            $('.searchBlogLoadArea .article___item').each(function(){
                var item_id = $(this).attr('item_id');
                params.stop_ids.push(item_id);
            })
            searchBlogLoad( params );
        }
    })



    // Открытие окна быстрого просмотра товара
    $(document).on('click', '.catalog_item_modal_link', function(e){
        if( !is_process(this) ){
            var params = { item_id: $(this).attr('item_id') };
            catalogItemModalLoad( params );
        }
    })

    // Выбор ТП в (модальной) карточке товара
    $(document).on('change', '.product_sku_checkbox', function(e){
        var sku_id = $(this).val();
        var item_id = $('#catalog_item_modal .product_sku_checkbox[value='+sku_id+']').attr('item_id');
        var params = { item_id: item_id, sku_id: sku_id };
        catalogItemModalLoad( params );
    })

    // Выбор ТП в карточке товара
    $(document).on('change', '.product_card_sku_checkbox', function(e){
        var sku_id = $(this).val();
        var item_id = $('.product_card_sku_checkbox[value='+sku_id+']').attr('item_id');
        var item_code = $('.product_card_sku_checkbox[value='+sku_id+']').attr('item_code');
        var params = { item_id: item_id, sku_id: sku_id };
        productCardReload( params );
    })




    // Выбор варианта оплаты
    $(document).on('change', '.order___form input[name=ds_id]', function(e){
        basketReload();
    })


    $(document).on('keyup', 'input.zip___input', function(e){
        var input = $(this);
        clearInterval(zipInterval);
        zipInterval = setInterval(function(){
            clearInterval(zipInterval);
            basketReload();
        }, 700);
    })


    // Оформление заказа
    $(document).on('click', '.order___button', function(e){
        if( !is_process(this) ){

            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'name': {
                    tag: 'input',  name_ru: 'Имя',
                    required: true,  required_error: 'Введите, пожалуйста, Ваше имя'
                },
                'last_name': {
                    tag: 'input',  name_ru: 'Фамилия',
                    required: true,  required_error: 'Введите, пожалуйста, Вашу фамилию'
                },
                'phone': {
                    tag: 'input',  name_ru: 'Телефон',  is_phone: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш телефон'
                },
                'email': {
                    tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){

                if( $('input[name=ds_id]:checked').length == 0 ){

                    show_error(this_form, "Выберите, пожалуйста, вариант доставки");

                } else if(
                    $('.np___input:visible').length > 0
                    &&
                    $('.np___input:visible').parent().find('input[type=hidden]').val().length == 0
                ){

                    var np_name = $('.np___input:visible').val();
                    if( np_name.length > 0 ){
                        var error_text = "Не допускается свободное написание названия населённого пункта.<br>Начните ввод в поле и выберите один из предложенных вариантов.";
                    } else {
                        var error_text = "Укажите, пожалуйста, населённый пункт для доставки";
                    }

                    show_error(this_form, error_text);
                    $('.np___input:visible').addClass('is___error').attr( 'oninput','$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', error_text);

                } else if(
                    $('.street___input:visible').length > 0
                    &&
                    $('.street___input:visible').val().length == 0
                ){

                    show_error(this_form, "Укажите, пожалуйста, улицу");
                    $('.street___input:visible').addClass('is___error').attr( 'oninput','$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', "Укажите, пожалуйста, улицу");

                } else if(
                    $('.house___input:visible').length > 0
                    &&
                    $('.house___input:visible').val().length == 0
                ){

                    show_error(this_form, "Укажите, пожалуйста, номер дома / корпус");
                    $('.house___input:visible').addClass('is___error').attr( 'oninput','$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', "Укажите, пожалуйста, номер / корпус");

                } else if(
                    $('.zip___input:visible').length > 0
                    &&
                    $('.zip___input:visible').val().length == 0
                ){

                    show_error(this_form, "Укажите, пожалуйста, ваш почтовый индекс");
                    $('.zip___input:visible').addClass('is___error').attr( 'oninput','$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', "Укажите, пожалуйста, ваш почтовый индекс");

                } else if(
                    $('p.sdek_address:visible').length > 0
                    &&
                    $('p.sdek_address:visible').html().length == 0
                ){

                    show_error(this_form, "Выберите на карте точку самовывоза");

                } else if(
                    $('p.pickpoint_address:visible').length > 0
                    &&
                    $('p.pickpoint_address:visible').html().length == 0
                ){

                    show_error(this_form, "Выберите на карте точку самовывоза");

                } else if( $('input[name=ps_id]:checked').length == 0 ){

                    show_error(this_form, "Выберите, пожалуйста, вариант оплаты");

                } else if( $(this_form).find('input[name=agree]:checked').length == 0 ){

                    show_error(this_form, "Отметьте, пожалуйста, согласие с условиями использования и политикой конфиденциальности");

                    $(this_form).find('input[name=agree]:visible').addClass('is___error').attr( 'oninput','$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', "Отметьте, пожалуйста, согласие с условиями использования и политикой конфиденциальности");

                    $('.form___checkbox').each(function(){
                        if( $(this).hasClass('is___error') ){
                            $(this).parent().find('.sort__value-link').addClass('sort__value-link_red');
                        }
                    })

                } else {

                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"order",  form_data: form_data  }, function( data ){

                        if (data.status == 'ok') {

                            $(this_form).append('<input type="hidden" name="order_id" value="'+data.order_id+'">');
                            $(this_form).attr('onsubmit', 'return true');
                            $(this_form).attr('method', 'post');
                            $(this_form).attr('action', '/order_success/');
                            setTimeout(function(){
                                $(this_form).submit();
                            }, 500)

                        } else if (data.status == 'quantity_error') {

                            basketReload();

                            var n = noty({closeWith: ['button'], timeout: 200000, layout: 'center', type: 'warning', text: data.text});

                        } else if (data.status == 'need_auth') {

                            if( $(this_form).find('.order_pass___block').length == 0 ){
                                $(this_form).find('.order_email___block').after('<div class="form__row order_pass___block"><input type="password" name="password" class="form__input is___error" oninput="$(this).removeClass(\"is___error\"); $(this).attr(\"title\", \"\"); $(this).parents(\"form\").find(\"p.error___p\").html(\"\");" placeholder="Пароль"></div>');
                            }
                            $(this_form).find('.order_pass___block input[name=password]').val('');
                            $(this_form).find('.order_pass___block input[name=password]').addClass("is___error").attr('oninput', '$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("");');

                            show_error( this_form, 'Такой Email уже зарегистрирован на сайте<br>Введите, пожалуйста, пароль для авторизации!' );
                            process(false);

                        } else if (data.status == 'wrong_password') {

                            if( $(this_form).find('.order_pass___block').length == 0 ){
                                $(this_form).find('.order_email___block').after('<div class="form__row order_pass___block"><input type="password" name="password" class="form__input"></div>');
                            }
                            $(this_form).find('.order_pass___block input[name=password]').val('');
                            $(this_form).find('.order_pass___block input[name=password]').addClass("is___error").attr('oninput', '$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("");');

                            show_error( this_form, 'Пароль не верный' );
                            process(false);

                        } else if (data.status == 'error') {
                            show_error(this_form, data.text);
                            process(false);
                        }

                    }, 'json')
                    .fail(function(response, status, xhr){
                        process(false);
                        show_error( this_form, "Ошибка запроса" );
                    })
                    .always(function(response, status, xhr){    })
                }
            // Если есть ошибки
            } else {
                show_error( this_form, [checkResult.errors[0]] );
                $('.form___checkbox:visible').each(function(){
                    if( $(this).hasClass('is___error') ){
                        $(this).parent().find('.sort__value-link').addClass('sort__value-link_red');
                    }
                })
            }

        }
    })



    // Повтор заказа
    $(document).on('click', '.order_repeat_link', function(e){
        if( !is_process(this) ){
            var order_id = $(this).attr('item_id');
            var n = noty({
                text        : 'Повторить заказ №'+order_id+' точь-в-точь?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                        $noty.close();

                        var postParams = {
                            action: "order_repeat",
                            order_id: order_id
                        };
                        process(true);
                        $.post("/ajax/ajax.php", postParams, function(data){
                            if (data.status == 'ok'){

                                create_form('order_success', 'post', '/order_success/');
                                $('.order_success').append('<input type="hidden" name="order_id" value="'+data.order_id+'">');
                                $('.order_success').attr('onsubmit', 'return true');
                                $('.order_success').attr('method', 'post');
                                $('.order_success').attr('action', '/order_success/');
                                $('.order_success').submit();

                            } else if (data.status == 'error'){
                                show_message( data.text, 'warning', 'hover' );
                            }
                        }, 'json')
                        .fail(function(data, status, xhr){
                            show_message( 'Ошибка запроса', 'warning', 'hover' );
                        })
                        .always(function(data, status, xhr){
                            process(false);
                        })

                    }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
                ]
            });
        }
    });
    
    
    
    
    
    
    $(document).on('click', '.js-item', function(e){
        $(this).parents('tr').find('.history-page__item_s').toggleClass('history-page__item_s-active');
    })














})
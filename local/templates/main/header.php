<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;   $asset = Asset::getInstance();
\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto'); use AOptima\ToolsCafeto as tools;
\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto'); use AOptima\ProjectCafeto as project;

$arURI = tools\funcs::arURI(tools\funcs::pureURL());

?><!DOCTYPE html>
<html lang="ru">

<head>
    <title><?$APPLICATION->ShowTitle()?></title> <?$APPLICATION->ShowHead()?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <!-- Open Graph Protocol-->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />
    <!-- Open Graph Protocol-->
    <!-- favicon -->
    <link rel="icon" type="image/png" href="/16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/48x48.png" sizes="48x48">
    <!--<link rel="apple-touch-icon" sizes="152x152" href="">-->
    <!-- favicon -->

    <? // CSS
    $asset->addCss(SITE_TEMPLATE_PATH."/css/grid.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/style.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/my.css"); ?>

    <script>var pickMap, sdekMap;</script>

    <? // JS
    if(
        $arURI[1] == 'basket'
        ||
        $arURI[1] == 'contacts'
    ){
        $asset->addJs("//api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&width=100%&height=100%");
    }
    $asset->addJs(SITE_TEMPLATE_PATH."/js/plugins.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/json.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.maskedinput.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/noty/packaged/jquery.noty.packaged.min.js"); ?>

    <meta name="yandex-verification" content="5ec05e44a81cacec" />
    <meta name="google-site-verification" content="xsNpfy5vA7omct8pRMyCrTBaWqpXBFGti-8q70Jt4DI" />

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2483651731744339');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2483651731744339&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body>

    <div id="panel"><?$APPLICATION->ShowPanel();?></div>

    <!-- Begin header -->
    <header class="header">
        <div class="header__wrapper">

            <div class="header__top">
                <a href="tel:<?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'SITE_PHONE')?>" class="link header__link"><?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'SITE_PHONE')?></a>
            </div>

            <div class="header__bottom">

                <a href="/" class="logo header__logo">
                    <img class="logo__img" src="<?=SITE_TEMPLATE_PATH?>/img/logo.svg" alt="Качественный кофе доставим прямо до двери">
                </a>

                <nav class="menu header__menu">

                    <? // top_menu
                    $APPLICATION->IncludeComponent( "aoptima:top_menu", "" ); ?>

                    <div class="menu__item has-children">

                        <? if( $USER->IsAuthorized() ){ ?>

                            <a href="/personal/" class="menu__link">Личный кабинет</a>
                            <div class="sub-menu">
                                <div class="sub-menu__wrapper">

                                    <? // personal
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:menu",  "header", // Шаблон меню
                                        Array(
                                            "ROOT_MENU_TYPE" => "personal", // Тип меню
                                            "MENU_CACHE_TYPE" => "A",
                                            "MENU_CACHE_TIME" => "3600",
                                            "MENU_CACHE_USE_GROUPS" => "N",
                                            "CACHE_SELECTED_ITEMS" => "N",
                                            "MENU_CACHE_GET_VARS" => array(""),
                                            "MAX_LEVEL" => "1",
                                            "CHILD_MENU_TYPE" => "left",
                                            "USE_EXT" => "N",
                                            "DELAY" => "N",
                                            "ALLOW_MULTI_SELECT" => "N"
                                        )
                                    ); ?>

                                    <a href="/out.php?to=<?=$_SERVER['REQUEST_URI']?>" class="link sub-menu__link">Выход</a>
                                </div>
                            </div>

                        <? } else { ?>

                            <a class="menu__link">Личный кабинет</a>
                            <div class="sub-menu">
                                <div class="sub-menu__wrapper">
                                    <a href="#log-in" rel="modal:open" class="link sub-menu__link">Вход</a>
                                    <a href="#sign-up" rel="modal:open" class="link sub-menu__link">Регистрация</a>
                                </div>
                            </div>

                        <? } ?>

                    </div>

                </nav>

                <button type="button" class="search-btn header__search-btn">
                    <svg width="16px" height="16px" viewBox="0 0 15.97 15.97">
                        <path fill="#050807" d="M38.28,37.34,34.44,33.5a6.71,6.71,0,1,0-.94.94l3.84,3.84a.66.66,0,0,0,.94-.94Zm-9-2.72a5.39,5.39,0,1,1,5.4-5.39A5.39,5.39,0,0,1,29.23,34.62Z" transform="translate(-22.5 -22.5)" />
                    </svg>
                </button>

                <div class="header__options">

                    <a href="/favorites/" class="favorites header__favorites">
                        <svg viewBox="0 0 20 19" width="20px" height="19px">
                            <path fill="#030303" stroke="#030303" stroke-width=".5" stroke-miterlimit="10" d="M18.5,6l-4.9-0.4c-0.1,0-0.2-0.1-0.3-0.2L11.5,1c-0.4-0.8-1.3-1.2-2.1-0.9C9,0.3,8.6,0.6,8.5,1L6.6,5.5 c0,0.1-0.1,0.2-0.2,0.2L1.5,6C0.6,6.1-0.1,6.8,0,7.7c0,0.4,0.2,0.9,0.6,1.2L4.3,12c0.1,0.1,0.1,0.2,0.1,0.3L3.3,17 c-0.1,0.5,0,1,0.3,1.4C3.8,18.8,4.3,19,4.8,19c0.3,0,0.6-0.1,0.9-0.3l4.1-2.6c0.1,0,0.2,0,0.3,0l4.2,2.5c0.3,0.2,0.6,0.3,0.9,0.3 c0.5,0,0.9-0.2,1.2-0.6c0.3-0.4,0.4-0.9,0.3-1.4l-1.2-4.7c0-0.1,0-0.2,0.1-0.3l3.8-3.1c0.5-0.4,0.7-1.1,0.5-1.8 C19.7,6.5,19.1,6.1,18.5,6z M18.8,8.1L15,11.2c-0.4,0.3-0.5,0.8-0.4,1.3l1.2,4.7c0,0.2,0,0.4-0.1,0.6c-0.1,0.1-0.3,0.2-0.5,0.2 c-0.1,0-0.2,0-0.3-0.1l-4.2-2.6c-0.2-0.1-0.4-0.2-0.7-0.2c-0.2,0-0.5,0.1-0.7,0.2l-4.1,2.6c-0.3,0.2-0.6,0.1-0.8-0.2 c-0.1-0.2-0.2-0.3-0.1-0.5l1.2-4.7c0.1-0.5-0.1-1-0.4-1.3L1.2,8.1C1,8,1,7.7,1,7.5c0.1-0.2,0.3-0.4,0.6-0.4l4.9-0.4 c0.5,0,0.9-0.3,1.1-0.8l1.9-4.5c0.1-0.3,0.5-0.5,0.8-0.4c0.2,0.1,0.3,0.2,0.4,0.4l1.9,4.5c0.2,0.4,0.6,0.7,1.1,0.8l4.9,0.4 c0.3,0,0.6,0.3,0.6,0.7C19,7.9,18.9,8.1,18.8,8.1L18.8,8.1z" />
                        </svg>
                        <p class="fav__count"></p>
                    </a>

                    <? // basket_small
                    $APPLICATION->IncludeComponent( "aoptima:basket_small", "header" ); ?>

                </div>

                <button type="button" class="mobile-menu-btn">
                    <svg viewBox="0 0 53 53" width="23" height="23">
                        <path d="M2,13.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,13.5,2,13.5z" />
                        <path d="M2,28.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,28.5,2,28.5z" />
                        <path d="M2,43.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,43.5,2,43.5z" />
                    </svg>
                </button>

            </div>
        </div>

<!--        <div class="sca-label">-->
<!--            <img src="--><?//=SITE_TEMPLATE_PATH?><!--/img/sca-member-label.png" class="sca-label__img">-->
<!--        </div>-->

    </header>
    <!-- End header -->

    <!-- Begin main -->
    <main class="main">

        <? if( tools\funcs::isMain() ){ ?>

            <? // main_slider
            $APPLICATION->IncludeComponent(
                "bitrix:news.list", "main_slider",
                array(
                    "COMPONENT_TEMPLATE" => "main_slider",
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => "8",
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "NAME",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array(
                        'LINK', 'LINK_TITLE',
                        'PHOTO_3840_1300', 'PHOTO_1920_650',
                        'PHOTO_1280_1600', 'PHOTO_640_800'
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "undefined",
                    "SET_LAST_MODIFIED" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => ""
                ), false
            ); ?>


            <? // Рекомендуем
            $GLOBALS['main_recommend']['PROPERTY_RECOMMEND'] = 1;
            $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"main_recommend", 
	array(
		"IS_AJAX" => "Y",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => project\catalog::IBLOCK_ID,
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "RAND",
		"ELEMENT_SORT_ORDER" => "ASC",
		"ELEMENT_SORT_FIELD2" => "NAME",
		"ELEMENT_SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "main_recommend",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "4",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
			2 => "",
		),
		"OFFERS_LIMIT" => "0",
		"TEMPLATE_THEME" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"CACHE_GROUPS" => "Y",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "Y",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"COMPONENT_TEMPLATE" => "main_recommend",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"BACKGROUND_IMAGE" => "-",
		"SEF_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"COMPATIBLE_MODE" => "Y",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
); ?>


            <? // Наши преимущества
            $APPLICATION->IncludeComponent(
                "bitrix:news.list", "main_advantages",
                array(
                    "COMPONENT_TEMPLATE" => "main_advantages",
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => "17",
                    "NEWS_COUNT" => "100",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "NAME",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(),
                    "PROPERTY_CODE" => array('SVG'),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "undefined",
                    "SET_LAST_MODIFIED" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => ""
                ),
                false
            ); ?>


            <section class="section seo">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="section__title seo__title"><? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/main_page_title.inc.php", Array(), Array("MODE"=>"html") ); ?></h4>
                            <div class="section__grid text">
                                <? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/main_page_text.inc.php", Array(), Array("MODE"=>"html") ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



        <? } else { ?>

            <? if( project\funcs::isContentPage() ){ ?>

                <div class="template page">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">

                                <? // Хлебные крошки
                                $APPLICATION->IncludeComponent(
                                    "bitrix:breadcrumb", "default",
                                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                                ); ?>

                                <div class="template__grid">

                                    <h1 class="title template__title page__title"><?$APPLICATION->ShowTitle()?></h1>

                                    <div class="page__grid text">

            <? } ?>

        <? } ?>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto');
use AOptima\ProjectCafeto as project;

\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto');
use AOptima\ToolsCafeto as tools;

$arURI = tools\funcs::arURI(tools\funcs::pureURL());

?>



<!-- Begin modal -->
<div class="modal quick-modal" id="catalog_item_modal"></div>
<!-- End modal -->



<!-- Begin modal -->
<a class="btn footer__btn сallbackWindowButtonHidden" href="#call" rel="modal:open" style="display: none;">Заказать звонок</a>
<div class="modal default-modal call-modal сallbackWindowBlock" id="call">

</div>
<!-- End modal -->



<!-- Begin modal -->
<div class="modal default-modal auth-modal" id="log-in">
    <p class="modal__title default-modal__title">Авторизация</p>
    <div class="modal__grid">
        <form class="form modal-form" onsubmit="return false">

            <div class="form__row icon-email">
                <input type="text" name="login" class="form__input" placeholder="Email / логин">
            </div>
            <div class="form__row icon-pw">
                <input type="password" name="password" class="form__input" placeholder="Пароль">
            </div>
            <div class="form__row modal__options">
                <a href="#sign-up" rel="modal:open" class="modal__option">Регистрация</a>
                <a href="#forget-pw" rel="modal:open" class="modal__option">Забыли пароль?</a>
            </div>

            <p class="error___p"></p>

            <button type="button" class="btn form__btn auth___button to___process">Войти</button>

            <? $APPLICATION->IncludeComponent(
                "bitrix:system.auth.form", "soc_auth",
                Array(
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "",
                    "REGISTER_URL" => "",
                    "SHOW_ERRORS" => "N"
                )
            ); ?>

        </form>
    </div>
</div>
<!-- End modal -->



<!-- Begin modal -->
<div class="modal default-modal" id="sign-up">
    <p class="modal__title default-modal__title">Регистрация</p>
    <div class="modal__grid">
        <form class="form modal-form" onsubmit="return false;">

            <div class="form__row icon-person">
                <input type="text" name="NAME" class="form__input" placeholder="Имя">
            </div>

            <div class="form__row icon-person">
                <input type="text" name="SECOND_NAME" class="form__input" placeholder="Отчество">
            </div>

            <div class="form__row icon-person">
                <input type="text" name="LAST_NAME" class="form__input" placeholder="Фамилия">
            </div>

            <div class="form__row icon-email">
                <input type="email" name="EMAIL" class="form__input" placeholder="Email адрес">
            </div>
            
            <div class="form__row icon-pw">
                <input type="password" name="PASSWORD" class="form__input" placeholder="Пароль">
            </div>

            <div class="form__row">

                <label class="sort__value form___label" style="white-space: unset;">
                    <input type="checkbox" value="Y" name="agree" class="form___checkbox" checked>
                    <p class="sort__value-link link sort__value-link_s">Я принимаю <a href="/oferta/" target="_blank">Условия использования</a></p>
                </label>

            </div>

            <div class="form__row">

                <label class="sort__value form___label" style="white-space: unset;">
                    <input type="checkbox" value="Y" name="oferta_agree" class="form___checkbox" checked>
                    <p class="sort__value-link link sort__value-link_s">Ознакомлен и согласен с условиями <a href="/oferta/" target="_blank">Публичной оферты</a></p>
                </label>

            </div>

            <p class="error___p"></p>

            <button type="button" class="btn form__btn register___button to___process">Регистрация</button>

            <? $APPLICATION->IncludeComponent(
                "bitrix:system.auth.form", "soc_auth",
                Array(
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "",
                    "REGISTER_URL" => "",
                    "SHOW_ERRORS" => "N"
                )
            ); ?>

        </form>
    </div>
</div>
<!-- End modal -->



<!-- Begin modal -->
<div class="modal default-modal" id="forget-pw">
    <p class="modal__title default-modal__title modal__title_s">Забыли пароль?</p>
    <p class="modal__note">Введите свой e-mail для восстановления</p>
    <div class="modal__grid">
        <form class="form modal-form" onsubmit="return false;">
            <div class="form__row icon-email">
                <input type="email" name="email" class="form__input" placeholder="Ваш Email">
            </div>

            <p class="error___p"></p>

            <button type="button" class="btn form__btn password___recovery_button to___process">Отправить</button>
        </form>
    </div>
</div>
<!-- End modal -->



<!-- Begin modal -->
<div class="modal default-modal" id="success">
    <div class="modal__icon">
        <svg width="50px" height="50px" viewBox="0 0 512 489.59">
            <path fill="#029d2e"
                  d="M244.8,500.79h-.15A244.79,244.79,0,0,1,71.8,82.81a243.16,243.16,0,0,1,173-71.6h.14a243,243,0,0,1,99.5,21.19,20.68,20.68,0,1,1-16.84,37.78,202.22,202.22,0,0,0-82.68-17.61h-.12A203.44,203.44,0,0,0,41.36,255.88c-.06,112.18,91.15,203.49,203.32,203.55h.12A203.42,203.42,0,0,0,448.22,256.12V235.29a20.69,20.69,0,1,1,41.37,0v20.84A244.8,244.8,0,0,1,244.8,500.79Z"
                  transform="translate(0 -11.21)" />
            <path fill="#029d2e"
                  d="M244.79,321.63a20.61,20.61,0,0,1-14.62-6.06l-67.23-67.23a20.68,20.68,0,0,1,29.24-29.25l52.61,52.61L476.69,39.8a20.68,20.68,0,0,1,29.25,29.25L259.42,315.57A20.62,20.62,0,0,1,244.79,321.63Z"
                  transform="translate(0 -11.21)" />
        </svg>
    </div>
    <p class="modal__title default-modal__title modal__title_s">Регистрация прошла успешно!</p>
    <p class="modal__note">Проверьте почту для завершения</p>
</div>
<!-- End modal -->




<? if( $arURI[1] == 'basket' ){ ?>


    <?php if( 0 ){ ?>

        <? // ПВЗ Пикпоинт
        $ds = new project\ds_pickpoint();
        $pickPointTerminals = $ds->getTerminals(); ?>
        <div id="pickpoint_pvz_window" class="modal points-modal">
            <div class="points-modal__wrapper">
                <div class="points-modal__map" style="width:100%;">
                    <div id="pickpoint_map" class="points-modal__map-grid modal___map"></div>
                </div>
            </div>
        </div>
        <script>
        $(document).ready(function(){
            // Дождёмся загрузки API и готовности DOM.
            ymaps.ready(init);
            function init () {
                pickMap = new ymaps.Map('pickpoint_map', {
                    center:[55.76, 37.64],
                    zoom:10,
                    //width: '100%',
                    //height: '100%',
                    searchControlProvider: 'yandex#search'
                });
                var clusterer = new ymaps.Clusterer({
                    clusterNumbers: [10],
                    clusterIconContentLayout: null
                });
                var geoObjects = [];
                <? $k = -1;
                foreach ( $pickPointTerminals as $key => $pickPointTerminal ){
                if( $pickPointTerminal['Latitude'] && $pickPointTerminal['Longitude'] ){
                $k++; ?>
                geoObjects[<?=$k?>] = new ymaps.Placemark([<?=$pickPointTerminal['Latitude']?>, <?=$pickPointTerminal['Longitude']?>], {
                    balloonContentBody: '<?=$pickPointTerminal['CitiName']?>, <?=str_replace(["'"], ["\'"], $pickPointTerminal['Address'])?><br><a class="pickPointButton" data-id="<?=$pickPointTerminal['Id']?>" data-city="<?=$pickPointTerminal['CitiName']?>" data-number="<?=$pickPointTerminal['Number']?>" data-address="<?=str_replace(["'"], ["\'"], $pickPointTerminal['Address'])?>">Выбрать точку</a>',
                    clusterCaption: '<?=$pickPointTerminal['CitiName']?>, <?=str_replace(["'"], ["\'"], $pickPointTerminal['Address'])?>'
                });
                <? }
                } ?>
                clusterer.add(geoObjects);
                pickMap.geoObjects.add(clusterer);
                // pickMap.setBounds(clusterer.getBounds(), {
                //     checkZoomRange: true
                // });
            }
            $(document).on('click', '.pickPointButton', function(e){
                var id = $(this).data('id');
                var number = $(this).data('number');
                var city = $(this).data('city');
                var address = city + ', ' +$(this).data('address')+' ['+number+']';
                $('.close-modal').trigger('click');
                $('p.pickpoint_address').html(address);
                $('p.pickpoint_city').html(city);
                $('.pickpoint_address_input').val(address);
                $('.pickpoint_point_id_input').val(id);
                $('.pickpoint_point_number_input').val(number);
                basketReload();
            })
        })
        </script>

    <? } ?>


    <?php if( 1 ){ ?>

        <? // ПВЗ СДЭК
        $ds = new project\ds_sdek_pvz();
        $sdekPVZList = $ds->getPVZList(); ?>

        <div id="sdek_pvz_window" class="modal points-modal">
            <div class="points-modal__wrapper">
                <div class="points-modal__map" style="width:100%;">
                    <div id="sdek_map" class="points-modal__map-grid modal___map"></div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                // Дождёмся загрузки API и готовности DOM.
                ymaps.ready(init);
                function init () {
                    sdekMap = new ymaps.Map('sdek_map', {
                        center:[55.76, 37.64],
                        zoom:10,
                        //width: '100%',
                        //height: '100%',
                        searchControlProvider: 'yandex#search'
                    });
                    var clusterer = new ymaps.Clusterer({
                        clusterNumbers: [10],
                        clusterIconContentLayout: null
                    });
                    var geoObjects = [];
                    <? $k = -1;
                    foreach ( $sdekPVZList as $key => $sdekPVZ ){
                    if( $sdekPVZ['coordX'] && $sdekPVZ['coordY'] ){
                    $k++; ?>
                    geoObjects[<?=$k?>] = new ymaps.Placemark([<?=$sdekPVZ['coordY']?>, <?=$sdekPVZ['coordX']?>], {
                        balloonContentBody: '<?=$sdekPVZ['City']?>, <?=str_replace(["'"], ["\'"], $sdekPVZ['Address'])?><br><a class="sdekPointButton" data-id="<?=$sdekPVZ['Code']?>" data-city="<?=$sdekPVZ['City']?>" data-city-code="<?=$sdekPVZ['CityCode']?>" data-number="<?=$sdekPVZ['Code']?>" data-address="<?=str_replace(["'"], ["\'"], $sdekPVZ['Address'])?>">Выбрать точку</a>',
                        clusterCaption: '<?=$sdekPVZ['City']?>, <?=str_replace(["'"], ["\'"], $sdekPVZ['Address'])?>'
                    });
                    <? }
                    } ?>
                    clusterer.add(geoObjects);
                    sdekMap.geoObjects.add(clusterer);
                    // pickMap.setBounds(clusterer.getBounds(), {
                    //     checkZoomRange: true
                    // });
                }
                $(document).on('click', '.sdekPointButton', function(e){
                    var id = $(this).data('id');
                    var number = $(this).data('number');
                    var city = $(this).data('city');
                    var cityCode = $(this).data('city-code');
                    var address = city + ', ' +$(this).data('address')+' ['+number+']';
                    $('.close-modal').trigger('click');
                    $('p.sdek_address').html(address);
                    $('.sdek_np_input').val(city);
                    $('.sdek_np_id_input').val(cityCode);
                    $('.sdek_address_input').val(address);
                    $('.sdek_point_id_input').val(id);
                    $('.sdek_point_number_input').val(number);
                    basketReload();
                })
            })
        </script>

    <?php unset( $sdekPVZList );
    } ?>




<? } ?>


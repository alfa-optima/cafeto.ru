<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="template catalog favorites-template">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "default",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

                <? // favorites
                $APPLICATION->IncludeComponent(
                    "aoptima:favorites", ""
                ); ?>

            </div>
        </div>
    </div>
</div>



<section class="section seo">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/fav_content.inc.php", Array(), Array("MODE"=>"html") ); ?>
            </div>
        </div>
    </div>
</section>



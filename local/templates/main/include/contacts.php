<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$mapPointLat = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'MAP_POINT_LAT');
$mapPointLng = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'MAP_POINT_LNG');
$address = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'CONTACTS_ADDRESS');
$email = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'CONTACTS_EMAIL');

?>



<div class="template default-template contacts">

    <div class="container">
        <div class="row">
            <div class="col-12">

                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "default",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>

            </div>
        </div>
    </div>

    <div class="template__grid">

        <? if( strlen($mapPointLat) > 0 && strlen($mapPointLng) > 0 ){ ?>

            <script>
                ymaps.ready(init);

                function init () {

                    var map = new ymaps.Map("contacts_map", {
                        center: [<?=$mapPointLat?>, <?=$mapPointLng?>],
                        zoom: 13
                    });

                    map.controls.add('zoomControl', { left: 5, top: 5 });

                    myPlacemark = new ymaps.Placemark([<?=$mapPointLat?>, <?=$mapPointLng?>], {
                        balloonContentHeader: "Мы здесь!",
                        balloonContentBody: "<?=$address?>",
                        // balloonContentFooter: "Подвал",
                        // hintContent: "Хинт метки"
                    });

                    map.geoObjects.add(myPlacemark);

                }
            </script>

            <div class="contacts__map" id="contacts_map"></div>

        <? } ?>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">

                        <div class="col-12 col-sm-4">
                            <div class="contacts__item">
                                <div class="contacts__icon"><img src="<?=SITE_TEMPLATE_PATH?>/img/icons/icon-contacts-1.png"></div>
                                <p class="contacts__name">Наши телефоны</p>
                                <p class="contacts__value"><?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'CONTACTS_PHONES')?></span></p>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="contacts__item">
                                <div class="contacts__icon"><img src="<?=SITE_TEMPLATE_PATH?>/img/icons/icon-contacts-2.png"></div>
                                <p class="contacts__name">Наш адрес</p>
                                <p class="contacts__value"><?=$address?></p>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4">
                            <div class="contacts__item">
                                <div class="contacts__icon"><img src="<?=SITE_TEMPLATE_PATH?>/img/icons/icon-contacts-3.png"></div>
                                <p class="contacts__name">Наш почтовый адрес</p>
                                <a href="mailto:<?=$email?>" class="contacts__value contacts__link"><?=$email?></a>
                            </div>
                        </div>

                    </div>

                    <section class="section contacts__form">

                        <h1 class="title section__title">Оставьте свои данные для звонка</h1>

                        <div class="section__grid">
                            
                            <? // feedback_form
                            $APPLICATION->IncludeComponent(
                                "aoptima:feedback_form", "default"
                            ); ?>

                        </div>
                    </section>

                </div>
            </div>
        </div>

    </div>
</div>
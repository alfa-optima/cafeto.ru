<div class="col-12 col-lg-9 col-xl-8">
    <div class="personal__content">

        <h1 class="title personal__title">Авторизация</h1>

        <div class="personal__list" style="margin-bottom: 45px;">
            <form class="edit-personal-form auth___form" onsubmit="return false;">

                <input name="login" type="text" class="personal__list-item" placeholder="Ваш Email / логин">

                <input name="password" type="password" class="personal__list-item"  placeholder="Пароль">

                <p class="error___p"></p>

                <button type="button" class="btn edit-personal-form__btn auth___button to___process">Войти</button>

            </form>
        </div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "soc_auth",
            Array(
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "",
                "REGISTER_URL" => "",
                "SHOW_ERRORS" => "N"
            )
        ); ?>

    </div>
</div>
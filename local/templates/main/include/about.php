<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="template default-template about">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "default",
                    Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s2" )
                ); ?>
            </div>
        </div>
    </div>

    <div class="template__grid">

        <header class="about__header">
            <picture>
                <source srcset="<?=SITE_TEMPLATE_PATH?>/img/about-img.jpg 1x, <?=SITE_TEMPLATE_PATH?>/img/about-img@2x.jpg 2x">
                <img class="about__header-img" src="<?=SITE_TEMPLATE_PATH?>/img/about-img.jpg" alt="alt">
            </picture>
        </header>

        <div class="container">
            <div class="row">
                <div class="col-12">

                    <section class="section about__desc">
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-10">

                                <h1 class="title section__title about__title"><? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/about_title.inc.php", Array(), Array("MODE"=>"html") ); ?></h1>

                                <div class="section__grid text">

                                    <? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/about_text.inc.php", Array(), Array("MODE"=>"html") ); ?>

                                </div>

                            </div>
                        </div>
                    </section>

                    <? // about_team
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list", "about_team",
                        array(
                            "COMPONENT_TEMPLATE" => "about_team",
                            "IBLOCK_TYPE" => "content",
                            "IBLOCK_ID" => "18",
                            "NEWS_COUNT" => "100",
                            "SORT_BY1" => "SORT",
                            "SORT_ORDER1" => "ASC",
                            "SORT_BY2" => "NAME",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array('POST', 'SVG'),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "PAGER_TEMPLATE" => ".default",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "undefined",
                            "SET_LAST_MODIFIED" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "SHOW_404" => "N",
                            "MESSAGE_404" => ""
                        ),
                        false
                    ); ?>

                </div>
            </div>
        </div>
    </div>
</div>
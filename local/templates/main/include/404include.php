<? include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Страница не найдена"); ?>


<div class="template page template-404">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <div class="breadcrumbs template__breadcrumbs page__breadcrumbs">
                    <a href="/" class="link breadcrumbs__item breadcrumb__link">Главная</a>
                    <p class="breadcrumbs__item breadcrumbs__sep">/</p>
                    <p class="breadcrumbs__item breadcrumbs__name">404 - Страница не найдена</p>
                </div>

                <div class="template__grid">
                    <h1 class="template-404__title">404<span>Страница не найдена</span></h1>
                    <div class="page__grid">
                        <p class="template-404__note">Выберите себе кофе в нашем <a href="/">каталоге</a> а мы пока все исправим</p>
                        <a href="/" class="btn template-404__btn">На главную</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;   $asset = Asset::getInstance();
\Bitrix\Main\Loader::includeModule('aoptima.toolscafeto'); use AOptima\ToolsCafeto as tools;
\Bitrix\Main\Loader::includeModule('aoptima.projectcafeto'); use AOptima\ProjectCafeto as project;

$catalog_first_level = project\catalog::first_level_sections();

$phone = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'SITE_PHONE');

$vk_link = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'VK_LINK');
$fb_link = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'FB_LINK');
$tw_link = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'TW_LINK');
$insta_link = \Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'INSTA_LINK');

?>


        <? if( project\funcs::isContentPage() ){ ?>

            </div></div></div></div></div>

        <? } ?>

        <? // subscribe_form
        $APPLICATION->IncludeComponent(
            "aoptima:subscribe_form", ""
        ); ?>

    </main>
    <!-- End main -->

    <!-- Begin footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer__top row">

                        <div class="col-12 col-lg-2">
                            <img class="footer__logo" src="<?=SITE_TEMPLATE_PATH?>/img/logo-w.svg">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/sca-member-logo-white.png" class="footer__sca-logo">
                        </div>

                        <div class="col-12 col-lg-4">
                            <div class="footer__col">
                                <p class="footer__name">Наши контакты</p>
                                <a href="tel:<?=preg_replace('/[^0-9]/', '', $phone)?>" class="link footer__link footer__text"><?=$phone?></a>
                                <p class="footer__text"><?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'SITE_ADDRESS')?></p>
                                <a href="mailto:<?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'SITE_EMAIL')?>"
                                   class="link footer__link footer__text"><?=\Bitrix\Main\Config\Option::get('aoptima.projectcafeto', 'SITE_EMAIL')?></a>

                                <a href="/company_details/" class="link footer__link footer__text">Реквизиты компании</a>

                                <a class="btn footer__btn openCallbackWindowButton to___process" style="cursor: pointer;">Заказать звонок</a>

                            </div>
                        </div>

                        <div class="col-12 col-lg-2">
                            <div class="footer__col">
                                <p class="footer__name">Каталог</p>
                                <? foreach( $catalog_first_level as $key => $sect ){ ?>
                                    <a href="<?=$sect['SECTION_PAGE_URL']?>" class="link footer__link footer__text"><?=$sect['NAME']?></a>
                                <? } ?>
                            </div>
                        </div>

                        <? // bottom_menu
                        $APPLICATION->IncludeComponent(
                            "bitrix:menu",  "bottom_menu", // Шаблон меню
                            Array(
                                "ROOT_MENU_TYPE" => "bottom_info", // Тип меню
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "CACHE_SELECTED_ITEMS" => "N",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MAX_LEVEL" => "1",
                                "CHILD_MENU_TYPE" => "left",
                                "USE_EXT" => "N",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "N"
                            )
                        ); ?>

                        <div class="col-12 col-lg-2">
                            <div class="footer__col">
                                <p class="footer__name">Соц.сети</p>
                                <? if( $vk_link ){ ?>
                                    <a target="_blank" href="<?=$vk_link?>" class="link footer__link footer__text">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="20" height="20">
                                            <path style="fill:#436EAB;" d="M440.649,295.361c16.984,16.582,34.909,32.182,50.142,50.436 c6.729,8.112,13.099,16.482,17.973,25.896c6.906,13.382,0.651,28.108-11.348,28.907l-74.59-0.034 c-19.238,1.596-34.585-6.148-47.489-19.302c-10.327-10.519-19.891-21.714-29.821-32.588c-4.071-4.444-8.332-8.626-13.422-11.932 c-10.182-6.609-19.021-4.586-24.84,6.034c-5.926,10.802-7.271,22.762-7.853,34.8c-0.799,17.564-6.108,22.182-23.751,22.986 c-37.705,1.778-73.489-3.926-106.732-22.947c-29.308-16.768-52.034-40.441-71.816-67.24 C58.589,258.194,29.094,200.852,2.586,141.904c-5.967-13.281-1.603-20.41,13.051-20.663c24.333-0.473,48.663-0.439,73.025-0.034 c9.89,0.145,16.437,5.817,20.256,15.16c13.165,32.371,29.274,63.169,49.494,91.716c5.385,7.6,10.876,15.201,18.694,20.55 c8.65,5.923,15.236,3.96,19.305-5.676c2.582-6.11,3.713-12.691,4.295-19.234c1.928-22.513,2.182-44.988-1.199-67.422 c-2.076-14.001-9.962-23.065-23.933-25.714c-7.129-1.351-6.068-4.004-2.616-8.073c5.995-7.018,11.634-11.387,22.875-11.387h84.298 c13.271,2.619,16.218,8.581,18.035,21.934l0.072,93.637c-0.145,5.169,2.582,20.51,11.893,23.931 c7.452,2.436,12.364-3.526,16.836-8.251c20.183-21.421,34.588-46.737,47.457-72.951c5.711-11.527,10.622-23.497,15.381-35.458 c3.526-8.875,9.059-13.242,19.056-13.049l81.132,0.072c2.406,0,4.84,0.035,7.17,0.434c13.671,2.33,17.418,8.211,13.195,21.561 c-6.653,20.945-19.598,38.4-32.255,55.935c-13.53,18.721-28.001,36.802-41.418,55.634 C424.357,271.756,425.336,280.424,440.649,295.361L440.649,295.361z" />
                                        </svg>
                                        Вконтакте
                                    </a>
                                <? } ?>
                                <? if( $fb_link ){ ?>
                                    <a target="_blank" href="<?=$fb_link?>" class="footer__link footer__text">
                                        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 12.01 20.01">
                                            <path fill="#3c578e" d="M37.5,21.19a19.88,19.88,0,0,0-3.86-.69c-3.9,0-5,2-5,5.11v2.57H25.49V32h3.17v8.47h4V32h3.47l.64-3.86H32.64V26.11c0-1,.41-1.63,1.93-1.63a12.62,12.62,0,0,1,2.11.22Z" transform="translate(-25.49 -20.5)"></path>
                                        </svg>
                                        Facebook
                                    </a>
                                <? } ?>

                                <? if( $tw_link ){ ?>
                                    <a target="_blank" href="<?=$tw_link?>" class="link footer__link footer__text">Twitter</a>
                                <? } ?>

                                <? if( $insta_link ){ ?>
                                    <a href="<?=$insta_link?>" target="_blank" class="footer__link footer__text">
                                        <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                            <linearGradient id="SVGID_111_" gradientUnits="userSpaceOnUse" x1="-46.0041" y1="634.1208" x2="-32.9334" y2="647.1917" gradientTransform="matrix(32 0 0 -32 1519 20757)">
                                                <stop  offset="0" style="stop-color:#FFC107"/>
                                                <stop  offset="0.507" style="stop-color:#F44336"/>
                                                <stop  offset="0.99" style="stop-color:#9C27B0"/>
                                            </linearGradient>
                                            <path style="fill:url(#SVGID_111_);" d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192 c88.352,0,160-71.648,160-160V160C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112 V160C48,98.24,98.24,48,160,48h192c61.76,0,112,50.24,112,112V352z"/>
                                            <linearGradient id="SVGID_112_" gradientUnits="userSpaceOnUse" x1="-42.2971" y1="637.8279" x2="-36.6404" y2="643.4846" gradientTransform="matrix(32 0 0 -32 1519 20757)">
                                                <stop  offset="0" style="stop-color:#FFC107"/>
                                                <stop  offset="0.507" style="stop-color:#F44336"/>
                                                <stop  offset="0.99" style="stop-color:#9C27B0"/>
                                            </linearGradient>
                                            <path style="fill:url(#SVGID_112_);" d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128 S326.688,128,256,128z M256,336c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80 C336,300.096,300.096,336,256,336z"/>
                                            <linearGradient id="SVGID_113_" gradientUnits="userSpaceOnUse" x1="-35.5456" y1="644.5793" x2="-34.7919" y2="645.3331" gradientTransform="matrix(32 0 0 -32 1519 20757)">
                                                <stop offset="0" style="stop-color:#FFC107"/>
                                                <stop offset="0.507" style="stop-color:#F44336"/>
                                                <stop offset="0.99" style="stop-color:#9C27B0"/>
                                            </linearGradient>
                                            <circle style="fill:url(#SVGID_113_);" cx="393.6" cy="118.4" r="17.056"/>
                                        </svg>Instagram
                                    </a>
                                <? } ?>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="footer__bottom row align-items-center">
                        <div class="col-12 col-xl-2">
                            <a href="http://diweb.by/" target="_blank" rel="noopener noreferrer" class="developer">
                                Разработка <img src="<?=SITE_TEMPLATE_PATH?>/img/developer-img.svg" alt="alt">
                            </a>
                        </div>
                        <div class="col-12 col-xl-4">
                            <p class="copyright">&copy; <?if( date('Y')==2019){echo(date('Y')); } else { echo("2019 - ".date('Y')); }?> All Rights Reserved, powered by cafeto.ru</p>
                        </div>
                        <div class="col-12 col-xl-6">
                            <div class="rules">
                                <a href="/personal_data_agreement/" class="link rules__link">Пользовательское соглашение</a>
                                <a href="/privacy_policy/" class="link rules__link">Политика конфиденциальности</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </footer>
    <!-- End footer -->


    <!-- Begin search -->
    <div class="search">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <header class="search__header">
                        <p class="search__name">Введите для поиска:</p>
                        <button type="button" class="search__close">
                            <svg width="18.5px" height="18.5px" viewBox="0 0 18.5 18.5">
                                <path fill="#000" d="M40.85,24.13l-.92-.91-8.35,8.36-7.66-7.66-.89.89,7.66,7.66-8.34,8.34.91.91,8.34-8.34,8,8,.89-.89-8-8Z" transform="translate(-22.35 -23.22)" />
                            </svg>
                        </button>
                    </header>
                    <div class="search__grid">
                        <form class="form search__form" action="/search/">
                            <div class="search__input">
                                <input type="text" name="q" placeholder="Я ищу">
                            </div>
                            <button type="submit" class="btn search__btn">Искать</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End search -->



    <? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/_site_modals.php'; ?>


    <a href="#catalog_item_modal" rel="modal:open" style="display: none;" class="catalog_item_modal_open"></a>


    <? // JS
    $asset->addJs(SITE_TEMPLATE_PATH."/js/main.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/my_f.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/my.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/shop.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/personal.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/modal.js"); ?>


    <input type="hidden" name="curURL" value="<?=$_SERVER['REQUEST_URI']?>">


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(56454889, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
       });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56454889" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153702194-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-153702194-1');
    </script>

</body>
</html>